cc.Class({
    extends: cc.Component,

    properties: {},

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {

    },

    init_Star(time_delay) {
        var that = this;
        setTimeout(function(){
            that.node.runAction(
                cc.repeatForever(
                    cc.sequence(
                        cc.scaleTo(1, 0.8 , 0.8),
                        cc.rotateBy(0.5, 30, 30),
                        cc.scaleTo(0.5, 0, 0),
                        cc.delayTime(8)
                    )
                )
            );
        },time_delay);
    }

    // update (dt) {},
});