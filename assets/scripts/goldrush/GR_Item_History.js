cc.Class({
    extends: cc.Component,

    properties: {
        Item_BG: {
            default: null,
            type: cc.Node
        },
        sprite_List: {
            default: [],
            type: [cc.SpriteFrame]
        },
        label_Session: {
            default: null,
            type: cc.Label
        },
        label_Time: {
            default: null,
            type: cc.Label
        },
        label_Type: {
            default: null,
            type: cc.Label
        },
        label_Bet: {
            default: null,
            type: cc.Label
        },
        label_User: {
            default: null,
            type: cc.Label
        },
        label_Gold: {
            default: null,
            type: cc.Label
        }
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {

    },

    // update (dt) {},

    init_Item_History(data) {
        // cc.log("Item History : ", data);
        this.Item_BG.getComponent(cc.Sprite).spriteFrame = this.sprite_List[data.bg_index];
        this.label_Session.string = data.sess;
        this.label_Time.string = data.time;
        switch (data.type) {
            case 0:
                this.label_Type.string = " ĐÀO ";
                break;
            case 1:
                this.label_Type.string = " GIẤU ";
                break;
            case 2:
                this.label_Type.string = " BỊ ĐÀO ";
                break;
            case 3:
                this.label_Type.string = " HỦY GIẤU ";
                break;
            default:
                break;
        }
        this.label_Bet.string = this.convert_money(data.bet);
        this.label_User.string = data.user;
        this.label_Gold.string = this.convert_money(data.gold);
    },

    convert_money(price) {
        return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " ";
    }
});