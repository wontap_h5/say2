var WarpConst = require("WarpConst");
var WebsocketClient = require("WebsocketClient");
var LobbyCtl = require("LobbyCtl");

var Slot1 = require("Pirate_Main");
var Slot2 = require("Slot_2_Main");
var Slot3 = require("Slot_3_Main");
var Slot4 = require("Slot_4_Main");

var GM = require("GM");
var Gold_Rush = cc.Class({
    extends: cc.Component,

    statics: {
        scope: null
    },

    properties: {
        Light_Logo: {
            default: null,
            type: cc.Node
        },
        Mask_Logo: {
            default: null,
            type: cc.Node
        },
        Star_Logo: {
            default: null,
            type: cc.Node
        },
        UI_Prepare: {
            default: null,
            type: cc.Node
        },
        UI_Game_Play: {
            default: null,
            type: cc.Node
        },
        UI_Open: {
            default: null,
            type: cc.Node
        },
        UI_Hide: {
            default: null,
            type: cc.Node
        },
        List_Pack_Open: {
            default: null,
            type: cc.Node
        },
        List_Pack_Hide: {
            default: null,
            type: cc.Node
        },
        Bet_selector: {
            default: [],
            type: cc.Toggle
        },
        Effect_Result_Open: {
            default: null,
            type: cc.Node
        },
        UI_Feature: {
            default: null,
            type: cc.Node
        },
        Popup_Hide_List: {
            default: null,
            type: cc.Node
        },
        Popup_History: {
            default: null,
            type: cc.Node
        },
        Popup_Guide: {
            default: null,
            type: cc.Node
        },
        Hide_List_Content: {
            default: null,
            type: cc.Node
        },
        Item_Hide_Prefab: {
            default: null,
            type: cc.Prefab
        },
        History_Content: {
            default: null,
            type: cc.Node
        },
        Item_History_Prefab: {
            default: null,
            type: cc.Prefab
        },
        label_Step_Count: {
            default: null,
            type: cc.Label
        },
        Popup_Notify: {
            default: null,
            type: cc.Node
        },
        label_Popup_Notify: {
            default: null,
            type: cc.Label
        },
        VS_P1: {
            default: null,
            type: cc.Node
        },
        VS_P2: {
            default: null,
            type: cc.Node
        },
        VS_P1_Avatar: {
            default: null,
            type: cc.Node
        },
        VS_P2_Avatar: {
            default: null,
            type: cc.Node
        },
        VS_Effect: {
            default: null,
            type: cc.Node
        },
        VS_P1_Name: {
            default: null,
            type: cc.Label
        },
        VS_P2_Name: {
            default: null,
            type: cc.Label
        },
        label_Time_Open_Counter: {
            default: null,
            type: cc.Label
        },
        label_Rush_Result: {
            default: null,
            type: cc.Label
        },
        label_Rush_Rate: {
            default: null,
            type: cc.Label
        },
        label_Rush_Gold: {
            default: null,
            type: cc.Label
        },
        PS_Rush_1: {
            default: null,
            type: cc.Node
        },
        Game_Drag: {
            default: null,
            type: cc.Node
        }
    },

    In_UI_Rushing: null,
    Pack_Open_ID: null,
    Pack_Hide_Selected: null,
    Hide_Count: null,
    Game_Bet: null,

    Unpack_Time: null,
    Unpack_Counting: null,
    minutes: null,
    seconds: null,
    check_Can_Open_Pack: null,

    Rush_Count: null,
    Rush_True_Number: null,


    Current_Hide_List: null,

    check_OnMiniGoldRushSubDone: null,
    check_OnMiniGoldRushHideDone: null,
    check_OnMiniGoldRushDelOneMatchDone: null,
    check_OnMiniGoldRushGetUserHistoryDone: null,
    check_OnMiniGoldRushFindMatchDone: null,
    check_OnMiniGoldRushGetHideListDone: null,
    check_OnMiniGoldRushChooseOneDone: null,

    check_Open_Star: null,
    check_Hide_Star: null,

    OnMiniGoldRushSubDone(res) {
        cc.log("OnMiniGoldRushSubDone : ", res);
        var raw_data = JSON.parse(res);
        if ("undefined" !== typeof raw_data.type) {
            if (raw_data.type === 23) {
                Gold_Rush.scope.check_OnMiniGoldRushSubDone = true;
            } else {
                Gold_Rush.scope.check_OnMiniGoldRushSubDone = false;
                Gold_Rush.scope.show_Popup_Notify(" Kết nối thất bại \n Vui lòng thử lại sau ! ");
            }
        } else {
            Gold_Rush.scope.check_OnMiniGoldRushSubDone = false;
            Gold_Rush.scope.show_Popup_Notify(" Kết nối thất bại \n Vui lòng thử lại sau ! ");
        }
        cc.log("OnMiniGoldRushSubDone : ", Gold_Rush.scope.check_OnMiniGoldRushSubDone);
    },

    OnMiniGoldRushHideDone(res) {
        cc.log("OnMiniGoldRushHideDone :  ", res);
        var raw_data = JSON.parse(res);
        Gold_Rush.scope.show_Popup_Notify(" Giấu thành công ! ");
        cc.log("OnMiniGoldRushHideDone | setBalance : ", raw_data.curent_user_gold);

        switch (GM.Stay) {
            case 0:
                cc.log("Playing on Lobby");
                Gold_Rush.scope.setBalance(raw_data.curent_user_gold);
                break;
            case 1:
                cc.log("Playing on Slot 1");
                GM.Gold_Lastest = raw_data.curent_user_gold;
                Slot1.scope.notify_Gold_State_Change();
                break;
            case 2:
                cc.log("Playing on Slot 2");
                GM.Gold_Lastest = raw_data.curent_user_gold;
                Slot2.scope.notify_Gold_State_Change();
                break;
            case 3:
                cc.log("Playing on Slot 3");
                GM.Gold_Lastest = raw_data.curent_user_gold;
                Slot3.scope.notify_Gold_State_Change();
                break;
            case 4:
                cc.log("Playing on Slot 4");
                GM.Gold_Lastest = raw_data.curent_user_gold;
                Slot4.scope.notify_Gold_State_Change();
                break;
            default:
                break;
        }
    },

    OnMiniGoldRushDelOneMatchDone(res) {
        cc.log("OnMiniGoldRushDelOneMatchDone : ", res);
        var raw_data = JSON.parse(res);
        if ("undefined" !== typeof raw_data.des) {
            cc.log("Delete Success");
            Gold_Rush.scope.check_OnMiniGoldRushDelOneMatchDone = true;

            cc.log("OnMiniGoldRushDelOneMatchDone | setBalance : ", raw_data.chips);

            switch (GM.Stay) {
                case 0:
                    cc.log("Playing on Lobby");
                    Gold_Rush.scope.setBalance(raw_data.chips);
                    break;
                case 1:
                    cc.log("Playing on Slot 1");
                    GM.Gold_Lastest = raw_data.chips
                    Slot1.scope.notify_Gold_State_Change();
                    break;
                case 2:
                    cc.log("Playing on Slot 2");
                    GM.Gold_Lastest = raw_data.chips;
                    Slot2.scope.notify_Gold_State_Change();
                    break;
                case 3:
                    cc.log("Playing on Slot 3");
                    GM.Gold_Lastest = raw_data.chips;
                    Slot3.scope.notify_Gold_State_Change();
                    break;
                case 4:
                    cc.log("Playing on Slot 4");
                    GM.Gold_Lastest = raw_data.chips;
                    Slot4.scope.notify_Gold_State_Change();
                    break;
                default:
                    break;
            }

            var row_ID = Gold_Rush.scope.Row_Del_Index;
            var child_Name = "" + Gold_Rush.scope.Current_Hide_List[row_ID].id;
            Gold_Rush.scope.Hide_List_Content.getChildByName(child_Name).destroy();
            Gold_Rush.scope.row_delete_ID = Gold_Rush.scope.Current_Hide_List[row_ID].id; // = 3215 3216 ...
            Gold_Rush.scope.Current_Hide_List = Gold_Rush.scope.pop_Item_In_List(Gold_Rush.scope.Current_Hide_List);
            cc.log(Gold_Rush.scope.Current_Hide_List);
        } else {
            Gold_Rush.scope.check_OnMiniGoldRushDelOneMatchDone = false;
            Gold_Rush.scope.show_Popup_Notify(" Xóa thất bại ! ");
        }
    },

    OnMiniGoldRushGetUserHistoryDone(res) {
        cc.log("OnMiniGoldRushGetUserHistoryDone : ", res);
        var raw_data = JSON.parse(res);
        Gold_Rush.scope.UI_Feature.active = true;
        Gold_Rush.scope.UI_Feature.position = cc.v2(-Gold_Rush.scope.Game_Drag.position.x, -Gold_Rush.scope.Game_Drag.position.y);
        Gold_Rush.scope.Popup_History.active = true;
        Gold_Rush.scope.History_Content.removeAllChildren(true);
        var Data_History = raw_data.history;
        for (var i = 0; i < Data_History.length; i++) {
            var item = cc.instantiate(Gold_Rush.scope.Item_History_Prefab);
            var data = Data_History[i];
            var name = decodeURIComponent(escape(data.enemy));
            if (name.length > 15) {
                name = name.substring(0, 11) + "...";
            }
            item.getComponent('GR_Item_History').init_Item_History({
                bg_index: i % 2,
                sess: "#" + data.id,
                time: data.time,
                type: data.code,
                bet: data.bet,
                user: name,
                gold: data.gold
            });
            Gold_Rush.scope.History_Content.addChild(item);
        }
    },

    OnMiniGoldRushFindMatchDone(res) {
        cc.log("OnMiniGoldRushFindMatchDone : ", res);
        var raw_data = JSON.parse(res);

        if ("undefined" !== typeof raw_data.result) {
            if (raw_data.result.length === 0) {
                cc.log("Match not Found");
                Gold_Rush.scope.UI_Prepare.children[0].active = true;
                Gold_Rush.scope.UI_Prepare.children[1].active = false;
                Gold_Rush.scope.UI_Prepare.children[2].active = false;
                Gold_Rush.scope.show_Popup_Notify(" Không tìm thấy đối thủ ! ");
                Gold_Rush.scope.In_UI_Rushing = false;
            }
        } else {
            cc.log("Found Match");
            Gold_Rush.scope.UI_Prepare.children[1].active = false; // Find Match
            Gold_Rush.scope.UI_Prepare.children[2].active = true; // VS
            Gold_Rush.scope.VS_P1.scale = 0;
            Gold_Rush.scope.VS_P2.scale = 0;
            Gold_Rush.scope.VS_Effect.scale = 0;
            Gold_Rush.scope.In_UI_Rushing = true;

            switch (Gold_Rush.scope.Game_Bet) {
                case 500:
                    Gold_Rush.scope.Bet_selector[0].interactable = true;
                    Gold_Rush.scope.Bet_selector[1].interactable = false;
                    Gold_Rush.scope.Bet_selector[2].interactable = false;
                    break;
                case 5000:
                    Gold_Rush.scope.Bet_selector[0].interactable = false;
                    Gold_Rush.scope.Bet_selector[1].interactable = true;
                    Gold_Rush.scope.Bet_selector[2].interactable = false;
                    break;
                case 50000:
                    Gold_Rush.scope.Bet_selector[0].interactable = false;
                    Gold_Rush.scope.Bet_selector[1].interactable = false;
                    Gold_Rush.scope.Bet_selector[2].interactable = true;
                    break;
            }

            switch (GM.Stay) {
                case 0:
                    cc.log("Playing on Lobby");
                    Gold_Rush.scope.setBalance(raw_data.curent_user_gold);
                    break;
                case 1:
                    cc.log("Playing on Slot 1");
                    GM.Gold_Lastest = raw_data.curent_user_gold;
                    Slot1.scope.notify_Gold_State_Change();
                    break;
                case 2:
                    cc.log("Playing on Slot 2");
                    GM.Gold_Lastest = raw_data.curent_user_gold;
                    Slot2.scope.notify_Gold_State_Change();
                    break;
                case 3:
                    cc.log("Playing on Slot 3");
                    GM.Gold_Lastest = raw_data.curent_user_gold;
                    Slot3.scope.notify_Gold_State_Change();
                    break;
                case 4:
                    cc.log("Playing on Slot 4");
                    GM.Gold_Lastest = raw_data.curent_user_gold;
                    Slot4.scope.notify_Gold_State_Change();
                    break;
                default:
                    break;
            }

            // Se thay doi khi server tra ve full Info
            var name = decodeURIComponent(escape(raw_data.owner));
            if (name.length > 15) {
                name = name.substring(0, 11) + "...";
            }
            Gold_Rush.scope.VS_P2_Name.string = " " + name + " ";

            var iam = decodeURIComponent(escape(LobbyCtl.userInfo.desc.username));
            if (iam.length > 15) {
                iam = iam.substring(0, 11) + "...";
            }
            Gold_Rush.scope.VS_P1_Name.string = " " + iam + " ";

            Gold_Rush.scope.VS_P1_Avatar.getComponent(cc.Sprite).spriteFrame = WarpConst.GameBase.playerAvaSpriteFrame;

            Gold_Rush.scope.VS_P1.runAction(
                cc.sequence(
                    cc.delayTime(0.2),
                    cc.scaleTo(0.1, 1.1, 1.1),
                    cc.scaleTo(0.05, 1, 1)
                )
            );

            Gold_Rush.scope.VS_P2.runAction(
                cc.sequence(
                    cc.delayTime(0.4),
                    cc.scaleTo(0.1, 1.1, 1.1),
                    cc.scaleTo(0.05, 1, 1)
                )
            );

            Gold_Rush.scope.VS_Effect.runAction(
                cc.sequence(
                    cc.delayTime(0.6),
                    cc.scaleTo(0, 0.7, 0.7),
                    cc.scaleTo(0.1, 0.8, 0.8),
                    cc.scaleTo(0.05, 0.6, 0.6)
                )
            );

            setTimeout(function () {
                cc.log("UI Open");
                Gold_Rush.scope.UI_Prepare.children[2].active = false;
                Gold_Rush.scope.UI_Prepare.active = false;
                Gold_Rush.scope.UI_Game_Play.active = true;
                Gold_Rush.scope.UI_Open.active = true;
                Gold_Rush.scope.UI_Hide.active = false;
                Gold_Rush.scope.Effect_Result_Open.active = false;

                for (let index = 0; index < 10; index++) {
                    var pack = Gold_Rush.scope.List_Pack_Open.children[index];
                    pack.children[0].active = true;
                    pack.children[1].active = false;
                    pack.children[2].active = false;
                    pack.children[3].active = false;
                    pack.children[5].active = true;
                    pack.children[0].getComponent(cc.Button).transition = cc.Button.Transition.Normal;
                }

                Gold_Rush.scope.check_Can_Open_Pack = true;
                Gold_Rush.scope.Unpack_Time = 5 * 60;
                Gold_Rush.scope.Unpack_Counting = true;
                Gold_Rush.scope.Rush_Count = 5;
                Gold_Rush.scope.Rush_True_Number = 0;
                Gold_Rush.scope.label_Step_Count.string = " 5 ";
                Gold_Rush.scope.setTimeCounter_Unpack();
                Gold_Rush.scope.scheduleOnce(Gold_Rush.scope._callback_Unpack_Timeout, 5 * 60);

                Gold_Rush.scope.create_Star_UI_Open();
            }, 3000);
        }
    },

    OnMiniGoldRushGetHideListDone(res) {
        cc.log("OnMiniGoldRushGetHideListDone : ", res);
        var raw_data = JSON.parse(res);
        Gold_Rush.scope.UI_Feature.active = true;
        Gold_Rush.scope.UI_Feature.position = cc.v2(-Gold_Rush.scope.Game_Drag.position.x, -Gold_Rush.scope.Game_Drag.position.y);
        Gold_Rush.scope.Popup_Hide_List.active = true;
        Gold_Rush.scope.Hide_List_Content.removeAllChildren(true);
        var Data_Hide_List = raw_data.hide_list;
        Gold_Rush.scope.Current_Hide_List = [];
        Gold_Rush.scope.Current_Hide_List = Data_Hide_List;

        for (var i = 0; i < Data_Hide_List.length; i++) {
            var item = cc.instantiate(Gold_Rush.scope.Item_Hide_Prefab);
            var data = Data_Hide_List[i];
            var clock = new Date(data.dateTime);
            item.getComponent('GR_Item_Hide').init_Item_Hide({
                bg_index: i % 2,
                // time: clock.toLocaleString("en-US"),
                time: data.dateTime,
                bet: data.bet,
                list: data.result,
                id: data.id
            });
            Gold_Rush.scope.Hide_List_Content.addChild(item);
        }
    },

    OnMiniGoldRushChooseOneDone(res) {
        cc.log("OnMiniGoldRushChooseOneDone : ", res);
        var raw_data = JSON.parse(res);
        if (raw_data.remain_rush === 0 && raw_data.chesId === -1) {
            cc.log("Time Out Rush");
            Gold_Rush.scope.show_Rush_Gold_Result(raw_data.total, raw_data.curent_user_gold);
        } else {
            cc.log("Rush Done");
            var pack_ID = raw_data.chesId - 1;
            Gold_Rush.scope.List_Pack_Open.children[pack_ID].children[0].active = false;
            Gold_Rush.scope.List_Pack_Open.children[pack_ID].children[5].active = false;
            Gold_Rush.scope.Rush_Count = raw_data.remain_rush;
            Gold_Rush.scope.label_Step_Count.string = " " + raw_data.remain_rush + " ";
            if (raw_data.rushOneIsTrue) {
                Gold_Rush.scope.Rush_True_Number += 1;
                cc.log("Rush_True_Number : ", Gold_Rush.scope.Rush_True_Number);
                Gold_Rush.scope.List_Pack_Open.children[pack_ID].children[1].active = true;
                Gold_Rush.scope.List_Pack_Open.children[pack_ID].children[1].getComponent(cc.Animation).play();

                setTimeout(function () {
                    Gold_Rush.scope.List_Pack_Open.children[pack_ID].children[3].active = true;
                    Gold_Rush.scope.List_Pack_Open.children[pack_ID].children[3].getComponent(cc.Label).string = GM.instance.convert_money(Gold_Rush.scope.Game_Bet / 5);
                }, 1500);

                setTimeout(function () {
                    Gold_Rush.scope.List_Pack_Open.children[pack_ID].children[3].active = false;
                    if (raw_data.remain_rush === 0) {
                        cc.log("Last One");
                        Gold_Rush.scope.show_Rush_Gold_Result(raw_data.total, raw_data.curent_user_gold);
                    } else {
                        Gold_Rush.scope.check_Can_Open_Pack = true;
                        cc.log("Can Open");
                    }
                }, 2500);
            } else {
                Gold_Rush.scope.List_Pack_Open.children[pack_ID].children[2].active = true;
                Gold_Rush.scope.List_Pack_Open.children[pack_ID].children[2].getComponent(cc.Animation).play();
                setTimeout(function () {
                    if (raw_data.remain_rush === 0) {
                        cc.log("Last One");
                        Gold_Rush.scope.show_Rush_Gold_Result(raw_data.total, raw_data.curent_user_gold);
                    } else {
                        Gold_Rush.scope.check_Can_Open_Pack = true;
                        cc.log("Can Open");
                    }
                }, 1500);
            }
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        Gold_Rush.scope = this;
        this.Pack_Hide_Selected = [];
        this.Pack_Open_ID = -1;
        this.Hide_Count = 0;
        this.Current_Hide_List = [];
        this.In_UI_Rushing = false; // true = Rushing   
        this.check_Can_Open_Pack = true;
        this.Game_Bet = 500;
        this.Unpack_Time = 5 * 60;
        this.Unpack_Counting = false;
        this.minutes = -1;
        this.seconds = -1;
        this.Rush_Count = 5;
        this.Rush_True_Number = 0;

        this.check_Open_Star = false;
        this.check_Hide_Star = false;
        for (let index = 0; index < 10; index++) {
            this.Pack_Hide_Selected.push(-1);
        }

        // WebsocketClient.OnMiniGoldRushSubDone = this.OnMiniGoldRushSubDone;
        // WebsocketClient.OnMiniGoldRushHideDone = this.OnMiniGoldRushHideDone;
        // WebsocketClient.OnMiniGoldRushDelOneMatchDone = this.OnMiniGoldRushDelOneMatchDone;
        // WebsocketClient.OnMiniGoldRushGetUserHistoryDone = this.OnMiniGoldRushGetUserHistoryDone;
        // WebsocketClient.OnMiniGoldRushFindMatchDone = this.OnMiniGoldRushFindMatchDone;
        // WebsocketClient.OnMiniGoldRushGetHideListDone = this.OnMiniGoldRushGetHideListDone;
        // WebsocketClient.OnMiniGoldRushChooseOneDone = this.OnMiniGoldRushChooseOneDone;

        this.check_OnMiniGoldRushSubDone = -1;
        this.check_OnMiniGoldRushHideDone = -1;
        this.check_OnMiniGoldRushDelOneMatchDone = -1;
        this.check_OnMiniGoldRushGetUserHistoryDone = -1;
        this.check_OnMiniGoldRushFindMatchDone = -1;
        this.check_OnMiniGoldRushGetHideListDone = -1;
        this.check_OnMiniGoldRushChooseOneDone = -1;
    },

    start() {
        this.create_Star();
        this.Light_Anim(this.Light_Logo, this.Mask_Logo, 0.04, 2);
    },

    Light_Anim(node_light, node_mask, time_step_move, repeat_at) {
        var pos_start = node_light.position; // v2(x,y)
        var pos_x_end = node_mask.position.x + (node_mask.width / 2); // x
        node_light.runAction(
            cc.repeatForever(
                cc.sequence(
                    cc.moveBy(time_step_move, 20, -10),
                    cc.callFunc(function () {
                        if (node_light.position.x > pos_x_end * (1 + repeat_at)) {
                            node_light.position = pos_start;
                        }
                    })
                ))
        );
    },

    create_Star() {
        for (let index = 0; index < this.Star_Logo.childrenCount; index++) {
            this.Star_Logo.children[index].scale = 0;
            this.Star_Logo.children[index].getComponent("GR_Star").init_Star(2000 * index);
        }
    },

    create_Star_UI_Open() {
        if (!this.check_Open_Star) {
            this.check_Open_Star = true;
            cc.log("create_Star_UI_Open   ");
            cc.log("create_Star_UI_Open");
            for (let index = 0; index < 10; index++) { // 10 packs
                const pack = this.List_Pack_Open.children[index];
                for (let x = 0; x < 6; x++) { // 6 stars each pack
                    pack.children[4].children[x].scale = 0;
                    pack.children[4].children[x].getComponent("GR_Star").init_Star((2000 + 200 * index) * x);
                }
            }
        }

    },

    create_Star_UI_Hide() {
        if (!this.check_Hide_Star) {
            this.check_Hide_Star = true;
            for (let index = 0; index < 10; index++) { // 10 packs
                const pack = this.List_Pack_Hide.children[index];
                for (let x = 0; x < 6; x++) { // 6 stars each pack
                    pack.children[2].children[x].scale = 0;
                    pack.children[2].children[x].getComponent("GR_Star").init_Star((2000 + 200 * index) * x);
                }
            }
        }
    },

    choose_Bet(toggle) {
        cc.log("choose_Bet : ", this.Bet_selector.indexOf(toggle));
        var index = this.Bet_selector.indexOf(toggle);
        switch (index) {
            case 0:
                this.Game_Bet = 500;
                break;
            case 1:
                this.Game_Bet = 5000;
                break;
            case 2:
                this.Game_Bet = 50000;
                break;
        }
        cc.log("Game_Bet : ", this.Game_Bet);
    },

    play_Rush() {
        cc.log("LobbyCtl.userInfo : ", WarpConst.GameBase.userInfo.desc.gold);
        cc.log("GM.Gold_Lastest : ", GM.Gold_Lastest);
        cc.log("Game_Bet : ", this.Game_Bet);

        if (GM.Stay === 0) {
            // Playing at Lobby
            if (WarpConst.GameBase.userInfo.desc.gold >= this.Game_Bet) {
                cc.log("can play at lobby");
                this.UI_Prepare.children[0].active = false; // Home
                this.UI_Prepare.children[2].active = false; // VS
                this.UI_Prepare.children[1].active = true; // Find Match
                this.goldRushFindOneMatch(this.Game_Bet);
            } else {
                cc.log("play_Rush : Khong du tien");
                this.show_Popup_Notify(" Không đủ tiền ! ");
            }
        } else {
            // Playing at Slot
            if (GM.Gold_Lastest >= this.Game_Bet) {
                cc.log("can play at slot");
                this.UI_Prepare.children[0].active = false; // Home
                this.UI_Prepare.children[2].active = false; // VS
                this.UI_Prepare.children[1].active = true; // Find Match
                this.goldRushFindOneMatch(this.Game_Bet);
            } else {
                cc.log("play_Rush : Khong du tien");
                this.show_Popup_Notify(" Không đủ tiền ! ");
            }
        }
    },

    exe_hide() {
        this.UI_Prepare.children[0].active = true; // Home
        this.UI_Prepare.children[1].active = false; // Find Match
        this.UI_Prepare.children[2].active = false; // Find Match
        this.UI_Prepare.active = false; // Find Match

        this.UI_Game_Play.active = true;
        this.UI_Open.active = false;
        this.Effect_Result_Open.active = false;
        this.UI_Hide.active = true;

        // sau khi set Toggle ve False thi no se nhay vao event Action Hide Pack
        for (let index = 0; index < 10; index++) {
            this.List_Pack_Hide.children[index].getComponent(cc.Toggle).isChecked = false;
        }

        // fix issue when toggle status change
        this.Hide_Count = 0;
        this.label_Step_Count.string = " 5 ";

        this.create_Star_UI_Hide();
        cc.log("Pack_Hide_Selected : ", this.Pack_Hide_Selected);
    },

    play_Hide() {
        cc.log("LobbyCtl.userInfo : ", WarpConst.GameBase.userInfo.desc.gold);
        cc.log("GM.Gold_Lastest : ", GM.Gold_Lastest);
        cc.log("Game_Bet : ", this.Game_Bet);

        if (GM.Stay === 0) {
            // Playing at Lobby
            if (WarpConst.GameBase.userInfo.desc.gold >= this.Game_Bet) {
                this.exe_hide();
            } else {
                cc.log("play_Rush : Khong du tien");
                this.show_Popup_Notify(" Không đủ tiền ! ");
            }
        } else {
            if (GM.Gold_Lastest >= this.Game_Bet) {
                this.exe_hide();
            } else {
                cc.log("play_Rush : Khong du tien");
                this.show_Popup_Notify(" Không đủ tiền ! ");
            }
        }


    },

    show_Rush_Gold_Result(result_rush, curent_user_gold) {
        var that = this;

        cc.log("show_Rush_Gold_Result | setBalance : ", curent_user_gold);
        switch (GM.Stay) {
            case 0:
                cc.log("Playing on Lobby");
                Gold_Rush.scope.setBalance(curent_user_gold);
                break;
            case 1:
                cc.log("Playing on Slot 1");
                GM.Gold_Lastest = curent_user_gold;
                Slot1.scope.notify_Gold_State_Change();
                break;
            case 2:
                cc.log("Playing on Slot 2");
                GM.Gold_Lastest = curent_user_gold;
                Slot2.scope.notify_Gold_State_Change();
                break;
            case 3:
                cc.log("Playing on Slot 3");
                GM.Gold_Lastest = curent_user_gold;
                Slot3.scope.notify_Gold_State_Change();
                break;
            case 4:
                cc.log("Playing on Slot 4");
                GM.Gold_Lastest = curent_user_gold;
                Slot4.scope.notify_Gold_State_Change();
                break;
            default:
                break;
        }
        this.Unpack_Counting = false;
        this.unscheduleAllCallbacks();

        this.Effect_Result_Open.active = true;

        var delta_gold = result_rush;
        if (this.Rush_True_Number < 3) {
            this.label_Rush_Result.string = " THUA CUỘC ";
            this.label_Rush_Result.node.color = cc.color(255, 255, 255, 255);
            this.label_Rush_Gold.string = GM.instance.convert_money(delta_gold);
            this.label_Rush_Gold.node.color = cc.color(255, 255, 255, 255);
        } else {
            this.label_Rush_Result.string = " CHIẾN THẮNG ";
            this.label_Rush_Result.node.color = cc.color(255, 200, 0, 255);
            this.label_Rush_Gold.string = " +" + GM.instance.convert_money(delta_gold);
            this.label_Rush_Gold.node.color = cc.color(255, 200, 0, 255);
        }
        this.label_Rush_Rate.string = this.Rush_True_Number + " / 5 ";
        this.Rush_True_Number = 0;

        this.PS_Rush_1.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
        setTimeout(function () {
            that.UI_Game_Play.active = false;
            that.UI_Prepare.active = true;
            that.UI_Prepare.children[0].active = true;
            that.UI_Prepare.children[1].active = false;
            that.UI_Prepare.children[2].active = false;

            that.In_UI_Rushing = false;
            that.Bet_selector[0].interactable = true;
            that.Bet_selector[1].interactable = true;
            that.Bet_selector[2].interactable = true;
        }, 2500);
    },

    show_Feature_Hide_List() {
        this.goldRushGetHideList();
    },

    show_Feature_History() {
        this.goldRushGetHis();
    },

    show_Feature_Guide() {
        this.UI_Feature.active = true;
        this.UI_Feature.position = cc.v2(-Gold_Rush.scope.Game_Drag.position.x, -Gold_Rush.scope.Game_Drag.position.y);
        this.Popup_Guide.active = true;
    },

    close_Feature_Hide_List() {
        this.Popup_Hide_List.active = false;
        this.UI_Feature.active = false;
    },

    close_Feature_History() {
        this.Popup_History.active = false;
        this.UI_Feature.active = false;
    },

    close_Feature_Guide() {
        this.Popup_Guide.active = false;
        this.UI_Feature.active = false;
    },

    // Event Open Pack
    goldRush() {
        cc.log("goldRush");
        this.goldRushChooseOne(this.Pack_Open_ID);
    },

    Exe_Open(id) {
        if (this.check_Can_Open_Pack) {
            cc.log("Can Open");
            this.check_Can_Open_Pack = false;
            if (this.Rush_Count > 0) {
                cc.log("Rush_Count > 0");
                this.Pack_Open_ID = id;
                this.goldRush();
            } else {
                cc.log("Can not Open");
            }
        } else {
            cc.log("Can not Open");
        }
    },

    Open_Pack_1() {
        this.Exe_Open(1);
    },
    Open_Pack_2() {
        this.Exe_Open(2);
    },
    Open_Pack_3() {
        this.Exe_Open(3);
    },
    Open_Pack_4() {
        this.Exe_Open(4);
    },
    Open_Pack_5() {
        this.Exe_Open(5);
    },
    Open_Pack_6() {
        this.Exe_Open(6);
    },
    Open_Pack_7() {
        this.Exe_Open(7);
    },
    Open_Pack_8() {
        this.Exe_Open(8);
    },
    Open_Pack_9() {
        this.Exe_Open(9);
    },
    Open_Pack_10() {
        this.Exe_Open(10);
    },

    show_Popup_Notify(note) {
        this.Popup_Notify.active = true;
        this.label_Popup_Notify.string = note;
    },

    close_Popup_Notify() {
        this.Popup_Notify.active = false;
    },

    // Cancel hiding Pack
    close_UI_Hide() {
        this.UI_Hide.active = false;
        this.UI_Game_Play.active = false;
        this.UI_Prepare.active = true;
        this.UI_Prepare.children[0].active = true;
        this.Bet_selector[0].interactable = true;
        this.Bet_selector[1].interactable = true;
        this.Bet_selector[2].interactable = true;
        this.Rush_True_Number = 0;
        this.Rush_Count = 5;
    },

    Back_2_G_Main() {
        if (this.In_UI_Rushing) {
            this.show_Popup_Notify(" Ván đấu chưa kết thúc ! ");
        } else {
            // try to use below code to
            this.Rush_True_Number = 0;
            this.Rush_Count = 5;
            cc.find("UIController").getComponent("UIController").hideGoldRush();
        }
    },

    // Event Hide Pack
    hideGold() {
        var list_hide = [];
        for (let index = 0; index < this.Pack_Hide_Selected.length; index++) {
            if (this.Pack_Hide_Selected[index] > 0) {
                list_hide.push(this.Pack_Hide_Selected[index]);
            }
        }

        cc.log("Hide list : ", list_hide);
        if (list_hide.length === 5) {
            cc.log("Setup Hide True");
            cc.log("LobbyCtl.userInfo : ", WarpConst.GameBase.userInfo.desc.gold);
            cc.log("GM.Gold_Lastest : ", GM.Gold_Lastest);
            cc.log("Game_Bet : ", this.Game_Bet);
            if (GM.Stay === 0) {
                cc.log("Submit Hide at Lobby");
                if (WarpConst.GameBase.userInfo.desc.gold >= this.Game_Bet) {
                    Gold_Rush.scope.close_UI_Hide();
                    switch (GM.Stay) {
                        case 0:
                            cc.log("Playing on Lobby");
                            this.changeBalance(-this.Game_Bet);
                            break;
                        case 1:
                            cc.log("Playing on Slot 1");
                            GM.Gold_Lastest -= this.Game_Bet
                            Slot1.scope.notify_Gold_State_Change();
                            break;
                        case 2:
                            cc.log("Playing on Slot 2");
                            GM.Gold_Lastest -= this.Game_Bet
                            Slot2.scope.notify_Gold_State_Change();
                            break;
                        case 3:
                            cc.log("Playing on Slot 3");
                            GM.Gold_Lastest -= this.Game_Bet
                            Slot3.scope.notify_Gold_State_Change();
                            break;
                        case 4:
                            cc.log("Playing on Slot 4");
                            GM.Gold_Lastest -= this.Game_Bet
                            Slot4.scope.notify_Gold_State_Change();
                            break;
                        default:
                            break;
                    }
                    this.goldRushHide(this.Game_Bet, JSON.stringify(list_hide));
                } else {
                    cc.log("play_Rush : Khong du tien");
                    this.show_Popup_Notify(" Không đủ tiền ! ");
                }
            } else {
                cc.log("Submit Hide at Slot");
                if (GM.Gold_Lastest >= this.Game_Bet) {
                    Gold_Rush.scope.close_UI_Hide();
                    switch (GM.Stay) {
                        case 0:
                            cc.log("Playing on Lobby");
                            this.changeBalance(-this.Game_Bet);
                            break;
                        case 1:
                            cc.log("Playing on Slot 1");
                            GM.Gold_Lastest -= this.Game_Bet
                            Slot1.scope.notify_Gold_State_Change();
                            break;
                        case 2:
                            cc.log("Playing on Slot 2");
                            GM.Gold_Lastest -= this.Game_Bet
                            Slot2.scope.notify_Gold_State_Change();
                            break;
                        case 3:
                            cc.log("Playing on Slot 3");
                            GM.Gold_Lastest -= this.Game_Bet
                            Slot3.scope.notify_Gold_State_Change();
                            break;
                        case 4:
                            cc.log("Playing on Slot 4");
                            GM.Gold_Lastest -= this.Game_Bet
                            Slot4.scope.notify_Gold_State_Change();
                            break;
                        default:
                            break;
                    }
                    this.goldRushHide(this.Game_Bet, JSON.stringify(list_hide));
                } else {
                    cc.log("play_Rush : Khong du tien");
                    this.show_Popup_Notify(" Không đủ tiền ! ");
                }
            }

        } else {
            cc.log("Hide False");
        }
    },

    Action_Hide_Pack(toggle) {
        cc.log(toggle.node.name);
        if (toggle.isChecked) {
            if (this.Hide_Count < 5) {
                cc.log("Can Hide");
                this.Hide_Count += 1;
                this.Pack_Hide_Selected[parseInt(toggle.node.name) - 1] = parseInt(toggle.node.name);
                this.label_Step_Count.string = "" + (5 - this.Hide_Count) + " ";
            }
            if (this.Hide_Count === 5) {
                for (let index = 0; index < this.Pack_Hide_Selected.length; index++) {
                    if (this.Pack_Hide_Selected[index] < 0) {
                        this.List_Pack_Hide.children[index].getComponent(cc.Toggle).enabled = false;
                    }
                }
            }
        } else {
            cc.log("Lala");
            this.Pack_Hide_Selected[parseInt(toggle.node.name) - 1] = -1;
            this.Hide_Count -= 1;
            this.label_Step_Count.string = " " + (5 - this.Hide_Count) + " ";
            if (this.Hide_Count === 4) {
                cc.log(" == 4");
                for (let index = 0; index < 10; index++) {
                    this.List_Pack_Hide.children[index].getComponent(cc.Toggle).enabled = true;
                }
            } else {
                cc.log(" !== 4");
            }
        }
        cc.log("this.Hide_Count : ", this.Hide_Count);
    },

    without_This_Item(row) {
        return row.id !== Gold_Rush.scope.row_delete_ID; // ID is 35468 not 0 1 2 3 
    },

    pop_Item_In_List(list) {
        return list.filter(this.without_This_Item);
    },

    delete_Hide_Row(hide_ID) {
        cc.log("Delelte Hide ID : ", hide_ID);
        var row_ID = -1;
        for (let index = 0; index < this.Current_Hide_List.length; index++) {
            if (this.Current_Hide_List[index].id === hide_ID) {
                row_ID = index;
                cc.log("Pack Data : ", this.Current_Hide_List[index]);
            }
        }

        if (row_ID !== -1) { //  = 0 1 2 3 4 ...
            cc.log("Can Delete");
            this.Row_Del_Index = row_ID;
            var row_Data = this.Current_Hide_List[row_ID];
            this.goldRushDelOneMtach(row_Data.bet, row_Data.id);
        }
    },

    setTimeCounter_Unpack() {
        this.minutes = Math.floor(this.Unpack_Time / 60);
        this.seconds = Math.floor(this.Unpack_Time % 60);
        if (this.minutes < 10) {
            this.minutes = "0" + this.minutes;
        }

        if (this.seconds < 10) {
            this.seconds = "0" + this.seconds;
        }
        this.label_Time_Open_Counter.string = this.minutes + ":" + this.seconds;
    },

    _callback_Unpack_Timeout() {
        cc.log("Unpack Chest Time Out");
        var that = this;
        this.Unpack_Counting = false;
        setTimeout(function () {
            that.label_Time_Open_Counter.string = "00:00";
        }, 20);

        this.Rush_Count = 0;
        this.check_Can_Open_Pack = false;
        this.goldRushChooseOne(-1);
    },

    onEnable() {
        cc.log("GR onEnable");
        this.goldRushSub();
        this.Bet_selector[0].interactable = true;
        this.Bet_selector[1].interactable = true;
        this.Bet_selector[2].interactable = true;
        this.Game_Bet = 500;
        this.Bet_selector[0].isChecked = true;

        WebsocketClient.OnMiniGoldRushSubDone = this.OnMiniGoldRushSubDone;
        WebsocketClient.OnMiniGoldRushHideDone = this.OnMiniGoldRushHideDone;
        WebsocketClient.OnMiniGoldRushDelOneMatchDone = this.OnMiniGoldRushDelOneMatchDone;
        WebsocketClient.OnMiniGoldRushGetUserHistoryDone = this.OnMiniGoldRushGetUserHistoryDone;
        WebsocketClient.OnMiniGoldRushFindMatchDone = this.OnMiniGoldRushFindMatchDone;
        WebsocketClient.OnMiniGoldRushGetHideListDone = this.OnMiniGoldRushGetHideListDone;
        WebsocketClient.OnMiniGoldRushChooseOneDone = this.OnMiniGoldRushChooseOneDone;
    },

    onDisable() {
        cc.log("GR onDisable");

        // WebsocketClient.OnMiniGoldRushSubDone = null;
        // WebsocketClient.OnMiniGoldRushHideDone = null;
        // WebsocketClient.OnMiniGoldRushDelOneMatchDone = null;
        // WebsocketClient.OnMiniGoldRushGetUserHistoryDone = null;
        // WebsocketClient.OnMiniGoldRushFindMatchDone = null;
        // WebsocketClient.OnMiniGoldRushGetHideListDone = null;
        // WebsocketClient.OnMiniGoldRushChooseOneDone = null;
    },

    update(dt) {
        if (this.Unpack_Counting) {
            this.Unpack_Time -= dt;
            this.setTimeCounter_Unpack();
        }
    },

    goldRushSub() {
        var str = {
            type: WarpConst.MiniGame.SUBSCRIBE_ROOM,
            bet: 0,
            chipType: 1

        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_GOLDRUSH;
        WebsocketClient.Socket.send(m);
    },
    goldRushHide(bet_choose, list_hide) {
        var str = {
            type: WarpConst.MiniGame.GOLDRUSH_HIDE,
            userId: LobbyCtl.userInfo.desc.id,
            username: LobbyCtl.userInfo.desc.username,
            bet: bet_choose, //bet: 500|5000|50000
            rs: list_hide
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_GOLDRUSH;
        WebsocketClient.Socket.send(m);
    },
    goldRushGetHis() {
        var str = {
            type: WarpConst.MiniGame.GET_USER_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_GOLDRUSH;
        WebsocketClient.Socket.send(m);
    },
    goldRushGetHideList() {
        var str = {
            type: WarpConst.MiniGame.GOLDRUSH_FIND_MATCH,
            bet: -1 * LobbyCtl.userInfo.desc.id //negative user id to get hide list
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_GOLDRUSH;
        WebsocketClient.Socket.send(m);
    },
    goldRushDelOneMtach(bet_hide, id_hide) {
        var str = {
            type: WarpConst.MiniGame.GOLDRUSH_DELETE_MATCH,
            bet: bet_hide, //500|5000|50000
            idToDel: id_hide //id of the item from hide list
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_GOLDRUSH;
        WebsocketClient.Socket.send(m);
    },
    goldRushFindOneMatch(bet_choose) {
        var str = {
            type: WarpConst.MiniGame.GOLDRUSH_FIND_MATCH,
            bet: bet_choose //bet: 500|5000|50000
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_GOLDRUSH;
        WebsocketClient.Socket.send(m);
    },
    goldRushChooseOne(pack_ID) {
        cc.log("pack_ID  : ", pack_ID);
        var str = {
            type: WarpConst.MiniGame.GOLDRUSH_RUSH_ONE,
            chest_id: pack_ID //from 1 to 10
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_GOLDRUSH;
        WebsocketClient.Socket.send(m);
    },

    //SET MONEY
    //minus
    changeBalance(balanceChange) {
        LobbyCtl.instance.addUserGold(balanceChange);
    },
    setBalance(balance) {
        LobbyCtl.instance.setUserGold(balance);
    }
});