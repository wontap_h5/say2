cc.Class({
    extends: cc.Component,

    properties: {
        Item_BG: {
            default: null,
            type: cc.Node
        },
        sprite_List: {
            default: [],
            type: [cc.SpriteFrame]
        },
        label_Time: {
            default: null,
            type: cc.Label
        },
        label_Bet: {
            default: null,
            type: cc.Label
        },
        label_List: {
            default: null,
            type: cc.Label
        }
    },

    hide_ID: null,
    GR_Main: null,

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        // this.GR_Main = cc.find("DaoVang").getComponent("GR_Main");
        this.GR_Main = cc.find("UIController/DaoVang").getComponent("GR_Main");
        // cc.log(this.GR_Main);
    },

    start() {

    },

    update(dt) {},

    init_Item_Hide(data) {
        // cc.log("Item Hide : ", data);
        this.Item_BG.getComponent(cc.Sprite).spriteFrame = this.sprite_List[data.bg_index];
        this.label_Time.string = data.time;
        this.label_Bet.string = this.convert_money(data.bet);
        this.label_List.string = data.list;

        this.hide_ID = data.id;
        this.node.name = "" + data.id;
    },

    request_Del_Hide() {
        // push ID ra Main
        cc.log(this.hide_ID);
        this.GR_Main.delete_Hide_Row(this.hide_ID);
    },

    convert_money(price) {
        return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " ";
    }
});