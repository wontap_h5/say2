cc.Class({
    extends: cc.Component,

    properties: {
        Item_BG: {
            default: null,
            type: cc.Node
        },
        sprite_List: {
            default: [],
            type: [cc.SpriteFrame]
        },
        label_Session: {
            default: null,
            type: cc.Label
        },
        label_Time: {
            default: null,
            type: cc.Label
        },
        label_Bet: {
            default: null,
            type: cc.Label
        },
        label_Gold: {
            default: null,
            type: cc.Label
        }
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {

    },

    // update (dt) {},

    init_Item_History(data) {
        // cc.log("Item History : ", data);
        this.Item_BG.getComponent(cc.Sprite).spriteFrame = this.sprite_List[data.bg_index];
        this.label_Session.string = data.sess;
        this.label_Time.string = data.time;
        this.label_Bet.string = "   " + this.convert_money(data.bet);
        this.label_Gold.string = this.convert_money(data.gold);
    },

    convert_money(price) {
        return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")  + " ";
    }
});