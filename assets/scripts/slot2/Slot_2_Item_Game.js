cc.Class({
    extends: cc.Component,

    properties: {
        sprite_List: {
            default: [],
            type: [cc.SpriteFrame]
        },
        shadow_sprite_List: {
            default: [],
            type: [cc.SpriteFrame]
        },
        fx_p1: {
            default: null,
            type: cc.Node
        },
        fx_p2: {
            default: null,
            type: cc.Node
        },
        fx_p3: {
            default: null,
            type: cc.Node
        },
        fx_p4: {
            default: null,
            type: cc.Node
        },
        fx_p5: {
            default: null,
            type: cc.Node
        },
        fx_p6: {
            default: null,
            type: cc.Node
        },
        fx_p7: {
            default: null,
            type: cc.Node
        }
    },

    item_index: null,
    Pirate_Controller: null,
    random_spriteFrame_index: null,

    // LIFE-CYCLE CALLBACKS:

    onLoad() {},

    start() {
        this.create_random_item_spriteFrame();
    },

    randomBetween(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    },

    // init as server data to this spin
    init_item(data) {
        this.item_index = parseInt(data.icon) - 1;
        cc.log(this.item_index);
        this.node.getComponent(cc.Sprite).spriteFrame = this.sprite_List[this.item_index];
    },

    // create random icon for each visit game play
    create_random_item_spriteFrame() {
        this.random_spriteFrame_index = this.randomBetween(0, 6);
        this.node.getComponent(cc.Sprite).spriteFrame = this.sprite_List[this.random_spriteFrame_index];
    },

    // hide icon before spin
    show_shadow_spriteFrame() {
        this.node.getComponent(cc.Sprite).spriteFrame = this.shadow_sprite_List[this.random_spriteFrame_index];
    },

    // show after complete run all anim
    show_icon() {
        this.node.getComponent(cc.Sprite).spriteFrame = this.sprite_List[this.item_index];
    },

    // hide icon false for play anim 
    hide_icon() {
        this.node.getComponent(cc.Sprite).spriteFrame = this.shadow_sprite_List[this.item_index];
    },

    // check icon will play anim or hide when play anim
    check_show_Anim(index) {
        if (this.item_index === index) {
            this.show_Anim();
        } else {
            // this.hide_icon();
        }
    },

    show_Anim() {
        this.node.getComponent(cc.Sprite).enabled = false;

        switch (this.item_index) {
            case 0:
                this.fx_p1.active = true;
                this.fx_p1.getComponent(cc.Animation).play("slot2_fx_1");
                break;
            case 1:
                this.fx_p2.active = true;
                this.fx_p2.getComponent(cc.Animation).play("slot2_fx_2");
                break;
            case 2:
                this.fx_p3.active = true;
                this.fx_p3.getComponent(cc.Animation).play("slot2_fx_3");
                break;
            case 3:
                this.fx_p4.active = true;
                this.fx_p4.getComponent(cc.Animation).play("slot2_fx_4");
                break;
            case 4:
                this.fx_p5.active = true;
                this.fx_p5.getComponent(cc.Animation).play("slot2_fx_5");
                break;
            case 5:
                this.fx_p6.active = true;
                this.fx_p6.getComponent(cc.Animation).play("slot2_fx_6");
                break;
            case 6:
                this.fx_p7.active = true;
                this.fx_p7.getComponent(cc.Animation).play("slot2_fx_7");
                break;
            default:
                break;
        }
        var that = this;
        this.node.runAction(
            cc.sequence(
                cc.delayTime(1),
                cc.callFunc(function () {
                    that.fx_p1.active = false;
                    that.fx_p2.active = false;
                    that.fx_p3.active = false;
                    that.fx_p4.active = false;
                    that.fx_p5.active = false;
                    that.fx_p6.active = false;
                    that.fx_p7.active = false;
                    that.node.getComponent(cc.Sprite).enabled = true;
                })
            )
        );
    },

    update(dt) {},
});