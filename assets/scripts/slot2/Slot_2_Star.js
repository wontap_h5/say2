cc.Class({
    extends: cc.Component,

    properties: {},

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {

    },

    init_Star(time_delay) {
        var that = this;
        setTimeout(function () {
            if (that.node) {
                cc.log("init_Star");
                that.node.runAction(
                    cc.repeatForever(
                        cc.sequence(
                            cc.scaleTo(0.5, 5, 5),
                            cc.rotateBy(0.5, 50, 50),
                            cc.scaleTo(0.5, 0, 0),
                            cc.delayTime(8)
                        )
                    )
                );
            } else {
                cc.log("init_Star Error");
            }
        }, time_delay);
    },

    remove_Star() {
        cc.log("remove_Star");
        this.node.stopAllActions();
    }

    // update (dt) {},
});