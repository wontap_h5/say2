cc.Class({
    extends: cc.Component,

    properties: {
        sprite_List: {
            default: [],
            type: [cc.SpriteFrame]
        },
        shadow_sprite_List: {
            default: [],
            type: [cc.SpriteFrame]
        }
    },

    item_index: null,
    random_spriteFrame_index: null,

    // LIFE-CYCLE CALLBACKS:

    onLoad() {},

    start() {
        this.create_random_item_spriteFrame();
    },

    randomBetween(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    },

    // init as server data to this spin
    init_item(data) {
        this.item_index = parseInt(data.icon) - 1;
        this.node.getComponent(cc.Sprite).spriteFrame = this.sprite_List[this.item_index];
    },

    // create random icon for each visit game play
    create_random_item_spriteFrame() {
        this.random_spriteFrame_index = this.randomBetween(0, 5); // Mini Slot has 6 big Icons
        this.node.getComponent(cc.Sprite).spriteFrame = this.sprite_List[this.random_spriteFrame_index];
    },

    // hide icon before spin
    show_shadow_spriteFrame() {
        this.node.getComponent(cc.Sprite).spriteFrame = this.shadow_sprite_List[this.random_spriteFrame_index];
    },

    // show after complete run all anim
    show_icon() {
        this.node.getComponent(cc.Sprite).spriteFrame = this.sprite_List[this.item_index];
    },

    // hide icon false for play anim 
    hide_icon() {
        this.node.getComponent(cc.Sprite).spriteFrame = this.shadow_sprite_List[this.item_index];
    },

    // check icon will play anim or hide when play anim
    check_show_Anim(list_icon) {
        if (list_icon.includes(this.item_index + 1)) {
            this.show_icon();
            var that = this;
            this.node.runAction(
                cc.sequence(
                    cc.scaleTo(0, 1, 1),
                    cc.scaleTo(0.25, 0.8, 0.8),
                    cc.scaleTo(0.25, 1.2, 1.2),
                    cc.scaleTo(0.25, 0.8, 0.8),
                    cc.scaleTo(0.25, 1, 1),
                    cc.callFunc(function () {
                        that.hide_icon();
                    })
                )
            );
        } else {
            // this.hide_icon();
        }

        // if (this.item_index === index) {
        //     this.show_icon(); // hien luon Icon - vi anim basic flat
        // } else {
        //     this.hide_icon();
        // }
    },

    update(dt) {},
});