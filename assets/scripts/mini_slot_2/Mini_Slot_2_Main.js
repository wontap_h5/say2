var WarpConst = require("WarpConst");
var WebsocketClient = require("WebsocketClient");

var LobbyCtl = require("LobbyCtl");
var Slot1 = require("Pirate_Main");
var Slot2 = require("Slot_2_Main");
var Slot3 = require("Slot_3_Main");
var Slot4 = require("Slot_4_Main");
var GM = require("GM");
var Mini_Slot_2 = cc.Class({
    extends: cc.Component,

    statics: {
        scope: null
    },

    properties: {
        Bet_selector: {
            default: [],
            type: cc.Toggle
        },
        GP_Column_1: {
            default: null,
            type: cc.Node
        },
        GP_Column_2: {
            default: null,
            type: cc.Node
        },
        GP_Column_3: {
            default: null,
            type: cc.Node
        },
        add_Gold_Effect: {
            default: null,
            type: cc.Node
        },
        PS_Add_Gold: {
            default: null,
            type: cc.Node
        },
        gold_Add_Node: {
            default: null,
            type: cc.Node
        },
        label_Gold_Add: {
            default: null,
            type: cc.Label
        },
        node_Jackpot: {
            default: null,
            type: cc.Node
        },
        label_Jackpot: {
            default: null,
            type: cc.Label
        },
        Popup_Rank: {
            default: null,
            type: cc.Node
        },
        Popup_Guide: {
            default: null,
            type: cc.Node
        },
        Popup_History: {
            default: null,
            type: cc.Node
        },
        Rank_Content: {
            default: null,
            type: cc.Node
        },
        Item_Rank_Prefab: {
            default: null,
            type: cc.Prefab
        },
        History_Content: {
            default: null,
            type: cc.Node
        },
        Item_History_Prefab: {
            default: null,
            type: cc.Prefab
        },
        Btn_Flash_Spin: {
            default: null,
            type: cc.Toggle
        },
        BG_No_Hu: {
            default: null,
            type: cc.Node
        },
        Effect_No_Hu: {
            default: null,
            type: cc.Node
        },
        PS_No_Hu: {
            default: null,
            type: cc.Node
        },
        label_Gold_No_Hu: {
            default: null,
            type: cc.Label
        },
        Node_Gold_No_Hu: {
            default: null,
            type: cc.Node
        },
        BG_Thang_Lon: {
            default: null,
            type: cc.Node
        },
        Effect_Thang_Lon: {
            default: null,
            type: cc.Node
        },
        PS_Thang_Lon: {
            default: null,
            type: cc.Node
        },
        label_Gold_Thang_Lon: {
            default: null,
            type: cc.Label
        },
        Node_Gold_Thang_Lon: {
            default: null,
            type: cc.Node
        },
        Node_Use_2_Stop_Anims: {
            default: null,
            type: cc.Node
        },
        Popup_Error_Action: {
            default: null,
            type: cc.Node
        },
        label_popup_error_action_mes: {
            default: null,
            type: cc.Label
        },
        Popup_Line_Bet: {
            default: null,
            type: cc.Node
        },
        label_Bet_Line: {
            default: null,
            type: cc.Label
        },
        btn_Spin_Manual: {
            default: null,
            type: cc.Node
        },
        List_Line_Bet: {
            default: null,
            type: cc.Node
        },
        Boom_List_Line : {
            default: null,
            type: cc.Node
        },
        Game_Drag: {
            default: null,
            type: cc.Node
        }
    },

    Spin_Count: null,
    Spining: null,
    Result_Spin_Server: null,
    Table_Matrix_3x3: null,
    Line_Selected_Matrix_1x20: null,
    SPIN_FLASH_ONLINE: null,

    Server_Data_Room: null,
    Spin_Bet: null,
    Last_Jackpot: null,
    Bet_Line_Count: null,

    // LIFE-CYCLE CALLBACKS:

    OnMiniSlot_2_SubDone(status, res) {
        cc.log("OnMiniSlot_2_SubDone : ", res);
        var data = JSON.parse(res);
        if (status == WarpConst.WarpResponseResultCode.SUCCESS) {
            if ("undefined" !== typeof data.type) {
                if (data.type === 23) {
                    Mini_Slot_2.scope.effect_Update_Gold(0, data.jackpot, Mini_Slot_2.scope.node_Jackpot, Mini_Slot_2.scope.label_Jackpot);
                    Mini_Slot_2.scope.Last_Jackpot = data.jackpot;
                    Mini_Slot_2.scope.Spin_Bet = data.bet;
                } else {
                    Mini_Slot_2.scope.Show_Popup_Error_Action(" Kết nối thất bại \n Vui lòng thử lại sau ! ");
                    Mini_Slot_2.scope.f5_bet();
                }
            } else {
                Mini_Slot_2.scope.Show_Popup_Error_Action(" Kết nối thất bại \n Vui lòng thử lại sau ! ");
                Mini_Slot_2.scope.f5_bet();
            }
        } else {
            Mini_Slot_2.scope.check_OnMiniSlotSubDone = false;
            Mini_Slot_2.scope.Show_Popup_Error_Action(" Đổi mức cược không thành công. \n Vui lòng thử lại sau. ");
            Mini_Slot_2.scope.f5_bet();
        }
    },

    f5_bet() {
        switch (Mini_Slot_2.scope.Spin_Bet) {
            case 100:
                Mini_Slot_2.scope.Bet_selector[0].isChecked = true;
                break;
            case 1000:
                Mini_Slot_2.scope.Bet_selector[1].isChecked = true;
                break;
            case 10000:
                Mini_Slot_2.scope.Bet_selector[2].isChecked = true;
                break;
        }
    },

    OnMiniSlot_2_StartDone(res) {
        cc.log("OnMiniSlot_2_StartDone : ", res);
        var raw_data = JSON.parse(res);
        if ("undefined" !== typeof raw_data.result) {
            var data = JSON.parse(raw_data.result);
            cc.log("OnMiniSlot_2_StartDone : ", JSON.stringify(data));
            Mini_Slot_2.scope.Result_Spin_Server = data;

            Mini_Slot_2.scope.Manager_Spin_Anim();

            var time_update_money = 3500;
            if (Mini_Slot_2.scope.SPIN_FLASH_ONLINE) {
                cc.log("Machine_Spin_Auto");
                Mini_Slot_2.scope.Machine_Spin_Flash();
                time_update_money = 1500;
            } else {
                cc.log("Machine_Spin_Manual");
                Mini_Slot_2.scope.Machine_Spin_Manual();
            }

            setTimeout(function () {
                switch (GM.Stay) {
                    case 0:
                        cc.log("Playing on Lobby");
                        Mini_Slot_2.scope.setBalance(data.gold);
                        break;
                    case 1:
                        cc.log("Playing on Slot 1");
                        GM.Gold_Lastest = data.gold;
                        Slot1.scope.notify_Gold_State_Change();
                        break;
                    case 2:
                        cc.log("Playing on Slot 2");
                        GM.Gold_Lastest = data.gold;
                        Slot2.scope.notify_Gold_State_Change();
                        break;
                    case 3:
                        cc.log("Playing on Slot 3");
                        GM.Gold_Lastest = data.gold;
                        Slot3.scope.notify_Gold_State_Change();
                        break;
                    case 4:
                        cc.log("Playing on Slot 4");
                        GM.Gold_Lastest = data.gold;
                        Slot4.scope.notify_Gold_State_Change();
                        break;
                    default:
                        break;
                }
            }, time_update_money);
        }
    },

    OnMiniSlot_2_GetUserHistoryDone(res) {
        cc.log("OnMiniSlot_2_GetUserHistoryDone : ", res);
        var data = JSON.parse(res);
        Mini_Slot_2.scope.Popup_History.active = true;
        Mini_Slot_2.scope.Popup_History.position = cc.v2(-Mini_Slot_2.scope.Game_Drag.position.x, -Mini_Slot_2.scope.Game_Drag.position.y);
        Mini_Slot_2.scope.History_Content.removeAllChildren(true);
        var Data_History = data.history;
        for (var i = 0; i < Data_History.length; i++) {
            var item = cc.instantiate(Mini_Slot_2.scope.Item_History_Prefab);
            var data = Data_History[i];
            item.getComponent('Mini_Slot_2_Item_History').init_Item_History({
                bg_index: i % 2,
                sess: "#" + data.id,
                time: data.time,
                bet: data.bet,
                gold: data.add
            });
            Mini_Slot_2.scope.History_Content.addChild(item);
        }
    },

    OnMiniSlot_2_GetGloryDone(res) {
        cc.log("OnMiniSlot_2_GetGloryDone : ", res);
        var data = JSON.parse(res);
        Mini_Slot_2.scope.Popup_Rank.active = true;
        Mini_Slot_2.scope.Popup_Rank.position = cc.v2(-Mini_Slot_2.scope.Game_Drag.position.x, -Mini_Slot_2.scope.Game_Drag.position.y);
        Mini_Slot_2.scope.Rank_Content.removeAllChildren(true);
        var Data_Rank = data.history;
        for (var i = 0; i < Data_Rank.length; i++) {
            var item = cc.instantiate(Mini_Slot_2.scope.Item_Rank_Prefab);
            var data = Data_Rank[i];
            var name = decodeURIComponent(escape(data.displayName));
            if (name.length > 15) {
                name = name.substring(0, 11) + "...";
            }
            item.getComponent('Mini_Slot_2_Item_Rank').init_Item_Rank({
                bg_index: i % 2,
                sess: "#" + data.id,
                time: data.time,
                account: name,
                bet: data.bet,
                gold: data.win
            });
            Mini_Slot_2.scope.Rank_Content.addChild(item);
        }
    },

    onLoad() {
        Mini_Slot_2.scope = this;
        this.Server_Data_Room = {};
        this.Spin_Bet = 100; // Default Mode 100 is selected
        this.Last_Jackpot = 0;
        this.Spining = false;
        this.Spin_Count = 0;
        this.Bet_Line_Count = 20;
        this.label_Bet_Line.string = this.Bet_Line_Count;
        this.Table_Matrix_3x3 = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9],
            [1, 8, 3],
            [7, 2, 9],
            [1, 5, 3],
            [1, 5, 9],
            [7, 5, 3],
            [4, 8, 6],
            [4, 2, 6],
            [7, 5, 9],
            [1, 2, 6],
            [4, 5, 9],
            [4, 5, 3],
            [7, 8, 6],
            [4, 2, 3],
            [7, 5, 6],
            [1, 5, 6],
            [4, 8, 9],
            [1, 8, 6]
        ];

        this.Line_Selected_Matrix_1x20 = [];
        for (let index = 0; index < 20; index++) {
            this.Line_Selected_Matrix_1x20.push(index + 1);
        }

        this.SPIN_FLASH_ONLINE = false;
    },

    OnJackpotChange(res) {
        cc.log("OnJackpotChange :  ", res);
        var data = JSON.parse(res);
        var old_jackpot = Mini_Slot_2.scope.Last_Jackpot;
        switch (Mini_Slot_2.scope.Spin_Bet) {
            case 100:
                if (data.roomId === 43) {
                    Mini_Slot_2.scope.effect_Update_Jackpot(old_jackpot, data.jackpot, Mini_Slot_2.scope.node_Jackpot, Mini_Slot_2.scope.label_Jackpot);
                    Mini_Slot_2.scope.Last_Jackpot = data.jackpot;
                }
                break;
            case 1000:
                if (data.roomId === 44) {
                    Mini_Slot_2.scope.effect_Update_Jackpot(old_jackpot, data.jackpot, Mini_Slot_2.scope.node_Jackpot, Mini_Slot_2.scope.label_Jackpot);
                    Mini_Slot_2.scope.Last_Jackpot = data.jackpot;
                }
                break;
            case 10000:
                if (data.roomId === 45) {
                    Mini_Slot_2.scope.effect_Update_Jackpot(old_jackpot, data.jackpot, Mini_Slot_2.scope.node_Jackpot, Mini_Slot_2.scope.label_Jackpot);
                    Mini_Slot_2.scope.Last_Jackpot = data.jackpot;
                }
                break;
        }
    },


    onEnable() {
        cc.log("onEnable");
        this.SPIN_FLASH_ONLINE = false;
        this.Spining = false;
        this.Spin_Bet = 100;
        this.Bet_selector[0].isChecked = true;

        WebsocketClient.OnMiniSlot_2_SubDone = this.OnMiniSlot_2_SubDone;
        WebsocketClient.OnMiniSlot_2_StartDone = this.OnMiniSlot_2_StartDone;
        WebsocketClient.OnMiniSlot_2_GetUserHistoryDone = this.OnMiniSlot_2_GetUserHistoryDone;
        WebsocketClient.OnMiniSlot_2_GetGloryDone = this.OnMiniSlot_2_GetGloryDone;
        WebsocketClient.OnJackpotChange = this.OnJackpotChange;

        this.subMiniSlot_2(100);
    },

    onDisable() {
        cc.log("onDisable");
    },

    start() {},

    update(dt) {},

    Back_2_G_Main() {
        if (!this.Spining && !this.SPIN_FLASH_ONLINE) {
            this.Node_Use_2_Stop_Anims.stopAllActions();
            cc.find("UIController").getComponent("UIController").hide_Mini_Slot_2();
        } else {
            cc.log("Can not close ");
            this.Show_Popup_Error_Action(" Không thể thoát trò chơi ! \n khi đang quay ");
        }
    },

    Show_Rank() {
        this.getMiniSlot_2_Glory();
    },
    Show_Guide() {
        this.Popup_Guide.active = true;
        this.Popup_Guide.position = cc.v2(-Mini_Slot_2.scope.Game_Drag.position.x, -Mini_Slot_2.scope.Game_Drag.position.y);
    },
    Show_History() {
        this.getMiniSlot_2_His();
    },

    Close_Rank() {
        this.Popup_Rank.active = false;
    },

    Close_Guide() {
        this.Popup_Guide.active = false;
    },

    Close_History() {
        this.Popup_History.active = false;
    },

    Game_Spin_Manual() {
        if (!this.Spining && !this.SPIN_FLASH_ONLINE) {
            // init Data and Spin
            
            if (this.Bet_Line_Count > 0) {
                cc.log("LobbyCtl.userInfo : ", WarpConst.GameBase.userInfo.desc.gold);
                cc.log("Spin_Bet_Bet : ", this.Spin_Bet);
                if (GM.Stay === 0) {
                    if (WarpConst.GameBase.userInfo.desc.gold >= this.Spin_Bet * this.Bet_Line_Count) {
                        this.Spin();
                    } else {
                        this.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                } else {
                    if (GM.Gold_Lastest >= this.Spin_Bet * this.Bet_Line_Count) {
                        this.Spin();
                    } else {
                        this.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                }
            } else {
                this.Show_Popup_Error_Action(" Phải chọn ít nhất 1 dòng cược ");
            }
        } else {
            cc.log("Game_Spin_Manual : Can not Spin");
        }
    },

    Machine_Spin_Manual() {
        var that = this;
        var list_line_win = [];
        if ("undefined" !== typeof this.Result_Spin_Server.winLine) {
            for (let index = 0; index < this.Result_Spin_Server.winLine.length; index++) {
                const line = this.Result_Spin_Server.winLine[index];
                list_line_win.push(line);
            }
        }
        cc.log("list_line_win : ", list_line_win);

        setTimeout(function () {
            // hide all Icon
            for (let x = 0; x < 3; x++) {
                that.GP_Column_1.children[x].getComponent("Mini_Slot_2_Item_Game").hide_icon();
                that.GP_Column_2.children[x].getComponent("Mini_Slot_2_Item_Game").hide_icon();
                that.GP_Column_3.children[x].getComponent("Mini_Slot_2_Item_Game").hide_icon();
            }

            // show all line if win
            if (list_line_win.length > 0) {
                var time_show_each_line = 2000;
                var spin_spec = 2000;
                var time_hide_boom_line = 3000;
                if (that.Result_Spin_Server.isJackpot) {
                    // Jackpot
                    spin_spec = 3000;
                    time_hide_boom_line = 4000;
                    time_show_each_line = 5000;
                    that.Effect_Spin_Special(1, that.Result_Spin_Server.win_chips);
                } else if (that.Result_Spin_Server.isBigwin) {
                    // Bigwin
                    spin_spec = 3000;
                    time_hide_boom_line = 4000;
                    time_show_each_line = 5000;
                    that.Effect_Spin_Special(2, that.Result_Spin_Server.win_chips);
                } else {
                    // Normal
                    if (that.Result_Spin_Server.win_chips > 0) {
                        that.add_Gold_Effect.active = true;
                        that.PS_Add_Gold.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                        that.effect_Update_Gold(0, that.Result_Spin_Server.win_chips, that.label_Gold_Add.node, that.label_Gold_Add);
                        setTimeout(function () {
                            that.add_Gold_Effect.active = false;
                        }, 2000);
                        time_show_each_line = 4000;
                    } else {
                        time_show_each_line = 2500;
                    }
                }

                // if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                //     cc.log("Quay Trung Thuong");
                //     spin_spec = 3000;
                //     time_show_each_line = 5000;
                //     // 1 : JACKPOT    -    2 : BIGWIN
                //     switch (that.Result_Spin_Server.spec_prize) {
                //         case "JACKPOT":
                //             that.Effect_Spin_Special(1, that.Result_Spin_Server.win_chips);
                //             break;
                //         case "BIGWIN":
                //             that.Effect_Spin_Special(2, that.Result_Spin_Server.win_chips);
                //             break;
                //         default:
                //             break;
                //     }
                // } else {
                //     cc.log("Quay Thuong");
                //     if (that.Result_Spin_Server.win_chips > 0) {
                //         that.add_Gold_Effect.active = true;
                //         that.PS_Add_Gold.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                //         that.effect_Update_Gold(0, that.Result_Spin_Server.win_chips, that.label_Gold_Add.node, that.label_Gold_Add);
                //         setTimeout(function () {
                //             that.add_Gold_Effect.active = false;
                //         }, 2000);
                //         time_show_each_line = 4000;
                //     } else {
                //         time_show_each_line = 2500;
                //     }
                // }

                // show All Icons
                that.Manual_spin_spec(list_line_win, spin_spec);
                // effect show result after Spin end

            } else {
                setTimeout(function () {
                    for (let x = 0; x < 3; x++) {
                        that.GP_Column_1.children[x].getComponent("Mini_Slot_2_Item_Game").show_icon();
                        that.GP_Column_2.children[x].getComponent("Mini_Slot_2_Item_Game").show_icon();
                        that.GP_Column_3.children[x].getComponent("Mini_Slot_2_Item_Game").show_icon();
                    }
                    that.Spining = false;
                    cc.log("Can Spins");
                    if (that.SPIN_FLASH_ONLINE) {
                        if (GM.Stay === 0) {
                            if (WarpConst.GameBase.userInfo.desc.gold >= that.Spin_Bet * that.Bet_Line_Count) {
                                that.Flash_Spin();
                            } else {
                                that.Btn_Flash_Spin.isChecked = false;
                                that.Spining = false;
                                that.SPIN_FLASH_ONLINE = false;
                                that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                            }
                        } else {
                            if (GM.Gold_Lastest >= that.Spin_Bet * that.Bet_Line_Count) {
                                that.Flash_Spin();
                            } else {
                                that.Btn_Flash_Spin.isChecked = false;
                                that.Spining = false;
                                that.SPIN_FLASH_ONLINE = false;
                                that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                            }
                        }
                    }
                }, 1500);
            }
        }, 3000);
    },

    Manual_spin_spec(list_line_win, spin_spec) {
        var that = this;
        setTimeout(function () {
            for (let index = 0; index < list_line_win.length; index++) {
                var line_id = list_line_win[index].line;
                that.Boom_List_Line.children[line_id - 1].active = true;
                var line_data = list_line_win[index].special.split("-");
                var icon_open_id = [];
                if (line_data.length == 1) {
                    // An 3 Icon giong nhau
                    var end = parseInt(line_data[0].charAt(5));
                    icon_open_id = [end];
                } else if (line_data.length == 2) {
                    // An 2 Icon 5 hoac 6, hoac 2 icon x va 1 icon 1
                    var start_left = parseInt(line_data[0].charAt(0));
                    var end_left = parseInt(line_data[0].charAt(5));
                    var start_right = parseInt(line_data[1].charAt(0));
                    var end_right = parseInt(line_data[1].charAt(5));

                    if (end_left === 1) {
                        // left co icon 1
                        icon_open_id = [1, end_right];
                    } else if (end_right === 1) {
                        // right co icon 1
                        icon_open_id = [1, end_left];
                    } else {
                        if (start_left > start_right) {
                            // An o left
                            icon_open_id = [end_left];
                        } else {
                            icon_open_id = [end_right];
                        }
                    }

                } else if (line_data.length == 3) {
                    // An 1 icon 5, icon 6, icon 1
                    var end_left = parseInt(line_data[0].charAt(5));
                    var end_mid = parseInt(line_data[1].charAt(5));
                    var end_right = parseInt(line_data[2].charAt(5));
                    var list_icon = [end_left, end_mid, end_right];
                    if (list_icon.includes(1) && list_icon.includes(5) && list_icon.includes(6)) {
                        icon_open_id = [1, 5, 6];
                    } else {
                        if (list_icon.includes(5)) {
                            icon_open_id = [1, 5];
                        } else {
                            icon_open_id = [1, 6];
                        }
                    }
                }

                cc.log("icon_open_id : ", icon_open_id);

                for (let x = 0; x < 3; x++) {
                    var location = that.Table_Matrix_3x3[line_id - 1][x];
                    var col_id = location % 3;
                    var child_id = Math.floor(location / 3);
                    if (col_id === 0) {
                        col_id = 3;
                        child_id -= 1;
                    }
                    switch (col_id) {
                        case 1:
                            // that.GP_Column_1.children[child_id].getComponent("Mini_Slot_2_Item_Game").check_show_Anim(icon_open_id - 1);
                            that.GP_Column_1.children[child_id].getComponent("Mini_Slot_2_Item_Game").check_show_Anim(icon_open_id);
                            break;
                        case 2:
                            that.GP_Column_2.children[child_id].getComponent("Mini_Slot_2_Item_Game").check_show_Anim(icon_open_id);
                            break;
                        case 3:
                            that.GP_Column_3.children[child_id].getComponent("Mini_Slot_2_Item_Game").check_show_Anim(icon_open_id);
                            break;
                        default:
                            break;
                    }
                }
            }
        }, spin_spec);

        setTimeout(function () {
            for (let index = 0; index < list_line_win.length; index++) {
                var line_id = list_line_win[index].line;
                that.Boom_List_Line.children[line_id - 1].active = false;
            }
            that.Spining = false;
            cc.log("Can Spins");
            if (that.SPIN_FLASH_ONLINE) {
                if (GM.Stay === 0) {
                    if (WarpConst.GameBase.userInfo.desc.gold >= that.Spin_Bet * that.Bet_Line_Count) {
                        that.Flash_Spin();
                    } else {
                        that.Btn_Flash_Spin.isChecked = false;
                        that.Spining = false;
                        that.SPIN_FLASH_ONLINE = false;
                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                } else {
                    if (GM.Gold_Lastest >= that.Spin_Bet * that.Bet_Line_Count) {
                        that.Flash_Spin();
                    } else {
                        that.Btn_Flash_Spin.isChecked = false;
                        that.Spining = false;
                        that.SPIN_FLASH_ONLINE = false;
                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                }
            } else {
                that.Manual_time_show_each_line(list_line_win, 1); //   # 10000
            }
        }, spin_spec + 1000);
    },

    Manual_time_show_each_line(list_line_win, time_show_each_line) {
        var that = this;
        this.Node_Use_2_Stop_Anims.stopAllActions();
        this.Node_Use_2_Stop_Anims.runAction(
            cc.sequence(
                cc.delayTime(time_show_each_line), // 1
                cc.callFunc(function () {
                    for (let x = 0; x < 3; x++) {
                        that.GP_Column_1.children[x].getComponent("Mini_Slot_2_Item_Game").hide_icon();
                        that.GP_Column_2.children[x].getComponent("Mini_Slot_2_Item_Game").hide_icon();
                        that.GP_Column_3.children[x].getComponent("Mini_Slot_2_Item_Game").hide_icon();
                    }
                    // show each line eat and hide it if win
                    for (let index = 0; index < list_line_win.length; index++) {
                        that.Node_Use_2_Stop_Anims.runAction(
                            cc.sequence(
                                cc.delayTime(1.5 * index),
                                cc.callFunc(function () {
                                    cc.log("Vao");
                                    if (!that.Spining) {
                                        var line_id = list_line_win[index].line;
                                        that.Boom_List_Line.children[line_id - 1].active = true;
                                        var line_data = list_line_win[index].special.split("-");
                                        var icon_open_id = [];
                                        if (line_data.length == 1) {
                                            // An 3 Icon giong nhau
                                            var end = parseInt(line_data[0].charAt(5));
                                            icon_open_id = [end];
                                        } else if (line_data.length == 2) {
                                            // An 2 Icon 5 hoac 6, hoac 2 icon x va 1 icon 1
                                            var start_left = parseInt(line_data[0].charAt(0));
                                            var end_left = parseInt(line_data[0].charAt(5));
                                            var start_right = parseInt(line_data[1].charAt(0));
                                            var end_right = parseInt(line_data[1].charAt(5));
                        
                                            if (end_left === 1) {
                                                // left co icon 1
                                                icon_open_id = [1, end_right];
                                                // if (start_left == 2) {
                                                //     icon_open_id = [1, 1, end_right];
                                                // } else {
                                                //     icon_open_id = [1, end_right, end_right];
                                                // }
                                            } else if (end_right === 1) {
                                                // right co icon 1
                                                icon_open_id = [1, end_left];
                                                // if (start_right == 2) {
                                                // icon_open_id = [end_left, 1, 1];
                                                // } else {
                                                // icon_open_id = [end_left, end_left, 1];
                                                // }
                                            } else {
                                                if (start_left > start_right) {
                                                    // An o left
                                                    // icon_open_id = [end_left, end_left];
                                                    icon_open_id = [end_left];
                                                } else {
                                                    // icon_open_id = [end_right, end_right];
                                                    icon_open_id = [end_right];
                                                }
                                            }
                        
                                        } else if (line_data.length == 3) {
                                            // An 1 icon 5, icon 6, icon 1
                                            // icon_open_id = [1, 5, 6];
                                            var end_left = parseInt(line_data[0].charAt(5));
                                            var end_mid = parseInt(line_data[1].charAt(5));
                                            var end_right = parseInt(line_data[2].charAt(5));
                                            var list_icon = [end_left, end_mid, end_right];
                                            if (list_icon.includes(1) && list_icon.includes(5) && list_icon.includes(6)) {
                                                icon_open_id = [1, 5, 6];
                                            } else {
                                                if (list_icon.includes(5)) {
                                                    icon_open_id = [1, 5];
                                                } else {
                                                    icon_open_id = [1, 6];
                                                }
                                            }
                                        }
                        
                                        cc.log("icon_open_id : ", icon_open_id);

                                        // var icon_open_id = parseInt(list_line_win[index].icon.charAt(7));
                                        for (let x = 0; x < 3; x++) {
                                            var location = that.Table_Matrix_3x3[line_id - 1][x];
                                            var col_id = location % 3; // 5
                                            var child_id = Math.floor(location / 3);
                                            if (col_id === 0) {
                                                col_id = 3;
                                                child_id -= 1;
                                            }
                                            switch (col_id) {
                                                case 1:
                                                    // that.GP_Column_1.children[child_id].getComponent("Mini_Slot_2_Item_Game").check_show_Anim(icon_open_id - 1);
                                                    that.GP_Column_1.children[child_id].getComponent("Mini_Slot_2_Item_Game").check_show_Anim(icon_open_id);
                                                    break;
                                                case 2:
                                                    that.GP_Column_2.children[child_id].getComponent("Mini_Slot_2_Item_Game").check_show_Anim(icon_open_id);
                                                    break;
                                                case 3:
                                                    that.GP_Column_3.children[child_id].getComponent("Mini_Slot_2_Item_Game").check_show_Anim(icon_open_id);
                                                    break;
                                                default:
                                                    break;
                                            }
                                            setTimeout(function () {
                                                that.Boom_List_Line.children[line_id - 1].active = false;
                                            }, 1000);
                                        }
                                    }
                                })
                            )
                        );
                    }

                    // show 15 icon after show all line eat
                    that.Node_Use_2_Stop_Anims.runAction(
                        cc.sequence(
                            cc.delayTime(1.5 * (list_line_win.length)), // co the chi la :  line_show_anim_count
                            cc.callFunc(function () {
                                for (let x = 0; x < 3; x++) {
                                    that.GP_Column_1.children[x].getComponent("Mini_Slot_2_Item_Game").show_icon();
                                    that.GP_Column_2.children[x].getComponent("Mini_Slot_2_Item_Game").show_icon();
                                    that.GP_Column_3.children[x].getComponent("Mini_Slot_2_Item_Game").show_icon();
                                }
                            })
                        )
                    );
                })
            )
        );
    },

    Machine_Spin_Flash() {
        var that = this;
        var list_line_win = [];
        if ("undefined" !== typeof this.Result_Spin_Server.winLine) {
            for (let index = 0; index < this.Result_Spin_Server.winLine.length; index++) {
                const line = this.Result_Spin_Server.winLine[index];
                list_line_win.push(line);
            }
        }
        cc.log("list_line_win : ", list_line_win);

        var time_result = 1250; // = time_Column_move + time_C3_delay

        // effect show result after Spin end
        setTimeout(function () {
            // hide all Icon
            for (let x = 0; x < 3; x++) {
                that.GP_Column_1.children[x].getComponent("Mini_Slot_2_Item_Game").hide_icon();
                that.GP_Column_2.children[x].getComponent("Mini_Slot_2_Item_Game").hide_icon();
                that.GP_Column_3.children[x].getComponent("Mini_Slot_2_Item_Game").hide_icon();
            }

            var time_show_all_icon = 2000;
            if (list_line_win.length > 0) {
                var spin_spec = 2000;
                if (that.Result_Spin_Server.isJackpot) {
                    // Jackpot
                    spin_spec = 3000;
                    time_show_all_icon = 5000;
                    that.Effect_Spin_Special(1, that.Result_Spin_Server.win_chips);
                } else if (that.Result_Spin_Server.isBigwin) {
                    // Bigwin
                    spin_spec = 3000;
                    time_show_all_icon = 5000;
                    that.Effect_Spin_Special(2, that.Result_Spin_Server.win_chips);
                } else {
                    // Normal
                    if (that.Result_Spin_Server.win_chips > 0) {
                        that.add_Gold_Effect.active = true;
                        time_show_all_icon = 4000;
                        that.PS_Add_Gold.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                        that.effect_Update_Gold(0, that.Result_Spin_Server.win_chips, that.label_Gold_Add.node, that.label_Gold_Add);
                        setTimeout(function () {
                            that.add_Gold_Effect.active = false;
                        }, 2000);
                    } else {
                        time_show_all_icon = 2500;
                    }
                }


                // if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                //     spin_spec = 3000;
                //     time_show_all_icon = 5000;
                //     cc.log("Quay Trung Thuong");
                //     // 1 : JACKPOT -  2 : BIGWIN
                //     switch (that.Result_Spin_Server.spec_prize) {
                //         case "JACKPOT":
                //             that.Effect_Spin_Special(1, that.Result_Spin_Server.win_chips);
                //             break;
                //         case "BIGWIN":
                //             that.Effect_Spin_Special(2, that.Result_Spin_Server.win_chips);
                //             break;
                //         default:
                //             break;
                //     }
                // } else {
                //     cc.log("Quay Thuong");
                //     if (that.Result_Spin_Server.win_chips > 0) {
                //         that.add_Gold_Effect.active = true;
                //         time_show_all_icon = 4000;
                //         that.PS_Add_Gold.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                //         that.effect_Update_Gold(0, that.Result_Spin_Server.win_chips, that.label_Gold_Add.node, that.label_Gold_Add);
                //         setTimeout(function () {
                //             that.add_Gold_Effect.active = false;
                //         }, 2000);
                //     } else {
                //         time_show_all_icon = 2500;
                //     }
                // }

                // show all effect if win
                that.Flash_spin_spec(list_line_win, spin_spec);
            }

            // show 15 icon after show all line eat
            that.Flash_time_show_all_icon(list_line_win, time_show_all_icon);
        }, time_result);
    },

    Game_Flash_Spin(toggle) {
        if (toggle.isChecked) {
            this.SPIN_FLASH_ONLINE = true;
        } else {
            this.SPIN_FLASH_ONLINE = false;
        }

        if (!this.Spining) {
            cc.log("Pre Spin");
            this.Spining = true;
            if (this.SPIN_FLASH_ONLINE) {
                cc.log("LobbyCtl.userInfo : ", WarpConst.GameBase.userInfo.desc.gold);
                cc.log("Spin_Bete_Bet : ", this.Spin_Bet);
                if (GM.Stay === 0) {
                    if (WarpConst.GameBase.userInfo.desc.gold >= this.Spin_Bet * this.Bet_Line_Count) {
                        this.Flash_Spin();
                    } else {
                        this.Btn_Flash_Spin.isChecked = false;
                        this.Spining = false;
                        this.SPIN_FLASH_ONLINE = false;
                        this.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                } else {
                    if (GM.Gold_Lastest >= this.Spin_Bet * this.Bet_Line_Count) {
                        this.Flash_Spin();
                    } else {
                        this.Btn_Flash_Spin.isChecked = false;
                        this.Spining = false;
                        this.SPIN_FLASH_ONLINE = false;
                        this.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                }

            } else {
                this.Spining = false; // uncheck , thay code ben duoi
                // this.Game_Spin_Manual();
            }
        } else {
            cc.log("Spinning");
        }
    },

    Flash_Spin() {
        // init Data and Spin
        this.Spin();
    },

    Flash_spin_spec(list_line_win, spin_spec) {
        var that = this;
        setTimeout(function () {
            for (let index = 0; index < list_line_win.length; index++) {
                var line_id = list_line_win[index].line;
                that.Boom_List_Line.children[line_id - 1].active = true;
                var line_data = list_line_win[index].special.split("-");
                var icon_open_id = [];
                if (line_data.length == 1) {
                    // An 3 Icon giong nhau
                    var end = parseInt(line_data[0].charAt(5));
                    icon_open_id = [end];
                } else if (line_data.length == 2) {
                    // An 2 Icon 5 hoac 6, hoac 2 icon x va 1 icon 1
                    var start_left = parseInt(line_data[0].charAt(0));
                    var end_left = parseInt(line_data[0].charAt(5));
                    var start_right = parseInt(line_data[1].charAt(0));
                    var end_right = parseInt(line_data[1].charAt(5));

                    if (end_left === 1) {
                        // left co icon 1
                        icon_open_id = [1, end_right];
                        // if (start_left == 2) {
                        //     icon_open_id = [1, 1, end_right];
                        // } else {
                        //     icon_open_id = [1, end_right, end_right];
                        // }
                    } else if (end_right === 1) {
                        // right co icon 1
                        icon_open_id = [1, end_left];
                        // if (start_right == 2) {
                        // icon_open_id = [end_left, 1, 1];
                        // } else {
                        // icon_open_id = [end_left, end_left, 1];
                        // }
                    } else {
                        if (start_left > start_right) {
                            // An o left
                            // icon_open_id = [end_left, end_left];
                            icon_open_id = [end_left];
                        } else {
                            // icon_open_id = [end_right, end_right];
                            icon_open_id = [end_right];
                        }
                    }

                } else if (line_data.length == 3) {
                    // An 1 icon 5, icon 6, icon 1
                    // icon_open_id = [1, 5, 6];
                    var end_left = parseInt(line_data[0].charAt(5));
                    var end_mid = parseInt(line_data[1].charAt(5));
                    var end_right = parseInt(line_data[2].charAt(5));
                    var list_icon = [end_left, end_mid, end_right];
                    if (list_icon.includes(1) && list_icon.includes(5) && list_icon.includes(6)) {
                        icon_open_id = [1, 5, 6];
                    } else {
                        if (list_icon.includes(5)) {
                            icon_open_id = [1, 5];
                        } else {
                            icon_open_id = [1, 6];
                        }
                    }
                }

                cc.log("icon_open_id : ", icon_open_id);

                for (let x = 0; x < 3; x++) {
                    var location = that.Table_Matrix_3x3[line_id - 1][x];
                    var col_id = location % 3;
                    var child_id = Math.floor(location / 3);
                    if (col_id === 0) {
                        col_id = 3;
                        child_id -= 1;
                    }
                    switch (col_id) {
                        case 1:
                            // that.GP_Column_1.children[child_id].getComponent("Mini_Slot_2_Item_Game").check_show_Anim(icon_open_id - 1);
                            that.GP_Column_1.children[child_id].getComponent("Mini_Slot_2_Item_Game").check_show_Anim(icon_open_id);
                            break;
                        case 2:
                            that.GP_Column_2.children[child_id].getComponent("Mini_Slot_2_Item_Game").check_show_Anim(icon_open_id);
                            break;
                        case 3:
                            that.GP_Column_3.children[child_id].getComponent("Mini_Slot_2_Item_Game").check_show_Anim(icon_open_id);
                            break;
                        default:
                            break;
                    }
                    // cc.log(col_id + ' - ' + child_id);
                }
            }
        }, spin_spec);
    },

    Flash_time_show_all_icon(list_line_win, time_show_all_icon) {
        var that = this;
        setTimeout(function () {
            for (let index = 0; index < list_line_win.length; index++) {
                var line_id = list_line_win[index].line;
                that.Boom_List_Line.children[line_id - 1].active = false;
            }

            for (let x = 0; x < 3; x++) {
                that.GP_Column_1.children[x].getComponent("Mini_Slot_2_Item_Game").show_icon();
                that.GP_Column_2.children[x].getComponent("Mini_Slot_2_Item_Game").show_icon();
                that.GP_Column_3.children[x].getComponent("Mini_Slot_2_Item_Game").show_icon();
            }
            that.Spining = false;
            cc.log("Can Spins");
            if (that.SPIN_FLASH_ONLINE) {
                if (GM.Stay === 0) {
                    if (WarpConst.GameBase.userInfo.desc.gold >= that.Spin_Bet * that.Bet_Line_Count) {
                        that.Flash_Spin();
                    } else {
                        that.Btn_Flash_Spin.isChecked = false;
                        that.Spining = false;
                        that.SPIN_FLASH_ONLINE = false;
                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                } else {
                    if (GM.Gold_Lastest >= that.Spin_Bet * that.Bet_Line_Count) {
                        that.Flash_Spin();
                    } else {
                        that.Btn_Flash_Spin.isChecked = false;
                        that.Spining = false;
                        that.SPIN_FLASH_ONLINE = false;
                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                }

            }
        }, time_show_all_icon);
    },

    Spin() {
        this.Spin_Count += 1;
        this.Spining = true;
        this.Node_Use_2_Stop_Anims.stopAllActions();

        if (this.Spin_Count === 1) {
            for (let index = 0; index < 3; index++) {
                this.GP_Column_1.children[index].getComponent("Mini_Slot_2_Item_Game").show_shadow_spriteFrame();
                this.GP_Column_2.children[index].getComponent("Mini_Slot_2_Item_Game").show_shadow_spriteFrame();
                this.GP_Column_3.children[index].getComponent("Mini_Slot_2_Item_Game").show_shadow_spriteFrame();
            }
        } else {
            for (let index = 0; index < 3; index++) {
                this.GP_Column_1.children[index].getComponent("Mini_Slot_2_Item_Game").hide_icon();
                this.GP_Column_2.children[index].getComponent("Mini_Slot_2_Item_Game").hide_icon();
                this.GP_Column_3.children[index].getComponent("Mini_Slot_2_Item_Game").hide_icon();
            }
        }

        var line_bets = [];
        for (let index = 0; index < this.Line_Selected_Matrix_1x20.length; index++) {
            if (this.Line_Selected_Matrix_1x20[index] > 0) {
                line_bets.push(this.Line_Selected_Matrix_1x20[index]);
            }
        }

        switch (GM.Stay) {
            case 0:
                cc.log("Playing on Lobby");
                Mini_Slot_2.scope.changeBalance(-this.Spin_Bet * this.Bet_Line_Count);
                break;
            case 1:
                cc.log("Playing on Slot 1");
                GM.Gold_Lastest -= this.Spin_Bet * this.Bet_Line_Count;
                Slot1.scope.notify_Gold_State_Change();
                break;
            case 2:
                cc.log("Playing on Slot 2");
                GM.Gold_Lastest -= this.Spin_Bet * this.Bet_Line_Count;
                Slot2.scope.notify_Gold_State_Change();
                break;
            case 3:
                cc.log("Playing on Slot 3");
                GM.Gold_Lastest -= this.Spin_Bet * this.Bet_Line_Count;
                Slot3.scope.notify_Gold_State_Change();
                break;
            case 4:
                cc.log("Playing on Slot 4");
                GM.Gold_Lastest -= this.Spin_Bet * this.Bet_Line_Count;
                Slot4.scope.notify_Gold_State_Change();
                break;
            default:
                break;
        }

        this.spinMiniSlot_2(this.Spin_Bet, line_bets);
    },

    Manager_Spin_Anim() {
        var that = this;
        this.btn_Spin_Manual.getComponent("sp.Skeleton").setAnimation(0, 'quay_active', false);
        var time_init_icon = 1000;
        var time_Column_move = 2;
        var time_C1_delay = 0.2;
        var time_C2_delay = 0.4;
        var time_C3_delay = 0.6;

        if (this.SPIN_FLASH_ONLINE) {
            cc.log("SPIN_FLASH_ONLINE");
            time_init_icon = 200;
            time_Column_move = 1;
            time_C1_delay = 0.05;
            time_C2_delay = 0.1;
            time_C3_delay = 0.15;
        } else {
            cc.log("SPIN Other Mode");
        }

        // init Icon result
        setTimeout(function () {
            for (let index = 0; index < 3; index++) {
                var data_col_1 = {
                    id: 3 * index,
                    icon: that.Result_Spin_Server.slots[3 * index]
                }
                that.GP_Column_1.children[index].getComponent("Mini_Slot_2_Item_Game").init_item(data_col_1);

                var data_col_2 = {
                    id: 3 * index + 1,
                    icon: that.Result_Spin_Server.slots[3 * index + 1]
                }
                that.GP_Column_2.children[index].getComponent("Mini_Slot_2_Item_Game").init_item(data_col_2);

                var data_col_3 = {
                    id: 3 * index + 2,
                    icon: that.Result_Spin_Server.slots[3 * index + 2]
                }
                that.GP_Column_3.children[index].getComponent("Mini_Slot_2_Item_Game").init_item(data_col_3);
            }
        }, time_init_icon);

        // Scroll 5 Column
        this.GP_Column_1.runAction(
            cc.sequence(
                cc.delayTime(time_C1_delay),
                cc.moveTo(0, -210, 2900),
                cc.moveTo(time_Column_move, -210, 190).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 1");
                }))
        );
        this.GP_Column_2.runAction(
            cc.sequence(
                cc.delayTime(time_C2_delay),
                cc.moveTo(0, 0, 2900),
                cc.moveTo(time_Column_move, 0, 190).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 2");
                }))
        );
        this.GP_Column_3.runAction(
            cc.sequence(
                cc.delayTime(time_C3_delay),
                cc.moveTo(0, 210, 2900),
                cc.moveTo(time_Column_move, 210, 190).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    that.btn_Spin_Manual.getComponent("sp.Skeleton").setAnimation(0, 'quay_inactive', false);
                    cc.log("End Scroll Column 3");
                }))
        );
    },

    Show_Popup_Line_Bet() {
        if (!this.Spining && !this.SPIN_FLASH_ONLINE) {
            this.Popup_Line_Bet.active = true;
            this.Popup_Line_Bet.position = cc.v2(-Mini_Slot_2.scope.Game_Drag.position.x, -Mini_Slot_2.scope.Game_Drag.position.y);
            var List_Bet = this.List_Line_Bet;
            for (let index = 0; index < 20; index++) {
                const child = List_Bet.children[index];
                if (this.Line_Selected_Matrix_1x20[index] > 0) {
                    cc.log("Show Line Bet : ", index);
                    child.children[0].active = false;
                } else {
                    child.children[0].active = true;
                }
            }
        } else {
            cc.log("isSpinning, therefore you can not Click into this Button");
        }
    },

    Close_Popup_Line_Bet() {
        this.Popup_Line_Bet.active = false;
        this.label_Bet_Line.string = this.Bet_Line_Count;
    },

    // show | hide line bet on List
    Select_One_Line(event, pos) {
        cc.log("Select_One_Line : ", pos);
        var ID = parseInt(pos);
        var state = this.List_Line_Bet.children[ID - 1].children[0].active;
        this.List_Line_Bet.children[ID - 1].children[0].active = !state;
        if (!state) { //  == active
            this.Bet_Line_Count -= 1;
            this.Line_Selected_Matrix_1x20[ID - 1] = -1;
        } else {
            this.Bet_Line_Count += 1;
            this.Line_Selected_Matrix_1x20[ID - 1] = ID;
        }
        cc.log(this.Line_Selected_Matrix_1x20);
    },

    Choose_Line_Chan() {
        this.State_Bet_Line_Chan(false);
        this.State_Bet_Line_Le(true);
        this.Bet_Line_Count = 10;

        for (let index = 0; index < this.Boom_List_Line.children.length; index++) {
            if ((index + 1) % 2 == 0) {
                this.Line_Selected_Matrix_1x20[index] = index + 1;
            } else {
                this.Line_Selected_Matrix_1x20[index] = -1;
            }
        }
    },

    Choose_Line_Le() {
        this.State_Bet_Line_Chan(true);
        this.State_Bet_Line_Le(false);
        this.Bet_Line_Count = 10;

        for (let index = 0; index < this.Boom_List_Line.children.length; index++) {
            const child = this.Boom_List_Line.children[index];
            if ((index + 1) % 2 == 1) {
                this.Line_Selected_Matrix_1x20[index] = index + 1;
            } else {
                this.Line_Selected_Matrix_1x20[index] = -1;
            }
        }
    },

    Choose_All_Line() {
        this.State_Bet_Line_Chan(false);
        this.State_Bet_Line_Le(false);
        this.Bet_Line_Count = 20;

        for (let index = 0; index < this.Boom_List_Line.children.length; index++) {
            this.Line_Selected_Matrix_1x20[index] = index + 1;
        }
    },

    Delete_All_Line() {
        this.State_Bet_Line_Chan(true);
        this.State_Bet_Line_Le(true);
        this.Bet_Line_Count = 0;
        for (let index = 0; index < this.Boom_List_Line.children.length; index++) {
            this.Line_Selected_Matrix_1x20[index] = -1;
        }
    },

    State_Bet_Line_Chan(isVisible) {
        for (let index = 0; index < 10; index++) {
            const line = this.List_Line_Bet.children[2 * index + 1];
            line.children[0].active = isVisible;
        }
    },

    State_Bet_Line_Le(isVisible) {
        for (let index = 0; index < 10; index++) {
            const line = this.List_Line_Bet.children[2 * index];
            line.children[0].active = isVisible;
        }
    },


    Show_Popup_Error_Action(mess) {
        this.Popup_Error_Action.active = true;
        this.Popup_Error_Action.position = cc.v2(-Mini_Slot_2.scope.Game_Drag.position.x, -Mini_Slot_2.scope.Game_Drag.position.y);
        this.label_popup_error_action_mes.string = mess;
    },

    Close_Popup_Error_Action() {
        this.Popup_Error_Action.active = false;
    },

    effect_Update_Gold(gold_start, gold_end, node, label) {
        var that = this;
        var gold_add = gold_end - gold_start;
        label.string = gold_start;

        var steps = 40;
        var delta_gold_add = -1;

        if (gold_add < 40) {
            steps = 10;
        }

        delta_gold_add = Math.floor(gold_add / steps);

        var add = cc.sequence(
            cc.delayTime(0.03),
            cc.callFunc(function () {
                gold_start += delta_gold_add;
                label.string = GM.instance.convert_money(gold_start);
            })
        );

        if (steps === 40) {
            node.runAction(
                cc.sequence(
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    cc.callFunc(function () {
                        gold_start = gold_end;
                        label.string = GM.instance.convert_money(gold_start);
                    })
                )
            );
        } else {
            node.runAction(
                cc.sequence(
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    cc.callFunc(function () {
                        gold_start = gold_end;
                        label.string = GM.instance.convert_money(gold_start);
                    })
                )
            );
        }

    },

    effect_Update_Jackpot(gold_start, gold_end, node, label) {
        var that = this;
        var gold_add = gold_end - gold_start;
        label.string = GM.instance.convert_money(gold_start);

        var steps = 5;
        var delta_gold_add = -1;

        delta_gold_add = Math.floor(gold_add / steps);

        var add = cc.sequence(
            cc.delayTime(0.1),
            cc.callFunc(function () {
                gold_start += delta_gold_add;
                label.string = GM.instance.convert_money(gold_start);
            })
        );

        node.runAction(
            cc.sequence(
                add,
                add,
                add,
                add,
                cc.callFunc(function () {
                    gold_start = gold_end;
                    label.string = GM.instance.convert_money(gold_start);
                })
            )
        );
    },

    choose_Bet(toggle) {
        if (!this.Spining) {
            if (this.SPIN_FLASH_ONLINE) {
                cc.log("Can not choose Bet because Auto or Flash mode is Active");
                this.Show_Popup_Error_Action(" Không thể chuyển mức cược \n khi đang quay ! ");
                Mini_Slot_2.scope.f5_bet();
            } else {
                cc.log("Can choose Bet");
                var index = this.Bet_selector.indexOf(toggle);
                switch (index) {
                    case 0:
                        this.subMiniSlot_2(100);
                        break;
                    case 1:
                        this.subMiniSlot_2(1000);
                        break;
                    case 2:
                        this.subMiniSlot_2(10000);
                        break;
                }
                cc.log("Spin_Bet : ", this.Spin_Bet);
            }
        } else {
            cc.log("Can not choose Bet");
            this.Show_Popup_Error_Action(" Không thể chuyển mức cược \n khi đang quay ! ");
            Mini_Slot_2.scope.f5_bet();
        }
    },



    randomBetween(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    },

    Effect_Spin_Special(type, gold_add) {
        var that = this;
        switch (type) {
            case 1:
                // Jackpot
                this.Effect_No_Hu.active = true;
                this.BG_No_Hu.runAction(
                    cc.sequence(
                        cc.scaleTo(0, 0.8, 0.8),
                        cc.scaleTo(0.1, 1.2, 1.2),
                        cc.scaleTo(0.05, 1, 1)
                    )
                );
                setTimeout(function () {
                    that.PS_No_Hu.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                    that.label_Gold_No_Hu.string = " 0 ";
                    that.effect_Update_Gold(0, gold_add, that.Node_Gold_No_Hu, that.label_Gold_No_Hu);
                }, 100);

                setTimeout(function () {
                    that.Effect_No_Hu.active = false;
                }, 2000); // 3000  2500
                break;
            case 2:
                // Big Win
                this.Effect_Thang_Lon.active = true;
                this.BG_Thang_Lon.runAction(
                    cc.sequence(
                        cc.scaleTo(0, 0.8, 0.8),
                        cc.scaleTo(0.1, 1.2, 1.2),
                        cc.scaleTo(0.05, 1, 1)
                    )
                );
                setTimeout(function () {
                    that.PS_Thang_Lon.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                    that.label_Gold_Thang_Lon.string = " 0 ";
                    that.effect_Update_Gold(0, gold_add, that.Node_Gold_Thang_Lon, that.label_Gold_Thang_Lon);
                }, 100);

                setTimeout(function () {
                    that.Effect_Thang_Lon.active = false;
                }, 2000);
                break;
            default:
                break;
        }
    },


    Test_Anim() {
        // var that = this;
        // this.Show_Popup_Connect_Error("Tham gia phòng thất bại !", "P/s : Vui lòng thử lại sau");
    },

    // Connect Server
    subMiniSlot_2(game_bet) {
        var str = {
            type: WarpConst.MiniGame.SUBSCRIBE_ROOM,
            bet: game_bet,
            chipType: 1
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_SLOT_2;
        WebsocketClient.Socket.send(m);
    },
    spinMiniSlot_2(bet_selected, line_bets) {
        var str = {
            type: WarpConst.MiniGame.START_MATCH,
            bet: bet_selected, //bet value: 100 | 1000 | 10000
            line: line_bets
        }
        cc.log("spinMiniSlot_2 : ",str);
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_SLOT_2;
        WebsocketClient.Socket.send(m);
    },

    getMiniSlot_2_His() {
        var str = {
            type: WarpConst.MiniGame.GET_USER_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_SLOT_2;
        WebsocketClient.Socket.send(m);
    },
    getMiniSlot_2_Glory() {
        var str = {
            type: WarpConst.MiniGame.GET_JACKPOT_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_SLOT_2;
        WebsocketClient.Socket.send(m);
    },

    //SET MONEY
    //minus
    changeBalance(balanceChange) {
        LobbyCtl.instance.addUserGold(balanceChange);
    },
    setBalance(balance) {
        LobbyCtl.instance.setUserGold(balance);
    }
});