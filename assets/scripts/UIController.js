var StringUtil = require('StringUtil');
var WarpConst = require("WarpConst");
var WebsocketClient = require("WebsocketClient");
var GM = require("GM");
var UIController = cc.Class({
    extends: cc.Component,

    properties: {
        taixiuPanel: cc.Node,
        taixiuCountDownLabel: cc.Label,
        taixiuCountDownMiniGameLabel: cc.Label,
        miniGamePanel: cc.Node,
        btnMiniGame: cc.Node,
        bgMiniGame: cc.Node,
        goldRushPanel: cc.Node,
        mini_Slot_1: cc.Node,
        miniPokerPanel: cc.Node,
        mini_Slot_2: cc.Node,

        alertPanel: cc.Node,
        alertLabel: cc.Label,
        splash_loading: cc.Node,
        sprite_Game_BG: {
            default: [],
            type: [cc.SpriteFrame]
        },
        game_Logo: {
            default: null,
            type: cc.Node
        },
        label_Process_Loading: cc.Label
    },

    statics: {
        instance: null
    },

    // count: 0,

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.node.parent = null;
        cc.game.addPersistRootNode(this.node);
        UIController.instance = this;
        this.count = 0;
        this._zIndex = 0; //popup zIndex
        this.taiXiuRemainTime = 0;
    },

    start() {

    },

    onEnable() {
        if (WarpConst.GameBase.userInfo != null) {
            WebsocketClient.OnTaiXiuSubDone = this.onTaiXiuSubDone;
            WebsocketClient.OnTaiXiuMiniRoomChange = this.onStateTaiXiu;
        }
    },

    onDisable() {
        WebsocketClient.OnTaiXiuSubDone = null;
        WebsocketClient.OnTaiXiuMiniRoomChange = null;
    },

    update(dt) {
        this.taiXiuRemainTime -= dt;
        this.taixiuCountDownLabel.string = StringUtil.formatDuration(this.taiXiuRemainTime);
        this.taixiuCountDownMiniGameLabel.string = StringUtil.formatDuration(this.taiXiuRemainTime);
    },

    //TAIXIU
    showTaiXiu(event, active) {
        if (active === 'true') {
            if (!WarpConst.GameBase.gameConfig.data.custom_active_games.tai_xiu) {
                this.showAlert(" Game Tài Xỉu hiện đang bảo trì. \n Vui lòng quay lại sau ! ");
                return;
            }
            //show
            this.openPopup(this.taixiuPanel);
            this.hideMiniGame();
            this.changeZIndex(this.taixiuPanel);
        } else {
            //hide
            this.closePopup(this.taixiuPanel);
        }
    },

    countDown(label, startNum) {
        var intervalId = setInterval(() => {
            label.string = startNum;
            startNum--;
            if (startNum < 50)
                clearInterval(intervalId);
        }, 1000);
    },

    checkLogin() {
        if (WarpConst.GameBase.userInfo == null) {
            this.showAlert("Vui lòng đăng nhập để thực hiện tính năng này");
            return;
        } else {
            this.showMiniGame();
        }
    },

    showMiniGame() {
        this.btnMiniGame.active = false;
        this.bgMiniGame.active = true;
    },

    hideMiniGame() {
        this.btnMiniGame.active = true;
        this.bgMiniGame.active = false;
    },

    showGoldRush() {
        if (!WarpConst.GameBase.gameConfig.data.custom_active_games.dao_vang) {
            this.showAlert(" Game Đào Vàng hiện đang bảo trì. \n Vui lòng quay lại sau ! ");
            return;
        }
        this.openPopup(this.goldRushPanel);
        this.hideMiniGame();
        this.changeZIndex(this.goldRushPanel);
    },

    hideGoldRush() {
        this.closePopup(this.goldRushPanel);
    },

    show_Mini_Slot_1() {
        if (!WarpConst.GameBase.gameConfig.data.custom_active_games.mini_slot) {
            this.showAlert(" Game Dino Egg hiện đang bảo trì. \n Vui lòng quay lại sau ! ");
            return;
        }
        this.openPopup(this.mini_Slot_1);
        this.hideMiniGame();
        this.changeZIndex(this.mini_Slot_1);
    },

    hide_Mini_Slot_1() {
        this.closePopup(this.mini_Slot_1);
    },

    show_Mini_Slot_2() {
        if (!WarpConst.GameBase.gameConfig.data.custom_active_games.mini_slot_2) {
            this.showAlert(" Game Halloween hiện đang bảo trì. \n Vui lòng quay lại sau ! ");
            return;
        }
        this.openPopup(this.mini_Slot_2);
        this.hideMiniGame();
        this.changeZIndex(this.mini_Slot_2);
    },

    hide_Mini_Slot_2() {
        this.closePopup(this.mini_Slot_2);
    },

    showMiniPoker() {
        if (!WarpConst.GameBase.gameConfig.data.custom_active_games.mini_poker) {
            this.showAlert(" Game Mini Poker hiện đang bảo trì. \n Vui lòng quay lại sau ! ");
            return;
        }
        this.openPopup(this.miniPokerPanel);
        this.hideMiniGame();
        this.changeZIndex(this.miniPokerPanel);
    },

    hideMiniPoker() {
        this.closePopup(this.miniPokerPanel);
    },

    openPopup(bg, cb) {
        bg.active = true;
        var scaleUp = cc.scaleTo(0.1, 1.2, 1.2);
        var scaleDown = cc.scaleTo(0.1, 1, 1);
        bg.runAction(cc.sequence(
            scaleUp,
            scaleDown,
            cc.callFunc(function () {
                if (cb) {
                    cb();
                }
            }, this)
        ));
    },

    closePopup(bg, cb) {
        var scaleUp = cc.scaleTo(0.1, 1.2, 1.2);
        var scaleDown = cc.scaleTo(0.1, 1, 1);
        bg.runAction(cc.sequence(
            scaleUp,
            scaleDown,
            cc.callFunc(function () {
                bg.active = false;
                if (cb) {
                    cb();
                }
            }, this)
        ));
    },

    changeZIndex(node) {
        this._zIndex++;
        node.zIndex = this._zIndex;
        this.miniGamePanel.zIndex = node.zIndex + 1;
    },

    onSubTaiXiu() {
        var msg = {
            type: 23,
            bet: 0,
            chipType: 1
        };

        var msgJSON = JSON.stringify(msg);
        var data = WebsocketClient.instance.encodeRequest(msgJSON);
        data[1] = WarpConst.WarpRequestTypeCode.TAI_XIU;
        WebsocketClient.Socket.send(data);
    },

    onTaiXiuSubDone(data) {
        cc.log("UIController SUB DONE", data);
        var self = UIController.instance;
        var room = JSON.parse(data).room;
        self.taiXiuRemainTime = room.timeCountDown;
    },

    onStateTaiXiu(data) {
        cc.log("UIController STATE", data);
        var self = UIController.instance;
        var dataJSON = JSON.parse(data);
        var type = dataJSON.type;
        if (type != 7) { //refund
            var room = dataJSON.room;
            if (!room) {
                return;
            }
            self.taiXiuRemainTime = room.timeCountDown;
        }
    },
    subMsg() {
        cc.log("UIController SUB");
        UIController.instance.onSubTaiXiu();
        WebsocketClient.OnTaiXiuSubDone = UIController.instance.onTaiXiuSubDone;
        WebsocketClient.OnTaiXiuMiniRoomChange = UIController.instance.onStateTaiXiu;
    },
    updateRemainTime(remainTime) {
        this.taiXiuRemainTime = remainTime;
        this.taixiuCountDownLabel.string = StringUtil.formatDuration(this.taiXiuRemainTime);
        this.taixiuCountDownMiniGameLabel.string = StringUtil.formatDuration(this.taiXiuRemainTime);
    },

    showAlert(text) {
        this.openPopup(this.alertPanel);
        this.changeZIndex(this.alertPanel);
        if (text != null) {
            this.alertLabel.string = text;
        } else {
            this.alertLabel.string = "";
        }
    },
    hideAlert() {
        this.closePopup(this.alertPanel);
    },
    closeAllMiniGame() {
        this.showTaiXiu(null, "false");
        this.hideGoldRush();
        this.hide_Mini_Slot_1();
    },

    show_Splash_Slot(name) {
        this.miniGamePanel.active = false;
        this.splash_loading.active = true;
        this.game_Logo.active = false;
        this.changeZIndex(this.splash_loading, false);
        var splash_ID = 0;
        if (name === "Slot1") {
            splash_ID = 0;
        } else if (name === "Slot2") {
            this.game_Logo.active = true;
            splash_ID = 1;
        } else if (name === "Slot3") {
            splash_ID = 2;
        } else if (name === "Slot4") {
            splash_ID = 3
        }
        this.splash_loading.getComponent(cc.Sprite).spriteFrame = this.sprite_Game_BG[splash_ID];

        var is4_3 = 7;
        if (GM.Screen === is4_3) {
            this.splash_loading.height = this.splash_loading.width * (4 / 3);
        }
        this.label_Process_Loading.string = " ĐANG TẢI : 0 % ";
    },

    update_load_Scene(percentage) {
        this.label_Process_Loading.string = "ĐANG TẢI : " + (100 * percentage).toFixed(0) + " %";
    },

    hide_Splash_Slot() {
        this.splash_loading.active = false;
        this.miniGamePanel.active = true;
    }

});