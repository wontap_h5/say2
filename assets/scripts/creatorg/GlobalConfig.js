// List type build

// Without game fake, checking : real server
window.BUILD_FACEBOOK = 0;

// Without game fake, checking | dual server : real & submit
window.BUILD_APPLE_STORE = 1;

// Include game fake, checking, real server
window.BUILD_APPLE_STORE_AND_GAME_FAKE = 2;

// Without game fake, checking | real server
window.BUILD_IPA_INSTALL = 3;

// Without game fake, checking | dual server : real & submit
window.BUILD_GOOGLE_PLAY = 4;

// Include game fake, checking, real server
window.BUILD_GOOGLE_PLAY_AND_GAME_FAKE = 5;

// Without game fake, checking | real server
window.BUILD_APK_INSTALL = 6;

// Without game fake, checking | real server
window.BUILD_WEB_MOBILE = 7;

// Setup Build Type
window.BUILD_TYPE = BUILD_APK_INSTALL;