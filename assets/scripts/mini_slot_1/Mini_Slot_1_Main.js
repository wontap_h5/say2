var WarpConst = require("WarpConst");
var WebsocketClient = require("WebsocketClient");

var LobbyCtl = require("LobbyCtl");
var Slot1 = require("Pirate_Main");
var Slot2 = require("Slot_2_Main");
var Slot3 = require("Slot_3_Main");
var Slot4 = require("Slot_4_Main");
var GM = require("GM");
var Mini_Slot_1 = cc.Class({
    extends: cc.Component,

    statics: {
        scope: null
    },

    properties: {
        Light_Logo: {
            default: null,
            type: cc.Node
        },
        Mask_Logo: {
            default: null,
            type: cc.Node
        },
        Bet_selector: {
            default: [],
            type: cc.Toggle
        },
        GP_Column_1: {
            default: null,
            type: cc.Node
        },
        GP_Column_2: {
            default: null,
            type: cc.Node
        },
        GP_Column_3: {
            default: null,
            type: cc.Node
        },
        GP_Column_4: {
            default: null,
            type: cc.Node
        },
        GP_Column_5: {
            default: null,
            type: cc.Node
        },
        add_Gold_Effect: {
            default: null,
            type: cc.Node
        },
        PS_Add_Gold: {
            default: null,
            type: cc.Node
        },
        gold_Add_Node: {
            default: null,
            type: cc.Node
        },
        label_Gold_Add: {
            default: null,
            type: cc.Label
        },
        node_Jackpot: {
            default: null,
            type: cc.Node
        },
        label_Jackpot: {
            default: null,
            type: cc.Label
        },
        Popup_Rank: {
            default: null,
            type: cc.Node
        },
        Popup_Guide: {
            default: null,
            type: cc.Node
        },
        Popup_History: {
            default: null,
            type: cc.Node
        },
        Rank_Content: {
            default: null,
            type: cc.Node
        },
        Item_Rank_Prefab: {
            default: null,
            type: cc.Prefab
        },
        History_Content: {
            default: null,
            type: cc.Node
        },
        Item_History_Prefab: {
            default: null,
            type: cc.Prefab
        },
        Btn_Auto_Spin: {
            default: null,
            type: cc.Toggle
        },
        Btn_Flash_Spin: {
            default: null,
            type: cc.Toggle
        },
        BG_No_Hu: {
            default: null,
            type: cc.Node
        },
        Effect_No_Hu: {
            default: null,
            type: cc.Node
        },
        PS_No_Hu: {
            default: null,
            type: cc.Node
        },
        label_Gold_No_Hu: {
            default: null,
            type: cc.Label
        },
        Node_Gold_No_Hu: {
            default: null,
            type: cc.Node
        },
        BG_Thang_Lon: {
            default: null,
            type: cc.Node
        },
        Effect_Thang_Lon: {
            default: null,
            type: cc.Node
        },
        PS_Thang_Lon: {
            default: null,
            type: cc.Node
        },
        label_Gold_Thang_Lon: {
            default: null,
            type: cc.Label
        },
        Node_Gold_Thang_Lon: {
            default: null,
            type: cc.Node
        },
        Node_Use_2_Stop_Anims: {
            default: null,
            type: cc.Node
        },
        Star_Logo: {
            default: null,
            type: cc.Node
        },
        Popup_Error_Action: {
            default: null,
            type: cc.Node
        },
        label_popup_error_action_mes: {
            default: null,
            type: cc.Label
        },
        Game_Drag: {
            default: null,
            type: cc.Node
        }
    },

    Spin_Count: null,
    Spining: null,
    Result_Spin_Server: null,
    Table_Matrix_3x3: null,
    SPIN_AUTO_ONLINE: null,
    SPIN_FLASH_ONLINE: null,

    check_OnMiniSlotSubDone: null,
    check_OnMiniSlotStartDone: null,
    check_OnMiniSlotGetUserHistoryDone: null,
    check_OnMiniSlotGetGloryDone: null,

    Server_Data_Room: null,
    Spin_Bet: null,
    Last_Jackpot: null,

    // LIFE-CYCLE CALLBACKS:

    OnMiniSlotSubDone(status, res) {
        cc.log("OnMiniSlotSubDone : ", res);
        var data = JSON.parse(res);

        if (status == WarpConst.WarpResponseResultCode.SUCCESS) {
            if ("undefined" !== typeof data.type) {
                if (data.type === 23) {
                    Mini_Slot_1.scope.check_OnMiniSlotSubDone = true;
                    Mini_Slot_1.scope.effect_Update_Gold(0, data.jackpot, Mini_Slot_1.scope.node_Jackpot, Mini_Slot_1.scope.label_Jackpot);
                    Mini_Slot_1.scope.Last_Jackpot = data.jackpot;
                    Mini_Slot_1.scope.Spin_Bet = data.bet;
                } else {
                    Mini_Slot_1.scope.check_OnMiniSlotSubDone = false;
                    Mini_Slot_1.scope.Show_Popup_Error_Action(" Kết nối thất bại \n Vui lòng thử lại sau ! ");
                    Mini_Slot_1.scope.f5_bet();
                }
            } else {
                Mini_Slot_1.scope.check_OnMiniSlotSubDone = false;
                Mini_Slot_1.scope.Show_Popup_Error_Action(" Kết nối thất bại \n Vui lòng thử lại sau ! ");
                Mini_Slot_1.scope.f5_bet();
            }
            cc.log("OnMiniSlotSubDone : ", Mini_Slot_1.scope.OnMiniSlotSubDone);
        } else {
            Mini_Slot_1.scope.check_OnMiniSlotSubDone = false;
            Mini_Slot_1.scope.Show_Popup_Error_Action(" Đổi mức cược không thành công. \n Vui lòng thử lại sau. ");
            Mini_Slot_1.scope.f5_bet();
        }
    },

    f5_bet() {
        switch (Mini_Slot_1.scope.Spin_Bet) {
            case 100:
                Mini_Slot_1.scope.Bet_selector[0].isChecked = true;
                break;
            case 1000:
                Mini_Slot_1.scope.Bet_selector[1].isChecked = true;
                break;
            case 10000:
                Mini_Slot_1.scope.Bet_selector[2].isChecked = true;
                break;
        }
    },

    OnMiniSlotStartDone(res) {
        var raw_data = JSON.parse(res);
        if ("undefined" !== typeof raw_data.result) {
            var data = JSON.parse(raw_data.result);
            cc.log("OnMiniSlotStartDone : ", JSON.stringify(data));
            Mini_Slot_1.scope.Result_Spin_Server = data;

            Mini_Slot_1.scope.Manager_Spin_Anim();

            var time_update_money = 3500;
            if (Mini_Slot_1.scope.SPIN_AUTO_ONLINE) {
                cc.log("SPIN_AUTO_ONLINE");
                Mini_Slot_1.scope.Machine_Spin_Auto();
            } else if (Mini_Slot_1.scope.SPIN_FLASH_ONLINE) {
                cc.log("Machine_Spin_Auto");
                Mini_Slot_1.scope.Machine_Spin_Flash();
                time_update_money = 1500;
            } else {
                cc.log("Machine_Spin_Manual");
                Mini_Slot_1.scope.Machine_Spin_Manual();
            }

            setTimeout(function () {
                switch (GM.Stay) {
                    case 0:
                        cc.log("Playing on Lobby");
                        Mini_Slot_1.scope.setBalance(data.gold);
                        break;
                    case 1:
                        cc.log("Playing on Slot 1");
                        GM.Gold_Lastest = data.gold;
                        Slot1.scope.notify_Gold_State_Change();
                        break;
                    case 2:
                        cc.log("Playing on Slot 2");
                        GM.Gold_Lastest = data.gold;
                        Slot2.scope.notify_Gold_State_Change();
                        break;
                    case 3:
                        cc.log("Playing on Slot 3");
                        GM.Gold_Lastest = data.gold;
                        Slot3.scope.notify_Gold_State_Change();
                        break;
                    case 4:
                        cc.log("Playing on Slot 4");
                        GM.Gold_Lastest = data.gold;
                        Slot4.scope.notify_Gold_State_Change();
                        break;
                    default:
                        break;
                }
            }, time_update_money);
        }
    },

    OnMiniSlotGetUserHistoryDone(res) {
        cc.log("OnMiniSlotGetUserHistoryDone : ", res);
        var data = JSON.parse(res);
        Mini_Slot_1.scope.Popup_History.active = true;
        Mini_Slot_1.scope.Popup_History.position = cc.v2(-Mini_Slot_1.scope.Game_Drag.position.x, -Mini_Slot_1.scope.Game_Drag.position.y);
        Mini_Slot_1.scope.History_Content.removeAllChildren(true);
        var Data_History = data.history;
        for (var i = 0; i < Data_History.length; i++) {
            var item = cc.instantiate(Mini_Slot_1.scope.Item_History_Prefab);
            var data = Data_History[i];
            item.getComponent('Mini_Slot_1_Item_History').init_Item_History({
                bg_index: i % 2,
                sess: "#" + data.id,
                time: data.time,
                bet: data.bet,
                gold: data.gold
            });
            Mini_Slot_1.scope.History_Content.addChild(item);
        }
    },

    OnMiniSlotGetGloryDone(res) {
        cc.log("OnMiniSlotGetGloryDone : ", res);
        var data = JSON.parse(res);
        Mini_Slot_1.scope.Popup_Rank.active = true;
        Mini_Slot_1.scope.Popup_Rank.position = cc.v2(-Mini_Slot_1.scope.Game_Drag.position.x, -Mini_Slot_1.scope.Game_Drag.position.y);

        Mini_Slot_1.scope.Rank_Content.removeAllChildren(true);
        var Data_Rank = data.history;
        for (var i = 0; i < Data_Rank.length; i++) {
            var item = cc.instantiate(Mini_Slot_1.scope.Item_Rank_Prefab);
            var data = Data_Rank[i];
            var name = decodeURIComponent(escape(data.displayName));
            if (name.length > 15) {
                name = name.substring(0, 11) + "...";
            }
            item.getComponent('Mini_Slot_1_Item_Rank').init_Item_Rank({
                bg_index: i % 2,
                sess: "#" + data.id,
                time: data.time,
                account: name,
                bet: data.bet,
                gold: data.win
            });
            Mini_Slot_1.scope.Rank_Content.addChild(item);
        }
    },

    onLoad() {
        Mini_Slot_1.scope = this;
        this.Server_Data_Room = {};
        this.Spin_Bet = 100; // Default Mode 100 is selected
        this.Last_Jackpot = 0;
        this.Spining = false;
        this.Spin_Count = 0;
        this.Table_Matrix_3x3 = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9],
            [1, 8, 3],
            [7, 2, 9],
            [1, 5, 3],
            [1, 5, 9],
            [7, 5, 3],
            [4, 8, 6],
            [4, 2, 6],
            [7, 5, 9],
            [1, 2, 6],
            [4, 5, 9],
            [4, 5, 3],
            [7, 8, 6],
            [4, 2, 3],
            [7, 5, 6],
            [1, 5, 6],
            [4, 8, 9],
            [1, 8, 6]
        ];

        this.SPIN_AUTO_ONLINE = false;
        this.SPIN_FLASH_ONLINE = false;

        // WebsocketClient.OnMiniSlotSubDone = this.OnMiniSlotSubDone;
        // WebsocketClient.OnMiniSlotStartDone = this.OnMiniSlotStartDone;
        // WebsocketClient.OnMiniSlotGetUserHistoryDone = this.OnMiniSlotGetUserHistoryDone;
        // WebsocketClient.OnMiniSlotGetGloryDone = this.OnMiniSlotGetGloryDone;

        this.check_OnMiniSlotSubDone = -1;
        this.check_OnMiniSlotStartDone = -1;
        this.check_OnMiniSlotGetUserHistoryDone = -1;
        this.check_OnMiniSlotGetGloryDone = -1;
    },

    OnJackpotChange(res) {
        cc.log("OnJackpotChange :  ", res);
        var data = JSON.parse(res);
        var old_jackpot = Mini_Slot_1.scope.Last_Jackpot;
        switch (Mini_Slot_1.scope.Spin_Bet) {
            case 100:
                if (data.roomId === 4) {
                    Mini_Slot_1.scope.effect_Update_Jackpot(old_jackpot, data.jackpot, Mini_Slot_1.scope.node_Jackpot, Mini_Slot_1.scope.label_Jackpot);
                    Mini_Slot_1.scope.Last_Jackpot = data.jackpot;
                }
                break;
            case 1000:
                if (data.roomId === 5) {
                    Mini_Slot_1.scope.effect_Update_Jackpot(old_jackpot, data.jackpot, Mini_Slot_1.scope.node_Jackpot, Mini_Slot_1.scope.label_Jackpot);
                    Mini_Slot_1.scope.Last_Jackpot = data.jackpot;
                }
                break;
            case 10000:
                if (data.roomId === 6) {
                    Mini_Slot_1.scope.effect_Update_Jackpot(old_jackpot, data.jackpot, Mini_Slot_1.scope.node_Jackpot, Mini_Slot_1.scope.label_Jackpot);
                    Mini_Slot_1.scope.Last_Jackpot = data.jackpot;
                }
                break;
        }
    },


    onEnable() {
        cc.log("onEnable");
        this.SPIN_AUTO_ONLINE = false;
        this.SPIN_FLASH_ONLINE = false;
        this.Spining = false;
        this.Spin_Bet = 100;
        this.Bet_selector[0].isChecked = true;

        WebsocketClient.OnMiniSlotSubDone = this.OnMiniSlotSubDone;
        WebsocketClient.OnMiniSlotStartDone = this.OnMiniSlotStartDone;
        WebsocketClient.OnMiniSlotGetUserHistoryDone = this.OnMiniSlotGetUserHistoryDone;
        WebsocketClient.OnMiniSlotGetGloryDone = this.OnMiniSlotGetGloryDone;
        WebsocketClient.OnJackpotChange = this.OnJackpotChange;

        this.subMiniSlot(100);

    },

    onDisable() {
        cc.log("onDisable");
        // WebsocketClient.OnMiniSlotSubDone = null;
        // WebsocketClient.OnMiniSlotStartDone = null;
        // WebsocketClient.OnMiniSlotGetUserHistoryDone = null;
        // WebsocketClient.OnMiniSlotGetGloryDone = null;

    },

    start() {
        this.create_Star();
        this.Light_Anim(this.Light_Logo, this.Mask_Logo, 0.04, 2);

    },

    create_Star() {
        for (let index = 0; index < this.Star_Logo.childrenCount; index++) {
            this.Star_Logo.children[index].scale = 0;
            this.Star_Logo.children[index].getComponent("Mini_Slot_1_Star").init_Star(1000 * index);
        }
    },

    update(dt) {},

    Back_2_G_Main() {
        if (!this.Spining && !this.SPIN_AUTO_ONLINE && !this.SPIN_FLASH_ONLINE) {
            this.Node_Use_2_Stop_Anims.stopAllActions();
            cc.find("UIController").getComponent("UIController").hide_Mini_Slot_1();
        } else {
            cc.log("Can not close");
            this.Show_Popup_Error_Action(" Không thể thoát trò chơi ! \n khi đang quay ");
        }
    },

    Light_Anim(node_light, node_mask, time_step_move, repeat_at) {
        var pos_start = node_light.position; // v2(x,y)
        var pos_x_end = node_mask.position.x + (node_mask.width / 2); // x
        node_light.runAction(
            cc.repeatForever(
                cc.sequence(
                    cc.moveBy(time_step_move, 20, -10),
                    cc.callFunc(function () {
                        if (node_light.position.x > pos_x_end * (1 + repeat_at)) {
                            node_light.position = pos_start;
                        }
                    })
                ))
        );
    },

    Show_Rank() {
        this.getMiniSlotGlory();
    },
    Show_Guide() {
        this.Popup_Guide.active = true;
        this.Popup_Guide.position = cc.v2(-Mini_Slot_1.scope.Game_Drag.position.x, -Mini_Slot_1.scope.Game_Drag.position.y);
    },
    Show_History() {
        this.getMiniSlotHis();
    },

    Close_Rank() {
        this.Popup_Rank.active = false;
    },

    Close_Guide() {
        this.Popup_Guide.active = false;
    },

    Close_History() {
        this.Popup_History.active = false;
    },

    Game_Spin_Manual() {
        if (!this.Spining && !this.SPIN_AUTO_ONLINE && !this.SPIN_FLASH_ONLINE) {
            // init Data and Spin
            cc.log("LobbyCtl.userInfo : ", WarpConst.GameBase.userInfo.desc.gold);
            cc.log("Spin_Bete_Bet : ", this.Spin_Bet);
            if (GM.Stay === 0) {
                if (WarpConst.GameBase.userInfo.desc.gold >= this.Spin_Bet) {
                    this.Spin();
                } else {
                    this.Show_Popup_Error_Action(" Không đủ tiền ! ");
                }
            } else {
                if (GM.Gold_Lastest >= this.Spin_Bet) {
                    this.Spin();
                } else {
                    this.Show_Popup_Error_Action(" Không đủ tiền ! ");
                }
            }

        }
    },

    Machine_Spin_Manual() {
        var that = this;
        var list_line_win = [];
        if ("undefined" !== typeof this.Result_Spin_Server.win_icon) {
            for (let index = 0; index < this.Result_Spin_Server.win_icon.length; index++) {
                const line = this.Result_Spin_Server.win_icon[index];
                if (line.icon.charAt(7) !== "6") {
                    cc.log("An khac Icon 6");
                    list_line_win.push(line);
                }
            }
        }
        cc.log("list_line_win : ", list_line_win);

        setTimeout(function () {
            // hide all Icon
            for (let x = 0; x < 3; x++) {
                that.GP_Column_1.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                that.GP_Column_2.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                that.GP_Column_3.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                that.GP_Column_4.children[x].getComponent("Mini_Slot_1_Item_Game_Small").hide_icon();
                that.GP_Column_5.children[x].getComponent("Mini_Slot_1_Item_Game_Small").hide_icon();
            }


            // show all line if win
            if (list_line_win.length > 0) {
                var time_show_each_line = 2000;
                var spin_spec = 2000;
                if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                    cc.log("Quay Trung Thuong");
                    spin_spec = 3000;
                    time_show_each_line = 5000;
                    // 1 : JACKPOT    -    2 : BIGWIN
                    switch (that.Result_Spin_Server.spec_prize) {
                        case "JACKPOT":
                            that.Effect_Spin_Special(1, that.Result_Spin_Server.win_chips);
                            break;
                        case "BIGWIN":
                            that.Effect_Spin_Special(2, that.Result_Spin_Server.win_chips);
                            break;
                        default:
                            break;
                    }
                } else {
                    cc.log("Quay Thuong");
                    if (that.Result_Spin_Server.win_chips > 0) {
                        that.add_Gold_Effect.active = true;
                        that.PS_Add_Gold.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                        that.effect_Update_Gold(0, that.Result_Spin_Server.win_chips, that.label_Gold_Add.node, that.label_Gold_Add);
                        setTimeout(function () {
                            that.add_Gold_Effect.active = false;
                        }, 2000);
                        time_show_each_line = 4000;
                    } else {
                        time_show_each_line = 2500;
                    }
                }

                // show All Icons
                that.Manual_spin_spec(list_line_win, spin_spec);
                // effect show result after Spin end

            } else {
                setTimeout(function () {
                    for (let x = 0; x < 3; x++) {
                        that.GP_Column_1.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                        that.GP_Column_2.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                        that.GP_Column_3.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                        that.GP_Column_4.children[x].getComponent("Mini_Slot_1_Item_Game_Small").show_icon();
                        that.GP_Column_5.children[x].getComponent("Mini_Slot_1_Item_Game_Small").show_icon();
                    }
                    that.Spining = false;
                    cc.log("Can Spins");
                    if (that.SPIN_FLASH_ONLINE) {
                        if (GM.Stay === 0) {
                            if (WarpConst.GameBase.userInfo.desc.gold >= that.Spin_Bet) {
                                that.Flash_Spin();
                            } else {
                                that.Btn_Flash_Spin.isChecked = false;
                                that.Spining = false;
                                that.SPIN_FLASH_ONLINE = false;
                                that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                            }
                        } else {
                            if (GM.Gold_Lastest >= that.Spin_Bet) {
                                that.Flash_Spin();
                            } else {
                                that.Btn_Flash_Spin.isChecked = false;
                                that.Spining = false;
                                that.SPIN_FLASH_ONLINE = false;
                                that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                            }
                        }

                    } else if (that.SPIN_AUTO_ONLINE) {
                        if (GM.Stay === 0) {
                            if (WarpConst.GameBase.userInfo.desc.gold >= that.Spin_Bet) {
                                that.Auto_Spin();
                            } else {
                                that.Btn_Auto_Spin.isChecked = false;
                                that.Spining = false;
                                that.SPIN_AUTO_ONLINE = false;
                                that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                            }
                        } else {
                            if (GM.Gold_Lastest >= that.Spin_Bet) {
                                that.Auto_Spin();
                            } else {
                                that.Btn_Auto_Spin.isChecked = false;
                                that.Spining = false;
                                that.SPIN_AUTO_ONLINE = false;
                                that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                            }
                        }
                    }
                }, 1500);
            }
        }, 3000);
    },

    Manual_spin_spec(list_line_win, spin_spec) {
        var that = this;
        setTimeout(function () {
            for (let index = 0; index < list_line_win.length; index++) {
                var line_id = list_line_win[index].line_num;
                var icon_open_id = parseInt(list_line_win[index].icon.charAt(7));
                for (let x = 0; x < 3; x++) {
                    var location = that.Table_Matrix_3x3[line_id - 1][x];
                    var col_id = location % 3;
                    var child_id = Math.floor(location / 3);
                    if (col_id === 0) {
                        col_id = 3;
                        child_id -= 1;
                    }
                    switch (col_id) {
                        case 1:
                            that.GP_Column_1.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                            break;
                        case 2:
                            that.GP_Column_2.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                            break;
                        case 3:
                            that.GP_Column_3.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                            break;
                        default:
                            break;
                    }
                }
            }
        }, spin_spec);

        setTimeout(function () {
            that.Spining = false;
            cc.log("Can Spins");
            if (that.SPIN_FLASH_ONLINE) {
                if (GM.Stay === 0) {
                    if (WarpConst.GameBase.userInfo.desc.gold >= that.Spin_Bet) {
                        that.Flash_Spin();
                    } else {
                        that.Btn_Flash_Spin.isChecked = false;
                        that.Spining = false;
                        that.SPIN_FLASH_ONLINE = false;
                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                } else {
                    if (GM.Gold_Lastest >= that.Spin_Bet) {
                        that.Flash_Spin();
                    } else {
                        that.Btn_Flash_Spin.isChecked = false;
                        that.Spining = false;
                        that.SPIN_FLASH_ONLINE = false;
                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                }
            } else if (that.SPIN_AUTO_ONLINE) {
                if (GM.Stay === 0) {
                    if (WarpConst.GameBase.userInfo.desc.gold >= that.Spin_Bet) {
                        that.Auto_Spin();
                    } else {
                        that.Btn_Auto_Spin.isChecked = false;
                        that.Spining = false;
                        that.SPIN_AUTO_ONLINE = false;
                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                } else {
                    if (GM.Gold_Lastest >= that.Spin_Bet) {
                        that.Auto_Spin();
                    } else {
                        that.Btn_Auto_Spin.isChecked = false;
                        that.Spining = false;
                        that.SPIN_AUTO_ONLINE = false;
                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                }

            } else {
                that.Manual_time_show_each_line(list_line_win, 1); //   # 10000
            }
        }, spin_spec + 1000);
    },

    Manual_time_show_each_line(list_line_win, time_show_each_line) {
        var that = this;
        this.Node_Use_2_Stop_Anims.stopAllActions();
        this.Node_Use_2_Stop_Anims.runAction(
            cc.sequence(
                cc.delayTime(time_show_each_line), // 1
                cc.callFunc(function () {
                    for (let x = 0; x < 3; x++) {
                        that.GP_Column_1.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                        that.GP_Column_2.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                        that.GP_Column_3.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                        that.GP_Column_4.children[x].getComponent("Mini_Slot_1_Item_Game_Small").hide_icon();
                        that.GP_Column_5.children[x].getComponent("Mini_Slot_1_Item_Game_Small").hide_icon();
                    }
                    // show each line eat and hide it if win
                    for (let index = 0; index < list_line_win.length; index++) {
                        that.Node_Use_2_Stop_Anims.runAction(
                            cc.sequence(
                                cc.delayTime(1.5 * index),
                                cc.callFunc(function () {
                                    cc.log("Vao");
                                    if (!that.Spining) {
                                        var line_id = list_line_win[index].line_num;
                                        var icon_open_id = parseInt(list_line_win[index].icon.charAt(7));
                                        for (let x = 0; x < 3; x++) {
                                            var location = that.Table_Matrix_3x3[line_id - 1][x];
                                            var col_id = location % 3; // 5
                                            var child_id = Math.floor(location / 3);
                                            if (col_id === 0) {
                                                col_id = 3;
                                                child_id -= 1;
                                            }
                                            switch (col_id) {
                                                case 1:
                                                    that.GP_Column_1.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                                                    break;
                                                case 2:
                                                    that.GP_Column_2.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                                                    break;
                                                case 3:
                                                    that.GP_Column_3.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                    }
                                })
                            )
                        );
                    }

                    // show 15 icon after show all line eat
                    that.Node_Use_2_Stop_Anims.runAction(
                        cc.sequence(
                            cc.delayTime(1.5 * (list_line_win.length)), // co the chi la :  line_show_anim_count
                            cc.callFunc(function () {
                                for (let x = 0; x < 3; x++) {
                                    that.GP_Column_1.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                                    that.GP_Column_2.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                                    that.GP_Column_3.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                                    that.GP_Column_4.children[x].getComponent("Mini_Slot_1_Item_Game_Small").show_icon();
                                    that.GP_Column_5.children[x].getComponent("Mini_Slot_1_Item_Game_Small").show_icon();
                                }
                            })
                        )
                    );
                })
            )
        );
    },

    Game_Auto_Spin(toggle) {
        if (this.SPIN_FLASH_ONLINE) {
            this.Btn_Auto_Spin.isChecked = false;
            this.Show_Popup_Error_Action(" Không thể chọn chế độ Tự quay \n khi đang ở chế độ quay Siêu tốc ");
            return;
        }
        cc.log("Van qua :))");

        if (toggle.isChecked) {
            this.SPIN_AUTO_ONLINE = true;
        } else {
            this.SPIN_AUTO_ONLINE = false;
        }

        if (!this.Spining) {
            cc.log("Pre Spin");
            this.Spining = true;
            if (this.SPIN_AUTO_ONLINE) {
                cc.log("LobbyCtl.userInfo : ", WarpConst.GameBase.userInfo.desc.gold);
                cc.log("Spin_Bete_Bet : ", this.Spin_Bet);

                if (GM.Stay === 0) {
                    if (WarpConst.GameBase.userInfo.desc.gold >= this.Spin_Bet) {
                        this.Auto_Spin();
                    } else {
                        this.Btn_Auto_Spin.isChecked = false;
                        this.Spining = false;
                        this.SPIN_AUTO_ONLINE = false;
                        this.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                } else {
                    if (GM.Gold_Lastest >= this.Spin_Bet) {
                        this.Auto_Spin();
                    } else {
                        this.Btn_Auto_Spin.isChecked = false;
                        this.Spining = false;
                        this.SPIN_AUTO_ONLINE = false;
                        this.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                }
            } else {
                this.Spining = false; // uncheck , thay code ben duoi
                // this.Game_Spin_Manual();
            }
        } else {
            cc.log("Spinning");
        }
    },

    Auto_Spin() {
        // init Data and Spin
        this.Spin();
    },

    Machine_Spin_Auto() {
        var that = this;
        var list_line_win = [];
        if ("undefined" !== typeof this.Result_Spin_Server.win_icon) {
            for (let index = 0; index < this.Result_Spin_Server.win_icon.length; index++) {
                const line = this.Result_Spin_Server.win_icon[index];
                if (line.icon.charAt(7) !== "6") {
                    cc.log("An khac Icon 6");
                    list_line_win.push(line);
                }
            }
        }
        cc.log("list_line_win : ", list_line_win);

        setTimeout(function () {
            // hide all Icon
            for (let x = 0; x < 3; x++) {
                that.GP_Column_1.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                that.GP_Column_2.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                that.GP_Column_3.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                that.GP_Column_4.children[x].getComponent("Mini_Slot_1_Item_Game_Small").hide_icon();
                that.GP_Column_5.children[x].getComponent("Mini_Slot_1_Item_Game_Small").hide_icon();
            }

            var time_show_each_line = 2000;
            // show all line if win
            if (list_line_win.length > 0) {
                var spin_spec = 2000;
                if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                    cc.log("Quay Trung Thuong");
                    spin_spec = 3000;
                    time_show_each_line = 5000;
                    // 1 : JACKPOT    -    2 : BIGWIN
                    switch (that.Result_Spin_Server.spec_prize) {
                        case "JACKPOT":
                            that.Effect_Spin_Special(1, that.Result_Spin_Server.win_chips);
                            break;
                        case "BIGWIN":
                            that.Effect_Spin_Special(2, that.Result_Spin_Server.win_chips);
                            break;
                        default:
                            break;
                    }
                } else {
                    cc.log("Quay Thuong");
                    if (that.Result_Spin_Server.win_chips > 0) {
                        that.add_Gold_Effect.active = true;
                        that.PS_Add_Gold.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                        that.effect_Update_Gold(0, that.Result_Spin_Server.win_chips, that.label_Gold_Add.node, that.label_Gold_Add);
                        setTimeout(function () {
                            that.add_Gold_Effect.active = false;
                        }, 2000);
                        time_show_each_line = 4000;
                    } else {
                        time_show_each_line = 2500;
                    }
                }

                // show All Icons
                that.Auto_spin_spec(list_line_win, spin_spec);
                // effect show result after Spin end
                that.Auto_time_show_each_line(list_line_win, time_show_each_line);
            } else {
                setTimeout(function () {
                    for (let x = 0; x < 3; x++) {
                        that.GP_Column_1.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                        that.GP_Column_2.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                        that.GP_Column_3.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                        that.GP_Column_4.children[x].getComponent("Mini_Slot_1_Item_Game_Small").show_icon();
                        that.GP_Column_5.children[x].getComponent("Mini_Slot_1_Item_Game_Small").show_icon();
                    }
                    that.Spining = false;
                    cc.log("Can Spins");
                    if (that.SPIN_FLASH_ONLINE) {
                        if (GM.Stay === 0) {
                            if (WarpConst.GameBase.userInfo.desc.gold >= that.Spin_Bet) {
                                that.Flash_Spin();
                            } else {
                                that.Btn_Flash_Spin.isChecked = false;
                                that.Spining = false;
                                that.SPIN_FLASH_ONLINE = false;
                                that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                            }
                        } else {
                            if (GM.Gold_Lastest >= that.Spin_Bet) {
                                that.Flash_Spin();
                            } else {
                                that.Btn_Flash_Spin.isChecked = false;
                                that.Spining = false;
                                that.SPIN_FLASH_ONLINE = false;
                                that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                            }
                        }

                    } else if (that.SPIN_AUTO_ONLINE) {
                        if (GM.Stay === 0) {
                            if (WarpConst.GameBase.userInfo.desc.gold >= that.Spin_Bet) {
                                that.Auto_Spin();
                            } else {
                                that.Btn_Auto_Spin.isChecked = false;
                                that.Spining = false;
                                that.SPIN_AUTO_ONLINE = false;
                                that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                            }
                        } else {
                            if (GM.Gold_Lastest >= that.Spin_Bet) {
                                that.Auto_Spin();
                            } else {
                                that.Btn_Auto_Spin.isChecked = false;
                                that.Spining = false;
                                that.SPIN_AUTO_ONLINE = false;
                                that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                            }
                        }

                    }
                }, 1500);
            }
        }, 3000);
    },

    Auto_spin_spec(list_line_win, spin_spec) {
        var that = this;
        setTimeout(function () {
            for (let index = 0; index < list_line_win.length; index++) {
                var line_id = list_line_win[index].line_num;
                var icon_open_id = parseInt(list_line_win[index].icon.charAt(7));
                for (let x = 0; x < 3; x++) {
                    var location = that.Table_Matrix_3x3[line_id - 1][x];
                    var col_id = location % 3;
                    var child_id = Math.floor(location / 3);
                    if (col_id === 0) {
                        col_id = 3;
                        child_id -= 1;
                    }
                    switch (col_id) {
                        case 1:
                            that.GP_Column_1.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                            break;
                        case 2:
                            that.GP_Column_2.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                            break;
                        case 3:
                            that.GP_Column_3.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                            break;
                        default:
                            break;
                    }
                }
            }
        }, spin_spec);
    },

    Auto_time_show_each_line(list_line_win, time_show_each_line) {
        var that = this;
        this.Node_Use_2_Stop_Anims.stopAllActions();
        setTimeout(function () {
            for (let x = 0; x < 3; x++) {
                that.GP_Column_1.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                that.GP_Column_2.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                that.GP_Column_3.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                that.GP_Column_4.children[x].getComponent("Mini_Slot_1_Item_Game_Small").hide_icon();
                that.GP_Column_5.children[x].getComponent("Mini_Slot_1_Item_Game_Small").hide_icon();
            }
            // show each line eat and hide it if win
            for (let index = 0; index < list_line_win.length; index++) {
                that.Node_Use_2_Stop_Anims.runAction(
                    cc.sequence(
                        cc.delayTime(1.5 * index),
                        cc.callFunc(function () {
                            cc.log("Vao");
                            var line_id = list_line_win[index].line_num;
                            var icon_open_id = parseInt(list_line_win[index].icon.charAt(7));
                            for (let x = 0; x < 3; x++) {
                                var location = that.Table_Matrix_3x3[line_id - 1][x];
                                var col_id = location % 3; // 5
                                var child_id = Math.floor(location / 3);
                                if (col_id === 0) {
                                    col_id = 3;
                                    child_id -= 1;
                                }
                                switch (col_id) {
                                    case 1:
                                        that.GP_Column_1.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                                        break;
                                    case 2:
                                        that.GP_Column_2.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                                        break;
                                    case 3:
                                        that.GP_Column_3.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        })
                    )
                );
            }

            // show 15 icon after show all line eat
            that.Node_Use_2_Stop_Anims.runAction(
                cc.sequence(
                    cc.delayTime(1.5 * (list_line_win.length)), // co the chi la :  line_show_anim_count
                    cc.callFunc(function () {
                        for (let x = 0; x < 3; x++) {
                            that.GP_Column_1.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                            that.GP_Column_2.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                            that.GP_Column_3.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                            that.GP_Column_4.children[x].getComponent("Mini_Slot_1_Item_Game_Small").show_icon();
                            that.GP_Column_5.children[x].getComponent("Mini_Slot_1_Item_Game_Small").show_icon();
                        }
                        setTimeout(function () {
                            that.Spining = false;
                            cc.log("Can Spins");
                            if (that.SPIN_FLASH_ONLINE) {
                                if (GM.Stay === 0) {
                                    if (WarpConst.GameBase.userInfo.desc.gold >= that.Spin_Bet) {
                                        that.Flash_Spin();
                                    } else {
                                        that.Btn_Flash_Spin.isChecked = false;
                                        that.Spining = false;
                                        that.SPIN_FLASH_ONLINE = false;
                                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                                    }
                                } else {
                                    if (GM.Gold_Lastest >= that.Spin_Bet) {
                                        that.Flash_Spin();
                                    } else {
                                        that.Btn_Flash_Spin.isChecked = false;
                                        that.Spining = false;
                                        that.SPIN_FLASH_ONLINE = false;
                                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                                    }
                                }

                            } else if (that.SPIN_AUTO_ONLINE) {
                                if (GM.Stay === 0) {
                                    if (WarpConst.GameBase.userInfo.desc.gold >= that.Spin_Bet) {
                                        that.Auto_Spin();
                                    } else {
                                        that.Btn_Auto_Spin.isChecked = false;
                                        that.Spining = false;
                                        that.SPIN_AUTO_ONLINE = false;
                                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                                    }
                                } else {
                                    if (GM.Gold_Lastest >= that.Spin_Bet) {
                                        that.Auto_Spin();
                                    } else {
                                        that.Btn_Auto_Spin.isChecked = false;
                                        that.Spining = false;
                                        that.SPIN_AUTO_ONLINE = false;
                                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                                    }
                                }

                            }
                        }, 500);
                    })
                )
            );
        }, time_show_each_line);
    },

    Machine_Spin_Flash() {
        var that = this;
        var list_line_win = [];
        if ("undefined" !== typeof this.Result_Spin_Server.win_icon) {
            for (let index = 0; index < this.Result_Spin_Server.win_icon.length; index++) {
                const line = this.Result_Spin_Server.win_icon[index];
                if (line.icon.charAt(7) !== "6") {
                    cc.log("An khac Icon 6");
                    list_line_win.push(line);
                }
            }
        }
        cc.log("list_line_win : ", list_line_win);

        var time_result = 1250; // = time_Column_move + time_C5_delay

        // effect show result after Spin end
        setTimeout(function () {
            // hide all Icon
            for (let x = 0; x < 3; x++) {
                that.GP_Column_1.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                that.GP_Column_2.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                that.GP_Column_3.children[x].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                that.GP_Column_4.children[x].getComponent("Mini_Slot_1_Item_Game_Small").hide_icon();
                that.GP_Column_5.children[x].getComponent("Mini_Slot_1_Item_Game_Small").hide_icon();
            }

            var time_show_all_icon = 2000;
            if (list_line_win.length > 0) {
                var spin_spec = 2000;
                if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                    spin_spec = 3000;
                    time_show_all_icon = 5000;
                    cc.log("Quay Trung Thuong");
                    // 1 : JACKPOT -  2 : BIGWIN
                    switch (that.Result_Spin_Server.spec_prize) {
                        case "JACKPOT":
                            that.Effect_Spin_Special(1, that.Result_Spin_Server.win_chips);
                            break;
                        case "BIGWIN":
                            that.Effect_Spin_Special(2, that.Result_Spin_Server.win_chips);
                            break;
                        default:
                            break;
                    }
                } else {
                    cc.log("Quay Thuong");
                    if (that.Result_Spin_Server.win_chips > 0) {
                        that.add_Gold_Effect.active = true;
                        time_show_all_icon = 4000;
                        that.PS_Add_Gold.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                        that.effect_Update_Gold(0, that.Result_Spin_Server.win_chips, that.label_Gold_Add.node, that.label_Gold_Add);
                        setTimeout(function () {
                            that.add_Gold_Effect.active = false;
                        }, 2000);
                    } else {
                        time_show_all_icon = 2500;
                    }
                }

                // show all effect if win
                that.Flash_spin_spec(list_line_win, spin_spec);
            }

            // show 15 icon after show all line eat
            that.Flash_time_show_all_icon(list_line_win, time_show_all_icon);
        }, time_result);
    },

    Game_Flash_Spin(toggle) {
        if (this.SPIN_AUTO_ONLINE) {
            this.Btn_Flash_Spin.isChecked = false;
            this.Show_Popup_Error_Action(" Không thể chọn chế độ Siêu tốc \n khi đang ở chế độ Tự quay ");
            return;
        }
        cc.log("Van qua :))");

        if (toggle.isChecked) {
            this.SPIN_FLASH_ONLINE = true;
        } else {
            this.SPIN_FLASH_ONLINE = false;
        }

        if (!this.Spining) {
            cc.log("Pre Spin");
            this.Spining = true;
            if (this.SPIN_FLASH_ONLINE) {
                cc.log("LobbyCtl.userInfo : ", WarpConst.GameBase.userInfo.desc.gold);
                cc.log("Spin_Bete_Bet : ", this.Spin_Bet);
                if (GM.Stay === 0) {
                    if (WarpConst.GameBase.userInfo.desc.gold >= this.Spin_Bet) {
                        this.Flash_Spin();
                    } else {
                        this.Btn_Flash_Spin.isChecked = false;
                        this.Spining = false;
                        this.SPIN_FLASH_ONLINE = false;
                        this.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                } else {
                    if (GM.Gold_Lastest >= this.Spin_Bet) {
                        this.Flash_Spin();
                    } else {
                        this.Btn_Flash_Spin.isChecked = false;
                        this.Spining = false;
                        this.SPIN_FLASH_ONLINE = false;
                        this.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                }

            } else {
                this.Spining = false; // uncheck , thay code ben duoi
                // this.Game_Spin_Manual();
            }
        } else {
            cc.log("Spinning");
        }
    },

    Flash_Spin() {
        // init Data and Spin
        this.Spin();
    },

    Flash_spin_spec(list_line_win, spin_spec) {
        var that = this;
        setTimeout(function () {
            for (let index = 0; index < list_line_win.length; index++) {
                var line_id = list_line_win[index].line_num;
                var icon_open_id = parseInt(list_line_win[index].icon.charAt(7));
                for (let x = 0; x < 3; x++) {
                    var location = that.Table_Matrix_3x3[line_id - 1][x];
                    var col_id = location % 3;
                    var child_id = Math.floor(location / 3);
                    if (col_id === 0) {
                        col_id = 3;
                        child_id -= 1;
                    }
                    switch (col_id) {
                        case 1:
                            that.GP_Column_1.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                            break;
                        case 2:
                            that.GP_Column_2.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                            break;
                        case 3:
                            that.GP_Column_3.children[child_id].getComponent("Mini_Slot_1_Item_Game_Big").check_show_Anim(icon_open_id - 1);
                            break;
                        default:
                            break;
                    }
                    // cc.log(col_id + ' - ' + child_id);
                }
            }
        }, spin_spec);
    },

    Flash_time_show_all_icon(list_line_win, time_show_all_icon) {
        var that = this;
        setTimeout(function () {
            for (let x = 0; x < 3; x++) {
                that.GP_Column_1.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                that.GP_Column_2.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                that.GP_Column_3.children[x].getComponent("Mini_Slot_1_Item_Game_Big").show_icon();
                that.GP_Column_4.children[x].getComponent("Mini_Slot_1_Item_Game_Small").show_icon();
                that.GP_Column_5.children[x].getComponent("Mini_Slot_1_Item_Game_Small").show_icon();
            }
            that.Spining = false;
            cc.log("Can Spins");
            if (that.SPIN_FLASH_ONLINE) {
                if (GM.Stay === 0) {
                    if (WarpConst.GameBase.userInfo.desc.gold >= that.Spin_Bet) {
                        that.Flash_Spin();
                    } else {
                        that.Btn_Flash_Spin.isChecked = false;
                        that.Spining = false;
                        that.SPIN_FLASH_ONLINE = false;
                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                } else {
                    if (GM.Gold_Lastest >= that.Spin_Bet) {
                        that.Flash_Spin();
                    } else {
                        that.Btn_Flash_Spin.isChecked = false;
                        that.Spining = false;
                        that.SPIN_FLASH_ONLINE = false;
                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                }

            } else if (that.SPIN_AUTO_ONLINE) {
                if (GM.Stay === 0) {
                    if (WarpConst.GameBase.userInfo.desc.gold >= that.Spin_Bet) {
                        that.Auto_Spin();
                    } else {
                        that.Btn_Auto_Spin.isChecked = false;
                        that.Spining = false;
                        that.SPIN_AUTO_ONLINE = false;
                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                } else {
                    if (GM.Gold_Lastest >= that.Spin_Bet) {
                        that.Auto_Spin();
                    } else {
                        that.Btn_Auto_Spin.isChecked = false;
                        that.Spining = false;
                        that.SPIN_AUTO_ONLINE = false;
                        that.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                }

            }
        }, time_show_all_icon);
    },

    Spin() {
        this.Spin_Count += 1;
        this.Spining = true;
        this.Node_Use_2_Stop_Anims.stopAllActions();

        if (this.Spin_Count === 1) {
            for (let index = 0; index < 3; index++) {
                this.GP_Column_1.children[index].getComponent("Mini_Slot_1_Item_Game_Big").show_shadow_spriteFrame();
                this.GP_Column_2.children[index].getComponent("Mini_Slot_1_Item_Game_Big").show_shadow_spriteFrame();
                this.GP_Column_3.children[index].getComponent("Mini_Slot_1_Item_Game_Big").show_shadow_spriteFrame();
                this.GP_Column_4.children[index].getComponent("Mini_Slot_1_Item_Game_Small").show_shadow_spriteFrame();
                this.GP_Column_5.children[index].getComponent("Mini_Slot_1_Item_Game_Small").show_shadow_spriteFrame();
            }
        } else {
            for (let index = 0; index < 3; index++) {
                this.GP_Column_1.children[index].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                this.GP_Column_2.children[index].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                this.GP_Column_3.children[index].getComponent("Mini_Slot_1_Item_Game_Big").hide_icon();
                this.GP_Column_4.children[index].getComponent("Mini_Slot_1_Item_Game_Small").hide_icon();
                this.GP_Column_5.children[index].getComponent("Mini_Slot_1_Item_Game_Small").hide_icon();
            }
        }

        switch (GM.Stay) {
            case 0:
                cc.log("Playing on Lobby");
                Mini_Slot_1.scope.changeBalance(-this.Spin_Bet);
                break;
            case 1:
                cc.log("Playing on Slot 1");
                GM.Gold_Lastest -= this.Spin_Bet;
                Slot1.scope.notify_Gold_State_Change();
                break;
            case 2:
                cc.log("Playing on Slot 2");
                GM.Gold_Lastest -= this.Spin_Bet;
                Slot2.scope.notify_Gold_State_Change();
                break;
            case 3:
                cc.log("Playing on Slot 3");
                GM.Gold_Lastest -= this.Spin_Bet;
                Slot3.scope.notify_Gold_State_Change();
                break;
            case 4:
                cc.log("Playing on Slot 4");
                GM.Gold_Lastest -= this.Spin_Bet;
                Slot4.scope.notify_Gold_State_Change();
                break;
            default:
                break;
        }

        this.spinMiniSlot(this.Spin_Bet);
    },

    Manager_Spin_Anim() {
        var that = this;

        var time_init_icon = 1000;
        var time_Column_move = 2;
        var time_C1_delay = 0.2;
        var time_C2_delay = 0.4;
        var time_C3_delay = 0.6;
        var time_C4_delay = 0.8;
        var time_C5_delay = 1;

        if (this.SPIN_FLASH_ONLINE) {
            cc.log("SPIN_FLASH_ONLINE");
            time_init_icon = 200;
            time_Column_move = 1;
            time_C1_delay = 0.05;
            time_C2_delay = 0.1;
            time_C3_delay = 0.15;
            time_C4_delay = 0.2;
            time_C5_delay = 0.25;
        } else {
            cc.log("SPIN Other Mode");
        }

        // init Icon result
        setTimeout(function () {
            for (let index = 0; index < 3; index++) {
                var data_col_1 = {
                    id: 5 * index,
                    icon: that.Result_Spin_Server.result_full[5 * index].charAt(5)
                }
                that.GP_Column_1.children[index].getComponent("Mini_Slot_1_Item_Game_Big").init_item(data_col_1);

                var data_col_2 = {
                    id: 5 * index + 1,
                    icon: that.Result_Spin_Server.result_full[5 * index + 1].charAt(5)
                }
                that.GP_Column_2.children[index].getComponent("Mini_Slot_1_Item_Game_Big").init_item(data_col_2);

                var data_col_3 = {
                    id: 5 * index + 2,
                    icon: that.Result_Spin_Server.result_full[5 * index + 2].substring(5) // if icon 10 return 10 :))
                }
                that.GP_Column_3.children[index].getComponent("Mini_Slot_1_Item_Game_Big").init_item(data_col_3);

                var data_col_4 = {
                    id: 5 * index + 3,
                    icon: that.Result_Spin_Server.result_full[5 * index + 3].substring(5)
                }
                that.GP_Column_4.children[index].getComponent("Mini_Slot_1_Item_Game_Small").init_item(data_col_4);

                var data_col_5 = {
                    id: 5 * index + 4,
                    icon: that.Result_Spin_Server.result_full[5 * index + 4].substring(5)
                }
                that.GP_Column_5.children[index].getComponent("Mini_Slot_1_Item_Game_Small").init_item(data_col_5);
            }
        }, time_init_icon);

        // Scroll 5 Column
        this.GP_Column_1.runAction(
            cc.sequence(
                cc.delayTime(time_C1_delay),
                cc.moveTo(0, -110, 2915),
                cc.moveTo(time_Column_move, -110, 155).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 1");
                }))
        );
        this.GP_Column_2.runAction(
            cc.sequence(
                cc.delayTime(time_C2_delay),
                cc.moveTo(0, 0, 2915),
                cc.moveTo(time_Column_move, 0, 155).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 2");
                }))
        );
        this.GP_Column_3.runAction(
            cc.sequence(
                cc.delayTime(time_C3_delay),
                cc.moveTo(0, 110, 2915),
                cc.moveTo(time_Column_move, 110, 155).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 3");
                }))
        );
        this.GP_Column_4.runAction(
            cc.sequence(
                cc.delayTime(time_C4_delay),
                cc.moveTo(0, -55, 2140),
                cc.moveTo(time_Column_move, -55, 115).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 4");
                }))
        );
        this.GP_Column_5.runAction(
            cc.sequence(
                cc.delayTime(time_C5_delay),
                cc.moveTo(0, 55, 2140),
                cc.moveTo(time_Column_move, 55, 115).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 5  ");
                    that.effect_Update_Jackpot(that.Last_Jackpot, that.Result_Spin_Server.jackpot, that.node_Jackpot, that.label_Jackpot);
                    that.Last_Jackpot = that.Result_Spin_Server.jackpot;
                }))
        );
    },

    Show_Popup_Error_Action(mess) {
        this.Popup_Error_Action.active = true;
        this.Popup_Error_Action.position = cc.v2(-Mini_Slot_1.scope.Game_Drag.position.x, -Mini_Slot_1.scope.Game_Drag.position.y);
        this.label_popup_error_action_mes.string = mess;
    },

    Close_Popup_Error_Action() {
        this.Popup_Error_Action.active = false;
    },

    effect_Update_Gold(gold_start, gold_end, node, label) {
        var that = this;
        var gold_add = gold_end - gold_start;
        label.string = gold_start;

        var steps = 40;
        var delta_gold_add = -1;

        if (gold_add < 40) {
            steps = 10;
        }

        delta_gold_add = Math.floor(gold_add / steps);

        var add = cc.sequence(
            cc.delayTime(0.03),
            cc.callFunc(function () {
                gold_start += delta_gold_add;
                label.string = GM.instance.convert_money(gold_start);
            })
        );

        if (steps === 40) {
            node.runAction(
                cc.sequence(
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    cc.callFunc(function () {
                        gold_start = gold_end;
                        label.string = GM.instance.convert_money(gold_start);
                    })
                )
            );
        } else {
            node.runAction(
                cc.sequence(
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    cc.callFunc(function () {
                        gold_start = gold_end;
                        label.string = GM.instance.convert_money(gold_start);
                    })
                )
            );
        }

    },

    effect_Update_Jackpot(gold_start, gold_end, node, label) {
        var that = this;
        var gold_add = gold_end - gold_start;
        label.string = GM.instance.convert_money(gold_start);

        var steps = 5;
        var delta_gold_add = -1;

        delta_gold_add = Math.floor(gold_add / steps);

        var add = cc.sequence(
            cc.delayTime(0.1),
            cc.callFunc(function () {
                gold_start += delta_gold_add;
                label.string = GM.instance.convert_money(gold_start);
            })
        );

        node.runAction(
            cc.sequence(
                add,
                add,
                add,
                add,
                cc.callFunc(function () {
                    gold_start = gold_end;
                    label.string = GM.instance.convert_money(gold_start);
                })
            )
        );
    },

    choose_Bet(toggle) {
        // test new logic
        // cc.log("Can choose Bet");
        // var index = this.Bet_selector.indexOf(toggle);
        // switch (index) {
        //     case 0:
        //         this.Spin_Bet = 100;
        //         break;
        //     case 1:
        //         this.Spin_Bet = 1000;
        //         break;
        //     case 2:
        //         this.Spin_Bet = 10000;
        //         break;
        // }
        // cc.log("Spin_Bet : ", this.Spin_Bet);

        if (!this.Spining) {
            if (this.SPIN_AUTO_ONLINE || this.SPIN_FLASH_ONLINE) {
                cc.log("Can not choose Bet because Auto or Flash mode is Active");
                this.Show_Popup_Error_Action(" Không thể chuyển mức cược \n khi đang quay ! ");
                Mini_Slot_1.scope.f5_bet();
            } else {
                cc.log("Can choose Bet");
                var index = this.Bet_selector.indexOf(toggle);
                switch (index) {
                    case 0:
                        this.subMiniSlot(100);
                        break;
                    case 1:
                        this.subMiniSlot(1000);
                        break;
                    case 2:
                        this.subMiniSlot(10000);
                        break;
                }
                cc.log("Spin_Bet : ", this.Spin_Bet);
            }
        } else {
            cc.log("Can not choose Bet");
            this.Show_Popup_Error_Action(" Không thể chuyển mức cược \n khi đang quay ! ");
            Mini_Slot_1.scope.f5_bet();
        }
    },

    randomBetween(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    },

    Effect_Spin_Special(type, gold_add) {
        var that = this;
        switch (type) {
            case 1:
                // Jackpot
                this.Effect_No_Hu.active = true;
                this.BG_No_Hu.getComponent(cc.Animation).play();
                setTimeout(function () {
                    that.PS_No_Hu.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                    that.label_Gold_No_Hu.string = " 0 ";
                    that.effect_Update_Gold(0, gold_add, that.Node_Gold_No_Hu, that.label_Gold_No_Hu);
                }, 100);

                setTimeout(function () {
                    that.Effect_No_Hu.active = false;
                }, 2000); // 3000  2500
                break;
            case 2:
                // Big Win
                this.Effect_Thang_Lon.active = true;
                this.BG_Thang_Lon.getComponent(cc.Animation).play();
                setTimeout(function () {
                    that.PS_Thang_Lon.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                    that.label_Gold_Thang_Lon.string = " 0 ";
                    that.effect_Update_Gold(0, gold_add, that.Node_Gold_Thang_Lon, that.label_Gold_Thang_Lon);
                }, 100);

                setTimeout(function () {
                    that.Effect_Thang_Lon.active = false;
                }, 2000);
                break;
            default:
                break;
        }
    },


    Test_Anim() {
        // var that = this;
        // this.Show_Popup_Connect_Error("Tham gia phòng thất bại !", "P/s : Vui lòng thử lại sau");
    },


    // Connect Server
    subMiniSlot(game_bet) {
        cc.log("game_bet : ", game_bet);
        var str = {
            type: WarpConst.MiniGame.SUBSCRIBE_ROOM,
            bet: game_bet,
            chipType: 1

        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_SLOT;
        WebsocketClient.Socket.send(m);
    },
    spinMiniSlot(bet_selected) {
        var str = {
            type: WarpConst.MiniGame.START_MATCH,
            bet: bet_selected, //bet value: 100 | 1000 | 10000
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_SLOT;
        WebsocketClient.Socket.send(m);
    },
    getMiniSlotHis() {
        var str = {
            type: WarpConst.MiniGame.GET_USER_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_SLOT;
        WebsocketClient.Socket.send(m);
    },
    getMiniSlotGlory() {
        var str = {
            type: WarpConst.MiniGame.GET_JACKPOT_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_SLOT;
        WebsocketClient.Socket.send(m);
    },

    //SET MONEY
    //minus
    changeBalance(balanceChange) {
        LobbyCtl.instance.addUserGold(balanceChange);
    },
    setBalance(balance) {
        LobbyCtl.instance.setUserGold(balance);
    }
});