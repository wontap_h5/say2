cc.Class({
    extends: cc.Component,

    properties: {},

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {

    },

    init_Star(time_delay) {
        var that = this;
        setTimeout(function(){
            that.node.runAction(
                cc.repeatForever(
                    cc.sequence(
                        cc.delayTime(3),
                        cc.scaleTo(0.5, 4, 4),
                        cc.rotateBy(0.5, 50, 50),
                        cc.scaleTo(0.5, 0, 0)
                    )
                )
            );
        },time_delay);
    }

    // update (dt) {},
});