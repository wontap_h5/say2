cc.Class({
    extends: cc.Component,

    properties: {
        // Item_BG: {
        //     default: null,
        //     type: cc.Node
        // },
        // sprite_List: {
        //     default: [],
        //     type: [cc.SpriteFrame]
        // },
        label_Time: {
            default: null,
            type: cc.Label
        },
        label_Account: {
            default: null,
            type: cc.Label
        },
        label_Bet: {
            default: null,
            type: cc.Label
        },
        label_Gold: {
            default: null,
            type: cc.Label
        },
        label_Des: {
            default: null,
            type: cc.Label
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {},

    start() {

    },

    update(dt) {},

    init_Item_Rank(data) {
        // this.Item_BG.getComponent(cc.Sprite).spriteFrame = this.sprite_List[data.bg_index];
        this.label_Time.string = data.time;
        this.label_Account.string = data.account;
        this.label_Bet.string = this.convert_money(data.bet);
        this.label_Gold.string = this.convert_money(data.gold);
        this.label_Des.string = data.des + " ";
    },

    convert_money(price) {
        return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " ";
    }
});