var WarpConst = require("WarpConst");
var WebsocketClient = require("WebsocketClient");
var GM = require("GM");
var Slot4 = cc.Class({
    extends: cc.Component,

    statics: {
        scope: null
    },

    properties: {
        UI_Select: {
            default: null,
            type: cc.Node
        },
        UI_Game_Play: {
            default: null,
            type: cc.Node
        },
        Selector: {
            default: null,
            type: cc.Node
        },
        label_Storage_Bet_100: {
            default: null,
            type: cc.Label
        },
        label_Storage_Bet_1k: {
            default: null,
            type: cc.Label
        },
        label_Storage_Bet_10k: {
            default: null,
            type: cc.Label
        },
        node_Storage_Bet_100: {
            default: null,
            type: cc.Node
        },
        node_Storage_Bet_1k: {
            default: null,
            type: cc.Node
        },
        node_Storage_Bet_10k: {
            default: null,
            type: cc.Node
        },
        UI_Setting: {
            default: null,
            type: cc.Node
        },
        Light_Logo: {
            default: null,
            type: cc.Node
        },
        Mask_Logo: {
            default: null,
            type: cc.Node
        },
        Light_Chest: {
            default: null,
            type: cc.Node
        },
        Mask_Chest: {
            default: null,
            type: cc.Node
        },
        Light_Spin: {
            default: null,
            type: cc.Node
        },
        Mask_Spin: {
            default: null,
            type: cc.Node
        },
        Setting_Music_Off: {
            default: null,
            type: cc.Node
        },
        Setting_Sound_Off: {
            default: null,
            type: cc.Node
        },
        BG_Spin_Manual: {
            default: null,
            type: cc.Node
        },
        GP_Column_1: {
            default: null,
            type: cc.Node
        },
        GP_Column_2: {
            default: null,
            type: cc.Node
        },
        GP_Column_3: {
            default: null,
            type: cc.Node
        },
        GP_Column_4: {
            default: null,
            type: cc.Node
        },
        GP_Column_5: {
            default: null,
            type: cc.Node
        },
        Popup_Line_Bet: {
            default: null,
            type: cc.Node
        },
        List_Line_Bet: {
            default: null,
            type: cc.Node
        },
        label_Bet_Line: {
            default: null,
            type: cc.Label
        },
        label_Bet_Price: {
            default: null,
            type: cc.Label
        },
        label_Total_Bet_Price: {
            default: null,
            type: cc.Label
        },
        Boom_List_Line: {
            default: null,
            type: cc.Node
        },
        sprite_Bom_List_Line: {
            default: [],
            type: [cc.SpriteFrame]
        },
        label_Total_Win: {
            default: null,
            type: cc.Label
        },
        label_Free: {
            default: null,
            type: cc.Label
        },
        label_User_Gold: {
            default: null,
            type: cc.Label
        },
        add_Gold_Effect: {
            default: null,
            type: cc.Node
        },
        PS_Add_Gold: {
            default: null,
            type: cc.Node
        },
        gold_Add_Node: {
            default: null,
            type: cc.Node
        },
        label_Gold_Add: {
            default: null,
            type: cc.Label
        },
        node_Jackpot: {
            default: null,
            type: cc.Node
        },
        label_Jackpot: {
            default: null,
            type: cc.Label
        },
        Popup_Rank: {
            default: null,
            type: cc.Node
        },
        Popup_Guide: {
            default: null,
            type: cc.Node
        },
        Popup_History: {
            default: null,
            type: cc.Node
        },
        Rank_Content: {
            default: null,
            type: cc.Node
        },
        Item_Rank_Prefab: {
            default: null,
            type: cc.Prefab
        },
        History_Content: {
            default: null,
            type: cc.Node
        },
        Item_History_Prefab: {
            default: null,
            type: cc.Prefab
        },
        Popup_Mess: {
            default: null,
            type: cc.Node
        },
        Feedback_Input: {
            default: null,
            type: cc.EditBox,
        },
        Auto_Spin_State: {
            default: null,
            type: cc.Node
        },
        auto_on_off_sprite: {
            default: [],
            type: [cc.SpriteFrame]
        },
        BG_No_Hu: {
            default: null,
            type: cc.Node
        },
        Effect_No_Hu: {
            default: null,
            type: cc.Node
        },
        PS_No_Hu: {
            default: null,
            type: cc.Node
        },
        label_Gold_No_Hu: {
            default: null,
            type: cc.Label
        },
        Node_Gold_No_Hu: {
            default: null,
            type: cc.Node
        },
        BG_Thang_Lon: {
            default: null,
            type: cc.Node
        },
        Effect_Thang_Lon: {
            default: null,
            type: cc.Node
        },
        PS_Thang_Lon: {
            default: null,
            type: cc.Node
        },
        label_Gold_Thang_Lon: {
            default: null,
            type: cc.Label
        },
        Node_Gold_Thang_Lon: {
            default: null,
            type: cc.Node
        },
        BG_Bonus: {
            default: null,
            type: cc.Node
        },
        Effect_Bonus: {
            default: null,
            type: cc.Node
        },
        PS_Bonus: {
            default: null,
            type: cc.Node
        },
        label_Gold_Bonus: {
            default: null,
            type: cc.Label
        },
        Node_Gold_Bonus: {
            default: null,
            type: cc.Node
        },
        music_background: {
            default: null,
            type: cc.AudioClip
        },
        sound_big_win: {
            default: null,
            type: cc.AudioClip
        },
        sound_button_click: {
            default: null,
            type: cc.AudioClip
        },
        sound_jackpot: {
            default: null,
            type: cc.AudioClip
        },
        sound_spin: {
            default: null,
            type: cc.AudioClip
        },
        sound_win: {
            default: null,
            type: cc.AudioClip
        },
        UI_Dao_Vang: {
            default: null,
            type: cc.Node
        },
        label_Count_Unpack_Chest: {
            default: null,
            type: cc.Label
        },
        label_Time_Unpack_Chest: {
            default: null,
            type: cc.Label
        },
        List_Pack_Chest: {
            default: null,
            type: cc.Node
        },
        label_Unpack_Chest_Storage: {
            default: null,
            type: cc.Label
        },
        Node_Use_2_Stop_Anims: {
            default: null,
            type: cc.Node
        },
        Popup_Bet_0_Line: {
            default: null,
            type: cc.Node
        },
        UI_Popup_Connect: {
            default: null,
            type: cc.Node
        },
        label_Popup_Error_Content: {
            default: null,
            type: cc.Label
        },
        label_Popup_Error_Des: {
            default: null,
            type: cc.Label
        },
        Star_Logo: {
            default: null,
            type: cc.Node
        },
        Star_Storage: {
            default: null,
            type: cc.Node
        },
        notifyAllAnim: cc.Node,
        notifyAllText: cc.RichText,
        UI_Sail: {
            default: null,
            type: cc.Node
        },
        UI_Pedestal: {
            default: null,
            type: cc.Node
        },
        Icon_Setting: {
            default: null,
            type: cc.Node
        },
        Icon_Rank: {
            default: null,
            type: cc.Node
        },
        Icon_Guide: {
            default: null,
            type: cc.Node
        },
        Icon_History: {
            default: null,
            type: cc.Node
        },
        Icon_Char: {
            default: null,
            type: cc.Node
        }
    },

    Game_Room_ID: null,
    Spin_Count: null,
    Spining: null,
    num_item_in_column: null,
    Bet_Line_Count: null,
    Result_Spin_Server: null,
    Table_Matrix_3x5: null,
    Line_Selected_Matrix_1x20: null,
    SPIN_AUTO_ONLINE: null,
    Data_Spin_Free: null,
    Free_Spin_Total: null,

    remote_music_background: null,

    Unpack_Total_Number: null,
    Unpack_Chest_Value: null,
    Unpack_Time: null,
    Unpack_Counting: null,
    minutes: null,
    seconds: null,

    Spin_Manual_Action_Show: null,

    check_OnSubscribeGameDone: null,
    check_OnJoinLobbyDone: null,
    check_OnSubscribeLobby: null,
    check_OnGetRoomsDone: null,
    check_OnJoinRoomsDone: null,
    check_OnSlotStartDone: null,
    check_OnSlotGetUserHistoryDone: null,
    check_OnSlotGetGloryDone: null,

    Server_Data_Room: null,
    Game_Room_Bet: null,
    User_Info: null,
    Spin_Bet: null,
    User_Gold: null,
    canUnpack: null,
    Fake_Data_Unpack: null,
    Last_Jackpot: null,
    Spin_Free: null,

    connect_err: null,
    canExit: null,
    // LIFE-CYCLE CALLBACKS:

    OnSubscribeGameDone(res) {
        cc.log("OnSubscribeGameDone : ", res);
        var data = JSON.parse(res);
        if ("undefined" !== typeof data.desc) {
            if (data.desc === "Success") {
                Slot4.scope.check_OnSubscribeGameDone = true;
                Slot4.scope.joinLobby();
            } else {
                Slot4.scope.check_OnSubscribeGameDone = false;
            }
        } else {
            Slot4.scope.check_OnSubscribeGameDone = false;
            Slot4.scope.Show_Popup_Connect_Error("Tham gia trò chơi thất bại !", "P/s : Vui lòng thử lại sau");
        }
        cc.log("check_OnSubscribeGameDone : ", Slot4.scope.check_OnSubscribeGameDone);
    },

    OnJoinLobbyDone(res) {
        cc.log("OnJoinLobbyDone : ", res);
        var data = JSON.parse(res);
        if ("undefined" !== typeof data.desc) {
            if (data.desc === "Success") {
                Slot4.scope.check_OnJoinLobbyDone = true;
                Slot4.scope.subLobby();
            } else {
                Slot4.scope.check_OnJoinLobbyDone = false;
                Slot4.scope.Show_Popup_Connect_Error("Tham gia trò chơi thất bại !", "P/s : Vui lòng thử lại sau");
            }
        } else {
            Slot4.scope.check_OnJoinLobbyDone = false;
            Slot4.scope.Show_Popup_Connect_Error("Tham gia trò chơi thất bại !", "P/s : Vui lòng thử lại sau");
        }
        cc.log("check_OnJoinLobbyDone : ", Slot4.scope.check_OnJoinLobbyDone);
    },

    OnSubscribeLobby(res) {
        cc.log("OnSubscribeLobby : ", res);
        var data = JSON.parse(res);
        if ("undefined" !== typeof data.desc) {
            if (data.desc === "Success") {
                Slot4.scope.check_OnSubscribeLobby = true;
                Slot4.scope.getRoom();
            } else {
                Slot4.scope.check_OnSubscribeLobby = false;
                Slot4.scope.Show_Popup_Connect_Error("Tham gia trò chơi thất bại !", "P/s : Vui lòng thử lại sau");
            }
        } else {
            Slot4.scope.check_OnSubscribeLobby = false;
            Slot4.scope.Show_Popup_Connect_Error("Tham gia trò chơi thất bại !", "P/s : Vui lòng thử lại sau");
        }
        cc.log("check_OnSubscribeLobby : ", Slot4.scope.check_OnSubscribeLobby);
    },

    OnGetRoomsDone(res) {
        cc.log("OnGetRoomsDone : ", res);
        var data = JSON.parse(res).rooms;
        Slot4.scope.Server_Data_Room = data;
        Slot4.scope.effect_Update_Gold(0, Slot4.scope.Server_Data_Room[0].funds, Slot4.scope.node_Storage_Bet_100, Slot4.scope.label_Storage_Bet_100);
        Slot4.scope.effect_Update_Gold(0, Slot4.scope.Server_Data_Room[1].funds, Slot4.scope.node_Storage_Bet_1k, Slot4.scope.label_Storage_Bet_1k);
        Slot4.scope.effect_Update_Gold(0, Slot4.scope.Server_Data_Room[2].funds, Slot4.scope.node_Storage_Bet_10k, Slot4.scope.label_Storage_Bet_10k);
    },

    OnJoinRoomsDone(res) {
        cc.log("OnJoinRoomsDone : ", res);
        var data = JSON.parse(res);
        Slot4.scope.User_Info = data;
        Slot4.scope.Free_Spin_Total = data.freeSpin;
        if (Slot4.scope.Game_Room_ID === 0) {
            Slot4.scope.Play_Game(0);
        } else {
            Slot4.scope.Play_Game(data.room.bet);
        }
    },

    OnSlotStartDone(res) {
        cc.log("OnSlotStartDone : ", res);
        var data = JSON.parse(res);
        if ("undefined" !== typeof data.rushed) {
            cc.log("Action Rush Gold");
            Slot4.scope.Anim_Unpack(data);
        } else {
            cc.log("Action Spin");
            Slot4.scope.Result_Spin_Server = data;
            // Slot4.scope.Free_Spin_Total = data.free_spins;
            // Slot4.scope.label_Free.string = data.free_spins;
            var line_bets_count = 0;
            for (let index = 0; index < Slot4.scope.Line_Selected_Matrix_1x20.length; index++) {
                if (Slot4.scope.Line_Selected_Matrix_1x20[index] > 0) {
                    line_bets_count += 1;
                }
            }
            cc.log("line_bets_count : ", line_bets_count);
            cc.log("Gold tru  : ", Slot4.scope.Spin_Bet * line_bets_count);
            if (!Slot4.scope.Spin_Free) {
                Slot4.scope.User_Gold -= Slot4.scope.Spin_Bet * line_bets_count;
                GM.Gold_Lastest = Slot4.scope.User_Gold;
                WarpConst.GameBase.userInfo.desc.gold = Slot4.scope.User_Gold;
                Slot4.scope.label_User_Gold.string = GM.instance.convert_money(Slot4.scope.User_Gold);
            } else {
                Slot4.scope.Free_Spin_Total -= 1;
            }
            Slot4.scope.label_Free.string = Slot4.scope.Free_Spin_Total;

            Slot4.scope.Manager_Spin_Anim();
            if (Slot4.scope.SPIN_AUTO_ONLINE) {
                Slot4.scope.Machine_Spin_Auto();
            } else {
                Slot4.scope.Machine_Spin_Manual();
            }
        }
    },

    OnSlotGetUserHistoryDone(res) {
        cc.log("OnSlotGetUserHistoryDone : ", res);
        var data = JSON.parse(res);
        Slot4.scope.Popup_History.active = true;
        Slot4.scope.History_Content.removeAllChildren(true);
        var Data_History = data.slotHistory;
        for (var i = 0; i < Data_History.length; i++) {
            var item = cc.instantiate(Slot4.scope.Item_History_Prefab);
            var data = Data_History[i];
            item.getComponent('Slot_4_Item_History').init_Item_History({
                bg_index: i % 2,
                sess: "#" + data.id,
                time: data.time,
                bet: data.bet,
                gold: data.total
            });
            Slot4.scope.History_Content.addChild(item);
        }
    },

    OnSlotGetGloryDone(res) {
        cc.log("OnSlotGetGloryDone : ", res);
        var data = JSON.parse(res);

        Slot4.scope.Popup_Rank.active = true;
        Slot4.scope.Rank_Content.removeAllChildren(true);
        var Data_Rank = data.slotHistory;
        for (var i = 0; i < Data_Rank.length; i++) {
            var item = cc.instantiate(Slot4.scope.Item_Rank_Prefab);
            var data = Data_Rank[i];
            var name = decodeURIComponent(escape(data.displayName));
            if (name.length > 15) {
                name = name.substring(0, 11) + "...";
            }
            item.getComponent('Slot_4_Item_Rank').init_Item_Rank({
                bg_index: i % 2,
                time: data.time,
                account: name,
                bet: data.bet,
                gold: data.win,
                des: decodeURIComponent(escape(data.desc))
            });
            Slot4.scope.Rank_Content.addChild(item);
        }
    },

    OnJackpotChange(res) {
        cc.log("OnJackpotChange :  ", res);
        var data = JSON.parse(res);
        var old_jackpot = Slot4.scope.Last_Jackpot;
        switch (Slot4.scope.Spin_Bet) {
            case 0:
                if (data.roomId === 39) {
                    Slot4.scope.effect_Update_Jackpot(old_jackpot, data.jackpot, Slot4.scope.node_Jackpot, Slot4.scope.label_Jackpot);
                    Slot4.scope.Last_Jackpot = data.jackpot;
                }
                break;
            case 100:
                if (data.roomId === 37) {
                    Slot4.scope.effect_Update_Jackpot(old_jackpot, data.jackpot, Slot4.scope.node_Jackpot, Slot4.scope.label_Jackpot);
                    Slot4.scope.Last_Jackpot = data.jackpot;
                }
                break;
            case 1000:
                if (data.roomId === 38) {
                    Slot4.scope.effect_Update_Jackpot(old_jackpot, data.jackpot, Slot4.scope.node_Jackpot, Slot4.scope.label_Jackpot);
                    Slot4.scope.Last_Jackpot = data.jackpot;
                }
                break;
            case 10000:
                if (data.roomId === 39) {
                    Slot4.scope.effect_Update_Jackpot(old_jackpot, data.jackpot, Slot4.scope.node_Jackpot, Slot4.scope.label_Jackpot);
                    Slot4.scope.Last_Jackpot = data.jackpot;
                }
                break;
        }


    },

    OnSendFeedBack(status, strJson) {
        if (status == WarpConst.WarpResponseResultCode.SUCCESS) {
            //var data = JSON.parse(strJson);
            Slot4.scope.Popup_Mess.active = false;
            Slot4.scope.Show_Popup_Connect_Error(" Gửi phản hồi thành công ! ", " Cảm ơn những góp ý từ bạn ...");
        } else {
            Slot4.scope.Show_Popup_Connect_Error(" Có lỗi xảy ra ", " Vui lòng thử lại sau ! ");
        }
    },

    OnMoneyChangeDone(res) {
        cc.log("Slot 1 OnMoneyChangeDone : ", res);
        var data = JSON.parse(res);
        switch (data.type) {
            case WarpConst.MoneyChangeType.BY_GOLD_RUSH:
                Slot4.scope.Show_Popup_Connect_Error("Bạn có thông báo từ game Đào Vàng", " ");
                break;
            case WarpConst.MoneyChangeType.BY_CARD:
                Slot4.scope.Show_Popup_Connect_Error("Nạp thẻ thành công, tài khoản được cộng ", " " + GM.instance.convert_money(data.change) + WarpConst.GameBase.MoneyName);
                break;
            case WarpConst.MoneyChangeType.BY_APPLE_ITEM:
                Slot4.scope.Show_Popup_Connect_Error("Nạp IAP thành công, tài khoản được cộng ", " " + GM.instance.convert_money(data.change) + WarpConst.GameBase.MoneyName);
                break;
            case WarpConst.MoneyChangeType.BY_BANNER_ADS:
                Slot4.scope.Show_Popup_Connect_Error("Xem banner thành công, tài khoản được cộng ", " " + GM.instance.convert_money(data.change) + WarpConst.GameBase.MoneyName);
                break;
            case WarpConst.MoneyChangeType.BY_VIDEO_ADS:
                Slot4.scope.Show_Popup_Connect_Error("Xem video thành công, tài khoản được cộng ", " " + GM.instance.convert_money(data.change) + WarpConst.GameBase.MoneyName);
                break;
            case WarpConst.MoneyChangeType.BY_TRANSFER_GOLD_GOLD:
                Slot4.scope.Show_Popup_Connect_Error("Tài khoản nhận được ", GM.instance.convert_money(data.change) + WarpConst.GameBase.MoneyName + " từ giao dịch chuyển " + WarpConst.GameBase.MoneyName + ".");
                break;
            case WarpConst.MoneyChangeType.BY_REF_CODE:
                Slot4.scope.Show_Popup_Connect_Error("Tài khoản nhận được ", GM.instance.convert_money(data.change) + WarpConst.GameBase.MoneyName + " từ nhập mã giới thiệu.");
                break;
            case WarpConst.MoneyChangeType.BY_SYSTEM_ADD:
                Slot4.scope.Show_Popup_Connect_Error("Tài khoản nhận được ", GM.instance.convert_money(data.change) + WarpConst.GameBase.MoneyName + " từ HỆ THỐNG.");
                break;
        }

        Slot4.scope.User_Gold = data.total;
        GM.Gold_Lastest = data.total;
        WarpConst.GameBase.userInfo.desc.gold = Slot4.scope.User_Gold;
        Slot4.scope.label_User_Gold.string = GM.instance.convert_money(data.total);
    },

    OnLoginOnOtherDevice2() {
        Slot4.scope.connect_err = true;
        Slot4.scope.Show_Popup_Connect_Error("Tài khoản đã bị đăng nhập", " ở thiết bị khác ");
    },

    OnDisconnect2() {
        Slot4.scope.connect_err = true;
        Slot4.scope.Show_Popup_Connect_Error(" Mất kết nối ", " Vui lòng khởi động lại trò chơi. ");
    },

    onLoad() {
        Slot4.scope = this;
        this.connect_err = false;
        this.Server_Data_Room = {};
        this.User_Info = {};
        this.Spin_Bet = -1;
        this.Game_Room_ID = -1;
        this.Spin_Free = false;
        this.User_Gold = 0;
        this.Last_Jackpot = 0;
        this.Bet_Line_Count = 20;
        this.Spining = false;
        this.Spin_Count = 0;
        this.num_item_in_column = 12;
        this.Table_Matrix_3x5 = [
            [1, 2, 3, 4, 5],
            [6, 7, 8, 9, 10],
            [11, 12, 13, 14, 15],
            [6, 7, 3, 9, 10],
            [6, 7, 13, 9, 10],
            [1, 2, 8, 4, 5],
            [11, 12, 8, 14, 15],
            [1, 12, 3, 14, 5],
            [11, 2, 13, 4, 15],
            [6, 2, 13, 4, 10],
            [11, 7, 3, 9, 15],
            [1, 7, 13, 9, 5],
            [6, 12, 8, 4, 10],
            [6, 2, 8, 14, 10],
            [11, 7, 8, 9, 15],
            [6, 2, 3, 4, 10],
            [1, 7, 8, 9, 5],
            [6, 12, 13, 14, 10],
            [11, 12, 8, 4, 5],
            [1, 2, 8, 14, 15]
        ];

        this.Free_Spin_Total = 0;

        this.Line_Selected_Matrix_1x20 = [];
        for (let index = 0; index < 20; index++) {
            this.Line_Selected_Matrix_1x20.push(index + 1);
        }

        this.SPIN_AUTO_ONLINE = false;

        this.Unpack_Total_Number = 5;
        this.Unpack_Chest_Value = 0;
        this.Unpack_Time = 2 * 60; // 2p
        this.Unpack_Counting = false;
        this.minutes = -1;
        this.seconds = -1;

        this.Fake_Data_Unpack = [{
                "rushed": 2500000,
                "rush_remains": 0,
                "type": 202,
                "desc": "gold_rush"
            },
            {
                "rushed": 50000,
                "rush_remains": 1,
                "type": 202,
                "desc": "gold_rush"
            },
            {
                "rushed": 5000000,
                "rush_remains": 2,
                "type": 202,
                "desc": "gold_rush"
            },
            {
                "rushed": 40000,
                "rush_remains": 3,
                "type": 202,
                "desc": "gold_rush"
            },
            {
                "rushed": 250000,
                "rush_remains": 4,
                "type": 202,
                "desc": "gold_rush"
            }
        ];

        this.Data_Spin_Free = [{
                "type": 202,
                "free_spins": 0,
                "chips": 116860,
                "jackpot": 2170030,
                "result_full": ["icon 7", "icon 7", "icon 7", "icon 6", "icon 6", "icon 7", "icon 6", "icon 7", "icon 5", "icon 7", "icon 6", "icon 1", "icon 5", "icon 7", "icon 5"],
                "win_icon": [{
                    "line_num": 14,
                    "icon": "5 icon 7",
                    "value": "30"
                }, {
                    "line_num": 16,
                    "icon": "4 icon 7",
                    "value": "6"
                }, {
                    "line_num": 19,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 20,
                    "icon": "4 icon 7",
                    "value": "6"
                }],
                "pay_lines": [{
                    "line_num": 14,
                    "icon": "5 icon 7",
                    "value": "30"
                }, {
                    "line_num": 16,
                    "icon": "4 icon 7",
                    "value": "6"
                }, {
                    "line_num": 19,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 20,
                    "icon": "4 icon 7",
                    "value": "6"
                }],
                "win_chips": 4400,
                "total_prize": 44,
                "result_line": [
                    ["icon 6", "icon 6", "icon 7", "icon 7", "icon 7"],
                    ["icon 5", "icon 6", "icon 7", "icon 7", "icon 7"],
                    ["icon 1", "icon 5", "icon 5", "icon 6", "icon 7"],
                    ["icon 5", "icon 6", "icon 7", "icon 7", "icon 7"],
                    ["icon 5", "icon 5", "icon 6", "icon 7", "icon 7"],
                    ["icon 6", "icon 6", "icon 7", "icon 7", "icon 7"],
                    ["icon 1", "icon 5", "icon 6", "icon 7", "icon 7"],
                    ["icon 1", "icon 6", "icon 7", "icon 7", "icon 7"],
                    ["icon 5", "icon 5", "icon 6", "icon 6", "icon 7"],
                    ["icon 5", "icon 6", "icon 7", "icon 7", "icon 7"],
                    ["icon 5", "icon 5", "icon 6", "icon 6", "icon 7"],
                    ["icon 5", "icon 5", "icon 6", "icon 6", "icon 7"],
                    ["icon 1", "icon 6", "icon 7", "icon 7", "icon 7"],
                    ["icon 7", "icon 7", "icon 7", "icon 7", "icon 7"],
                    ["icon 5", "icon 5", "icon 6", "icon 6", "icon 7"],
                    ["icon 6", "icon 7", "icon 7", "icon 7", "icon 7"],
                    ["icon 5", "icon 6", "icon 6", "icon 7", "icon 7"],
                    ["icon 1", "icon 5", "icon 7", "icon 7", "icon 7"],
                    ["icon 1", "icon 6", "icon 6", "icon 6", "icon 7"],
                    ["icon 5", "icon 7", "icon 7", "icon 7", "icon 7"]
                ]
            },
            {
                "type": 202,
                "free_spins": 0,
                "spec_prize": "JACKPOT",
                "chips": 1371762,
                "jackpot": 500000,
                "result_full": ["icon 6", "icon 1", "icon 4", "icon 4", "icon 5", "icon 1", "icon 1", "icon 1", "icon 1", "icon 1", "icon 4", "icon 6", "icon 6", "icon 5", "icon 6"],
                "win_icon": [{
                    "line_num": 2,
                    "icon": "5 icon 1",
                    "value": "5000"
                }, {
                    "line_num": 3,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 4,
                    "icon": "4 icon 1",
                    "value": "30"
                }, {
                    "line_num": 5,
                    "icon": "4 icon 1",
                    "value": "30"
                }, {
                    "line_num": 10,
                    "icon": "3 icon 1",
                    "value": "5"
                }, {
                    "line_num": 13,
                    "icon": "3 icon 1",
                    "value": "5"
                }, {
                    "line_num": 14,
                    "icon": "4 icon 1",
                    "value": "30"
                }, {
                    "line_num": 15,
                    "icon": "3 icon 1",
                    "value": "5"
                }, {
                    "line_num": 16,
                    "icon": "3 icon 1",
                    "value": "5"
                }, {
                    "line_num": 17,
                    "icon": "3 icon 1",
                    "value": "5"
                }],
                "pay_lines": [{
                    "line_num": 2,
                    "icon": "5 icon 1",
                    "value": "5000"
                }, {
                    "line_num": 3,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 4,
                    "icon": "4 icon 1",
                    "value": "30"
                }, {
                    "line_num": 5,
                    "icon": "4 icon 1",
                    "value": "30"
                }, {
                    "line_num": 10,
                    "icon": "3 icon 1",
                    "value": "5"
                }, {
                    "line_num": 13,
                    "icon": "3 icon 1",
                    "value": "5"
                }, {
                    "line_num": 14,
                    "icon": "4 icon 1",
                    "value": "30"
                }, {
                    "line_num": 15,
                    "icon": "3 icon 1",
                    "value": "5"
                }, {
                    "line_num": 16,
                    "icon": "3 icon 1",
                    "value": "5"
                }, {
                    "line_num": 17,
                    "icon": "3 icon 1",
                    "value": "5"
                }],
                "win_chips": 1280337,
                "total_prize": 5117,
                "result_line": [
                    ["icon 1", "icon 4", "icon 4", "icon 5", "icon 6"],
                    ["icon 1", "icon 1", "icon 1", "icon 1", "icon 1"],
                    ["icon 4", "icon 5", "icon 6", "icon 6", "icon 6"],
                    ["icon 1", "icon 1", "icon 1", "icon 1", "icon 4"],
                    ["icon 1", "icon 1", "icon 1", "icon 1", "icon 6"],
                    ["icon 1", "icon 1", "icon 4", "icon 5", "icon 6"],
                    ["icon 1", "icon 4", "icon 5", "icon 6", "icon 6"],
                    ["icon 4", "icon 5", "icon 5", "icon 6", "icon 6"],
                    ["icon 1", "icon 4", "icon 4", "icon 6", "icon 6"],
                    ["icon 1", "icon 1", "icon 1", "icon 4", "icon 6"],
                    ["icon 1", "icon 1", "icon 4", "icon 4", "icon 6"],
                    ["icon 1", "icon 1", "icon 5", "icon 6", "icon 6"],
                    ["icon 1", "icon 1", "icon 1", "icon 4", "icon 6"],
                    ["icon 1", "icon 1", "icon 1", "icon 1", "icon 5"],
                    ["icon 1", "icon 1", "icon 1", "icon 4", "icon 6"],
                    ["icon 1", "icon 1", "icon 1", "icon 4", "icon 4"],
                    ["icon 1", "icon 1", "icon 1", "icon 5", "icon 6"],
                    ["icon 1", "icon 1", "icon 5", "icon 6", "icon 6"],
                    ["icon 1", "icon 4", "icon 4", "icon 5", "icon 6"],
                    ["icon 1", "icon 1", "icon 5", "icon 6", "icon 6"]
                ]
            },
            {
                "type": 202,
                "free_spins": 0,
                "spec_prize": "BIGWIN",
                "chips": 2330262,
                "jackpot": 23002450,
                "result_full": ["icon 6", "icon 6", "icon 5", "icon 6", "icon 6", "icon 6", "icon 1", "icon 6", "icon 4", "icon 5", "icon 1", "icon 5", "icon 4", "icon 5", "icon 1"],
                "win_icon": [{
                    "line_num": 1,
                    "icon": "4 icon 6",
                    "value": "10"
                }, {
                    "line_num": 6,
                    "icon": "5 icon 6",
                    "value": "75"
                }, {
                    "line_num": 8,
                    "icon": "3 icon 5",
                    "value": "3"
                }, {
                    "line_num": 10,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 11,
                    "icon": "3 icon 1",
                    "value": "5"
                }, {
                    "line_num": 13,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 14,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 15,
                    "icon": "3 icon 1",
                    "value": "5"
                }, {
                    "line_num": 16,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 17,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 18,
                    "icon": "3 icon 5",
                    "value": "3"
                }, {
                    "line_num": 19,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 20,
                    "icon": "3 icon 6",
                    "value": "2"
                }],
                "pay_lines": [{
                    "line_num": 1,
                    "icon": "4 icon 6",
                    "value": "10"
                }, {
                    "line_num": 6,
                    "icon": "5 icon 6",
                    "value": "75"
                }, {
                    "line_num": 8,
                    "icon": "3 icon 5",
                    "value": "3"
                }, {
                    "line_num": 10,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 11,
                    "icon": "3 icon 1",
                    "value": "5"
                }, {
                    "line_num": 13,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 14,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 15,
                    "icon": "3 icon 1",
                    "value": "5"
                }, {
                    "line_num": 16,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 17,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 18,
                    "icon": "3 icon 5",
                    "value": "3"
                }, {
                    "line_num": 19,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 20,
                    "icon": "3 icon 6",
                    "value": "2"
                }],
                "win_chips": 1150000,
                "total_prize": 115,
                "result_line": [
                    ["icon 5", "icon 6", "icon 6", "icon 6", "icon 6"],
                    ["icon 1", "icon 4", "icon 5", "icon 6", "icon 6"],
                    ["icon 1", "icon 1", "icon 4", "icon 5", "icon 5"],
                    ["icon 1", "icon 4", "icon 5", "icon 5", "icon 6"],
                    ["icon 1", "icon 4", "icon 4", "icon 5", "icon 6"],
                    ["icon 6", "icon 6", "icon 6", "icon 6", "icon 6"],
                    ["icon 1", "icon 1", "icon 5", "icon 5", "icon 6"],
                    ["icon 5", "icon 5", "icon 5", "icon 6", "icon 6"],
                    ["icon 1", "icon 1", "icon 4", "icon 6", "icon 6"],
                    ["icon 4", "icon 5", "icon 6", "icon 6", "icon 6"],
                    ["icon 1", "icon 1", "icon 1", "icon 4", "icon 5"],
                    ["icon 1", "icon 4", "icon 4", "icon 6", "icon 6"],
                    ["icon 5", "icon 5", "icon 6", "icon 6", "icon 6"],
                    ["icon 5", "icon 5", "icon 6", "icon 6", "icon 6"],
                    ["icon 1", "icon 1", "icon 1", "icon 4", "icon 6"],
                    ["icon 5", "icon 5", "icon 6", "icon 6", "icon 6"],
                    ["icon 1", "icon 4", "icon 6", "icon 6", "icon 6"],
                    ["icon 4", "icon 5", "icon 5", "icon 5", "icon 6"],
                    ["icon 1", "icon 5", "icon 6", "icon 6", "icon 6"],
                    ["icon 1", "icon 5", "icon 6", "icon 6", "icon 6"]
                ]
            },
            {
                "type": 202,
                "free_spins": 1,
                "spec_prize": "FREESPIN",
                "chips": 1373862,
                "jackpot": 500420,
                "result_full": ["icon 6", "icon 5", "icon 6", "icon 6", "icon 3", "icon 6", "icon 5", "icon 6", "icon 5", "icon 6", "icon 3", "icon 3", "icon 1", "icon 5", "icon 5"],
                "win_icon": [{
                    "line_num": 1,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 2,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 4,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 6,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 10,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 11,
                    "icon": "3 icon 5",
                    "value": "3"
                }, {
                    "line_num": 13,
                    "icon": "4 icon 6",
                    "value": "10"
                }, {
                    "line_num": 14,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 15,
                    "icon": "3 icon 5",
                    "value": "3"
                }, {
                    "line_num": 16,
                    "icon": "4 icon 6",
                    "value": "10"
                }, {
                    "line_num": 19,
                    "icon": "3 icon 3",
                    "value": "20"
                }, {
                    "line_num": 20,
                    "icon": "3 icon 5",
                    "value": "3"
                }],
                "pay_lines": [{
                    "line_num": 1,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 2,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 4,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 6,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 10,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 11,
                    "icon": "3 icon 5",
                    "value": "3"
                }, {
                    "line_num": 13,
                    "icon": "4 icon 6",
                    "value": "10"
                }, {
                    "line_num": 14,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 15,
                    "icon": "3 icon 5",
                    "value": "3"
                }, {
                    "line_num": 16,
                    "icon": "4 icon 6",
                    "value": "10"
                }, {
                    "line_num": 19,
                    "icon": "3 icon 3",
                    "value": "20"
                }, {
                    "line_num": 20,
                    "icon": "3 icon 5",
                    "value": "3"
                }],
                "win_chips": 4100,
                "total_prize": 61,
                "result_line": [
                    ["icon 3", "icon 5", "icon 6", "icon 6", "icon 6"],
                    ["icon 5", "icon 5", "icon 6", "icon 6", "icon 6"],
                    ["icon 1", "icon 3", "icon 3", "icon 5", "icon 5"],
                    ["icon 5", "icon 5", "icon 6", "icon 6", "icon 6"],
                    ["icon 1", "icon 5", "icon 5", "icon 6", "icon 6"],
                    ["icon 3", "icon 5", "icon 6", "icon 6", "icon 6"],
                    ["icon 3", "icon 3", "icon 5", "icon 5", "icon 6"],
                    ["icon 3", "icon 3", "icon 5", "icon 6", "icon 6"],
                    ["icon 1", "icon 3", "icon 5", "icon 5", "icon 6"],
                    ["icon 1", "icon 5", "icon 6", "icon 6", "icon 6"],
                    ["icon 3", "icon 5", "icon 5", "icon 5", "icon 6"],
                    ["icon 1", "icon 3", "icon 5", "icon 5", "icon 6"],
                    ["icon 3", "icon 6", "icon 6", "icon 6", "icon 6"],
                    ["icon 5", "icon 5", "icon 6", "icon 6", "icon 6"],
                    ["icon 3", "icon 5", "icon 5", "icon 5", "icon 6"],
                    ["icon 5", "icon 6", "icon 6", "icon 6", "icon 6"],
                    ["icon 3", "icon 5", "icon 5", "icon 6", "icon 6"],
                    ["icon 1", "icon 3", "icon 5", "icon 6", "icon 6"],
                    ["icon 3", "icon 3", "icon 3", "icon 6", "icon 6"],
                    ["icon 5", "icon 5", "icon 5", "icon 6", "icon 6"]
                ]
            },
            {
                "type": 202,
                "free_spins": 0,
                "spec_prize": "BONUS",
                "chips": 1374262,
                "jackpot": 500670,
                "result_full": ["icon 3", "icon 7", "icon 6", "icon 6", "icon 4", "icon 6", "icon 2", "icon 4", "icon 2", "icon 5", "icon 5", "icon 6", "icon 2", "icon 7", "icon 6"],
                "win_icon": [{
                    "line_num": 5,
                    "icon": "3 icon 2",
                    "value": "100"
                }, {
                    "line_num": 12,
                    "icon": "3 icon 2",
                    "value": "100"
                }, {
                    "line_num": 13,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 16,
                    "icon": "3 icon 6",
                    "value": "2"
                }],
                "pay_lines": [{
                    "line_num": 5,
                    "icon": "3 icon 2",
                    "value": "100"
                }, {
                    "line_num": 12,
                    "icon": "3 icon 2",
                    "value": "100"
                }, {
                    "line_num": 13,
                    "icon": "3 icon 6",
                    "value": "2"
                }, {
                    "line_num": 16,
                    "icon": "3 icon 6",
                    "value": "2"
                }],
                "win_chips": 400,
                "total_prize": 204,
                "result_line": [
                    ["icon 3", "icon 4", "icon 6", "icon 6", "icon 7"],
                    ["icon 2", "icon 2", "icon 4", "icon 5", "icon 6"],
                    ["icon 2", "icon 5", "icon 6", "icon 6", "icon 7"],
                    ["icon 2", "icon 2", "icon 5", "icon 6", "icon 6"],
                    ["icon 2", "icon 2", "icon 2", "icon 5", "icon 6"],
                    ["icon 3", "icon 4", "icon 4", "icon 6", "icon 7"],
                    ["icon 4", "icon 5", "icon 6", "icon 6", "icon 7"],
                    ["icon 3", "icon 4", "icon 6", "icon 6", "icon 7"],
                    ["icon 2", "icon 5", "icon 6", "icon 6", "icon 7"],
                    ["icon 2", "icon 5", "icon 6", "icon 6", "icon 7"],
                    ["icon 2", "icon 2", "icon 5", "icon 6", "icon 6"],
                    ["icon 2", "icon 2", "icon 2", "icon 3", "icon 4"],
                    ["icon 4", "icon 5", "icon 6", "icon 6", "icon 6"],
                    ["icon 4", "icon 5", "icon 6", "icon 7", "icon 7"],
                    ["icon 2", "icon 2", "icon 4", "icon 5", "icon 6"],
                    ["icon 5", "icon 6", "icon 6", "icon 6", "icon 7"],
                    ["icon 2", "icon 2", "icon 3", "icon 4", "icon 4"],
                    ["icon 2", "icon 5", "icon 6", "icon 6", "icon 7"],
                    ["icon 4", "icon 4", "icon 5", "icon 6", "icon 6"],
                    ["icon 3", "icon 4", "icon 6", "icon 7", "icon 7"]
                ],
                "bonus_num": 2,
                "desc": "current_is_free_spin"
            }
        ];

        WebsocketClient.OnSubscribeGameDone = this.OnSubscribeGameDone;
        WebsocketClient.OnJoinLobbyDone = this.OnJoinLobbyDone;
        WebsocketClient.OnSubscribeLobby = this.OnSubscribeLobby;
        WebsocketClient.OnGetRoomsDone = this.OnGetRoomsDone;
        WebsocketClient.OnJoinRoomsDone = this.OnJoinRoomsDone;
        WebsocketClient.OnSlotStartDone = this.OnSlotStartDone;
        WebsocketClient.OnSlotGetUserHistoryDone = this.OnSlotGetUserHistoryDone;
        WebsocketClient.OnSlotGetGloryDone = this.OnSlotGetGloryDone;
        WebsocketClient.OnJackpotChange = this.OnJackpotChange;
        WebsocketClient.OnNotifyAll = this.OnNotifyAll;
        WebsocketClient.OnSendFeedBack = this.OnSendFeedBack;
        WebsocketClient.OnMoneyChangeDone = this.OnMoneyChangeDone;
        WebsocketClient.OnLoginOnOtherDevice2 = this.OnLoginOnOtherDevice2;
        WebsocketClient.OnDisconnect2 = this.OnDisconnect2;


        this.check_OnSubscribeGameDone = -1;
        this.check_OnJoinLobbyDone = -1;
        this.check_OnSubscribeLobby = -1;
        this.check_OnGetRoomsDone = -1;
        this.check_OnJoinRoomsDone = -1;
        this.check_OnSlotStartDone = -1;
        this.check_OnSlotGetUserHistoryDone = -1;
        this.check_OnSlotGetGloryDone = -1;

        var is16_9 = 1;
        var is18_9 = 2;
        var is185_9 = 3;
        var is19_9 = 4;
        var is195_9 = 5;
        var is20_9 = 6;
        var is4_3 = 7;

        var size = 1;
        cc.log("GGG Screen : ", GM.Screen);
        switch (GM.Screen) {
            case is4_3:
                this.Icon_Char.active = false;
                this.UI_Sail.scale = 1.25;
                this.UI_Sail.getComponent(cc.Widget).enabled = false;
                this.UI_Sail.position = cc.v2(-90, -30);
                this.Icon_Setting.getComponent(cc.Widget).enabled = false;
                this.Icon_Rank.getComponent(cc.Widget).enabled = false;
                this.Icon_Guide.getComponent(cc.Widget).enabled = false;
                this.Icon_History.getComponent(cc.Widget).enabled = false;
                var x = this.Icon_Setting.x;
                this.Icon_Setting.position = cc.v2(x, 560);
                this.Icon_Guide.position = cc.v2(x - 120, 560);
                this.Icon_Rank.position = cc.v2(x - 240, 560);
                this.Icon_History.position = cc.v2(x - 360, 560);
                break;
            case is16_9:
                size = 1;
                break;
            case is18_9:
                size = 0.88;
                this.UI_Select.children[1].y += 10;
                this.Icon_Char.getComponent(cc.Widget).enabled = false;
                var x = this.Icon_Char.x;
                var y = this.Icon_Char.y;
                this.Icon_Char.position = cc.v2(x + 50, y);
                break;
            case is185_9:
                size = 0.86;
                this.UI_Select.children[1].y += 15;
                this.Icon_Char.getComponent(cc.Widget).enabled = false;
                var x = this.Icon_Char.x;
                var y = this.Icon_Char.y;
                this.Icon_Char.position = cc.v2(x + 65, y);
                break;
            case is19_9:
                size = 0.83;
                this.UI_Select.children[1].y += 20;
                this.Icon_Char.getComponent(cc.Widget).enabled = false;
                var x = this.Icon_Char.x;
                var y = this.Icon_Char.y;
                this.Icon_Char.position = cc.v2(x + 85, y);
                break;
            case is195_9:
                size = 0.81;
                this.UI_Select.children[1].y += 25;
                this.Icon_Char.scale = size;
                this.Icon_Char.getComponent(cc.Widget).enabled = false;
                var x = this.Icon_Char.x;
                var y = this.Icon_Char.y;
                this.Icon_Char.position = cc.v2(x + 100, y - 100);
                break;
            case is20_9:
                size = 0.76;
                this.UI_Select.children[1].y += 30;
                this.Icon_Char.getComponent(cc.Widget).enabled = false;
                var x = this.Icon_Char.x;
                var y = this.Icon_Char.y;
                this.Icon_Char.position = cc.v2(x + 100, y - 100);
                break;
            default:
                break;
        }

        if (GM.Screen != is4_3) {
            this.UI_Sail.scale = size;
            this.UI_Pedestal.scale = size;
            this.Popup_Rank.scale = size;
            this.Popup_Guide.scale = size;
            this.Popup_History.scale = size;
            this.Popup_Line_Bet.scale = size;
            this.UI_Dao_Vang.scale = size;
        }
    },

    start() {
        Slot4.scope.canExit = true;
        this.Pack_ID = -1;
        this.subSlotGame();

        this.create_Star();
        if (GM.instance.music_Slot_4_State === 1) {
            this.remote_music_background = cc.audioEngine.play(this.music_background, true, 1);
        }
        this.Light_Anim(this.Light_Logo, this.Mask_Logo, 0.04, 2);
        this.Light_Anim(this.Light_Chest, this.Mask_Chest, 0.2, 1);
        this.Light_Anim(this.Light_Spin, this.Mask_Spin, 0.02, 5);

        Slot4.scope.keepAlive = setInterval(function () {
            if (WarpConst.GameBase.userInfo != null) {
                cc.log("keepAlive");
                var str = {};
                var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
                m[1] = WarpConst.WarpRequestTypeCode.SEND_KEEP_ALIVE;
                WebsocketClient.Socket.send(m);
            }
        }, 3000);
    },

    create_Star() {
        for (let index = 0; index < this.Star_Storage.childrenCount; index++) {
            this.Star_Storage.children[index].scale = 0;
            this.Star_Storage.children[index].getComponent("Slot_4_Star").init_Star(1000 * index);
        }

        for (let index = 0; index < this.Star_Logo.childrenCount; index++) {
            this.Star_Logo.children[index].scale = 0;
            this.Star_Logo.children[index].getComponent("Slot_4_Star").init_Star(1000 * index);
        }
    },

    // new Add 25/03/2019
    onDestroy() {
        WebsocketClient.OnSubscribeGameDone = null;
        WebsocketClient.OnJoinLobbyDone = null;
        WebsocketClient.OnSubscribeLobby = null;
        WebsocketClient.OnGetRoomsDone = null;
        WebsocketClient.OnJoinRoomsDone = null;
        WebsocketClient.OnSlotStartDone = null;
        WebsocketClient.OnSlotGetUserHistoryDone = null;
        WebsocketClient.OnSlotGetGloryDone = null;
        WebsocketClient.OnJackpotChange = null;
        WebsocketClient.OnNotifyAll = null;
        WebsocketClient.OnSendFeedBack = null;
        WebsocketClient.OnMoneyChangeDone = null;
        WebsocketClient.OnLoginOnOtherDevice2 = null;
        WebsocketClient.OnDisconnect2 = null;
    },

    update(dt) {
        if (this.Unpack_Counting) {
            this.Unpack_Time -= dt;
            this.setTimeCounter_Unpack();
        }
    },


    OnNotifyAll(jsonStr) {
        var data = JSON.parse(jsonStr);
        var game = decodeURIComponent(escape(data.gameName));
        switch (game) {
            case "MINI_SLOT":
                game = "DINO EGG";
                break;
            case "SLOT":
                game = "PIRATES";
                break;
            case "SLOT_2":
                game = "NOTHING TO FEAR";
                break;
            case "SLOT_3":
                game = "AQUAMEN";
                break;
            case "SLOT_4":
                game = "ALADDIN'S ADVENTURE";
                break;
            case "MINI_POKER":
                game = "MINI POKER";
                break;
            case "MINI_SLOT_2":
                game = "HALLOWEEN";
                break;
            default:
                break;
        }
        Slot4.scope.notifyAllText.string = "<color=#ffffff> Chúc mừng người chơi</c> <color=#1bdd00> " + decodeURIComponent(escape(data.userName)) + " </c> <color=#ffffff> đã nổ hũ game</c> <color=#ffc800> " + game + "</c> <color=#ffffff> trị giá</c> <color=#ffc800> " + GM.instance.convert_money(data.jackpot) + " </c> <color=#ffffff> Gold </c>";
        if (Slot4.scope.JacpotAnimRunning !== null) {
            clearTimeout(Slot4.scope.JacpotAnimRunning);
        }
        Slot4.scope.notifyAllAnim.active = true;
        Slot4.scope.JacpotAnimRunning = setTimeout(function () {
            Slot4.scope.notifyAllAnim.active = false;
            Slot4.scope.JacpotAnimRunning = null;
        }, 3000);
    },

    Choose_Mode(event, index) {
        var id = parseInt(index);
        cc.log("Choose_Mode : ", index);
        if (id === 0) {
            this.Game_Room_ID = 0;
            this.joinRoom(10000);
        } else {
            this.Game_Room_ID = 1;
            this.joinRoom(id);
        }
    },

    Play_Game(bet) {
        GM.Stay = 4;
        cc.log("Clicked");
        cc.log(this.UI_Game_Play.active);
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.Spin_Bet = bet;
        this.Free_Spin_Total = this.User_Info.freeSpin;
        this.User_Gold = this.User_Info.users[0].gold;

        GM.Gold_Lastest = this.User_Gold;

        this.UI_Select.active = false;
        this.UI_Game_Play.active = true;
        this.Node_Use_2_Stop_Anims.stopAllActions();
        this.Bet_Line_Count = 20;
        this.label_Bet_Line.string = this.Bet_Line_Count;
        this.Line_Selected_Matrix_1x20 = [];
        for (let index = 0; index < 20; index++) {
            this.Line_Selected_Matrix_1x20.push(index + 1);
        }

        if (bet === 0) {
            this.label_Bet_Price.string = " Thử ";
        } else {
            this.label_Bet_Price.string = GM.instance.convert_money(bet);
        }
        this.label_Total_Bet_Price.string = GM.instance.convert_money(this.Bet_Line_Count * bet);

        for (let index = 0; index < 20; index++) {
            this.Boom_List_Line.children[index].getComponent(cc.Sprite).spriteFrame = this.sprite_Bom_List_Line[0];
        }

        this.label_Free.string = this.Free_Spin_Total;
        this.label_User_Gold.string = GM.instance.convert_money(this.User_Gold);
        this.Last_Jackpot = this.User_Info.room.funds;
        this.effect_Update_Gold(0, this.User_Info.room.funds, this.node_Jackpot, this.label_Jackpot);
    },

    Back_2_G_Main() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }

        if (!this.Spining && !this.SPIN_AUTO_ONLINE && this.canExit) {
            this.Node_Use_2_Stop_Anims.stopAllActions();
            clearInterval(Slot4.scope.keepAlive);
            GM.Stay = 0;
            for (let index = 0; index < this.Star_Storage.childrenCount; index++) {
                this.Star_Storage.children[index].getComponent("Slot_4_Star").remove_Star();
            }

            for (let a = 0; a < this.Star_Logo.childrenCount; a++) {
                this.Star_Logo.children[a].getComponent("Slot_4_Star").remove_Star();
            }
            cc.audioEngine.stopAll();

            var data = "Lobby";
            cc.director.preloadScene(
                data,
                function (completedCount, totalCount, item) {},
                function () {
                    cc.director.loadScene(data);
                }
            );
        } else {
            this.Show_Popup_Connect_Error(" Ván đấu chưa kết thúc ! ", " ");
        }
    },

    Show_Hide_Setting() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        var check = this.UI_Setting.active;
        this.UI_Setting.active = !check;
        if (!check) {
            if (GM.instance.music_Slot_4_State === 0) {
                this.Setting_Music_Off.active = true;
            } else {
                this.Setting_Music_Off.active = false;
            }

            if (GM.instance.sound_Slot_4_State === 0) {
                this.Setting_Sound_Off.active = true;
            } else {
                this.Setting_Sound_Off.active = false;
            }
        }
    },

    Light_Anim(node_light, node_mask, time_step_move, repeat_at) {
        var pos_start = node_light.position; // v2(x,y)
        var pos_x_end = node_mask.position.x + (node_mask.width / 2); // x
        node_light.runAction(
            cc.repeatForever(
                cc.sequence(
                    cc.moveBy(time_step_move, 20, -10),
                    cc.callFunc(function () {
                        if (node_light.position.x > pos_x_end * (1 + repeat_at)) {
                            node_light.position = pos_start;
                        }
                    })
                ))
        );
    },

    Setting_Music() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.Setting_Music_Off.active = !this.Setting_Music_Off.active;
        if (this.Setting_Music_Off.active) {
            cc.audioEngine.stop(this.remote_music_background);
            GM.instance.music_Slot_4_State = 0;
        } else {
            this.remote_music_background = cc.audioEngine.playMusic(this.music_background, true, 1);
            GM.instance.music_Slot_4_State = 1;
        }
        cc.sys.localStorage.setItem("music_Slot_4", "" + GM.instance.music_Slot_4_State);
    },

    Setting_Sound() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.Setting_Sound_Off.active = !this.Setting_Sound_Off.active;
        if (this.Setting_Sound_Off.active) {
            GM.instance.sound_Slot_4_State = 0;
        } else {
            GM.instance.sound_Slot_4_State = 1;
        }
        cc.sys.localStorage.setItem("sound_Slot_4", "" + GM.instance.sound_Slot_4_State);
    },

    Setting_Facebook() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        window.open("https://www.facebook.com/H%C5%A9-To-Ch%C6%A1i-Game-N%E1%BB%95-H%C5%A9-%C4%90%E1%BB%89nh-Cao-1905624156416681/");
    },
    Setting_Mess() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.Popup_Mess.active = true;
        this.Feedback_Input.string = "";
    },

    Hide_Setting_Mess() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.Popup_Mess.active = false;
    },

    Send_Feedback() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        if (this.Feedback_Input.string.length == 0) {
            return;
        }

        var str = {
            userId: WarpConst.GameBase.userInfo.desc.id,
            clientVersion: WarpConst.GameBase.clientVersion,
            platform: WarpConst.GameBase.platform,
            model: WarpConst.GameBase.model,
            type: 3,
            content: this.Feedback_Input.string
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.FEEDBACK;
        WebsocketClient.Socket.send(m);
        cc.log(this.Feedback_Input.string);
    },

    Show_Rank() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.getSlotGlory();
    },
    Show_Guide() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.Popup_Guide.active = true;
    },
    Show_History() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.getSlotUserHistory();
    },

    Close_Rank() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.Popup_Rank.active = false;
    },

    Close_Guide() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.Popup_Guide.active = false;
    },

    Close_History() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.Popup_History.active = false;
    },

    Game_Spin_Manual() {
        if (!this.Spining && !this.SPIN_AUTO_ONLINE) {
            // init Data and Spin
            this.init_Spin();
            if (this.Spin_Bet === 0) {
                this.Machine_Spin_Manual();
            }
        }
    },

    Machine_Spin_Manual() {
        var that = this;
        var list_line_win = [];
        if ("undefined" !== typeof this.Result_Spin_Server.pay_lines) {
            list_line_win = this.Result_Spin_Server.pay_lines;
        }
        cc.log("list_line_win : ", list_line_win);

        setTimeout(function () {
            // hide all Icon
            for (let x = 0; x < 3; x++) {
                that.GP_Column_1.children[x].getComponent("Slot_4_Item_Game").hide_icon();
                that.GP_Column_2.children[x].getComponent("Slot_4_Item_Game").hide_icon();
                that.GP_Column_3.children[x].getComponent("Slot_4_Item_Game").hide_icon();
                that.GP_Column_4.children[x].getComponent("Slot_4_Item_Game").hide_icon();
                that.GP_Column_5.children[x].getComponent("Slot_4_Item_Game").hide_icon();
            }

            var time_show_each_line = 2000;
            // show all line if win
            if (list_line_win.length > 0) {
                if (GM.instance.sound_Slot_4_State === 1) {
                    cc.audioEngine.play(that.sound_win, false, 1);
                }
                var spin_spec = 2000;
                var time_hide_boom_line = 3000;
                if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                    cc.log("Quay Trung Thuong");
                    spin_spec = 3000;
                    time_hide_boom_line = 4000;
                    time_show_each_line = 5000;
                    // 1 : JACKPOT    -    2 : BIGWIN    -    3 : FREESPIN    -    4 : BONUS
                    switch (that.Result_Spin_Server.spec_prize) {
                        case "JACKPOT":
                            that.Effect_Spin_Special(1, that.Result_Spin_Server.win_chips);
                            break;
                        case "BIGWIN":
                            that.Effect_Spin_Special(2, that.Result_Spin_Server.win_chips);
                            break;
                        case "FREESPIN":
                            if (that.Result_Spin_Server.win_chips > 0) {
                                that.add_Gold_Effect.active = true;
                                that.PS_Add_Gold.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                                that.add_Gold_Effect.active = true;
                                that.effect_Update_Gold(0, that.Result_Spin_Server.win_chips, that.label_Gold_Add.node, that.label_Gold_Add);
                                setTimeout(function () {
                                    that.add_Gold_Effect.active = false;
                                }, 2000);
                            }
                            break;
                        case "BONUS":
                            that.Unpack_Total_Number = that.Result_Spin_Server.bonus_num;
                            that.Open_UI_Dao_Vang();
                            break;
                        default:
                            break;
                    }
                } else {
                    cc.log("Quay Thuong");
                    if (that.Result_Spin_Server.win_chips > 0) {
                        that.add_Gold_Effect.active = true;
                        that.PS_Add_Gold.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                        that.effect_Update_Gold(0, that.Result_Spin_Server.win_chips, that.label_Gold_Add.node, that.label_Gold_Add);
                        setTimeout(function () {
                            that.add_Gold_Effect.active = false;
                        }, 2000);
                        time_show_each_line = 4000;
                    } else {
                        time_show_each_line = 2500;
                    }
                }

                if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                    if (that.Result_Spin_Server.spec_prize !== "BONUS") {
                        that.Manual_spin_spec(list_line_win, spin_spec);
                        that.Manual_time_hide_boom_line(list_line_win, time_hide_boom_line);
                    }
                } else {
                    that.Manual_spin_spec(list_line_win, spin_spec);
                    that.Manual_time_hide_boom_line(list_line_win, time_hide_boom_line);
                }
            }

            // effect show result after Spin end
            if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                if (that.Result_Spin_Server.spec_prize !== "BONUS") {
                    that.Manual_time_show_each_line(list_line_win, time_show_each_line);
                }
            } else {
                that.Manual_time_show_each_line(list_line_win, time_show_each_line);
            }

        }, 3000);
    },

    Manual_spin_spec(list_line_win, spin_spec) {
        var that = this;
        setTimeout(function () {
            for (let index = 0; index < list_line_win.length; index++) {
                var line_id = list_line_win[index].line_num;
                that.Boom_List_Line.children[line_id - 1].children[1].active = true;
                var icon_open_id = parseInt(list_line_win[index].icon.charAt(7));
                for (let x = 0; x < 5; x++) {
                    var location = that.Table_Matrix_3x5[line_id - 1][x];
                    var col_id = location % 5;
                    var child_id = Math.floor(location / 5);
                    if (col_id === 0) {
                        col_id = 5;
                        child_id -= 1;
                    }
                    switch (col_id) {
                        case 1:
                            that.GP_Column_1.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                            break;
                        case 2:
                            that.GP_Column_2.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                            break;
                        case 3:
                            that.GP_Column_3.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                            break;
                        case 4:
                            that.GP_Column_4.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                            break;
                        case 5:
                            that.GP_Column_5.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                            break;
                        default:
                            break;
                    }
                    // cc.log(col_id + ' - ' + child_id);
                }
            }

            that.Free_Spin_Total = that.Result_Spin_Server.free_spins;
            if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                if (that.Result_Spin_Server.spec_prize !== "BONUS") {
                    that.label_Total_Win.string = GM.instance.convert_money(that.Result_Spin_Server.win_chips);
                } else {
                    that.label_Total_Win.string = GM.instance.convert_money(that.Result_Spin_Server.win_chips + that.Unpack_Chest_Value);
                }
            } else {
                that.label_Total_Win.string = GM.instance.convert_money(that.Result_Spin_Server.win_chips);
            }
            that.label_Free.string = GM.instance.convert_money(that.Free_Spin_Total);
            if (that.Spin_Bet !== 0) {
                that.User_Gold = that.Result_Spin_Server.chips;
                GM.Gold_Lastest = Slot4.scope.User_Gold;
                WarpConst.GameBase.userInfo.desc.gold = Slot4.scope.User_Gold;
                that.label_User_Gold.string = GM.instance.convert_money(that.User_Gold);
            }
            that.label_Jackpot.string = GM.instance.convert_money(that.Result_Spin_Server.jackpot);
        }, spin_spec);
    },

    Manual_time_hide_boom_line(list_line_win, time_hide_boom_line) {
        var that = this;
        setTimeout(function () {
            for (let index = 0; index < list_line_win.length; index++) {
                var line_id = list_line_win[index].line_num;
                that.Boom_List_Line.children[line_id - 1].children[1].active = false;
            }
            that.Spining = false;
            cc.log("Can Spins");
            cc.log("Check Auto Spin available");
            if (that.SPIN_AUTO_ONLINE) {
                cc.log("Auto Spins Online");
                if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                    if (that.Result_Spin_Server.spec_prize !== "BONUS") {
                        that.Auto_Spin();
                    } else {
                        that.SPIN_AUTO_ONLINE = false;
                        that.Auto_Spin_State.getComponent(cc.Sprite).spriteFrame = that.auto_on_off_sprite[1];
                    }
                } else {
                    that.Auto_Spin();
                }
            }
        }, time_hide_boom_line);
    },

    Manual_time_show_each_line(list_line_win, time_show_each_line) {
        var that = this;
        this.Node_Use_2_Stop_Anims.stopAllActions();
        setTimeout(function () {
            if (list_line_win.length > 0) {
                for (let x = 0; x < 3; x++) {
                    that.GP_Column_1.children[x].getComponent("Slot_4_Item_Game").hide_icon();
                    that.GP_Column_2.children[x].getComponent("Slot_4_Item_Game").hide_icon();
                    that.GP_Column_3.children[x].getComponent("Slot_4_Item_Game").hide_icon();
                    that.GP_Column_4.children[x].getComponent("Slot_4_Item_Game").hide_icon();
                    that.GP_Column_5.children[x].getComponent("Slot_4_Item_Game").hide_icon();
                }

                // show each line eat and hide it if win
                for (let index = 0; index < list_line_win.length; index++) {
                    that.Node_Use_2_Stop_Anims.runAction(
                        cc.sequence(
                            cc.delayTime(1.5 * index),
                            cc.callFunc(function () {
                                cc.log("Vao");
                                if (!that.Spining) {
                                    var line_id = list_line_win[index].line_num;
                                    that.Boom_List_Line.children[line_id - 1].children[1].active = true;
                                    var icon_open_id = parseInt(list_line_win[index].icon.charAt(7));
                                    for (let x = 0; x < 5; x++) {
                                        var location = that.Table_Matrix_3x5[line_id - 1][x];
                                        var col_id = location % 5;
                                        var child_id = Math.floor(location / 5);
                                        if (col_id === 0) {
                                            col_id = 5;
                                            child_id -= 1;
                                        }
                                        switch (col_id) {
                                            case 1:
                                                that.GP_Column_1.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                                                break;
                                            case 2:
                                                that.GP_Column_2.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                                                break;
                                            case 3:
                                                that.GP_Column_3.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                                                break;
                                            case 4:
                                                that.GP_Column_4.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                                                break;
                                            case 5:
                                                that.GP_Column_5.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                                                break;
                                            default:
                                                break;
                                        }
                                        setTimeout(function () {
                                            that.Boom_List_Line.children[line_id - 1].children[1].active = false;
                                        }, 1000);
                                    }
                                }
                            })
                        )
                    );
                }

                // show 15 icon after show all line eat
                that.Node_Use_2_Stop_Anims.runAction(
                    cc.sequence(
                        cc.delayTime(1.5 * (list_line_win.length)),
                        cc.callFunc(function () {
                            for (let x = 0; x < 3; x++) {
                                that.GP_Column_1.children[x].getComponent("Slot_4_Item_Game").show_icon();
                                that.GP_Column_2.children[x].getComponent("Slot_4_Item_Game").show_icon();
                                that.GP_Column_3.children[x].getComponent("Slot_4_Item_Game").show_icon();
                                that.GP_Column_4.children[x].getComponent("Slot_4_Item_Game").show_icon();
                                that.GP_Column_5.children[x].getComponent("Slot_4_Item_Game").show_icon();
                            }
                            Slot4.scope.canExit = true;
                        })
                    )
                );
            } else {
                // show 15 icon after show all line eat
                for (let x = 0; x < 3; x++) {
                    that.GP_Column_1.children[x].getComponent("Slot_4_Item_Game").show_icon();
                    that.GP_Column_2.children[x].getComponent("Slot_4_Item_Game").show_icon();
                    that.GP_Column_3.children[x].getComponent("Slot_4_Item_Game").show_icon();
                    that.GP_Column_4.children[x].getComponent("Slot_4_Item_Game").show_icon();
                    that.GP_Column_5.children[x].getComponent("Slot_4_Item_Game").show_icon();
                }

                if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                    if (that.Result_Spin_Server.spec_prize !== "BONUS") {
                        that.label_Total_Win.string = GM.instance.convert_money(that.Result_Spin_Server.win_chips);
                    } else {
                        that.label_Total_Win.string = GM.instance.convert_money(that.Result_Spin_Server.win_chips + that.Unpack_Chest_Value);
                    }
                } else {
                    that.label_Total_Win.string = GM.instance.convert_money(that.Result_Spin_Server.win_chips);
                }
                that.Free_Spin_Total = that.Result_Spin_Server.free_spins;
                that.label_Free.string = GM.instance.convert_money(that.Result_Spin_Server.free_spins);
                that.label_Jackpot.string = GM.instance.convert_money(that.Result_Spin_Server.jackpot);
                if (that.Spin_Bet !== 0) {
                    that.User_Gold = that.Result_Spin_Server.chips;
                    GM.Gold_Lastest = Slot4.scope.User_Gold;
                    WarpConst.GameBase.userInfo.desc.gold = Slot4.scope.User_Gold;
                    that.label_User_Gold.string = GM.instance.convert_money(that.User_Gold);
                }

                that.Spining = false;
                Slot4.scope.canExit = true;
                cc.log("Can Spins");
                if (that.SPIN_AUTO_ONLINE) {
                    cc.log("Auto Spins Online");
                    that.Auto_Spin();
                }
            }
        }, time_show_each_line);
    },


    Game_Auto_Spin() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }

        this.SPIN_AUTO_ONLINE = !this.SPIN_AUTO_ONLINE;
        if (this.SPIN_AUTO_ONLINE) {
            this.Auto_Spin_State.getComponent(cc.Sprite).spriteFrame = this.auto_on_off_sprite[0];
        } else {
            this.Auto_Spin_State.getComponent(cc.Sprite).spriteFrame = this.auto_on_off_sprite[1];
        }

        if (!this.Spining) {
            cc.log("Pre Spin");

            this.Spining = true;
            if (this.SPIN_AUTO_ONLINE) {
                this.Auto_Spin();
            } else {
                this.Spining = false;
                Slot4.scope.canExit = true;
            }
        } else {
            cc.log("Spinning");
        }
    },

    Auto_Spin() {
        // init Data and Spin
        this.init_Spin();

        if (this.Spin_Bet === 0) {
            this.Machine_Spin_Auto();
        }
    },

    Machine_Spin_Auto() {
        var that = this;
        var list_line_win = [];
        if ("undefined" !== typeof this.Result_Spin_Server.pay_lines) {
            list_line_win = this.Result_Spin_Server.pay_lines;
        }
        cc.log("list_line_win : ", list_line_win);

        // effect show result after Spin end
        setTimeout(function () {
            // hide all Icon
            for (let x = 0; x < 3; x++) {
                that.GP_Column_1.children[x].getComponent("Slot_4_Item_Game").hide_icon();
                that.GP_Column_2.children[x].getComponent("Slot_4_Item_Game").hide_icon();
                that.GP_Column_3.children[x].getComponent("Slot_4_Item_Game").hide_icon();
                that.GP_Column_4.children[x].getComponent("Slot_4_Item_Game").hide_icon();
                that.GP_Column_5.children[x].getComponent("Slot_4_Item_Game").hide_icon();
            }

            var time_hide_boom_line = 3000;
            var time_show_all_icon = 2000;
            if (list_line_win.length > 0) {
                if (GM.instance.sound_Slot_4_State === 1) {
                    cc.audioEngine.play(that.sound_win, false, 1);
                }
                var spin_spec = 2000;
                if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                    spin_spec = 3000;
                    cc.log("Quay Trung Thuong");
                    // JACKPOT - BIGWIN - BONUS - FREESPIN
                    // 1 : JACKPOT
                    // 2 : BIGWIN
                    // 3 : FREESPIN
                    // 4 : BONUS

                    switch (that.Result_Spin_Server.spec_prize) {
                        case "JACKPOT":
                            that.Effect_Spin_Special(1, that.Result_Spin_Server.win_chips);
                            break;
                        case "BIGWIN":
                            that.Effect_Spin_Special(2, that.Result_Spin_Server.win_chips);
                            break;
                        case "FREESPIN":
                            if (that.Result_Spin_Server.win_chips > 0) {
                                that.add_Gold_Effect.active = true;
                                that.PS_Add_Gold.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                                that.effect_Update_Gold(0, that.Result_Spin_Server.win_chips, that.label_Gold_Add.node, that.label_Gold_Add);
                                setTimeout(function () {
                                    that.add_Gold_Effect.active = false;
                                }, 2000);
                            }
                            break;
                        case "BONUS":
                            that.Unpack_Total_Number = that.Result_Spin_Server.bonus_num;
                            that.Open_UI_Dao_Vang();
                            break;
                        default:
                            break;
                    }

                    time_hide_boom_line = 4000;
                    time_show_all_icon = 5000;
                } else {
                    if (that.Result_Spin_Server.win_chips > 0) {
                        that.add_Gold_Effect.active = true;
                        time_show_all_icon = 4000;
                        that.PS_Add_Gold.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                        that.effect_Update_Gold(0, that.Result_Spin_Server.win_chips, that.label_Gold_Add.node, that.label_Gold_Add);
                        setTimeout(function () {
                            that.add_Gold_Effect.active = false;
                        }, 2000);
                    } else {
                        time_show_all_icon = 2500;
                    }
                }

                // show all effect if win

                if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                    if (that.Result_Spin_Server.spec_prize !== "BONUS") {
                        that.Auto_spin_spec(list_line_win, spin_spec);
                        that.Auto_time_hide_boom_line(list_line_win, time_hide_boom_line);
                    }
                } else {
                    that.Auto_spin_spec(list_line_win, spin_spec);
                    that.Auto_time_hide_boom_line(list_line_win, time_hide_boom_line);
                }

            } else {
                if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                    if (that.Result_Spin_Server.spec_prize !== "BONUS") {
                        that.label_Total_Win.string = GM.instance.convert_money(that.Result_Spin_Server.win_chips);
                    } else {
                        that.label_Total_Win.string = GM.instance.convert_money(that.Result_Spin_Server.win_chips + that.Unpack_Chest_Value);
                    }
                } else {
                    that.label_Total_Win.string = GM.instance.convert_money(that.Result_Spin_Server.win_chips);
                }
                that.Free_Spin_Total = that.Result_Spin_Server.free_spins;
                that.label_Free.string = GM.instance.convert_money(that.Result_Spin_Server.free_spins);
                if (that.Spin_Bet !== 0) {
                    that.User_Gold = that.Result_Spin_Server.chips;
                    GM.Gold_Lastest = Slot4.scope.User_Gold;
                    WarpConst.GameBase.userInfo.desc.gold = Slot4.scope.User_Gold;
                    that.label_User_Gold.string = GM.instance.convert_money(that.User_Gold);
                }
                that.label_Jackpot.string = GM.instance.convert_money(that.Result_Spin_Server.jackpot);
            }

            // show 15 icon after show all line eat
            if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                if (that.Result_Spin_Server.spec_prize !== "BONUS") {
                    cc.log("show 15 icon after show all line eat");
                    that.Auto_time_show_all_icon(list_line_win, time_show_all_icon);
                }
            } else {
                that.Auto_time_show_all_icon(list_line_win, time_show_all_icon);
            }
        }, 3000);
    },

    Auto_spin_spec(list_line_win, spin_spec) {
        var that = this;
        setTimeout(function () {
            for (let index = 0; index < list_line_win.length; index++) {
                var line_id = list_line_win[index].line_num;
                that.Boom_List_Line.children[line_id - 1].children[1].active = true;
                var icon_open_id = parseInt(list_line_win[index].icon.charAt(7));
                for (let x = 0; x < 5; x++) {
                    var location = that.Table_Matrix_3x5[line_id - 1][x];
                    var col_id = location % 5;
                    var child_id = Math.floor(location / 5);
                    if (col_id === 0) {
                        col_id = 5;
                        child_id -= 1;
                    }
                    switch (col_id) {
                        case 1:
                            that.GP_Column_1.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                            break;
                        case 2:
                            that.GP_Column_2.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                            break;
                        case 3:
                            that.GP_Column_3.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                            break;
                        case 4:
                            that.GP_Column_4.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                            break;
                        case 5:
                            that.GP_Column_5.children[child_id].getComponent("Slot_4_Item_Game").check_show_Anim(icon_open_id - 1);
                            break;
                        default:
                            break;
                    }
                    cc.log(col_id + ' - ' + child_id);
                }
            }
        }, spin_spec);
    },

    Auto_time_hide_boom_line(list_line_win, time_hide_boom_line) {
        var that = this;
        setTimeout(function () {
            for (let index = 0; index < list_line_win.length; index++) {
                var line_id = list_line_win[index].line_num;
                that.Boom_List_Line.children[line_id - 1].children[1].active = false;
            }
            that.Free_Spin_Total = that.Result_Spin_Server.free_spins;
            if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                if (that.Result_Spin_Server.spec_prize !== "BONUS") {
                    that.label_Total_Win.string = GM.instance.convert_money(that.Result_Spin_Server.win_chips);
                } else {
                    that.label_Total_Win.string = GM.instance.convert_money(that.Result_Spin_Server.win_chips + that.Unpack_Chest_Value);
                }
            } else {
                that.label_Total_Win.string = GM.instance.convert_money(that.Result_Spin_Server.win_chips);
            }
            that.label_Free.string = GM.instance.convert_money(that.Result_Spin_Server.free_spins);
            if (that.Spin_Bet !== 0) {
                that.User_Gold = that.Result_Spin_Server.chips;
                GM.Gold_Lastest = Slot4.scope.User_Gold;
                WarpConst.GameBase.userInfo.desc.gold = Slot4.scope.User_Gold;
                that.label_User_Gold.string = GM.instance.convert_money(that.User_Gold);
            }
            that.label_Jackpot.string = GM.instance.convert_money(that.Result_Spin_Server.jackpot);
        }, time_hide_boom_line);
    },

    Auto_time_show_all_icon(list_line_win, time_show_all_icon) {
        var that = this;
        setTimeout(function () {
            for (let x = 0; x < 3; x++) {
                that.GP_Column_1.children[x].getComponent("Slot_4_Item_Game").show_icon();
                that.GP_Column_2.children[x].getComponent("Slot_4_Item_Game").show_icon();
                that.GP_Column_3.children[x].getComponent("Slot_4_Item_Game").show_icon();
                that.GP_Column_4.children[x].getComponent("Slot_4_Item_Game").show_icon();
                that.GP_Column_5.children[x].getComponent("Slot_4_Item_Game").show_icon();
            }
            that.Spining = false;
            Slot4.scope.canExit = true;
            cc.log("Can Spins");
            if (that.SPIN_AUTO_ONLINE) {
                if ("undefined" !== typeof that.Result_Spin_Server.spec_prize) {
                    if (that.Result_Spin_Server.spec_prize !== "BONUS") {
                        that.Auto_Spin();
                    } else {
                        that.SPIN_AUTO_ONLINE = false;
                        that.Auto_Spin_State.getComponent(cc.Sprite).spriteFrame = that.auto_on_off_sprite[1];
                    }
                } else {
                    that.Auto_Spin();
                }
            }
        }, time_show_all_icon);
    },

    init_Spin() {
        var line_bets = [];
        for (let index = 0; index < this.Line_Selected_Matrix_1x20.length; index++) {
            if (this.Line_Selected_Matrix_1x20[index] > 0) {
                line_bets.push(this.Line_Selected_Matrix_1x20[index]);
            }
        }

        if (this.Spin_Bet === 0) {
            // Trial Mode
            if (this.Free_Spin_Total > 0) {
                cc.log("Quay = Free");
                // this.label_Free.string = this.Free_Spin_Total;
                this.Spin_Free = true;
                this.Spin();
            } else {
                cc.log("Quay Tra Phi");
                if (this.Spin_Bet === 0) {
                    cc.log("Quay thu");
                    this.Spin_Free = false;
                    this.Spin();
                } else {
                    if (this.User_Gold >= this.Spin_Bet * line_bets.length) {
                        cc.log("Quay that");
                        this.Spin_Free = false;
                        this.Spin();
                    } else {
                        cc.log("Khong Du Tien");
                        this.Show_Popup_Connect_Error(" Không đủ tiền ! ", "  ");
                        this.Spining = false;
                        Slot4.scope.canExit = true;
                        this.SPIN_AUTO_ONLINE = false;
                        this.Auto_Spin_State.getComponent(cc.Sprite).spriteFrame = this.auto_on_off_sprite[1];
                    }
                }
            }
        } else {
            // Real Mode
            if (line_bets.length > 0) {
                if (this.Free_Spin_Total > 0) {
                    cc.log("Quay = Free");
                    // this.label_Free.string = this.Free_Spin_Total;
                    this.Spin_Free = true;
                    this.Spin();
                } else {
                    cc.log("Quay Tra Phi ");
                    if (this.Spin_Bet === 0) {
                        cc.log("Quay thu");
                        this.Spin_Free = false;
                        this.Spin();
                    } else {
                        if (this.User_Gold >= this.Spin_Bet * line_bets.length) {
                            cc.log("Quay that");
                            this.Spin_Free = false;
                            this.Spin();
                        } else {
                            cc.log("Khong Du Tien");
                            this.Show_Popup_Connect_Error(" Không đủ tiền ! ", "  ");
                            this.Spining = false;
                            Slot4.scope.canExit = true;
                            this.SPIN_AUTO_ONLINE = false;
                            this.Auto_Spin_State.getComponent(cc.Sprite).spriteFrame = this.auto_on_off_sprite[1];
                        }
                    }
                }
            } else {
                cc.log("Phai chon it nhat 1 dong cuoc");
                this.Popup_Bet_0_Line.active = true;
            }
        }


    },

    Spin() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_spin, false, 1);
        }

        this.Spin_Count += 1;
        this.Spining = true;
        Slot4.scope.canExit = false;
        this.label_Total_Win.string = 0;
        this.Node_Use_2_Stop_Anims.stopAllActions();

        if (this.Spin_Count === 1) {
            for (let index = 0; index < 3; index++) {
                this.GP_Column_1.children[index].getComponent("Slot_4_Item_Game").show_shadow_spriteFrame();
                this.GP_Column_2.children[index].getComponent("Slot_4_Item_Game").show_shadow_spriteFrame();
                this.GP_Column_3.children[index].getComponent("Slot_4_Item_Game").show_shadow_spriteFrame();
                this.GP_Column_4.children[index].getComponent("Slot_4_Item_Game").show_shadow_spriteFrame();
                this.GP_Column_5.children[index].getComponent("Slot_4_Item_Game").show_shadow_spriteFrame();
            }
        } else {
            for (let index = 0; index < 3; index++) {
                this.GP_Column_1.children[index].getComponent("Slot_4_Item_Game").hide_icon();
                this.GP_Column_2.children[index].getComponent("Slot_4_Item_Game").hide_icon();
                this.GP_Column_3.children[index].getComponent("Slot_4_Item_Game").hide_icon();
                this.GP_Column_4.children[index].getComponent("Slot_4_Item_Game").hide_icon();
                this.GP_Column_5.children[index].getComponent("Slot_4_Item_Game").hide_icon();
            }
        }

        switch (this.Spin_Bet) {
            case 0:
                if (this.Spin_Free) {
                    this.Free_Spin_Total -= 1;
                }
                this.label_Free.string = this.Free_Spin_Total;
                var len_data = this.Data_Spin_Free.length;
                this.Result_Spin_Server = this.Data_Spin_Free[GM.instance.randomBetween(0, len_data - 1)];
                this.Manager_Spin_Anim();
                break;
            case 100:
            case 1000:
            case 10000:
                var line_bets = [];
                for (let index = 0; index < this.Line_Selected_Matrix_1x20.length; index++) {
                    if (this.Line_Selected_Matrix_1x20[index] > 0) {
                        line_bets.push(this.Line_Selected_Matrix_1x20[index]);
                    }
                }
                this.getSpin_Result(line_bets, this.Spin_Bet);
                break;
            default:
                break;
        }
        cc.log(this.Result_Spin_Server);
    },

    Manager_Spin_Anim() {
        var that = this;
        // init Icon result
        setTimeout(function () {
            for (let index = 0; index < 3; index++) {
                var data_col_1 = {
                    id: 5 * index,
                    icon: that.Result_Spin_Server.result_full[5 * index].charAt(5)
                }
                that.GP_Column_1.children[index].getComponent("Slot_4_Item_Game").init_item(data_col_1);

                var data_col_2 = {
                    id: 5 * index + 1,
                    icon: that.Result_Spin_Server.result_full[5 * index + 1].charAt(5)
                }
                that.GP_Column_2.children[index].getComponent("Slot_4_Item_Game").init_item(data_col_2);

                var data_col_3 = {
                    id: 5 * index + 2,
                    icon: that.Result_Spin_Server.result_full[5 * index + 2].charAt(5)
                }
                that.GP_Column_3.children[index].getComponent("Slot_4_Item_Game").init_item(data_col_3);

                var data_col_4 = {
                    id: 5 * index + 3,
                    icon: that.Result_Spin_Server.result_full[5 * index + 3].charAt(5)
                }

                that.GP_Column_4.children[index].getComponent("Slot_4_Item_Game").init_item(data_col_4);

                var data_col_5 = {
                    id: 5 * index + 4,
                    icon: that.Result_Spin_Server.result_full[5 * index + 4].charAt(5)
                }
                that.GP_Column_5.children[index].getComponent("Slot_4_Item_Game").init_item(data_col_5);
            }
        }, 1000);

        // Spin Anim
        this.BG_Spin_Manual.runAction(
            cc.sequence(
                cc.rotateBy(3, 5 * 360, 5 * 360).easing(cc.easeSineOut()), //easeSineOut  easeBackInOut
                cc.callFunc(function () {
                    cc.log("End Spin Manual");
                }))
        );

        // Scroll 5 Column
        this.GP_Column_1.runAction(
            cc.sequence(
                cc.delayTime(0.2),
                cc.moveTo(0, -410, 6000),
                cc.moveTo(2, -410, 300).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 1");
                }))
        );
        this.GP_Column_2.runAction(
            cc.sequence(
                cc.delayTime(0.4),
                cc.moveTo(0, -210, 6000),
                cc.moveTo(2, -210, 300).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 2");
                }))
        );
        this.GP_Column_3.runAction(
            cc.sequence(
                cc.delayTime(0.6),
                cc.moveTo(0, 0, 6000),
                cc.moveTo(2, 0, 300).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 3");
                }))
        );
        this.GP_Column_4.runAction(
            cc.sequence(
                cc.delayTime(0.8),
                cc.moveTo(0, 210, 6000),
                cc.moveTo(2, 210, 300).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 4");
                }))
        );
        this.GP_Column_5.runAction(
            cc.sequence(
                cc.delayTime(1),
                cc.moveTo(0, 410, 6000),
                cc.moveTo(2, 410, 300).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 5  ");
                }))
        );
    },


    Open_UI_Dao_Vang() {
        this.UI_Dao_Vang.active = true;
        this.Unpack_Time = 2 * 60;
        this.Unpack_Counting = true;
        this.setTimeCounter_Unpack();
        this.scheduleOnce(this._callback_Unpack_Timeout, 2 * 60);
        this.label_Count_Unpack_Chest.string = this.Unpack_Total_Number;
        this.canUnpack = true;
        this.Unpack_Chest_Value = 0;
        this.label_Unpack_Chest_Storage.string = 0;
    },

    setTimeCounter_Unpack() {
        this.minutes = Math.floor(this.Unpack_Time / 60);
        this.seconds = Math.floor(this.Unpack_Time % 60);
        if (this.minutes < 10) {
            this.minutes = "0" + this.minutes;
        }

        if (this.seconds < 10) {
            this.seconds = "0" + this.seconds;
        }
        this.label_Time_Unpack_Chest.string = this.minutes + ":" + this.seconds;
    },

    _callback_Unpack_Timeout() {
        cc.log("Unpack Chest Time Out");
        this.Unpack_Counting = false;
        this.label_Time_Unpack_Chest.string = "00:00";
        this.UI_Dao_Vang.active = false;
        this.SPIN_AUTO_ONLINE = false;
        this.Auto_Spin_State.getComponent(cc.Sprite).spriteFrame = this.auto_on_off_sprite[1];
        this.Spining = false;
        cc.log("Can Spins");
        this.getDaoVang(-1);
    },

    show_Bonus() {
        var that = this;
        this.Unpack_Counting = false;
        this.unscheduleAllCallbacks(); // new add to cancle timer callback _ uncheck
        this.UI_Dao_Vang.active = false;
        this.Effect_Bonus.active = true;
        this.BG_Bonus.runAction(cc.scaleTo(0, 0, 0));
        setTimeout(function () {
            that.PS_Bonus.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
        }, 150);
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_big_win, false, 1);
        }

        this.BG_Bonus.runAction(
            cc.sequence(
                cc.delayTime(0),
                cc.scaleTo(0.05, 0, 0),
                cc.scaleTo(0.05, 0.2, 0.2),
                cc.scaleTo(0.05, 0.4, 0.4),
                cc.scaleTo(0.05, 0.6, 0.6),
                cc.scaleTo(0.05, 0.8, 0.8),
                cc.scaleTo(0.05, 1, 1),
                cc.scaleTo(0.05, 1.2, 1.2),
                cc.scaleTo(0.05, 0.8, 0.8),
                cc.scaleTo(0.05, 1.2, 1.2),
                cc.scaleTo(0.05, 1, 1),
                cc.callFunc(function () {
                    that.label_Gold_Bonus.string = "0";
                    that.effect_Update_Gold(0, that.Unpack_Chest_Value, that.Node_Gold_Bonus, that.label_Gold_Bonus);
                })
            )
        );

        setTimeout(function () {
            that.Effect_Bonus.active = false;
            if (that.SPIN_AUTO_ONLINE) {
                that.Auto_spin_spec(that.Result_Spin_Server.pay_lines, 0);
                that.Auto_time_hide_boom_line(that.Result_Spin_Server.pay_lines, 1000);
                that.Auto_time_show_all_icon(that.Result_Spin_Server.pay_lines, 2000);
            } else {
                that.Manual_spin_spec(that.Result_Spin_Server.pay_lines, 0);
                that.Manual_time_hide_boom_line(that.Result_Spin_Server.pay_lines, 1000);
                that.Manual_time_show_each_line(that.Result_Spin_Server.pay_lines, 2000);
            }
            that.SPIN_AUTO_ONLINE = false;
            that.Auto_Spin_State.getComponent(cc.Sprite).spriteFrame = that.auto_on_off_sprite[1];
        }, 3000);

    },

    Anim_Unpack(pack_data) {
        cc.log("Anim_Unpack : ", pack_data);
        cc.log("this.Pack_ID : ", this.Pack_ID);

        var that = this;
        this.Unpack_Total_Number = pack_data.rush_remains;
        this.label_Count_Unpack_Chest.string = pack_data.rush_remains;
        var value = pack_data.rushed;
        this.Unpack_Chest_Value += value;

        var time_show_result = 2000;
        if (this.Pack_ID != -1) {
            this.List_Pack_Chest.children[this.Pack_ID - 1].children[0].active = false;
            this.List_Pack_Chest.children[this.Pack_ID - 1].children[1].active = true;
            setTimeout(function () {
                that.List_Pack_Chest.children[that.Pack_ID - 1].children[2].active = true;
                that.List_Pack_Chest.children[that.Pack_ID - 1].children[2].getComponent(cc.Label).string = GM.instance.convert_money(value);
            }, 200);
            setTimeout(function () {
                that.List_Pack_Chest.children[that.Pack_ID - 1].children[0].active = true;
                that.List_Pack_Chest.children[that.Pack_ID - 1].children[1].active = false;
                that.List_Pack_Chest.children[that.Pack_ID - 1].children[2].active = false;
                that.label_Unpack_Chest_Storage.string = GM.instance.convert_money(that.Unpack_Chest_Value);
                that.canUnpack = true;
            }, 1000);
        } else {
            time_show_result = 0;
        }



        if (pack_data.rush_remains === 0) {
            setTimeout(function () {
                that.show_Bonus();
            }, time_show_result);
        }
    },

    Unpack_Ruong_Vang(event, index) {
        if (this.canUnpack) {
            this.canUnpack = false;
            if (this.Unpack_Total_Number > 0) {
                this.Pack_ID = parseInt(index);
                if (this.Game_Room_ID === 0) {
                    this.Unpack_Total_Number -= 1;
                    this.Anim_Unpack(this.Fake_Data_Unpack[this.Unpack_Total_Number]);
                } else {
                    this.getDaoVang(this.Unpack_Total_Number);
                }
            }
        }
    },

    Show_Popup_Line_Bet() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        if (!this.Spining && !this.SPIN_AUTO_ONLINE) {
            this.Popup_Line_Bet.active = true;
            var List_Bet = this.Popup_Line_Bet.children[4];
            for (let index = 0; index < 20; index++) {
                const child = List_Bet.children[index];
                if (this.Line_Selected_Matrix_1x20[index] > 0) {
                    cc.log("Show Line Bet : ", index);
                    child.children[0].active = false;
                } else {
                    child.children[0].active = true;
                }
            }
        } else {
            cc.log("isSpinning, therefore you can not Click into this Button");
        }
    },

    Close_Popup_Line_Bet() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.Popup_Line_Bet.active = false;
        this.label_Bet_Line.string = this.Bet_Line_Count;
        this.label_Total_Bet_Price.string = GM.instance.convert_money(this.Bet_Line_Count * this.Spin_Bet);
        for (let index = 0; index < this.Line_Selected_Matrix_1x20.length; index++) {
            const child = this.Boom_List_Line.children[index];
            if (this.Line_Selected_Matrix_1x20[index] === -1) {
                // hide
                child.getComponent(cc.Sprite).spriteFrame = this.sprite_Bom_List_Line[1];
            } else {
                // show
                child.getComponent(cc.Sprite).spriteFrame = this.sprite_Bom_List_Line[0];
            }
        }
    },

    // show | hide line bet on List
    Select_One_Line(event, pos) {
        cc.log("Select_One_Line : ", pos);
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        var ID = parseInt(pos);
        var state = this.List_Line_Bet.children[ID - 1].children[0].active;
        this.List_Line_Bet.children[ID - 1].children[0].active = !state;
        if (!state) { //  == active
            this.Bet_Line_Count -= 1;
            this.Line_Selected_Matrix_1x20[ID - 1] = -1;
            this.Boom_List_Line.children[ID - 1].getComponent(cc.Sprite).spriteFrame = this.sprite_Bom_List_Line[1];

        } else {
            this.Bet_Line_Count += 1;
            this.Line_Selected_Matrix_1x20[ID - 1] = ID;
            this.Boom_List_Line.children[ID - 1].getComponent(cc.Sprite).spriteFrame = this.sprite_Bom_List_Line[0];
        }
        cc.log(this.Line_Selected_Matrix_1x20);
    },

    Choose_Line_Chan() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.State_Bet_Line_Chan(false);
        this.State_Bet_Line_Le(true);
        this.Bet_Line_Count = 10;
        for (let index = 0; index < this.Boom_List_Line.children.length; index++) {
            const child = this.Boom_List_Line.children[index];
            if ((index + 1) % 2 == 0) {
                child.getComponent(cc.Sprite).spriteFrame = this.sprite_Bom_List_Line[0];
                this.Line_Selected_Matrix_1x20[index] = index + 1;
            } else {
                child.getComponent(cc.Sprite).spriteFrame = this.sprite_Bom_List_Line[1];
                this.Line_Selected_Matrix_1x20[index] = -1;
            }
        }
    },

    Choose_Line_Le() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.State_Bet_Line_Chan(true);
        this.State_Bet_Line_Le(false);
        this.Bet_Line_Count = 10;

        for (let index = 0; index < this.Boom_List_Line.children.length; index++) {
            const child = this.Boom_List_Line.children[index];
            if ((index + 1) % 2 == 1) {
                child.getComponent(cc.Sprite).spriteFrame = this.sprite_Bom_List_Line[0];
                this.Line_Selected_Matrix_1x20[index] = index + 1;
            } else {
                child.getComponent(cc.Sprite).spriteFrame = this.sprite_Bom_List_Line[1];
                this.Line_Selected_Matrix_1x20[index] = -1;
            }
        }
    },

    Choose_All_Line() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.State_Bet_Line_Chan(false);
        this.State_Bet_Line_Le(false);
        this.Bet_Line_Count = 20;
        for (let index = 0; index < this.Boom_List_Line.children.length; index++) {
            const child = this.Boom_List_Line.children[index];
            child.getComponent(cc.Sprite).spriteFrame = this.sprite_Bom_List_Line[0];
            this.Line_Selected_Matrix_1x20[index] = index + 1;
        }
    },

    Delete_All_Line() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }
        this.State_Bet_Line_Chan(true);
        this.State_Bet_Line_Le(true);
        this.Bet_Line_Count = 0;
        for (let index = 0; index < this.Boom_List_Line.children.length; index++) {
            const child = this.Boom_List_Line.children[index];
            child.getComponent(cc.Sprite).spriteFrame = this.sprite_Bom_List_Line[1];
            this.Line_Selected_Matrix_1x20[index] = -1;
        }
    },

    State_Bet_Line_Chan(isVisible) {
        for (let index = 0; index < 10; index++) {
            const line = this.List_Line_Bet.children[2 * index + 1];
            line.children[0].active = isVisible;
        }
    },

    State_Bet_Line_Le(isVisible) {
        for (let index = 0; index < 10; index++) {
            const line = this.List_Line_Bet.children[2 * index];
            line.children[0].active = isVisible;
        }
    },


    // Show line when user click special boom line to view
    Show_Line_Eat(event, index) {
        var that = this;
        this.Boom_List_Line.children[index - 1].children[1].active = true;
        setTimeout(function () {
            that.Boom_List_Line.children[index - 1].children[1].active = false;
        }, 500);
    },

    Show_UI_Select() {
        if (GM.instance.sound_Slot_4_State === 1) {
            cc.audioEngine.play(this.sound_button_click, false, 1);
        }

        if (!this.Spining && !this.SPIN_AUTO_ONLINE) {
            this.UI_Select.active = true;
            this.UI_Game_Play.active = false;
            this.effect_Update_Gold(0, Slot4.scope.Server_Data_Room[0].funds, this.node_Storage_Bet_100, this.label_Storage_Bet_100);
            this.effect_Update_Gold(0, Slot4.scope.Server_Data_Room[1].funds, this.node_Storage_Bet_1k, this.label_Storage_Bet_1k);
            this.effect_Update_Gold(0, Slot4.scope.Server_Data_Room[2].funds, this.node_Storage_Bet_10k, this.label_Storage_Bet_10k);
        } else {
            cc.log("isSpinning or Auto spin state is ON, therefore you can not click into this button");
        }
    },

    effect_Update_Gold(gold_start, gold_end, node, label) {
        var that = this;
        var gold_add = gold_end - gold_start;
        label.string = gold_start;

        var steps = 40;
        var delta_gold_add = -1;

        delta_gold_add = Math.floor(gold_add / steps);

        var add = cc.sequence(
            cc.delayTime(0.03),
            cc.callFunc(function () {
                gold_start += delta_gold_add;
                label.string = GM.instance.convert_money(gold_start);
            })
        );

        node.runAction(
            cc.sequence(
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                cc.callFunc(function () {
                    gold_start = gold_end;
                    label.string = GM.instance.convert_money(gold_start);
                })
            )
        );
    },

    effect_Update_Jackpot(gold_start, gold_end, node, label) {
        cc.log("effect_Update_Jackpot");
        cc.log(label);
        var gold_add = gold_end - gold_start;
        label.string = GM.instance.convert_money(gold_start);

        var steps = 5;
        var delta_gold_add = -1;

        delta_gold_add = Math.floor(gold_add / steps);

        var add = cc.sequence(
            cc.delayTime(0.1),
            cc.callFunc(function () {
                gold_start += delta_gold_add;
                label.string = GM.instance.convert_money(gold_start);
            })
        );

        node.runAction(
            cc.sequence(
                add,
                add,
                add,
                add,
                cc.callFunc(function () {
                    cc.log(label);
                    gold_start = gold_end;
                    label.string = GM.instance.convert_money(gold_start);
                })
            )
        );
    },


    randomBetween(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    },

    Effect_Spin_Special(type, gold_add) {
        var that = this;
        switch (type) {
            case 1:
                // Jackpot
                this.Effect_No_Hu.active = true;
                this.BG_No_Hu.runAction(cc.scaleTo(0, 0, 0));
                setTimeout(function () {
                    that.PS_No_Hu.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                }, 150);
                if (GM.instance.sound_Slot_4_State === 1) {
                    cc.audioEngine.play(this.sound_jackpot, false, 1);
                }
                this.BG_No_Hu.runAction(
                    cc.sequence(
                        cc.delayTime(0),
                        cc.scaleTo(0.05, 0, 0),
                        cc.scaleTo(0.05, 0.2, 0.2),
                        cc.scaleTo(0.05, 0.4, 0.4),
                        cc.scaleTo(0.05, 0.6, 0.6),
                        cc.scaleTo(0.05, 0.8, 0.8),
                        cc.scaleTo(0.05, 1, 1),
                        cc.scaleTo(0.05, 1.2, 1.2),
                        cc.scaleTo(0.05, 0.8, 0.8),
                        cc.scaleTo(0.05, 1.2, 1.2),
                        cc.scaleTo(0.05, 1, 1),
                        cc.callFunc(function () {
                            that.label_Gold_No_Hu.string = " 0 ";
                            that.effect_Update_Gold(0, gold_add, that.Node_Gold_No_Hu, that.label_Gold_No_Hu);
                        })
                    )
                );

                setTimeout(function () {
                    that.Effect_No_Hu.active = false;
                }, 3000); // 3000
                break;
            case 2:
                // Big Win
                this.Effect_Thang_Lon.active = true;
                this.BG_Thang_Lon.runAction(cc.scaleTo(0, 0, 0));
                setTimeout(function () {
                    that.PS_Thang_Lon.getComponent("sp.Skeleton").setAnimation(0, 'animation', false);
                }, 150);
                if (GM.instance.sound_Slot_4_State === 1) {
                    cc.audioEngine.play(this.sound_big_win, false, 1);
                }
                this.BG_Thang_Lon.runAction(
                    cc.sequence(
                        cc.delayTime(0),
                        cc.scaleTo(0.05, 0, 0),
                        cc.scaleTo(0.05, 0.2, 0.2),
                        cc.scaleTo(0.05, 0.4, 0.4),
                        cc.scaleTo(0.05, 0.6, 0.6),
                        cc.scaleTo(0.05, 0.8, 0.8),
                        cc.scaleTo(0.05, 1, 1),
                        cc.scaleTo(0.05, 1.2, 1.2),
                        cc.scaleTo(0.05, 0.8, 0.8),
                        cc.scaleTo(0.05, 1.2, 1.2),
                        cc.scaleTo(0.05, 1, 1),
                        cc.callFunc(function () {
                            that.label_Gold_Thang_Lon.string = " 0 ";
                            that.effect_Update_Gold(0, gold_add, that.Node_Gold_Thang_Lon, that.label_Gold_Thang_Lon);
                        })
                    )
                );

                setTimeout(function () {
                    that.Effect_Thang_Lon.active = false;
                }, 3000);
                break;
            case 3:

                break;
            case 4:

                break;

            default:
                break;
        }
    },

    Hide_Popup_Bet_0_Line() {
        this.Popup_Bet_0_Line.active = false;
        this.SPIN_AUTO_ONLINE = false;
        this.Auto_Spin_State.getComponent(cc.Sprite).spriteFrame = this.auto_on_off_sprite[1];
        this.Spining = false;
        Slot4.scope.canExit = true;
    },

    Show_Popup_Connect_Error(content, des) {
        this.UI_Popup_Connect.active = true;
        this.label_Popup_Error_Content.string = content;
        this.label_Popup_Error_Des.string = des;
    },

    Close_Popup_Connect() {
        this.UI_Popup_Connect.active = false;
        if (this.connect_err) {
            cc.game.end();
        }
    },


    Test_Anim() {

    },


    // Connect Server
    subSlotGame() {
        //{"desc":"Success"}
        var str = {
            gameId: 4
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.SUBSCRIBE_GAME;
        WebsocketClient.Socket.send(m);
    },
    joinLobby() {
        //{"desc":"Success"}
        var str = {
            lobbyId: WarpConst.LobbyId.SLOT4
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.JOIN_LOBBY;
        WebsocketClient.Socket.send(m);
    },
    subLobby() {
        /*
        {"listBet":{"koin":[1000,2000,5000,10000,20000,50000,100000,200000,500000,1000000,2000000,5000000],"gold":[5,100,200,500,1000,2000,5000,10000,20000,50000,100000,200000]},"listBetAvailable":{"koin":[],"gold":[10000,5000,2000,1000,500,200,100,5]},"desc":"Success"}
        */
        var str = {
            lobbyId: WarpConst.LobbyId.SLOT4
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.SUBSCRIBE_LOBBY;
        WebsocketClient.Socket.send(m);
    },
    getRoom() {
        //{"rooms":[{"timeCountDown":0,"timeCountDownNew":10,"stateNew":0,"id":1,"name":null,"type":null,"chipType":1,"bet":100,"maxUsers":0,"max_player":10000,"curr_num_of_player":973,"locked":false,"started":true,"quickplay":false,"funds":3024114,"reserve":45578711},{"timeCountDown":0,"timeCountDownNew":10,"stateNew":-1,"id":2,"name":null,"type":null,"chipType":1,"bet":1000,"maxUsers":0,"max_player":10000,"curr_num_of_player":104,"locked":false,"started":true,"quickplay":false,"funds":5155784,"reserve":6619082},{"timeCountDown":0,"timeCountDownNew":10,"stateNew":0,"id":3,"name":null,"type":null,"chipType":1,"bet":10000,"maxUsers":0,"max_player":10000,"curr_num_of_player":79,"locked":false,"started":true,"quickplay":false,"funds":56855202,"reserve":9793833}]}
        var str = {
            lobbyId: WarpConst.LobbyId.SLOT4
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.GET_ROOMS;
        WebsocketClient.Socket.send(m);
    },
    joinRoom(room_bet) {
        //{"freeSpin":0,"users":[{"id":3,"username":"longtnh123","order":973,"seatOrder":973,"owner":false,"sessionId":179317170,"isPlayer":false,"remainCardCount":0,"chipChange":0,"displayName":"longtnh123","email":null,"mobile":"","address":null,"avatar":"m3","faceBookId":null,"lobbyId":18,"gender":1,"koin":100,"gold":107880,"exp":0,"expTarget":0,"level":0,"allLevel":0,"win":0,"loss":0,"draw":0,"providerCode":"nAoT2","online":false,"isReady":true,"extra":{},"isFirstLoginToday":true,"expired":false,"kicked":false,"link_fb":false,"link_acc":true,"isFirstPurchase":false,"isLikeReward":true,"isRateReward":true,"properties":{},"playedCards":[],"acquiredCards":[]}],"room":{"timeCountDown":0,"timeCountDownNew":10,"stateNew":0,"id":1,"name":null,"type":null,"chipType":1,"bet":100,"maxUsers":0,"max_player":10000,"curr_num_of_player":974,"locked":false,"started":true,"quickplay":false,"funds":3024114,"reserve":45578711}}

        //type = 1: gold| 0: koin - room bet
        var str = {
            bet: room_bet,
            type: 1
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.JOIN_ROOM;
        WebsocketClient.Socket.send(m);
    },
    getSpin_Result(line_bets, gold_bet) {
        //send: {"type":202,"deal":1,"line":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]}
        //recv: {"free_spins":0,"chips":106480,"jackpot":3031150,"result_full":["icon 4","icon 5","icon 4","icon 3","icon 5","icon 6","icon 6","icon 5","icon 7","icon 5","icon 7","icon 3","icon 7","icon 7","icon 4"],"win_icon":[{"line_num":6,"icon":"3 icon 5","value":"3"},{"line_num":14,"icon":"3 icon 5","value":"3"}],"type":202,"pay_lines":[{"line_num":6,"icon":"3 icon 5","value":"3"},{"line_num":14,"icon":"3 icon 5","value":"3"}],"win_chips":600,"total_prize":6,"result_line":[["icon 3","icon 4","icon 4","icon 5","icon 5"],["icon 5","icon 5","icon 6","icon 6","icon 7"],["icon 3","icon 4","icon 7","icon 7","icon 7"],["icon 4","icon 5","icon 6","icon 6","icon 7"],["icon 5","icon 6","icon 6","icon 7","icon 7"],["icon 3","icon 4","icon 5","icon 5","icon 5"],["icon 3","icon 4","icon 5","icon 7","icon 7"],["icon 3","icon 4","icon 4","icon 5","icon 7"],["icon 3","icon 4","icon 5","icon 7","icon 7"],["icon 3","icon 5","icon 5","icon 6","icon 7"],["icon 4","icon 4","icon 6","icon 7","icon 7"],["icon 4","icon 5","icon 6","icon 7","icon 7"],["icon 3","icon 3","icon 5","icon 5","icon 6"],["icon 5","icon 5","icon 5","icon 6","icon 7"],["icon 4","icon 5","icon 6","icon 7","icon 7"],["icon 3","icon 4","icon 5","icon 5","icon 6"],["icon 4","icon 5","icon 5","icon 6","icon 7"],["icon 3","icon 5","icon 6","icon 7","icon 7"],["icon 3","icon 3","icon 5","icon 5","icon 7"],["icon 4","icon 4","icon 5","icon 5","icon 7"]]}

        cc.log(line_bets);
        var str = {
            type: WarpConst.WarpRequestTypeCode.START_MATCH,
            deal: 1, //Quay that
            line: line_bets, //selected line
            bet: gold_bet
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.UPDATE_PEERS;
        WebsocketClient.Socket.send(m);
    },
    getSlotUserHistory() {
        var str = {
            type: WarpConst.MiniGame.GET_USER_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.UPDATE_PEERS;
        WebsocketClient.Socket.send(m);
    },
    getSlotGlory() {
        var str = {
            type: WarpConst.MiniGame.GET_JACKPOT_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.UPDATE_PEERS;
        WebsocketClient.Socket.send(m);
    },
    getDaoVang(count_left) {
        var str = {
            type: WarpConst.WarpRequestTypeCode.START_MATCH,
            deal: 2, //Đào vàng
            line: [count_left] //selected item (length of array = 1)
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.UPDATE_PEERS;
        WebsocketClient.Socket.send(m);
    },
    notify_Gold_State_Change() {
        cc.log("On notify_Gold_State_Change");
        this.effect_Update_Gold(this.User_Gold, GM.Gold_Lastest, this.label_User_Gold.node, this.label_User_Gold);
        this.User_Gold = GM.Gold_Lastest;
        WarpConst.GameBase.userInfo.desc.gold = Slot4.scope.User_Gold;
    },

    onEnable() {
        cc.log("MrGiangNo1 Slot4 onEnable");
    },

    onDisable() {
        cc.log("MrGiangNo1 Slot4 onDisable");
    }
});