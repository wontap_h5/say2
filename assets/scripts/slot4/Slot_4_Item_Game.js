cc.Class({
    extends: cc.Component,

    properties: {
        sprite_List: {
            default: [],
            type: [cc.SpriteFrame]
        },
        shadow_sprite_List: {
            default: [],
            type: [cc.SpriteFrame]
        },
        item_icon: {
            default: null,
            type: cc.Node
        },
        fx_p1: {
            default: null,
            type: cc.Node
        }
    },

    item_index: null,
    random_spriteFrame_index: null,

    // LIFE-CYCLE CALLBACKS:

    onLoad() {},

    start() {
        this.create_random_item_spriteFrame();
    },

    randomBetween(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    },

    // init as server data to this spin
    init_item(data) {
        this.item_index = parseInt(data.icon) - 1;
        cc.log(this.item_index);
        this.item_icon.getComponent(cc.Sprite).spriteFrame = this.sprite_List[this.item_index];
    },

    // create random icon for each visit game play
    create_random_item_spriteFrame() {
        this.random_spriteFrame_index = this.randomBetween(0, 6);
        // this.node.getComponent(cc.Sprite).spriteFrame = this.sprite_List[this.random_spriteFrame_index];
        this.item_icon.getComponent(cc.Sprite).spriteFrame = this.sprite_List[this.random_spriteFrame_index];
    },

    // hide icon before spin
    show_shadow_spriteFrame() {
        // this.node.getComponent(cc.Sprite).spriteFrame = this.shadow_sprite_List[this.random_spriteFrame_index];
        this.item_icon.getComponent(cc.Sprite).spriteFrame = this.shadow_sprite_List[this.random_spriteFrame_index];
    },

    // show after complete run all anim
    show_icon() {
        // this.node.getComponent(cc.Sprite).spriteFrame = this.sprite_List[this.item_index];
        this.item_icon.getComponent(cc.Sprite).spriteFrame = this.sprite_List[this.item_index];
    },

    // hide icon false for play anim 
    hide_icon() {
        // this.node.getComponent(cc.Sprite).spriteFrame = this.shadow_sprite_List[this.item_index];
        this.item_icon.getComponent(cc.Sprite).spriteFrame = this.shadow_sprite_List[this.item_index];
        
    },

    // check icon will play anim or hide when play anim
    check_show_Anim(index) {
        if (this.item_index === index) {
            this.show_Anim();
        } else {
            // this.hide_icon();
        }
    },

    show_Anim() {
        // this.node.getComponent(cc.Sprite).enabled = false;
        this.fx_p1.active = true;
        this.show_icon();
        var that = this;
        this.node.runAction(
            cc.sequence(
                cc.delayTime(1),
                cc.callFunc(function () {
                    that.fx_p1.active = false;
                    // that.node.getComponent(cc.Sprite).enabled = true;

                    that.hide_icon();
                })
            )
        );
    },

    update(dt) {},
});