var NumberTo = cc.Class({
    extends: cc.Component,

    __ctor__(target, from, to, duration) {
        this.target = target;
        this.from = from || 0;
        this.to = to || 0;
        this.duration = duration || 0;
        this.inscreeNumber = to - from;
        if(this.target && this.target.cb_schedule_numberto) {
            this.target.unschedule(this.target.cb_schedule_numberto);
        }
        this.target.cb_schedule_numberto = this.update.bind(this);
        this.target.schedule(this.target.cb_schedule_numberto, 0.016);        
    },

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
    },

    update (dt) {
        if(!this.startTime) {
            this.startTime = 0;
        }
        if(this.startTime < this.duration) {
            this.startTime += dt;
            dt = this.from + (this.inscreeNumber * (Math.min(this.startTime / this.duration, 1)));
            if(this._onUpdate) this._onUpdate(dt);
        } else {
            if(this.target && this.target.cb_schedule_numberto) {
                this.target.unschedule(this.target.cb_schedule_numberto);
            }
            if(this._onFinished) this._onFinished();
        }
    },

    onFinished(cb) {
        this._onFinished = cb;
        return this;
    },
    onUpdate(cb) {        
        this._onUpdate = cb; //mean: onUpdate(dt) = cb(n)
        return this;
    }

});
