var GM = cc.Class({
    extends: cc.Component,

    properties: {

    },

    music_Lobby_State: null,
    sound_Lobby_State: null,
    music_Pirate_State: null,
    sound_Pirate_State: null,
    music_Slot_2_State: null,
    sound_Slot_2_State: null,
    music_Slot_3_State: null,
    sound_Slot_3_State: null,
    music_Slot_4_State: null,
    sound_Slot_4_State: null,
    // LIFE-CYCLE CALLBACKS:

    statics: {
        instance: null,
        Stay: null,
        Gold_Lastest: null,
        Screen: null,
    },

    onLoad() {
        GM.instance = this;
        // Stay : 0 = Lobby, 1 = Slot 1, 2 = Slot 2
        GM.Stay = 0;
        GM.Gold_Lastest = 0;

        // Lobby
        // music_Lobby_State :   0 == OFF , 1 == ON
        var lobby_music_save = cc.sys.localStorage.getItem("music_Lobby");
        if (lobby_music_save != null) {
            this.music_Lobby_State = parseInt(lobby_music_save);
        } else {
            this.music_Lobby_State = 1;
            cc.sys.localStorage.setItem("music_Lobby", "1");
        }

        // Slot 1
        // music_Pirate_State :   0 == OFF , 1 == ON
        var pirate_music_save = cc.sys.localStorage.getItem("music_Pirate");
        if (pirate_music_save != null) {
            this.music_Pirate_State = parseInt(pirate_music_save);
        } else {
            this.music_Pirate_State = 1;
            cc.sys.localStorage.setItem("music_Pirate", "1");
        }

        // sound_Pirate_State :   0 == OFF , 1 == ON
        var pirate_sound_save = cc.sys.localStorage.getItem("sound_Pirate");
        if (pirate_sound_save != null) {
            this.sound_Pirate_State = parseInt(pirate_sound_save);
        } else {
            this.sound_Pirate_State = 1;
            cc.sys.localStorage.setItem("sound_Pirate", "1");
        }


        // Slot 2
        // music_Slot_2_State :   0 == OFF , 1 == ON
        var slot_2_music_save = cc.sys.localStorage.getItem("music_Slot_2");
        if (slot_2_music_save != null) {
            this.music_Slot_2_State = parseInt(slot_2_music_save);
        } else {
            this.music_Slot_2_State = 1;
            cc.sys.localStorage.setItem("music_Slot_2", "1");
        }

        // sound_Slot_2_State :   0 == OFF , 1 == ON
        var slot_2_sound_save = cc.sys.localStorage.getItem("sound_Slot_2");
        if (slot_2_sound_save != null) {
            this.sound_Slot_2_State = parseInt(slot_2_sound_save);
        } else {
            this.sound_Slot_2_State = 1;
            cc.sys.localStorage.setItem("sound_Slot_2", "1");
        }

        // Slot 3
        // music_Slot_3_State :   0 == OFF , 1 == ON
        var slot_3_music_save = cc.sys.localStorage.getItem("music_Slot_3");
        if (slot_3_music_save != null) {
            this.music_Slot_3_State = parseInt(slot_3_music_save);
        } else {
            this.music_Slot_3_State = 1;
            cc.sys.localStorage.setItem("music_Slot_3", "1");
        }

        // sound_Slot_3_State :   0 == OFF , 1 == ON
        var slot_3_sound_save = cc.sys.localStorage.getItem("sound_Slot_3");
        if (slot_3_sound_save != null) {
            this.sound_Slot_3_State = parseInt(slot_3_sound_save);
        } else {
            this.sound_Slot_3_State = 1;
            cc.sys.localStorage.setItem("sound_Slot_3", "1");
        }

        // Slot 4
        // music_Slot_4_State :   0 == OFF , 1 == ON
        var slot_4_music_save = cc.sys.localStorage.getItem("music_Slot_4");
        if (slot_4_music_save != null) {
            this.music_Slot_4_State = parseInt(slot_4_music_save);
        } else {
            this.music_Slot_4_State = 1;
            cc.sys.localStorage.setItem("music_Slot_4", "1");
        }

        // sound_Slot_4_State :   0 == OFF , 1 == ON
        var slot_4_sound_save = cc.sys.localStorage.getItem("sound_Slot_4");
        if (slot_4_sound_save != null) {
            this.sound_Slot_4_State = parseInt(slot_4_sound_save);
        } else {
            this.sound_Slot_4_State = 1;
            cc.sys.localStorage.setItem("sound_Slot_4", "1");
        }

    },

    start() {},

    convert_money(price) {
        // add 2 space to fix font error
        return " " + price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " ";
    },

    randomBetween(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    // update (dt) {},
});