var stringutil = {
    formatNumber(number) {
        var num = number.toFixed(0);
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    },
    formatDuration(number) {
        var num = number.toFixed(0);
        if(num <= 0) {
            return 0;
        }
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    },
    TaixiuColor: {
        TAI: 0,
        XIU: 1,
        XX1: 2,
        XX2: 3,
        XX3: 4,
        TONG: 5
    }    
}

module.exports = stringutil;