// URL check Game state
var URL_API = "http://creatorg.huto.top/api/index.php";
var URL_GAME_FAKE = "";
var URL_GAME_IP = "";

// Create Manifest
var typeUpload = "";
var setup_provider = "";
var client_version = "1.0";
// Game version = version của bản Remote-Assests trên Host 
switch (BUILD_TYPE) {
    case BUILD_FACEBOOK:
        typeUpload = "__________";
        setup_provider = "iAHya";
        break;
    case BUILD_APPLE_STORE:
        typeUpload = "applestore";
        setup_provider = "iAHya";
        client_version = "1.0";
        break;
    case BUILD_APPLE_STORE_AND_GAME_FAKE:
        typeUpload = "applestore_fake";
        setup_provider = "iAHya";
        client_version = "1.0";
        break;
    case BUILD_IPA_INSTALL:
        typeUpload = "ipa";
        setup_provider = "iAHya";
        client_version = "1.0";
        break;
    case BUILD_GOOGLE_PLAY:
        typeUpload = "googleplay";
        setup_provider = "iAHya";
        client_version = "1.0";
        break;
    case BUILD_GOOGLE_PLAY_AND_GAME_FAKE:
        typeUpload = "googleplay_fake";
        setup_provider = "oYasO"; // ok
        client_version = "3.0"; // 1.0 = init, 2.0 = ON features
        break;
    case BUILD_APK_INSTALL:
        typeUpload = "apk";
        setup_provider = "iAHya"; // ok
        client_version = "2.0"; // 1.0 = init
        break;
    case BUILD_WEB_MOBILE:
        typeUpload = "__________";
        setup_provider = "iAHya";
        break;
    default:
        break;
}
// Game version = version của bản Update

var Global_Game_Custom_Manifest = JSON.stringify({
    "packageUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/game/remote-assets/",
    "remoteManifestUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/game/remote-assets/project.manifest",
    "remoteVersionUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/game/remote-assets/version.manifest",
    "version": client_version,
    "assets": {},
    "searchPaths": []
});

var Splash = cc.Class({
    extends: cc.Component,

    properties: {
        loadingProgressBar: {
            default: null,
            type: cc.ProgressBar
        },
        label_Update_Version: {
            default: null,
            type: cc.RichText
        },
        label_Update_Progress: {
            default: null,
            type: cc.Label
        },
        label_Current_Version: {
            default: null,
            type: cc.Label
        },
        logo: {
            default: null,
            type: cc.Node
        },
        background: {
            default: null,
            type: cc.Node
        },
        sprite_Logo: {
            default: [],
            type: [cc.SpriteFrame]
        },
        sprite_BG: {
            default: [],
            type: [cc.SpriteFrame]
        },
        behind_logo: {
            default: null,
            type: cc.Node
        }
    },

    statics: {
        scope: null,
    },
    Global_Game_StoragePath: '',
    creator_current_version: '',
    creator_new_version: '',

    CheckRequestFail: null,
    TimeOutCheckBadRequest: null,

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        Splash.scope = this;

        this.behind_logo.active = false;

        var s_time = 0.5;

        var logo_id = 0;

        var s_from = 0;
        var s_to = 0;

        switch (BUILD_TYPE) {
            case BUILD_APPLE_STORE:
                // Unknown 
                logo_id = 1;
                s_from = 0.9;
                s_to = 1;
                break;
            case BUILD_GOOGLE_PLAY:
                // iSay
                this.behind_logo.active = true;
                this.logo.x = 15;
                this.logo.y = -55;
                logo_id = 2;
                s_from = 0.7;
                s_to = 0.75;
                break;
            case BUILD_FACEBOOK:
            case BUILD_WEB_MOBILE:
            case BUILD_APK_INSTALL:
            case BUILD_IPA_INSTALL:
                // Say2 
                logo_id = 1;
                s_from = 0.9;
                s_to = 1;
                break;
            case BUILD_APPLE_STORE_AND_GAME_FAKE:
            case BUILD_GOOGLE_PLAY_AND_GAME_FAKE:
                // OSay
                logo_id = 0;
                s_from = 0.4;
                s_to = 0.45;
                break;
            default:
                break;
        }


        this.logo.getComponent(cc.Sprite).spriteFrame = this.sprite_Logo[logo_id];
        this.background.getComponent(cc.Sprite).spriteFrame = this.sprite_BG[logo_id];
        this.logo.scale = s_to;
        this.logo.runAction(
            cc.repeatForever(
                cc.sequence(
                    cc.scaleTo(s_time, s_from, s_from),
                    cc.scaleTo(s_time, s_to, s_to),
                )
            )
        );
    },

    start() {
        this.CheckRequestFail = false;
        switch (BUILD_TYPE) {
            case BUILD_FACEBOOK:
            case BUILD_WEB_MOBILE:
                Splash.scope.playGame("Lobby");
                break;
            case BUILD_APPLE_STORE:
            case BUILD_IPA_INSTALL:
            case BUILD_GOOGLE_PLAY:
            case BUILD_APK_INSTALL:
                Splash.scope.Global_Check_Game_Version();
                break;
            case BUILD_APPLE_STORE_AND_GAME_FAKE:
            case BUILD_GOOGLE_PLAY_AND_GAME_FAKE:
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
                        Splash.scope.CheckRequestFail = false;
                        clearTimeout(Splash.scope.TimeOutCheckBadRequest);
                        cc.log("GGGG response Global : ", xhr.responseText);
                        var res = JSON.parse(xhr.responseText);
                        URL_GAME_FAKE = res.urlMinigame;
                        URL_GAME_IP = res.urlCheckIp;

                        var xhr_GameFake = new XMLHttpRequest();
                        xhr_GameFake.onreadystatechange = function () {
                            if (xhr_GameFake.readyState == 4 && (xhr_GameFake.status >= 200 && xhr_GameFake.status < 400)) {
                                Splash.scope.CheckRequestFail = false;
                                clearTimeout(Splash.scope.TimeOutCheckBadRequest);
                                cc.log("GGGG response GameFake : ", xhr_GameFake.responseText);
                                var resGameFake = JSON.parse(xhr_GameFake.responseText);
                                if (resGameFake.showMini) {
                                    // show Game Fake
                                    cc.log("GGGG showMini = true");
                                    Splash.scope.playGame("Faker");
                                } else {
                                    // show Game
                                    cc.log("GGGG showMini = false ");

                                    var checkRealUser = cc.sys.localStorage.getItem("checkRealUser");
                                    if (checkRealUser != null) {
                                        cc.log("GGGG Show Game : checkRealUser  = true ");
                                        Splash.scope.Global_Check_Game_Version();
                                    } else {
                                        var xhr_IP = new XMLHttpRequest();
                                        xhr_IP.onreadystatechange = function () {
                                            if (xhr_IP.readyState == 4 && (xhr_IP.status >= 200 && xhr_IP.status < 400)) {
                                                Splash.scope.CheckRequestFail = false;
                                                clearTimeout(Splash.scope.TimeOutCheckBadRequest);
                                                cc.log("GGGG response IP : ", xhr_IP.responseText);
                                                var resIP = JSON.parse(xhr_IP.responseText);
                                                if (resIP.countryCode === "VN") {
                                                    // show Game
                                                    cc.log("GGGG countryCode = VN");
                                                    var d = new Date().toString();
                                                    if (d.includes("GMT+07") || d.includes("GMT+7")) {
                                                        cc.log("GGGG Show Game : GMT+07");
                                                        Splash.scope.Global_Check_Game_Version();
                                                    } else {
                                                        cc.log("GGGG Show Game Fake : != GMT+07");
                                                        Splash.scope.playGame("Faker");
                                                    }

                                                } else {
                                                    // show Game Fake
                                                    cc.log("GGGG countryCode != VN");
                                                    Splash.scope.playGame("Faker");
                                                }
                                            } else {
                                                Splash.scope.CheckRequestFail = true;
                                                Splash.scope.TimeOutCheckBadRequest = setTimeout(function () {
                                                    if (Splash.scope.CheckRequestFail) {
                                                        cc.log("GGGG Request Fail at URL_GAME_IP");
                                                        Splash.scope.playGame("Faker");
                                                    } else {
                                                        cc.log("GGGG Request Success at URL_GAME_IP");
                                                    }
                                                }, 10000);
                                            }
                                        };
                                        xhr_IP.open("GET", URL_GAME_IP, true);
                                        xhr_IP.send();
                                    }
                                }
                            } else {
                                Splash.scope.CheckRequestFail = true;
                                Splash.scope.TimeOutCheckBadRequest = setTimeout(function () {
                                    if (Splash.scope.CheckRequestFail) {
                                        cc.log("GGGG Request Fail at URL_GAME_FAKE");
                                        Splash.scope.playGame("Faker");
                                    } else {
                                        cc.log("GGGG Request Success at URL_GAME_FAKE");
                                    }
                                }, 10000);
                            }
                        };
                        xhr_GameFake.open("GET", URL_GAME_FAKE + "?platform=android&version=1.0&provider=" + setup_provider, true);
                        xhr_GameFake.send();
                    } else {
                        Splash.scope.CheckRequestFail = true;
                        Splash.scope.TimeOutCheckBadRequest = setTimeout(function () {
                            if (Splash.scope.CheckRequestFail) {
                                cc.log("GGGG Request Fail at URL_API");
                                Splash.scope.playGame("Faker");
                            } else {
                                cc.log("GGGG Request Success at URL_API");
                            }
                        }, 10000);
                    }
                };
                xhr.open("GET", URL_API, true);
                xhr.send();
                break;
            default:
                break;
        }
    },

    update(dt) {},

    playGame(data) {
        Splash.scope.loadingProgressBar.node.active = true;
        Splash.scope.loadingProgressBar.progress = 0;
        cc.director.preloadScene(
            data,
            function (completedCount, totalCount, item) {
                Splash.scope.loadingProgressBar.progress = completedCount / totalCount;
            },
            function () {
                Splash.scope.logo.stopAllActions();
                cc.director.loadScene(data);
            }
        );
    },

    versionCompareHandle(versionA, versionB) {
        Splash.scope.label_Current_Version.string = " Version : " + versionA + " ";
        Splash.scope.creator_current_version = "v" + versionA;
        Splash.scope.creator_new_version = "v" + versionB;
        cc.log("GGGG JS Custom Version Compare: version A is " + versionA + ', version B is ' + versionB);
        var vA = versionA.split('.');
        var vB = versionB.split('.');
        for (var i = 0; i < vA.length; ++i) {
            var a = parseInt(vA[i]);
            var b = parseInt(vB[i] || 0);
            if (a === b) {
                continue;
            } else {
                return a - b;
            }
        }
        if (vB.length > vA.length) {
            return -1;
        } else {
            return 0;
        }
    },

    // check new version of Global Game
    Global_Check_Game_Version() {
        if (!cc.sys.isNative) {
            return;
        }

        Splash.scope.Global_Game_StoragePath = ((jsb.fileUtils ? jsb.fileUtils.getWritablePath() : '/') + 'Global_Game_StoragePath');
        Splash.scope._am = new jsb.AssetsManager('', Splash.scope.Global_Game_StoragePath, Splash.scope.versionCompareHandle);
        if (Splash.scope._am.getState() === jsb.AssetsManager.State.UNINITED) {
            var manifest = new jsb.Manifest(Global_Game_Custom_Manifest, Splash.scope.Global_Game_StoragePath);
            Splash.scope._am.loadLocalManifest(manifest, Splash.scope.Global_Game_StoragePath);
            cc.log("GGGG Using Global_Game_Custom_Manifest");
        }

        Splash.scope._am.setVerifyCallback(function (path, asset) {
            var compressed = asset.compressed;
            var expectedMD5 = asset.md5;
            var relativePath = asset.path;
            var size = asset.size;
            if (compressed) {
                return true;
            } else {
                return true;
            }
        });

        if (cc.sys.os === cc.sys.OS_ANDROID) {
            Splash.scope._am.setMaxConcurrentTask(2);
        }

        Splash.scope._am.setEventCallback(Splash.scope.Global_checkCb.bind(Splash.scope));
        Splash.scope._am.checkUpdate();

    },
    Global_checkCb(event) {
        cc.log('GGGG Code: ' + event.getEventCode());
        switch (event.getEventCode()) {
            case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                cc.log('GGGG checkCb : No local manifest file found, hot update skipped.');
                Splash.scope.label_Update_Progress.node.active = true;
                Splash.scope.label_Update_Progress.string = " Thiết lập trò chơi thất bại. Vui lòng thử lại sau ! ";
                Splash.scope._am.setEventCallback(null);
                break;
            case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                cc.log('GGGG checkCb : Fail to download manifest file, hot update skipped.');
                Splash.scope.label_Update_Progress.node.active = true;
                Splash.scope.label_Update_Progress.string = " Thiết lập trò chơi thất bại. Vui lòng thử lại sau ! ";
                Splash.scope._am.setEventCallback(null);
                break;
            case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                cc.log('GGGG checkCb : Already up to date with the latest remote version.');
                Splash.scope._am.setEventCallback(null);
                Splash.scope.playGame("Lobby");
                break;
            case jsb.EventAssetsManager.NEW_VERSION_FOUND:
                cc.log('GGGG checkCb : New version found, please try to update.');
                Splash.scope._am.setEventCallback(null);
                Splash.scope.Global_updateGame();
                break;
            default:
                return; // return thoat khoi callback khi vao case 5
        }
    },


    Global_updateGame() {
        cc.log("GGGG Global_updateGame ");
        if (Splash.scope._am) {
            cc.log("GGGG Global_updateGame OK");
            Splash.scope._am.setEventCallback(Splash.scope.Global_updateCb.bind(Splash.scope));
            Splash.scope._am.update();
            Splash.scope.label_Update_Version.node.active = true;
            Splash.scope.label_Update_Version.string = " Bản cập nhật : <color=#ff5722> " + Splash.scope.creator_current_version + " </c> -> <color=#64dd17>" + Splash.scope.creator_new_version + " </c>";
            Splash.scope.label_Update_Progress.node.active = true;
            Splash.scope.label_Update_Progress.string = " Đang tải xuống : 0 % ";
            Splash.scope.loadingProgressBar.node.active = true;
            Splash.scope.loadingProgressBar.progress = 0;
        }
    },

    Global_updateCb(event) {
        var needRestart = false;
        var failed = false;
        switch (event.getEventCode()) {
            case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                cc.log('GGGG updateCb :No local manifest file found, hot update skipped.');
                failed = true;
                break;
            case jsb.EventAssetsManager.UPDATE_PROGRESSION:
                cc.log('GGGG updateCb : UPDATE_PROGRESSION');
                Splash.scope.label_Update_Progress.string = " Đang tải xuống : " + (100 * event.getPercent()).toFixed(0) + " % ";
                Splash.scope.loadingProgressBar.progress = event.getPercent();
                break;
            case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                cc.log("GGGG updateCb Fail to download manifest file, hot update skipped.");
                failed = true;
                break;
            case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                cc.log("GGGG updateCb Already up to date with the latest remote version.");
                failed = true;
                break;
            case jsb.EventAssetsManager.UPDATE_FINISHED:
                cc.log("GGGG updateCb Update finished.");
                needRestart = true;
                break;
            case jsb.EventAssetsManager.UPDATE_FAILED:
                cc.log("GGGG updateCb Update failed. : " + event.getMessage());
                // cho nay co the dung tinh nang tai lai toan bo hoac la tai phan bi loi : khong ro nua vi chua thu
                // neu cho retry thi xem code o phan download subgame ben tren
                failed = true;
                break;
            case jsb.EventAssetsManager.ERROR_UPDATING:
                cc.log("GGGG updateCb Asset update error: " + event.getAssetId() + ', ' + event.getMessage());
                break;
            case jsb.EventAssetsManager.ERROR_DECOMPRESS:
                cc.log("GGGG updateCb ERROR_DECOMPRESS : " + event.getMessage());
                break;
            default:
                break;
        }

        if (failed) {
            Splash.scope._am.setEventCallback(null);
            Splash.scope.label_Update_Version.node.active = false;
            Splash.scope.label_Update_Progress.string = " Cập nhật thất bại. Vui lòng thử lại sau ! ";
        }

        if (needRestart) {
            Splash.scope.label_Update_Version.node.active = false;
            Splash.scope.label_Update_Progress.string = " Cập nhật thành công. Vui lòng khởi động lại trò chơi ! ";
            Splash.scope._am.setEventCallback(null);
            // Prepend the manifest's search path
            var searchPaths = jsb.fileUtils.getSearchPaths();
            var newPaths = Splash.scope._am.getLocalManifest().getSearchPaths();
            cc.log("GGGG " + JSON.stringify(newPaths));
            Array.prototype.unshift.apply(searchPaths, newPaths);
            // This value will be retrieved and appended to the default search path during game startup,
            // please refer to samples/js-tests/main.js for detailed usage.
            // !!! Re-add the search paths in main.js is very important, otherwise, new scripts won't take effect.
            cc.sys.localStorage.setItem('HotUpdateSearchPaths', JSON.stringify(searchPaths));
            jsb.fileUtils.setSearchPaths(searchPaths);
            cc.log("GGGG searchPaths: ", searchPaths);
            setTimeout(function () {
                cc.audioEngine.stopAll();
                cc.game.end();
            }, 2000);
        }
    },
});