cc.Class({
  extends: cc.Component,

  properties: {
    list_Sprite: {
      default: [],
      type: [cc.SpriteFrame]
    }
  },

  // LIFE-CYCLE CALLBACKS:

  // onLoad () {},

  start() {
    this.node.getComponent(cc.Sprite).spriteFrame = this.list_Sprite[
      this.randomBetween(0, this.list_Sprite.length - 1)
    ];
  },

  randomBetween(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  // update (dt) {},
});