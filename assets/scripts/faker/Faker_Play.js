var Faker_GM = require("Faker_GM");
cc.Class({
    extends: cc.Component,

    properties: {
        prefab: {
            default: null,
            type: cc.Prefab
        },
        boss_dot_1: {
            default: null,
            type: cc.Node
        },
        boss_dot_2: {
            default: null,
            type: cc.Node
        },
        PLAYER_1: {
            default: null,
            type: cc.Node
        },
        PLAYER_2: {
            default: null,
            type: cc.Node
        },
        sprite_Player_1: {
            default: [],
            type: [cc.SpriteFrame]
        },
        sprite_Player_2: {
            default: [],
            type: [cc.SpriteFrame]
        },
        sprite_Player_Bot: {
            default: [],
            type: [cc.SpriteFrame]
        },
        label_storage_1: {
            default: null,
            type: cc.Label
        },
        label_storage_2: {
            default: null,
            type: cc.Label
        },
        label_stor_count: {
            default: null,
            type: cc.Node
        },
        list_highlight: {
            default: null,
            type: cc.Node
        },
        UI_Hand: {
            default: null,
            type: cc.Node
        },
        Menu: {
            default: null,
            type: cc.Node
        },
        Popup_win: {
            default: null,
            type: cc.Node
        },
        Avatar_Player_Win: {
            default: null,
            type: cc.Node
        },
        Avatar_Player_Lost: {
            default: null,
            type: cc.Node
        },
        label_Win_Score: {
            default: null,
            type: cc.Label
        },
        label_Lost_Score: {
            default: null,
            type: cc.Label
        },
        sprite_Player: {
            default: [],
            type: [cc.SpriteFrame]
        },
        UI_Intro_Match: {
            default: null,
            type: cc.Node
        },
        Intro_VS: {
            default: null,
            type: cc.Node
        },
        Intro_Player_1: {
            default: null,
            type: cc.Node
        },
        Intro_Player_2: {
            default: null,
            type: cc.Node
        },
        Intro_Avatar_Player_1: {
            default: null,
            type: cc.Node
        },
        Intro_Avatar_Player_2: {
            default: null,
            type: cc.Node
        },
        Game_BG: {
            default: null,
            type: cc.Node
        },
        sprite_BG: {
            default: [],
            type: [cc.SpriteFrame]
        },
    },

    canOpenMenu: null,
    gameState: null,
    player_1: null,
    player_2: null,
    hold_ID: null,
    target: null,
    boss_dot_1_available: null,
    boss_dot_2_available: null,


    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        if (cc.sys.isNative && cc.sys.isMobile) {
            cc.log("Mobile");

        } else {
            cc.log("Web");
        }
    },

    start() {
        cc.log("GM_BG_ID : ", Faker_GM.instance.GM_BG_ID);
        this.Game_BG.getComponent(cc.Sprite).spriteFrame = this.sprite_BG[Faker_GM.instance.GM_BG_ID - 1];
        this.newGame(null, "true");
    },

    randomBetween(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    },

    Bot_Online() {
        var list_hold_have_dots = [];
        for (let index = 1; index < 6; index++) {
            if (this.gameState[index].dots.length > 0) {
                list_hold_have_dots.push(index);
            }
        }

        if (list_hold_have_dots.length > 0) {
            var id_drop = this.randomBetween(0, list_hold_have_dots.length - 1);
            this.hold_ID = list_hold_have_dots[id_drop];
            var type_drop = this.randomBetween(0, 1);
            if (type_drop == 0) {
                this.drop_left();
            } else {
                this.drop_right();
            }
            this.block_touch_all();
        }
    },

    update(dt) {},

    _update_label_count() {
        for (let index = 0; index < 12; index++) {
            this.label_stor_count.children[index].getComponent(cc.Label).string = this.gameState[index].dots.length;
        }
    },

    block_touch_all() {
        this.UI_Hand.active = false;
        this.canOpenMenu = false;
        this.Menu.active = false;
        this.list_highlight.active = false;
    },

    player_choose_left() {
        this.block_touch_all();
        if (this.state_player_1.play) {
            this.drop_left();
        } else {
            this.drop_right();
        }
    },

    player_choose_right() {
        this.block_touch_all();
        if (this.state_player_1.play) {
            this.drop_right();
        } else {
            this.drop_left();
        }
    },

    drop_right() {
        cc.log("Right : ", this.hold_ID);
        this.target = this.hold_ID;
        var data = this.gameState[this.hold_ID].dots;
        var hold_count = data.length;
        this.gameState[this.hold_ID].dots = [];
        for (let a = hold_count; a > 0; a--) {
            this.node.runAction(
                cc.sequence(
                    cc.delayTime((hold_count + 1 - a) * 0.5),
                    cc.callFunc(function () {
                        this.target = (this.hold_ID + 1 + (hold_count - a)) % 12;
                        this.gameState[this.target].dots.push(data[a - 1]);
                        for (let index = 0; index < data.length; index++) {
                            var dot = cc.find("FplayController/" + data[index]);
                            dot.runAction(
                                cc.moveTo(
                                    0.4,
                                    this.gameState[this.target].pos[0] + 40 * this.random_float(),
                                    this.gameState[this.target].pos[1] + 40 * this.random_float() - 55
                                )
                            );
                        }
                        data.pop();
                        this._update_label_count();
                    }, this)
                )
            );
        }

        cc.log(this.target);
        cc.log(this.gameState);

        this.node.runAction(
            cc.sequence(
                cc.delayTime((hold_count + 1) * 0.5),
                cc.callFunc(function () {
                    this._update_label_count();
                    var next_1_step = (this.target + 1) % 12;
                    if (this.gameState[next_1_step].dots.length > 0) {
                        cc.log("O ke tiep co quan -> check Rai tiep or die");
                        if (next_1_step !== 0 && next_1_step !== 6) {
                            cc.log("O Ke tiep la Dan -> rai tiep");
                            this.hold_ID = next_1_step;
                            this.drop_right();
                        } else {
                            cc.log("O Ke tiep la Boss");
                            if (this.gameState[next_1_step].dots.length > 0) {
                                cc.log("O Boss co quan -> end tune");

                                this.swap_tune();
                                this.open_touch_for_player();
                                this.check_win();
                            } else {
                                cc.log("O Boss khong co quan -> check eat");
                                this.check_eat_or_die_right(next_1_step);
                            }
                        }
                    } else {
                        cc.log("O ke tiep k co quan nao");
                        this.check_eat_or_die_right(next_1_step);
                    }
                }, this)
            )
        );
    },

    check_eat_or_die_right(next_1_step) {
        var next_2_step = (next_1_step + 1) % 12;
        if (this.gameState[next_2_step].dots.length > 0) {
            cc.log("O ke tiep co quan -> dc an");
            var dots_can_eat = this.gameState[next_2_step].dots;
            cc.log("SO quan dc an : ", dots_can_eat.length);
            this.move_dots_eat(dots_can_eat, next_2_step);

            this.gameState[next_2_step].dots = [];
            this._update_label_count();
            this.check_eat_combo_right(next_2_step);

            cc.log(this.gameState);
        } else {
            cc.log(
                "O ke tiep k co quan nao -> 2 o lien tiep k co quan -> end tune"
            );

            this.swap_tune();
            this.open_touch_for_player();
            this.check_win();
        }
    },

    check_eat_combo_right(next_2_step) {
        var next_3_step = (next_2_step + 1) % 12;
        if (this.gameState[next_3_step].dots.length > 0) {
            cc.log("vua an 1 o xong , O ke tiep co quan  -> end tune");

            this.swap_tune();
            this.open_touch_for_player();
            this.check_win();
        } else {
            cc.log(
                "O ke tiep khong co quan -> neu o sau do co quan thi dc an lan 2"
            );
            var next_4_step = (next_3_step + 1) % 12;
            if (this.gameState[next_4_step].dots.length > 0) {
                cc.log("O ke tiep co quan nao ->duoc an lan 2");
                var eat_2 = this.gameState[next_4_step].dots;

                this.move_dots_eat(eat_2, next_4_step);

                this.gameState[next_4_step].dots = [];
                this._update_label_count();
                this.check_eat_combo_right(next_4_step);
            } else {
                cc.log("O ke tiep k co quan nao -> end tune");

                this.swap_tune();
                this.open_touch_for_player();
                this.check_win();
            }
        }
    },

    drop_left() {
        cc.log("Left : ", this.hold_ID);
        this.target = this.hold_ID;
        var data = this.gameState[this.hold_ID].dots;
        var hold_count = data.length;
        this.gameState[this.hold_ID].dots = [];
        cc.log("hold_count : ", hold_count);
        cc.log("gameState : ", this.gameState[this.hold_ID].dots);
        for (let a = hold_count; a > 0; a--) {
            this.node.runAction(
                cc.sequence(
                    cc.delayTime((hold_count + 1 - a) * 0.5),
                    cc.callFunc(function () {
                        this.target = (this.hold_ID + 11 - (hold_count - a)) % 12;
                        this.gameState[this.target].dots.push(data[a - 1]);
                        for (let index = 0; index < data.length; index++) {
                            var dot = cc.find("FplayController/" + data[index]);
                            dot.runAction(
                                cc.moveTo(
                                    0.4,
                                    this.gameState[this.target].pos[0] + 40 * this.random_float(),
                                    this.gameState[this.target].pos[1] + 40 * this.random_float() - 55
                                )
                            );
                        }
                        data.pop();
                        this._update_label_count();
                    }, this)
                )
            );
        }

        cc.log(this.target);
        cc.log(this.gameState);

        this.node.runAction(
            cc.sequence(
                cc.delayTime((hold_count + 1) * 0.5),
                cc.callFunc(function () {
                    this._update_label_count();
                    var next_1_step = -1;
                    if (this.target > 0) {
                        next_1_step = this.target - 1;
                    } else {
                        next_1_step = 11;
                    }
                    if (this.gameState[next_1_step].dots.length > 0) {
                        cc.log("O ke tiep co quan -> check Rai tiep or die");
                        if (next_1_step !== 0 && next_1_step !== 6) {
                            cc.log("O Ke tiep la Dan -> rai tiep");
                            this.hold_ID = next_1_step;
                            this.drop_left();
                        } else {
                            cc.log("O Ke tiep la Boss");
                            if (this.gameState[next_1_step].dots.length > 0) {
                                cc.log("O Boss co quan -> end tune");

                                this.swap_tune();
                                this.open_touch_for_player();
                                this.check_win();
                            } else {
                                cc.log("O Boss khong co quan -> check eat");
                                this.check_eat_or_die_left(next_1_step);
                            }
                        }
                    } else {
                        cc.log("O ke tiep k co quan nao");
                        this.check_eat_or_die_left(next_1_step);
                    }
                }, this)
            )
        );
    },

    check_eat_or_die_left(next_1_step) {
        var next_2_step = -1;
        if (next_1_step > 0) {
            next_2_step = next_1_step - 1;
        } else {
            next_2_step = 11;
        }
        if (this.gameState[next_2_step].dots.length > 0) {
            cc.log("O ke tiep co quan -> dc an");
            var dots_can_eat = this.gameState[next_2_step].dots;
            cc.log("SO quan dc an : ", dots_can_eat.length);
            this.move_dots_eat(dots_can_eat, next_2_step);

            this.gameState[next_2_step].dots = [];
            this._update_label_count();
            this.check_eat_combo_left(next_2_step);

            cc.log(this.gameState);
        } else {
            cc.log(
                "O ke tiep k co quan nao -> 2 o lien tiep k co quan -> end tune"
            );

            this.swap_tune();
            this.open_touch_for_player();
            this.check_win();
        }
    },

    check_eat_combo_left(next_2_step) {
        var next_3_step = -1;
        if (next_2_step > 0) {
            next_3_step = next_2_step - 1;
        } else {
            next_3_step = 11;
        }
        if (this.gameState[next_3_step].dots.length > 0) {
            cc.log("vua an 1 o xong , O ke tiep co quan  -> end tune");

            this.swap_tune();
            this.open_touch_for_player();
            this.check_win();
        } else {
            cc.log(
                "O ke tiep khong co quan -> neu o sau do co quan thi dc an lan 2"
            );
            var next_4_step = -1;
            if (next_3_step > 0) {
                next_4_step = next_3_step - 1;
            } else {
                next_4_step = 11;
            }
            if (this.gameState[next_4_step].dots.length > 0) {
                cc.log("O ke tiep co quan nao ->duoc an lan 2");
                var eat_2 = this.gameState[next_4_step].dots;

                this.move_dots_eat(eat_2, next_4_step);

                this.gameState[next_4_step].dots = [];
                this._update_label_count();
                this.check_eat_combo_left(next_4_step);
            } else {
                cc.log("O ke tiep k co quan nao -> end tune");

                this.swap_tune();
                this.open_touch_for_player();
                this.check_win();
            }
        }
    },

    swap_tune() {
        this.state_player_1.play = this.state_player_2.play;
        this.state_player_2.play = !this.state_player_2.play;
    },

    check_win() {
        cc.log("boss_dot_1_available : ", this.boss_dot_1_available);
        cc.log("boss_dot_2_available : ", this.boss_dot_2_available);
        cc.log("gameState : ", this.gameState);
        cc.log("state_player_1 : ", this.state_player_1);
        cc.log("state_player_2 : ", this.state_player_2);

        if (!this.boss_dot_1_available && !this.boss_dot_2_available) {
            for (let x = 1; x < 6; x++) {
                if (this.gameState[x].dots.length > 0) {
                    for (let y = 0; y < this.gameState[x].dots.length; y++) {
                        var move_dot = cc.find(
                            "FplayController/" + this.gameState[x].dots[y]
                        );
                        move_dot.runAction(
                            cc.moveTo(
                                0,
                                -560 + 40 * this.random_float(),
                                150 + 40 * this.random_float()
                            )
                        );
                    }
                }
            }

            for (let a = 7; a < 12; a++) {
                if (this.gameState[a].dots.length > 0) {
                    for (let b = 0; b < this.gameState[a].dots.length; b++) {
                        var move_dot = cc.find(
                            "FplayController/" + this.gameState[a].dots[b]
                        );
                        move_dot.runAction(
                            cc.moveTo(
                                0,
                                600 + 40 * this.random_float(),
                                -245 + 40 * this.random_float()
                            )
                        );
                    }
                }
            }

            for (let index = 1; index < 6; index++) {
                this.state_player_1.storage += this.gameState[index].dots.length;
                this.state_player_2.storage += this.gameState[index + 6].dots.length;
            }

            this.label_storage_1.string = this.state_player_1.storage;
            this.label_storage_2.string = this.state_player_2.storage;

            if (this.state_player_1.storage < this.state_player_2.storage) {
                cc.log("Player 1 lost - Player 2 win");
                this.Popup_win.active = true;
                this.label_Win_Score.string = this.state_player_2.storage;
                this.label_Lost_Score.string = this.state_player_1.storage;
                this.Avatar_Player_Win.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[1];
                if (!Faker_GM.instance.GM_TYPE_PLAY) {
                    // Bot lost
                    this.Avatar_Player_Lost.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[5];
                } else {
                    // Player 1 lost
                    this.Avatar_Player_Lost.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[3];
                }

            } else if (this.state_player_1.storage > this.state_player_2.storage) {
                cc.log("Player 1 win - Player 2 lost");
                this.Popup_win.active = true;
                this.label_Win_Score.string = this.state_player_1.storage;
                this.label_Lost_Score.string = this.state_player_2.storage;
                this.Avatar_Player_Lost.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[4];
                if (!Faker_GM.instance.GM_TYPE_PLAY) {
                    // Bot win
                    this.Avatar_Player_Win.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[2];
                } else {
                    // Player 1 win
                    this.Avatar_Player_Win.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[0];
                }
            } else {
                cc.log("Player 1 = Player 2");
                this.Popup_win.active = true;
                this.label_Player_Win.string = "";
            }

        } else {
            if (this.state_player_1.play) {
                var storage_empty = true;
                for (let index = 1; index < 6; index++) {
                    if (this.gameState[index].dots.length > 0) {
                        storage_empty = false;
                    }
                }

                if (storage_empty) {
                    cc.log("Player 1 lost");
                    this.Popup_win.active = true;

                    this.label_Win_Score.string = this.state_player_2.storage;
                    this.label_Lost_Score.string = this.state_player_1.storage;
                    this.Avatar_Player_Win.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[1];
                    if (!Faker_GM.instance.GM_TYPE_PLAY) {
                        // Bot lost
                        this.Avatar_Player_Lost.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[5];
                    } else {
                        // Player 1 lost
                        this.Avatar_Player_Lost.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[3];
                    }
                } else {
                    cc.log("Chua thua");
                    if (!Faker_GM.instance.GM_TYPE_PLAY) {
                        this.scheduleOnce(this.Bot_Online, 1);
                    }
                }
            } else {
                var storage_empty = true;
                for (let index = 7; index < 12; index++) {
                    if (this.gameState[index].dots.length > 0) {
                        storage_empty = false;
                    }
                }

                if (storage_empty) {
                    cc.log("Player 2 lost");
                    this.Popup_win.active = true;
                    this.label_Win_Score.string = this.state_player_1.storage;
                    this.label_Lost_Score.string = this.state_player_2.storage;
                    this.Avatar_Player_Lost.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[4];
                    if (!Faker_GM.instance.GM_TYPE_PLAY) {
                        // Bot win
                        this.Avatar_Player_Win.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[2];
                    } else {
                        // Player 1 win
                        this.Avatar_Player_Win.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[0];
                    }
                } else {
                    cc.log("Chua thua");
                }
            }
        }
    },

    move_dots_eat(dots_can_eat, next_2_step) {
        if (this.state_player_1.play) {
            cc.log("Player 1 eat");
            this.state_player_1.storage += dots_can_eat.length;

            if (next_2_step == 0) {
                cc.log("Eat quan 1");
                if (this.boss_dot_1_available) {
                    this.boss_dot_1.runAction(cc.moveTo(0.5, -560, 150));
                    this.boss_dot_1_available = false;
                    this.state_player_1.storage += 5;
                }
            } else if (next_2_step == 6) {
                cc.log("Eat quan 2");
                if (this.boss_dot_2_available) {
                    this.boss_dot_2.runAction(cc.moveTo(0.5, -560, 150));
                    this.boss_dot_2_available = false;
                    this.state_player_1.storage += 5;
                }
            }

            this.label_storage_1.string = this.state_player_1.storage;

            for (let x = 0; x < dots_can_eat.length; x++) {
                var dotEat = cc.find("FplayController/" + dots_can_eat[x]);
                dotEat.runAction(
                    cc.moveTo(
                        0.5,
                        -560 + 40 * this.random_float(),
                        150 + 40 * this.random_float()
                    )
                );
            }
        } else {
            cc.log("Player 2 eat");
            this.state_player_2.storage += dots_can_eat.length;

            if (next_2_step == 0) {
                cc.log("Eat quan 1");
                if (this.boss_dot_1_available) {
                    this.boss_dot_1.runAction(cc.moveTo(0.5, 600, -245));
                    this.boss_dot_1_available = false;
                    this.state_player_2.storage += 5;
                }
            } else if (next_2_step == 6) {
                cc.log("Eat quan 2");
                if (this.boss_dot_2_available) {
                    this.boss_dot_2.runAction(cc.moveTo(0.5, 600, -245));
                    this.boss_dot_2_available = false;
                    this.state_player_2.storage += 5;
                }
            }

            this.label_storage_2.string = this.state_player_2.storage;

            for (let x = 0; x < dots_can_eat.length; x++) {
                var dotEat = cc.find("FplayController/" + dots_can_eat[x]);
                dotEat.runAction(
                    cc.moveTo(
                        0.5,
                        600 + 40 * this.random_float(),
                        -245 + 40 * this.random_float()
                    )
                );
            }
        }

    },

    hold_action(event, index) {
        cc.log("hold_action : ", index);
        if (parseInt(index) < 6) {
            if (Faker_GM.instance.GM_TYPE_PLAY) {
                this.UI_Hand.active = true;
                this.UI_Hand.position = cc.v2(
                    853 + this.gameState[index].pos[0],
                    480 + this.gameState[index].pos[1] - 55
                );
                this.hold_ID = parseInt(index);
                cc.log(this.hold_ID);
            }
        } else {
            this.UI_Hand.active = true;
            this.UI_Hand.position = cc.v2(
                853 + this.gameState[index].pos[0],
                480 + this.gameState[index].pos[1] - 55
            );
            this.hold_ID = parseInt(index);
            cc.log(this.hold_ID);
        }

    },

    open_touch_for_player() {
        this.canOpenMenu = true;

        this.list_highlight.active = true;
        if (this.state_player_1.play) {
            for (let index = 0; index < 5; index++) {
                if (this.gameState[index + 1].dots.length > 0) {
                    this.list_highlight.children[index].active = true;
                } else {
                    this.list_highlight.children[index].active = false;
                }
            }

            for (let index = 5; index < 10; index++) {
                this.list_highlight.children[index].active = false;
            }
        } else {
            for (let index = 5; index < 10; index++) {
                if (this.gameState[index + 2].dots.length > 0) {
                    this.list_highlight.children[index].active = true;
                } else {
                    this.list_highlight.children[index].active = false;
                }
            }

            for (let index = 0; index < 5; index++) {
                this.list_highlight.children[index].active = false;
            }
        }

        if (Faker_GM.instance.GM_TYPE_PLAY) {
            // PvP
            this.PLAYER_1.getComponent(cc.Sprite).spriteFrame = this.sprite_Player_1[
                this.state_player_1.play ? 0 : 1
            ];
        } else {
            // Bot
            this.PLAYER_1.getComponent(cc.Sprite).spriteFrame = this.sprite_Player_Bot[
                this.state_player_1.play ? 0 : 1
            ];
        }

        this.PLAYER_2.getComponent(cc.Sprite).spriteFrame = this.sprite_Player_2[
            this.state_player_2.play ? 0 : 1
        ];
    },

    continues() {
        this.Menu.active = false;
    },

    show_Intro_Match() {
        this.UI_Intro_Match.active = true;

        if (!Faker_GM.instance.GM_TYPE_PLAY) {
            this.Intro_Avatar_Player_1.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[2];
        } else {
            this.Intro_Avatar_Player_1.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[0];
        }
        this.Intro_Avatar_Player_2.getComponent(cc.Sprite).spriteFrame = this.sprite_Player[1];

        this.Intro_Player_1.scale = 0;
        this.Intro_Player_2.scale = 0;
        this.Intro_VS.scale = 0;

        this.Intro_Player_1.runAction(
            cc.sequence(
                cc.delayTime(0.2),
                cc.scaleTo(0.15, 1.2, 1.2),
                cc.scaleTo(0.05, 1, 1)
            )
        );

        this.Intro_Player_2.runAction(
            cc.sequence(
                cc.delayTime(0.4),
                cc.scaleTo(0.15, 1.2, 1.2),
                cc.scaleTo(0.05, 1, 1)
            )
        );

        this.Intro_VS.runAction(
            cc.sequence(
                cc.delayTime(0.8),
                cc.scaleTo(0, 1.1, 1.1),
                cc.scaleTo(0.15, 1.5, 1.5),
                cc.scaleTo(0.05, 1, 1)
            )
        );

        setTimeout(function () {
            this.UI_Intro_Match.active = false;
            if (!Faker_GM.instance.GM_TYPE_PLAY) {
                this.scheduleOnce(this.Bot_Online, 1);
            }
        }.bind(this), 2000);
    },

    show_Menu() {
        if (this.canOpenMenu) {
            this.Menu.active = !this.Menu.active;
        } else {
            cc.log("Can not Open Menu");
        }
    },

    close_popup_win() {
        this.Popup_win.active = false;
    },

    newGame(event, first) {
        this.node.stopAllActions();
        this.Popup_win.active = false;
        this.Menu.active = false;

        this.boss_dot_1.position = cc.v2(-480, -55);
        this.boss_dot_2.position = cc.v2(480, -55);

        this.boss_dot_1_available = true;
        this.boss_dot_2_available = true;

        this.gameState = [{
                id: 0,
                pos: [-480, -55],
                dots: []
            },
            {
                id: 1,
                pos: [-314, 98],
                dots: [1, 2, 3, 4, 5]
            },
            {
                id: 2,
                pos: [-161, 98],
                dots: [6, 7, 8, 9, 10]
            },
            {
                id: 3,
                pos: [-7, 98],
                dots: [11, 12, 13, 14, 15]
            },
            {
                id: 4,
                pos: [148, 98],
                dots: [16, 17, 18, 19, 20]
            },
            {
                id: 5,
                pos: [307.5, 98],
                dots: [21, 22, 23, 24, 25]
            },
            {
                id: 6,
                pos: [480, -55],
                dots: []
            },
            {
                id: 7,
                pos: [307.5, -100],
                dots: [26, 27, 28, 29, 30]
            },
            {
                id: 8,
                pos: [148, -100],
                dots: [31, 32, 33, 34, 35]
            },
            {
                id: 9,
                pos: [-7, -100],
                dots: [36, 37, 38, 39, 40]
            },
            {
                id: 10,
                pos: [-161, -100],
                dots: [41, 42, 43, 44, 45]
            },
            {
                id: 11,
                pos: [-314, -100],
                dots: [46, 47, 48, 49, 50]
            }
        ];

        this.state_player_1 = {
            play: true,
            storage: 0
        };
        this.state_player_2 = {
            play: false,
            storage: 0
        };

        this.label_storage_1.string = this.state_player_1.storage;
        this.label_storage_2.string = this.state_player_2.storage;

        if (first === "true") {
            // init
            var count = 1;
            for (let index = 0; index < this.gameState.length; index++) {
                if (index !== 0 && index !== 6) {
                    for (let a = 0; a < 5; a++) {
                        var info = this.gameState[index];
                        var dot = cc.instantiate(this.prefab);
                        dot.position = cc.v2(
                            info.pos[0] + 40 * this.random_float(),
                            info.pos[1] + 40 * this.random_float() - 55 // -55 do Board y < FplayController y
                        );
                        dot.name = JSON.stringify(count);
                        count += 1;
                        this.node.addChild(dot);
                    }
                }
            }
        } else {
            // renew
            var list_children = cc.find("FplayController").children;
            for (let index = 0; index < this.gameState.length; index++) {
                if (index !== 0 && index !== 6) {
                    for (let a = 0; a < 5; a++) {
                        var info = this.gameState[index];
                        var dot = list_children[info.dots[a] - 1];
                        dot.position = cc.v2(
                            info.pos[0] + 40 * this.random_float(),
                            info.pos[1] + 40 * this.random_float() - 55
                        );
                    }
                }
            }
        }

        this.hold_ID = -1;
        this.canOpenMenu = true;
        this.open_touch_for_player();
        this._update_label_count();

        this.show_Intro_Match();
    },

    quitGame() {
        cc.director.loadScene("Faker");
    },

    random_float() {
        return (Math.random() * 2 - 1).toFixed(1);
    }
});