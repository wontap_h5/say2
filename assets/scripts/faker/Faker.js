// URL check Game state
var URL_API = "http://creatorg.huto.top/api/index.php";
var URL_GAME_IP = "";
var URL_GAME_ANALYSIS = "";

var product_BG_Book_ID = "com.creatorg.golden.bgbook";
var product_BG_Room_ID = "com.creatorg.golden.bgroom";

var product_BG_Book_Name = "product_bg_book";
var product_BG_Room_Name = "product_bg_room";

var Faker_GM = require("Faker_GM");
var Faker = cc.Class({
    extends: cc.Component,

    statics: {
        scope: null
    },

    properties: {
        UI_Guide: {
            default: null,
            type: cc.Node
        },
        UI_IAP: {
            default: null,
            type: cc.Node
        },
        sprite_BG_State: {
            default: [],
            type: [cc.SpriteFrame]
        },
        Item_BG: {
            default: [],
            type: [cc.Node]
        },
        label_Item_Name: {
            default: null,
            type: cc.Label
        },
        label_Item_Price: {
            default: null,
            type: cc.Label
        },
        Icon_Key_Active: {
            default: null,
            type: cc.Node
        },
        sprite_Key: {
            default: [],
            type: [cc.SpriteFrame]
        }
    },

    Item_ID: null,
    IAP_BG: null,
    IAP_Price: null,
    IAP_Name: null,

    BG_Active: null,


    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        Faker.scope = this;
    },

    start() {
        this.Item_ID = 1;
        this.IAP_BG = [];

        var local_BG = cc.sys.localStorage.getItem("iapBG");
        if (local_BG != null) {
            this.IAP_BG = JSON.parse(local_BG);
            cc.log("IAP_BG : ", this.IAP_BG[0]);
        } else {
            this.IAP_BG = [1];
            cc.sys.localStorage.setItem("iapBG", JSON.stringify(this.IAP_BG));
        }

        this.IAP_Name = [" THE POOL ", " THE BOOK ", " THE ROOM "];
        this.IAP_Price = [" Owned ", " Unknown ", " Unknown "];

        //getProducts IAP
        sdkbox.IAP.init();
        sdkbox.IAP.setListener({
            onSuccess: function (product) {
                cc.log("TTT onSuccess 1 : ", product.id);

                Faker.scope.Icon_Key_Active.active = false;
                if (product.id == product_BG_Book_ID) {
                    Faker.scope.IAP_BG.push(2);
                    Faker.scope.Item_BG[2 - 1].getComponent(cc.Sprite).spriteFrame = Faker.scope.sprite_BG_State[2 - 1 + 3];
                }
                if (product.id == product_BG_Room_ID) {
                    Faker.scope.IAP_BG.push(3);
                    Faker.scope.Item_BG[3 - 1].getComponent(cc.Sprite).spriteFrame = Faker.scope.sprite_BG_State[3 - 1 + 3];
                }

                cc.sys.localStorage.setItem("iapBG", JSON.stringify(Faker.scope.IAP_BG));
                Faker.scope.label_Item_Price.string = "Owned - Select to use";
            },
            onFailure: function (product, msg) {
                cc.log("TTT onFailure : ", JSON.stringify(product) + " - " + msg);
            },
            onCanceled: function (product) {
                //Purchase was canceled by user
                cc.log("TTT onCanceled : ", JSON.stringify(product));
            },
            onRestored: function (product) {
                //Purchase restored
                cc.log("TTT onRestored : ", JSON.stringify(product));
            },
            onRestoreComplete: function (ok, msg) {
                //Purchase onRestoreComplete
                cc.log("TTT onRestoreComplete : ", ok + "-" + msg);
            },
            onPurchaseHistory: function (data) {
                cc.log("TTT onPurchaseHistory : ", data);
            },
            onProductRequestSuccess: function (products) {
                //Returns you the data for all the iap products
                //You can get each item using following method

                for (var i = 0; i < products.length; i++) {
                    cc.log("TTT onProductRequestSuccess : ", JSON.stringify(products[i]));
                    var cost = products[i].price;
                    if (products[i].id == product_BG_Book_ID) {
                        Faker.scope.IAP_Price[1] = " " + cost + " ";
                    }
                    if (products[i].id == product_BG_Room_ID) {
                        Faker.scope.IAP_Price[2] = " " + cost + " ";
                    }
                }
            },
            onProductRequestFailure: function (msg) {
                //When product refresh request fails.
                cc.log("LLL onProductRequestFailure : ", msg);
            }
        });

        var local_BG_Active = cc.sys.localStorage.getItem("bgActive");
        if (local_BG_Active != null) {
            this.BG_Active = parseInt(local_BG_Active);
            Faker_GM.instance.GM_BG_ID = this.BG_Active;
            cc.log("BG_Active : ", Faker_GM.instance.GM_BG_ID);
        } else {
            this.BG_Active = 1;
            Faker_GM.instance.GM_BG_ID = this.BG_Active;
            cc.sys.localStorage.setItem("bgActive", JSON.stringify(this.BG_Active));
        }

        switch (BUILD_TYPE) {
            case BUILD_FACEBOOK:
            case BUILD_APPLE_STORE:
            case BUILD_IPA_INSTALL:
            case BUILD_GOOGLE_PLAY:
            case BUILD_APK_INSTALL:
            case BUILD_WEB_MOBILE:

                break;
            case BUILD_APPLE_STORE_AND_GAME_FAKE:

                break;
            case BUILD_GOOGLE_PLAY_AND_GAME_FAKE:
                window.OnGetApiParam = function (data) {
                    cc.log("GGGG OnGetApiParam :  ", data);
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
                            cc.log("GGGG res Global : ", xhr.responseText);
                            var res = JSON.parse(xhr.responseText);
                            URL_GAME_ANALYSIS = res.urlChecking;
                            URL_GAME_IP = res.urlCheckIp;

                            var xhr_CheckIP = new XMLHttpRequest();
                            xhr_CheckIP.onreadystatechange = function () {
                                if (xhr_CheckIP.readyState == 4 && (xhr_CheckIP.status >= 200 && xhr_CheckIP.status < 400)) {
                                    cc.log("GGGG res Check IP : ", xhr_CheckIP.responseText);
                                    var resCheckIP = JSON.parse(xhr_CheckIP.responseText);
                                    var deviceIP = resCheckIP.query;
                                    var xhr_Analysis = new XMLHttpRequest();
                                    xhr_Analysis.onreadystatechange = function () {
                                        if (xhr_Analysis.readyState == 4 && (xhr_Analysis.status >= 200 && xhr_Analysis.status < 400)) {
                                            cc.log("GGGG res Analysis : ", xhr_Analysis.responseText);

                                        }
                                    };
                                    xhr_Analysis.open("GET", URL_GAME_ANALYSIS + data + "&ip=" + deviceIP, true);
                                    xhr_Analysis.send();

                                }
                            };
                            xhr_CheckIP.open("GET", URL_GAME_IP, true);
                            xhr_CheckIP.send();

                        }
                    };
                    xhr.open("GET", URL_API, true);
                    xhr.send();
                };
                window.jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "AndroidGetApiParam", "(Ljava/lang/String;)V", "cretorg");
                break;
            default:
                break;
        }
    },

    close_UI_Guide() {
        this.UI_Guide.active = false;
    },

    show_UI_Guide() {
        this.UI_Guide.active = true;
    },

    PvP() {
        Faker_GM.instance.GM_TYPE_PLAY = true;
        cc.director.loadScene("Fplay");
    },

    Bot() {
        Faker_GM.instance.GM_TYPE_PLAY = false;
        cc.director.loadScene("Fplay");
    },

    show_UI_IAP() {
        this.UI_IAP.active = true;
        this.Item_ID = 1;
        this.Item_BG[0].active = true;
        this.Item_BG[1].active = false;
        this.Item_BG[2].active = false;

        this.Item_BG[0].getComponent(cc.Sprite).spriteFrame = this.sprite_BG_State[3];
        this.label_Item_Name.string = this.IAP_Name[this.Item_ID - 1];
        this.label_Item_Price.string = this.IAP_Price[this.Item_ID - 1];

        if (this.BG_Active !== 1) {
            this.Icon_Key_Active.active = false;
            this.label_Item_Price.string = "Owned - Select to use";
        } else {
            this.Icon_Key_Active.getComponent(cc.Sprite).spriteFrame = this.sprite_Key[1];
            this.label_Item_Price.string = "Active";
        }
    },

    close_UI_IAP() {
        this.UI_IAP.active = false;
    },

    next_2_Item() {
        this.Item_ID += 1;
        if (this.Item_ID > 3) {
            this.Item_ID = 1;
        }

        for (let index = 0; index < 3; index++) {
            if (index === (this.Item_ID - 1)) {
                this.Item_BG[index].active = true;
                if (this.IAP_BG.includes(this.Item_ID)) {
                    this.Item_BG[index].getComponent(cc.Sprite).spriteFrame = this.sprite_BG_State[index + 3];
                }
            } else {
                this.Item_BG[index].active = false;
            }
        }

        this.show_Item_Info();
    },

    back_2_Item() {
        this.Item_ID -= 1;
        if (this.Item_ID < 1) {
            this.Item_ID = 3;
        }

        for (let index = 0; index < 3; index++) {
            if (index === (this.Item_ID - 1)) {
                this.Item_BG[index].active = true;
                if (this.IAP_BG.includes(this.Item_ID)) {
                    this.Item_BG[index].getComponent(cc.Sprite).spriteFrame = this.sprite_BG_State[index + 3];
                }
            } else {
                this.Item_BG[index].active = false;
            }
        }

        this.show_Item_Info();
    },

    show_Item_Info() {
        this.label_Item_Name.string = this.IAP_Name[this.Item_ID - 1];
        if (this.IAP_BG.includes(this.Item_ID)) {
            if (this.Item_ID !== this.BG_Active) {
                this.Icon_Key_Active.active = false;
                this.label_Item_Price.string = "Owned - Select to use";
            } else {
                this.Icon_Key_Active.active = true;
                this.Icon_Key_Active.getComponent(cc.Sprite).spriteFrame = this.sprite_Key[1];
                this.label_Item_Price.string = "Active";
            }
        } else {
            this.label_Item_Price.string = this.IAP_Price[this.Item_ID - 1];
            this.Icon_Key_Active.active = true;
            this.Icon_Key_Active.getComponent(cc.Sprite).spriteFrame = this.sprite_Key[0];
        }
    },

    unpack_Item() {
        cc.log("Buy : ", this.Item_ID);
        if (!this.IAP_BG.includes(this.Item_ID)) {
            if (this.Item_ID === 2) {
                sdkbox.IAP.purchase(product_BG_Book_Name);
            }

            if (this.Item_ID === 3) {
                sdkbox.IAP.purchase(product_BG_Room_Name);
            }
        }
    },

    use_Item() {
        if (this.IAP_BG.includes(this.Item_ID)) {
            if (this.Item_ID !== this.BG_Active) {
                this.Icon_Key_Active.active = true;
                this.Icon_Key_Active.getComponent(cc.Sprite).spriteFrame = this.sprite_Key[1];
                this.label_Item_Price.string = "Active";
                this.BG_Active = this.Item_ID;
                Faker_GM.instance.GM_BG_ID = this.BG_Active;
                cc.sys.localStorage.setItem("bgActive", JSON.stringify(this.BG_Active));
            }
        }
    }

    // update (dt) {},
});