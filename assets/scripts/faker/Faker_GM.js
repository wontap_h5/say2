var Faker_GM = cc.Class({
  extends: cc.Component,

  properties: {},

  GM_TYPE_PLAY: null,
  GM_BG_ID: null,

  statics: {
    instance: null
  },
  // LIFE-CYCLE CALLBACKS:

  onLoad() {
    Faker_GM.instance = this;
    this.GM_TYPE_PLAY = false;
    this.GM_BG_ID = -1;
  },

  start() {}

  // update (dt) {},
});