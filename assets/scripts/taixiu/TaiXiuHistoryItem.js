
cc.Class({
    extends: cc.Component,

    properties: {
        iconBig: cc.SpriteFrame,
        iconSmall: cc.SpriteFrame
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.sprite = this.getComponent(cc.Sprite);
        this.anim = this.getComponent(cc.Animation);
    },
    start () {

    },

    setInfor(isBig) {
        if(isBig) {
            this.sprite.spriteFrame = this.iconBig;
        } else {
            this.sprite.spriteFrame = this.iconSmall;
        }
    },

    jump(value) {
        this.anim.enabled = value;
    }



    // update (dt) {},
});
