var StringUtil = require('StringUtil');
var UIController = require('UIController');
var TaiXiuController = require('TaiXiuController');
cc.Class({
    extends: cc.Component,

    properties: {
        autoPos: {
            default: true
        },
        isButtonMiniGame: {
            default: true
        },
        isBowlTX: {
            default: false
        }
    },


    // LIFE-CYCLE CALLBACKS:
    onEnable() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.onBeginDrag, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onDrag, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onEndDrag, this);

        if (this.isButtonMiniGame) {
            this.isDrag = false;
        }
    },
    onDisable() {
        this.node.off(cc.Node.EventType.TOUCH_START, this.onBeginDrag, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onDrag, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onEndDrag, this);
    },

    start() {

    },

    onBeginDrag(event) {        
        
        if (this.isButtonMiniGame) {
            var delayAction = cc.delayTime(0.1);
            this.node.runAction(cc.sequence(
                delayAction,
                cc.callFunc(function () {
                    if (!this.isDrag) {
                        UIController.instance.checkLogin();
                        cc.log("CLICK");
                    }
                }, this)
            ));
        } else {
            UIController.instance.changeZIndex(this.node.parent);                    
        }
    },

    onDrag(event) {
        this.isDrag = true;
        var delta = event.getDelta();
        this.node.position = new cc.v2(this.node.position.x + delta.x, this.node.position.y + delta.y);
        if(this.isBowlTX) {
            var nodeW = this.node.width;
            var nodeH = this.node.height;
            if(this.node.position.x > nodeW / 2 || this.node.position.x < -nodeW / 2 ||
                 this.node.position.y > nodeH / 2 || this.node.position.y < -nodeH / 2) {
                     TaiXiuController.instance.showResult();
                 }
        }
    },

    onEndDrag() {
        this.isDrag = false;
        var nodeW = this.node.width;
        var nodeH = this.node.height;
        var screenW = cc.winSize.width;
        var screenH = cc.winSize.height;
        var minX = -screenW / 2 + nodeW / 2;
        var maxX = screenW / 2 - nodeW / 2;

        var _out = false;
        var pos = this.node.position;
        if (pos.x < -screenW / 2) {
            pos.x = -screenW / 2;
            _out = true;
        } else if (pos.x > screenW / 2) {
            pos.x = screenW / 2;
            _out = true;
        }

        if (pos.y < -screenH / 2) {
            pos.y = -screenH / 2;
            _out = true;
        } else if (pos.y > screenH / 2) {
            pos.y = screenH / 2;
            _out = true;
        }

        if (_out) {
            var move = cc.moveTo(0.3, pos);
            this.node.runAction(move);
        } else {
            if (this.autoPos) {
                if (this.node.position.x > 0) {
                    var moveRight = cc.moveTo(0.3, cc.v2(maxX, this.node.position.y));
                    this.node.runAction(moveRight);
                } else {
                    var moveLeft = cc.moveTo(0.3, cc.v2(minX, this.node.position.y));
                    this.node.runAction(moveLeft);
                }
            }
        }




    }

    // update (dt) {},
});
