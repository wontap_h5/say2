var UIController = require('UIController');
var StringUtil = require('StringUtil');
var WarpConst = require("WarpConst");
var WebsocketClient = require("WebsocketClient");
var NumberTo = require('NumberTo');

var LobbyCtl = require('LobbyCtl');
var Slot1 = require("Pirate_Main");
var Slot2 = require("Slot_2_Main");
var Slot3 = require("Slot_3_Main");
var Slot4 = require("Slot_4_Main");
var GM = require("GM");


var State = {
    BET: 0,
    GATE: 1
};

var Type = {
    NONE: 0,
    INGAME: 1,
    NEWGAME: 2,
    RESULT: 3,
    PAYBACK: 7,
};

var Gate = {
    XIU: 0,
    TAI: 1
};

var TaiXiuController = cc.Class({
    extends: cc.Component,

    properties: {

        gamePanel: cc.Node,
        //History In Game
        hisRowParent: cc.Node,
        hisItem: cc.Prefab,

        //Popup History
        hisPanel: cc.Node,
        hisRowPrefab: cc.Prefab,
        hisRowPopupParent: cc.Node,
        hisScroll: cc.ScrollView,

        //CHAT
        chatScrollView: cc.ScrollView,
        chatInput: cc.EditBox,
        chatPanel: cc.Node,
        chatParent: cc.Node,
        chatRowPrefab: cc.Prefab,

        //KEYBOARD
        keyboardPanel: cc.Node,
        defaultKeyboard: cc.Node,
        otherKeyboard: cc.Node,

        //HELP
        helpPanel: cc.Node,

        //Graph
        graphPanel: cc.Node,
        grid1: cc.Node,
        grid2: cc.Node,
        grid3: cc.Node,
        grid4: cc.Node,
        taixiuIconPrefab: cc.Prefab,
        colors: {
            type: [cc.Color],
            default: []
        },
        xxParent: {
            type: [cc.Node],
            default: []
        },
        prevMatchLabel: cc.Label,
        prevResultLabel: cc.Label,
        page1: cc.Node,
        page2: cc.Node,
        taiLabel: {
            type: [cc.Label],
            default: []
        },
        xiuLabel: {
            type: [cc.Label],
            default: []
        },
        taixiuNumberPrefab: cc.Prefab,

        idLabel: cc.Label,
        //BIG
        bigCountLabel: cc.Label,
        bigBetLabel: cc.Label,
        myBigBetLabel: cc.Label,
        bigGateImage: cc.Node,
        bigTurnLabel: cc.Label,

        //SMALL
        smallCountLabel: cc.Label,
        smallBetLabel: cc.Label,
        mySmallBetLabel: cc.Label,
        smallGateImage: cc.Node,
        smallTurnLabel: cc.Label,

        statusLabel: cc.Label,
        remainTimeLabel: cc.Label,
        resultTimeLabel: cc.Label,
        refundLabel: cc.Label,

        diceEffect: cc.Node,
        diceResultParent: cc.Node,
        dices: {
            default: [],
            type: [cc.Sprite]
        },
        diceSprite: {
            default: [],
            type: [cc.SpriteFrame]
        },
        bowlImage: cc.Node,
        toggleHold: cc.Toggle
    },
    statics: {
        instance: null,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        TaiXiuController.instance = this;
        this.dragScript = this.gamePanel.getComponent('Drag');
        this.bigGateAnim = this.bigGateImage.getComponent(cc.Animation);
        this.smallGateAnim = this.smallGateImage.getComponent(cc.Animation);
        this.diceEffectAnim = this.diceEffect.getComponent(cc.Animation);
        this.shouldFakeUser = true;

        //history row pool
        this.hisRowPool = new cc.NodePool("HisRowPool");
        for (var i = 0; i < 50; i++) {
            var hisRow = cc.instantiate(this.hisRowPrefab);
            this.hisRowPool.put(hisRow);
        }

        //chat pool
        this.chatPool = new cc.NodePool("ChatPool");
        for (var i = 0; i < 50; i++) {
            var chatRow = cc.instantiate(this.chatRowPrefab);
            this.chatPool.put(chatRow);
        }
        //his icon pool
        this.hisIconPool = new cc.NodePool("HisIconPool");
        for (var i = 0; i < 200; i++) {
            var icon = cc.instantiate(this.taixiuIconPrefab);
            this.hisIconPool.put(icon);
        }

        //his item in game
        this.hisItemPool = new cc.NodePool("HisItemPool");
        for (var i = 0; i < 16; i++) {
            var item = cc.instantiate(this.hisItem);
            this.hisItemPool.put(item);
        }

        //number graph pool
        this.graphNumberPool = new cc.NodePool("GraphNumberPool");
        for (var i = 0; i < 100; i++) {
            var number = cc.instantiate(this.taixiuNumberPrefab);
            this.graphNumberPool.put(number);
        }

        // var hisFake = '{"type":23,"history":[{"faces":[4,1,5],"point":10,"matchId":15976544},{"faces":[6,2,4],"point":12,"matchId":15976867},{"faces":[5,3,4],"point":12,"matchId":15977171},{"faces":[1,5,5],"point":11,"matchId":15977486},{"faces":[2,4,4],"point":10,"matchId":15977803},{"faces":[5,1,3],"point":9,"matchId":15978127},{"faces":[4,3,5],"point":12,"matchId":15978441},{"faces":[3,4,3],"point":10,"matchId":15978765},{"faces":[5,5,3],"point":13,"matchId":15979102},{"faces":[1,3,1],"point":5,"matchId":15979420},{"faces":[4,3,3],"point":10,"matchId":15979709},{"faces":[2,5,1],"point":8,"matchId":15980028},{"faces":[2,1,5],"point":8,"matchId":15980324},{"faces":[1,5,2],"point":8,"matchId":15980630},{"faces":[6,2,4],"point":12,"matchId":15980951},{"faces":[6,3,1],"point":10,"matchId":15981264},{"faces":[6,2,3],"point":11,"matchId":15981591},{"faces":[3,4,3],"point":10,"matchId":15981922},{"faces":[5,3,5],"point":13,"matchId":15982274},{"faces":[1,4,1],"point":6,"matchId":15982609},{"faces":[4,4,2],"point":10,"matchId":15982944},{"faces":[6,2,3],"point":11,"matchId":15983265},{"faces":[2,4,5],"point":11,"matchId":15983569},{"faces":[2,5,2],"point":9,"matchId":15983889},{"faces":[2,6,4],"point":12,"matchId":15984185},{"faces":[4,4,5],"point":13,"matchId":15984487},{"faces":[5,4,1],"point":10,"matchId":15984747},{"faces":[2,2,5],"point":9,"matchId":15985005},{"faces":[2,6,6],"point":14,"matchId":15985289},{"faces":[1,6,5],"point":12,"matchId":15985594},{"faces":[2,4,6],"point":12,"matchId":15985897},{"faces":[3,5,2],"point":10,"matchId":15986199},{"faces":[6,1,5],"point":12,"matchId":15986492},{"faces":[1,3,2],"point":6,"matchId":15986773},{"faces":[1,6,4],"point":11,"matchId":15987049},{"faces":[3,2,3],"point":8,"matchId":15987356},{"faces":[3,5,6],"point":14,"matchId":15987647},{"faces":[6,6,5],"point":17,"matchId":15987928},{"faces":[2,1,4],"point":7,"matchId":15988219},{"faces":[5,5,5],"point":15,"matchId":15988516},{"faces":[4,4,3],"point":11,"matchId":15988783},{"faces":[3,5,6],"point":14,"matchId":15989037},{"faces":[4,2,4],"point":10,"matchId":15989296},{"faces":[5,2,5],"point":12,"matchId":15989535},{"faces":[3,5,3],"point":11,"matchId":15989780},{"faces":[2,1,6],"point":9,"matchId":15990026},{"faces":[6,5,4],"point":15,"matchId":15990271},{"faces":[3,1,5],"point":9,"matchId":15990550},{"faces":[3,1,4],"point":8,"matchId":15990845},{"faces":[3,1,1],"point":5,"matchId":15991145},{"faces":[6,1,1],"point":8,"matchId":15991423},{"faces":[1,4,4],"point":9,"matchId":15991690},{"faces":[1,1,3],"point":5,"matchId":15991939},{"faces":[4,3,2],"point":9,"matchId":15992175},{"faces":[4,4,5],"point":13,"matchId":15992412},{"faces":[6,5,6],"point":17,"matchId":15992642},{"faces":[4,1,6],"point":11,"matchId":15992864},{"faces":[5,1,1],"point":7,"matchId":15993105},{"faces":[6,5,4],"point":15,"matchId":15993338},{"faces":[4,2,2],"point":8,"matchId":15993554},{"faces":[3,4,4],"point":11,"matchId":15993785},{"faces":[1,1,3],"point":5,"matchId":15994040},{"faces":[4,4,5],"point":13,"matchId":15994317},{"faces":[6,6,1],"point":13,"matchId":15994577},{"faces":[4,3,3],"point":10,"matchId":15994878},{"faces":[2,4,2],"point":8,"matchId":15995167},{"faces":[2,3,4],"point":9,"matchId":15995450},{"faces":[1,4,3],"point":8,"matchId":15995744},{"faces":[4,4,2],"point":10,"matchId":15996073},{"faces":[5,2,4],"point":11,"matchId":15996400},{"faces":[2,5,3],"point":10,"matchId":15996682},{"faces":[2,1,3],"point":6,"matchId":15996996},{"faces":[3,1,6],"point":10,"matchId":15997300},{"faces":[6,6,6],"point":18,"matchId":15997569},{"faces":[2,6,4],"point":12,"matchId":15997802},{"faces":[4,1,3],"point":8,"matchId":15998064},{"faces":[6,3,6],"point":15,"matchId":15998363},{"faces":[5,3,4],"point":12,"matchId":15998659},{"faces":[3,5,2],"point":10,"matchId":15998933},{"faces":[2,5,5],"point":12,"matchId":15999228},{"faces":[5,4,1],"point":10,"matchId":15999509},{"faces":[4,1,1],"point":6,"matchId":15999809},{"faces":[5,5,5],"point":15,"matchId":16000089},{"faces":[5,3,3],"point":11,"matchId":16000390},{"faces":[2,5,4],"point":11,"matchId":16000674},{"faces":[5,3,6],"point":14,"matchId":16000976},{"faces":[2,2,6],"point":10,"matchId":16001290},{"faces":[1,1,3],"point":5,"matchId":16001604},{"faces":[5,4,1],"point":10,"matchId":16001869},{"faces":[6,2,6],"point":14,"matchId":16002127},{"faces":[1,6,4],"point":11,"matchId":16002402},{"faces":[2,6,4],"point":12,"matchId":16002647},{"faces":[5,3,4],"point":12,"matchId":16002874},{"faces":[5,6,6],"point":17,"matchId":16003135},{"faces":[6,1,2],"point":9,"matchId":16003413},{"faces":[5,5,5],"point":15,"matchId":16003726},{"faces":[6,4,2],"point":12,"matchId":16004033},{"faces":[1,3,6],"point":10,"matchId":16004355},{"faces":[3,1,5],"point":9,"matchId":16004649},{"faces":[4,3,2],"point":9,"matchId":16004897}],"betMin":0,"betMax":0,"match_id":16005161,"chipType":1,"room":{"maxBet":3103125,"minBet":3103125,"maxRate":1,"minRate":1,"state":1,"timeCountDown":2,"minCount":177,"maxCount":230,"timeCountDownNew":10,"stateNew":-1,"id":10,"name":"TAIXIU","type":null,"chipType":1,"bet":1000,"maxUsers":0,"max_player":10000,"curr_num_of_player":-14155,"locked":false,"started":false,"quickplay":false,"funds":293492,"reserve":-169169}}';        
    },

    onEnable() {
        this.refundLabel.string = " ";
        this.onSubTaiXiu();
        this.diceResultParent.active = false;
    },

    onDisable() {
        cc.log("TX  onDisable");
    },

    start() {
        this.bigGateAnim.enabled = false;
        this.smallGateAnim.enabled = false;
        this.keyboardPanel.active = false;

        this.listBetValue = [500, 1000, 5000, 10000, 20000, 50000, 100000, 500000];
        this.betGate = -1;
        this.bigBetValue = 0;
        this.smallBetValue = 0;
        this.myBigBet = 0;
        this.mySmallBet = 0;
        this.shouldOpen = false;

        this.currentId = 0;
        this.prevId = -1;
    },

    updateHisItemInGame(data) {
        var start = 0;
        if (data.length > 16) {
            start = data.length - 16;
        }

        if (this.hisRowParent.childrenCount == 0) {
            for (var i = 0; i < 16; i++) {
                var item = cc.instantiate(this.hisItem);
                item.parent = this.hisRowParent;
                item.active = false;
            }
        }

        for (var i = start; i < data.length; i++) {
            var item = this.hisRowParent.children[i - start];
            item.position.y = 0;
            item.active = true;
            var isBig = data[i].point > 10;
            var script = item.getComponent('TaiXiuHistoryItem');
            script.setInfor(isBig);
            script.jump(false);
            if (i == data.length - 1) {
                script.jump(true);
            }

        }

    },

    getHistory() {
        var str = {
            type: WarpConst.MiniGame.GET_JACKPOT_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.TAI_XIU;
        WebsocketClient.Socket.send(m);

    },

    getHistoryResponse(data) {
        var self = TaiXiuController.instance;
        cc.log("HIS NEW: ", data);
        var dataJSON = JSON.parse(data);
        var hisData = dataJSON.history;
        if (hisData) {
            self.updateHisItemInGame(hisData);
        }
    },

    //KEYBOARD
    showKeyboard(event, gateId) {
        var gate = parseInt(gateId);
        if ((gate == 1 && this.mySmallBet > 0) || (gate == 0 && this.myBigBet > 0)) {
            UIController.instance.showAlert("Bạn không thể đặt 2 cửa trong một phiên");
            return;
        }
        this.betGate = gate;
        this.keyboardPanel.active = true;
        this.showDefaultKeyboard();
        // this.dragScript.enabled = false;
        // //move to origin
        // var moveAction = cc.moveTo(0.3, cc.v2(-365, 183));
        // this.gamePanel.runAction(moveAction);
    },

    hideKeyboard() {
        if (this.defaultKeyboard.active) {
            this.keyboardPanel.active = false;
            // this.dragScript.enabled = true;
            this.bigBetValue = 0;
            this.smallBetValue = 0;
            this.updateTurnBetValue();
        } else {
            this.showDefaultKeyboard();
        }

    },

    showDefaultKeyboard() {
        this.defaultKeyboard.active = true;
        this.otherKeyboard.active = false;
    },

    showOtherKeyboard() {
        this.defaultKeyboard.active = false;
        this.otherKeyboard.active = true;
    },

    //CHAT
    sendChat() {
        var self = this;
        if (this.chatInput.string == "") {
            return;
        }
        var str = self.chatInput.string

        var msg = {
            message: str
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(msg));
        m[1] = WarpConst.WarpRequestTypeCode.GLOBAL_CHAT;
        WebsocketClient.Socket.send(m);
        this.chatInput.string = " ";
    },

    toggleChat() {
        if (this.chatPanel.active) {
            this.chatPanel.active = false;
        } else {
            this.chatPanel.active = true;
            this.chatScrollView.scrollToBottom(1);
        }

    },

    //HELP
    showHelp() {
        UIController.instance.openPopup(this.helpPanel);
    },

    closeHelp() {
        UIController.instance.closePopup(this.helpPanel);
    },

    //History
    showHistory() {
        UIController.instance.openPopup(this.hisPanel, function () {
            var msg = {
                type: WarpConst.MiniGame.GET_USER_HISTORY
            };
            var m = WebsocketClient.instance.encodeRequest(JSON.stringify(msg));
            m[1] = WarpConst.WarpRequestTypeCode.TAI_XIU;
            WebsocketClient.Socket.send(m);
        });
        this.hisScroll.scrollToTop(0.3);
    },

    closeHistory() {
        UIController.instance.closePopup(this.hisPanel);
        while (this.hisRowPopupParent.childrenCount > 0) {
            this.hisRowPool.put(this.hisRowPopupParent.children[0]);
        }
    },

    getUserHistory(data) {
        var self = TaiXiuController.instance;
        var dataJSON = JSON.parse(data);
        cc.log("HISTORY: ", dataJSON);
        var history = dataJSON.history;

        for (var i = 0; i < history.length; i++) {
            var item = history[i];
            var id = item.id;
            var time = item.time;
            var betMax = item.betMax;
            var betMin = item.betMin;
            var result = item.result;
            var win = item.win;

            var hisRow = self.hisRowPool.get();
            hisRow.parent = self.hisRowPopupParent;
            var script = hisRow.getComponent('TaiXiuHistoryRow');
            script.setInfor(i, id, time, betMax > 0 ? "TÀI" : "XỈU", betMax > 0 ? betMax : betMin, decodeURIComponent(escape(result)), win);
        }
    },

    //GRAPH
    showGraph() {
        UIController.instance.openPopup(this.graphPanel);
        this.showGrid1();
        this.showGrid2();
        this.showGrid3();
        this.showGrid4();
    },

    hideGraph() {
        var sefl = this;
        UIController.instance.closePopup(this.graphPanel, function () {


            while (sefl.grid1.childrenCount > 0) {
                sefl.hisIconPool.put(sefl.grid1.children[0]);
            }

            while (sefl.grid4.childrenCount > 0) {
                sefl.hisIconPool.put(sefl.grid4.children[0]);
            }

            while (sefl.xxParent[0].childrenCount > 0) {
                sefl.hisIconPool.put(sefl.xxParent[0].children[0]);
            }

            while (sefl.xxParent[1].childrenCount > 0) {
                sefl.hisIconPool.put(sefl.xxParent[1].children[0]);
            }

            while (sefl.xxParent[2].childrenCount > 0) {
                sefl.hisIconPool.put(sefl.xxParent[2].children[0]);
            }

            while (sefl.grid3.childrenCount > 0) {
                sefl.graphNumberPool.put(sefl.grid3.children[0]);
            }

        });

    },

    showGrid1() {
        var prevMatchId = this.hisData[this.hisData.length - 1].matchId;
        var prevPoint = this.hisData[this.hisData.length - 1].point;
        var prevFace = this.hisData[this.hisData.length - 1].faces;
        this.prevMatchLabel.string = "#" + prevMatchId;
        this.prevResultLabel.string = (prevPoint > 10 ? "TÀI" : "XỈU") + " " + prevFace[0] + "-" + prevFace[1] + "-" + prevFace[2];

        var gridW = this.grid1.width;
        var gridH = this.grid1.height;
        var iconW = 20;
        var length = 21;
        var startX = -gridW / 2;
        var startY = -gridH / 2;
        var oneX = (gridW - (iconW * (length - 1))) / (length - 1);
        var oneY = gridH / 15;
        var n = this.hisData.length;

        var drawing = this.grid1.getComponent(cc.Graphics);
        drawing.clear();
        drawing.lineWidth = 1;
        if (n > length) {
            //hien full
            for (var i = n - length, j = 0; i < n; i++, j++) {
                var point = this.hisData[i].point;
                var icon = this.hisIconPool.get();
                icon.parent = this.grid1;
                var x = startX + j * (oneX + iconW);
                var y = startY + (point - 3) * oneY;
                icon.position = cc.v2(x, y);
                var script = icon.getComponent('TaiXiuIcon');
                script.setColor(point > 10 ? this.colors[StringUtil.TaixiuColor.TAI] : this.colors[StringUtil.TaixiuColor.XIU]);

                if (i == n - length) {
                    drawing.moveTo(x, y);
                } else {
                    drawing.lineTo(x, y);
                }
            }
            drawing.strokeColor = this.colors[StringUtil.TaixiuColor.TONG];
            drawing.stroke();
        } else {
            //khong full
            for (var i = 0, j = 0; i < n; i++, j++) {
                var point = this.hisData[i].point;
                var icon = this.hisIconPool.get();
                icon.parent = this.grid1;
                var x = startX + j * (oneX + 20);
                var y = startY + (point - 3) * oneY;
                icon.position = cc.v2(x, y);
                var script = icon.getComponent('TaiXiuIcon');
                script.setColor(point > 10 ? this.colors[StringUtil.TaixiuColor.TAI] : this.colors[StringUtil.TaixiuColor.XIU]);

                if (i == 0) {
                    drawing.moveTo(x, y);
                } else {
                    drawing.lineTo(x, y);
                }
            }
            drawing.strokeColor = this.colors[StringUtil.TaixiuColor.TONG];
            drawing.stroke();
        }

    },

    showGrid2() {
        var gridW = this.grid2.width;
        var gridH = this.grid2.height;
        var iconW = 20;
        var length = 21;
        var startX = -gridW / 2;
        var startY = -gridH / 2;
        var oneX = (gridW - (iconW * (length - 1))) / (length - 1);
        var oneY = gridH / 5;
        var n = this.hisData.length;

        if (n > length) {
            //3
            for (var k = 0; k < 3; k++) {
                //21
                var dice = 0;
                var drawing = this.xxParent[k].getComponent(cc.Graphics);
                drawing.clear();
                drawing.lineWidth = 1;
                for (var i = n - length, j = 0; i < n; i++, j++) {
                    var face = this.hisData[i].faces;
                    dice = face[k];
                    var icon = this.hisIconPool.get();
                    icon.parent = this.xxParent[k];

                    var x = startX + j * (oneX + iconW);
                    var y = startY + (dice - 1) * oneY;
                    icon.position = cc.v2(x, y);
                    var script = icon.getComponent('TaiXiuIcon');
                    if (k == 0) {
                        script.setColor(this.colors[StringUtil.TaixiuColor.XX1]);
                    } else if (k == 1) {
                        script.setColor(this.colors[StringUtil.TaixiuColor.XX2]);
                    } else if (k == 2) {
                        script.setColor(this.colors[StringUtil.TaixiuColor.XX3]);
                    }

                    if (i == n - length) {
                        drawing.moveTo(x, y);
                    } else {
                        drawing.lineTo(x, y);
                    }

                }
                drawing.strokeColor = this.colors[2 + k];
                drawing.stroke();
            }
        } else {
            for (var k = 0; k < 3; k++) {
                //21
                var dice = 0;
                var drawing = this.xxParent[k].getComponent(cc.Graphics);
                drawing.clear();
                drawing.lineWidth = 1;
                for (var i = 0, j = 0; i < n; i++, j++) {
                    var face = this.hisData[i].faces;
                    dice = face[k];
                    var icon = this.hisIconPool.get();
                    icon.parent = this.xxParent[k];

                    var x = startX + j * (oneX + iconW);
                    var y = startY + (dice - 1) * oneY;
                    icon.position = cc.v2(x, y);
                    var script = icon.getComponent('TaiXiuIcon');
                    if (k == 0) {
                        script.setColor(this.colors[StringUtil.TaixiuColor.XX1]);
                    } else if (k == 1) {
                        script.setColor(this.colors[StringUtil.TaixiuColor.XX2]);
                    } else if (k == 2) {
                        script.setColor(this.colors[StringUtil.TaixiuColor.XX3]);
                    }

                    if (i == 0) {
                        drawing.moveTo(x, y);
                    } else {
                        drawing.lineTo(x, y);
                    }

                }
                drawing.strokeColor = this.colors[2 + k];
                drawing.stroke();
            }
        }

    },

    showGrid3() {
        //create array to store 20 col
        var arrTemp = [];
        cc.log("TX HISDATA: ", this.hisData);
        var tempBig = !(this.hisData[0].point > 10);
        for (var i = 0; i < this.hisData.length; i++) {
            var point = this.hisData[i].point;
            var isBig = point > 10;

            if (isBig != tempBig) {
                if (arrTemp.length > 0) {
                    //truoc khi khai bao bien moi thi nhet het so 0 vao cac array con thieu (chua du 5 phan tu)
                    var arrBefore = arrTemp[arrTemp.length - 1];
                    if (arrBefore.length < 5) {
                        var minus = 5 - arrBefore.length;
                        for (var j = 0; j < minus; j++) {
                            arrBefore.push(0);
                        }
                    }
                }
                var arr5 = [];
                arr5.push(point);
                tempBig = isBig;
                arrTemp.push(arr5);
            } else {
                var arr5 = arrTemp[arrTemp.length - 1];
                cc.log("TX    ___ ", (arrTemp.length - 1));
                if (arr5.length < 5) {
                    arr5.push(point);
                }
            }
        } //lay dc du lieu roi     


        var taiCount = 0;
        var xiuCount = 0;

        var start = 0;
        if (arrTemp.length >= 20) {
            start = arrTemp.length - 20;
        }

        for (var i = start; i < arrTemp.length; i++) {
            var arr = arrTemp[i];
            for (var j = 0; j < arr.length; j++) {
                var point = arr[j];
                if (point > 0) {
                    if (point > 10) {
                        taiCount++;
                    } else {
                        xiuCount++;
                    }
                }
                var number = this.graphNumberPool.get();
                number.parent = this.grid3;
                var script = number.getComponent("TaiXiuNumber");
                script.setNumber(point);
            }
        }


        this.taiLabel[0].string = "TÀI:" + taiCount;
        this.xiuLabel[0].string = "XỈU:" + xiuCount;

    },

    showGrid4() {
        var n = this.hisData.length;
        var taiCount = 0;
        for (var i = 0; i < n; i++) {
            var point = this.hisData[i].point;
            if (point > 10) {
                taiCount++;
            }
            var icon = this.hisIconPool.get();
            icon.parent = this.grid4;
            var script = icon.getComponent('TaiXiuIcon');
            script.setColor(point > 10 ? this.colors[StringUtil.TaixiuColor.TAI] : this.colors[StringUtil.TaixiuColor.XIU]);
        }
        this.taiLabel[1].string = "TÀI:" + taiCount;
        this.xiuLabel[1].string = "XỈU:" + (n - taiCount);

    },

    toggleXX(event, type) {
        if (type == 0) {
            this.grid1.active = event.isChecked;
        } else if (type == 1) {
            this.xxParent[0].active = event.isChecked;
        } else if (type == 2) {
            this.xxParent[1].active = event.isChecked;
        } else if (type == 3) {
            this.xxParent[2].active = event.isChecked;
        }
    },

    nextGraph() {
        var self = this;
        var zoomIn = cc.scaleTo(0.1, 1.2);
        var zoomOut = cc.scaleTo(0.1, 0.8);
        this.page1.runAction(cc.sequence(
            zoomOut,
            cc.callFunc(function () {
                self.page1.active = false;
                self.page1.setScale(1, 1);
                self.page2.active = true;

                this.page2.runAction(cc.sequence(
                    zoomIn,
                    cc.callFunc(function () {
                        self.page2.setScale(1, 1);
                    }, this)
                ));

            }, this)
        ));


    },

    prevGraph() {
        var self = this;
        var zoomIn = cc.scaleTo(0.1, 1.2);
        var zoomOut = cc.scaleTo(0.1, 0.8);
        this.page1.active = true;
        this.page1.setScale(1, 1);
        this.page2.setScale(1, 1);

        this.page2.runAction(cc.sequence(
            zoomOut,
            cc.callFunc(function () {
                self.page1.active = true;
                self.page2.active = false;

                this.page1.runAction(cc.sequence(
                    zoomIn,
                    cc.callFunc(function () {
                        self.page2.setScale(1, 1);
                        self.page1.setScale(1, 1);
                    }, this)
                ))
            }, this)
        ));


    },

    update(dt) {
        this.remainTime -= dt;
        this.remainTimeLabel.string = StringUtil.formatDuration(this.remainTime);
        this.resultTimeLabel.string = StringUtil.formatDuration(this.remainTime);
        UIController.instance.updateRemainTime(this.remainTime);

        if (this.currentType == Type.RESULT && this.remainTime < 3 && this.shouldOpen) {
            this.showResult();
            this.shouldOpen = false;
        }
    },

    changeHold(event) {
        if (event.isChecked) {
            this.shouldOpen = true;
        } else {
            this.shouldOpen = false;
            if (this.currentType == Type.RESULT) {
                this.showResult();
            }
        }
    },

    //HANDLER
    onSubTaiXiu() {
        var msg = {
            type: 23,
            bet: 0,
            chipType: 1
        };

        var msgJSON = JSON.stringify(msg);
        var data = WebsocketClient.instance.encodeRequest(msgJSON);
        data[1] = WarpConst.WarpRequestTypeCode.TAI_XIU;
        WebsocketClient.Socket.send(data);
        WebsocketClient.OnTaiXiuSubDone = this.onTaiXiuSubDone;
        WebsocketClient.OnTaiXiuMiniRoomChange = this.onStateTaiXiu;
        WebsocketClient.OnWorldChat = this.onChat;
        WebsocketClient.OnAddBetDone = this.betResponse;
        WebsocketClient.OnTaiXiuGetUserHistoryDone = this.getUserHistory;
        WebsocketClient.OnTaiXiuHistoryDone = this.getHistoryResponse;
    },
    onUnSubTaiXiu() {
        cc.log("TX UNSUB");
        WebsocketClient.OnTaiXiuSubDone = null;
        WebsocketClient.OnTaiXiuMiniRoomChange = null;
        WebsocketClient.OnWorldChat = null;
        WebsocketClient.OnAddBetDone = null;
        WebsocketClient.OnTaiXiuGetUserHistoryDone = null;
        WebsocketClient.OnTaiXiuHistoryDone = null;

        setTimeout(function () {
            UIController.instance.subMsg();
        }, 3000);

    },

    onTaiXiuSubDone(data) {
        var taixiuData = JSON.parse(data);
        var hisData = taixiuData.history;
        TaiXiuController.instance.taixiuData = taixiuData;
        TaiXiuController.instance.hisData = hisData;
        TaiXiuController.instance.updateHisItemInGame(hisData);
        TaiXiuController.instance.onFirstState();
    },

    onFirstState() {
        var self = this;
        this.idLabel.string = "#" + this.taixiuData.match_id;
        this.currentId = this.taixiuData.matchId;
        var room = this.taixiuData.room;
        this.remainTimeLabel.string = StringUtil.formatDuration(room.timeCountDown);

        var bigCount = room.maxCount;
        var bigBet = room.maxBet;
        this.myBigBet = this.taixiuData.betMax;

        var smallCount = room.minCount;
        var smallBet = room.minBet;
        this.mySmallBet = this.taixiuData.betMin;

        this.prevBigBet = 0;
        this.prevSmallBet = 0;

        this.bigCountLabel.string = bigCount;
        new NumberTo(this.bigBetLabel, 0, bigBet, 1).onUpdate(function (num) {
            self.bigBetLabel.string = StringUtil.formatNumber(num);
        });
        new NumberTo(this.myBigBetLabel, 0, this.myBigBet, 1).onUpdate(function (num) {
            self.myBigBetLabel.string = StringUtil.formatNumber(num);
        });

        this.smallCountLabel.string = smallCount;
        new NumberTo(this.smallBetLabel, 0, smallBet, 1).onUpdate(function (num) {
            self.smallBetLabel.string = StringUtil.formatNumber(num);
        });
        new NumberTo(this.mySmallBetLabel, 0, this.mySmallBet, 1).onUpdate(function (num) {
            self.mySmallBetLabel.string = StringUtil.formatNumber(num);
        });

        self.diceResultParent.active = false;
    },

    onStateTaiXiu(data) {
        cc.log("TX STATE==", data);
        var self = TaiXiuController.instance;
        var dataJSON = JSON.parse(data);
        var type = dataJSON.type;
        if (type == 6) { //server auto unsub
            cc.log("TX6");
            setTimeout(function () {
                self.onSubTaiXiu();
            }, 2000);

        }
        self.currentType = type;
        if (type == Type.PAYBACK) { //refund
            var total = dataJSON.total; //tien cua minh
            var pot = dataJSON.pot;
            var payback = dataJSON.payback;

            switch (GM.Stay) {
                case 0:
                    cc.log("Playing on Lobby");
                    self.setBalance(total);
                    break;
                case 1:
                    cc.log("Playing on Slot 1");
                    GM.Gold_Lastest = total;
                    Slot1.scope.notify_Gold_State_Change();
                    break;
                case 2:
                    cc.log("Playing on Slot 2");
                    GM.Gold_Lastest = total;
                    Slot2.scope.notify_Gold_State_Change();
                    break;
                case 3:
                    cc.log("Playing on Slot 3");
                    GM.Gold_Lastest = total;
                    Slot3.scope.notify_Gold_State_Change();
                    break;
                case 4:
                    cc.log("Playing on Slot 4");
                    GM.Gold_Lastest = total;
                    Slot4.scope.notify_Gold_State_Change();
                    break;
                default:
                    break;
            }

            self.refundLabel.string = "trả cửa\n".toUpperCase() + StringUtil.formatNumber(payback);
            if (pot == Gate.TAI) {
                self.myBigBet -= payback;
                self.myBigBetLabel.string = StringUtil.formatNumber(self.myBigBet);
            } else {
                self.mySmallBet -= payback;
                self.mySmallBetLabel.string = StringUtil.formatNumber(self.mySmallBet);
            }
        } else {
            var room = dataJSON.room;
            if (!room) {
                return;
            }
            //INGAME
            var state = room.state;
            TaiXiuController.instance.currentState = state;
            self.remainTime = room.timeCountDown;
            var bigCount = room.maxCount;
            self.bigCount = bigCount;
            var bigBet = room.maxBet;
            var smallCount = room.minCount;
            self.smallCount = smallCount;
            var smallBet = room.minBet;

            self.currentState = state;

            if (state == State.BET) {
                self.statusLabel.string = "THỜI GIAN ĐẶT CỬA";
                self.refundLabel.string = " ";
            } else if (state == State.GATE) {
                self.statusLabel.string = "THỜI GIAN CÂN CỬA";
                self.keyboardPanel.active = false;
            }

            self.bigCountLabel.string = bigCount;
            new NumberTo(self.bigBetLabel, self.prevBigBet, bigBet, 1).onUpdate(function (num) {
                self.bigBetLabel.string = StringUtil.formatNumber(num);
            });

            self.smallCountLabel.string = smallCount;
            new NumberTo(self.smallBetLabel, self.prevSmallBet, smallBet, 1).onUpdate(function (num) {
                self.smallBetLabel.string = StringUtil.formatNumber(num);
            });

            if (type == Type.INGAME) {
                self.diceResultParent.active = false;
                self.bigGateAnim.enabled = false;
                self.smallGateAnim.enabled = false;
                self.diceEffect.active = false;
                self.shouldOpen = self.toggleHold.isChecked;
            }

            if (type == Type.NEWGAME) {
                if (dataJSON.match_id) {
                    self.idLabel.string = "#" + dataJSON.match_id;
                    self.currentId = dataJSON.match_id;
                }
                self.bigGateAnim.enabled = false;
                self.smallGateAnim.enabled = false;

                self.myBigBet = 0;
                self.mySmallBet = 0;
                self.myBigBetLabel.string = self.myBigBet;
                self.mySmallBetLabel.string = self.mySmallBet;

                self.bigBetValue = 0;
                self.smallBetValue = 0;

                self.updateTurnBetValue();

                self.diceEffect.active = false;
                self.diceResultParent.active = false;

                self.hideBowl();

                self.refundLabel.string = " ";
            }

            if (type == Type.RESULT) {
                self.resultTimeLabel.node.active = true;
                self.remainTimeLabel.node.active = false;
                self.refundLabel.string = " ";
                self.statusLabel.string = "thời gian hiển thị kết quả".toUpperCase();

                self.keyboardPanel.active = false;
                cc.log("DATA: ", dataJSON);
                if (dataJSON.vi) {
                    self.resultData = dataJSON;
                    self.diceEffect.active = true;
                    self.diceEffectAnim.play();

                    //if hold, show bowl and disbled drag
                    if (self.toggleHold.isChecked) {
                        self.showBowl();
                    }
                    // } else {
                    //     self.showResult();
                    // }
                }
            } else {
                self.resultTimeLabel.node.active = false;
                self.remainTimeLabel.node.active = true;
            }

            self.prevBigBet = bigBet;
            self.prevSmallBet = smallBet;
        }



    },


    onChat(data) {
        var self = TaiXiuController.instance;
        var dataJSON = JSON.parse(data);
        cc.log("chat: ", dataJSON);
        var msg = dataJSON.message;
        var user = dataJSON.user;
        var userName = user.displayName;

        if (self.chatPool.size() == 0) {
            self.chatPool.put(self.chatParent.children[0]);
        }

        var chatRow = self.chatPool.get();
        chatRow.parent = self.chatParent;
        chatRow.getComponent(cc.RichText).string = '<color=#FFEB1D>' + userName + ':</color> ' + decodeURIComponent(escape(msg));
        self.chatScrollView.scrollToBottom(1);

    },

    listBetClick(event, index) {
        if (index < 0 || index > this.listBetValue.length - 1) {
            return;
        }

        var value = this.listBetValue[index];
        if (this.betGate == Gate.TAI) {
            this.bigBetValue += value;
            this.smallBetValue = 0;
        } else {
            this.smallBetValue += value;
            this.bigBetValue = 0;
        }
        this.updateTurnBetValue();
    },

    //-1 back, 10 x 1000
    numberClick(event, number) {
        var num = parseInt(number);
        if (this.betGate == Gate.TAI) {
            if (num == -1) {
                this.bigBetValue = Math.floor(this.bigBetValue / 10);
            } else if (num == 10) {
                this.bigBetValue *= 1000;
            } else {
                this.bigBetValue = this.bigBetValue * 10 + num;
            }
            this.smallBetValue = 0;
            if (this.bigBetValue > WarpConst.GameBase.userInfo.desc.gold) {
                this.bigBetValue = WarpConst.GameBase.userInfo.desc.gold;
            }
        } else {
            if (num == -1) {
                this.smallBetValue = Math.floor(this.smallBetValue / 10);
            } else if (num == 10) {
                this.bigBetValue *= 1000;
            } else {
                this.smallBetValue = (this.smallBetValue * 10) + num;
            }
            this.bigBetValue = 0;
            if (this.smallBetValue > WarpConst.GameBase.userInfo.desc.gold) {
                this.smallBetValue = WarpConst.GameBase.userInfo.desc.gold;
            }
        }

        this.updateTurnBetValue();
    },

    doubleBet() {
        if (this.currentState != State.BET) {
            //alert
            UIController.instance.showAlert("Đã hết thời gian đặt cược");
            return;
        }
        if (this.betGate == Gate.TAI) {
            this.bigBetValue *= 2;
        } else {
            this.smallBetValue *= 2;
        }
        this.updateTurnBetValue();
    },


    updateTurnBetValue() {
        if (this.bigBetValue > 0) {
            this.bigTurnLabel.string = StringUtil.formatNumber(this.bigBetValue);
        } else {
            this.bigTurnLabel.string = "ĐẶT CỬA";
        }
        cc.log(this.smallBetValue);
        if (this.smallBetValue > 0) {
            this.smallTurnLabel.string = StringUtil.formatNumber(this.smallBetValue);
        } else {
            this.smallTurnLabel.string = "ĐẶT CỬA";
        }

    },

    bet() {
        var value = this.betGate == Gate.TAI ? this.bigBetValue : this.smallBetValue;
        if (value < 200) {
            UIController.instance.showAlert("Số gold đặt cược tối thiểu là 200");
            return;
        }
        //check player money
        if (GM.Stay === 0) {
            if (WarpConst.GameBase.userInfo.desc.gold < value) {
                UIController.instance.showAlert("Bạn không đủ gold để đặt");
                return;
            }
        } else {
            if (GM.Gold_Lastest < value) {
                UIController.instance.showAlert("Bạn không đủ gold để đặt");
                return;
            }
        }

        if (this.currentState != State.BET) {
            //check state bet
            UIController.instance.showAlert("Đã hết thời gian đặt cược");
            return;
        }

        var msg = {
            type: WarpConst.MiniGame.ADD_BET,
            bet: value,
            chipType: 1,
            pot: this.betGate
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(msg));
        m[1] = WarpConst.WarpRequestTypeCode.TAI_XIU;
        WebsocketClient.Socket.send(m);

        this.hideKeyboard();
    },

    betResponse(data) {
        var self = TaiXiuController.instance;
        var dataJSON = JSON.parse(data);

        if (!dataJSON.bet) {
            self.bigBetValue = 0;
            self.smallBetValue = 0;
            self.updateTurnBetValue();
            return;
        }
        var bet = dataJSON.bet;
        var total = dataJSON.total;
        var totalPot = dataJSON.totalPot;
        var gate = dataJSON.pot;
        var currentGold = dataJSON.totalGold;

        if (bet === total) {
            //lan dau tien dat cua thi fake user +1
            if (gate == 1) {
                self.bigCount += 1;
            } else {
                self.smallCount += 1;
            }
            self.bigCountLabel.string = self.bigCount;
            self.smallCountLabel.string = self.smallCount;
        }

        cc.log('TX BET ', JSON.stringify(dataJSON));

        switch (GM.Stay) {
            case 0:
                cc.log("Playing on Lobby");
                self.setBalance(currentGold);
                break;
            case 1:
                cc.log("Playing on Slot 1");
                GM.Gold_Lastest = currentGold;
                Slot1.scope.notify_Gold_State_Change();
                break;
            case 2:
                cc.log("Playing on Slot 2");
                GM.Gold_Lastest = currentGold;
                Slot2.scope.notify_Gold_State_Change();
                break;
            case 3:
                cc.log("Playing on Slot 3");
                GM.Gold_Lastest = currentGold;
                Slot3.scope.notify_Gold_State_Change();
                break;
            case 4:
                cc.log("Playing on Slot 4");
                GM.Gold_Lastest = currentGold;
                Slot4.scope.notify_Gold_State_Change();
                break;
            default:
                break;
        }

        if (gate == Gate.TAI) {
            self.myBigBet += bet;

            self.myBigBetLabel.string = StringUtil.formatNumber(self.myBigBet);
            // self.bigBetLabel.string = StringUtil.formatNumber(totalPot);

            self.bigBetValue = 0;

        } else {
            self.mySmallBet += bet;

            self.mySmallBetLabel.string = StringUtil.formatNumber(self.mySmallBet);
            // self.smallBetLabel.string = StringUtil.formatNumber(totalPot);

            self.smallBetValue = 0;
        }

        self.updateTurnBetValue();

    },

    diceEffectDone() {
        this.diceResultParent.active = true;
        var vi = this.resultData.vi;
        var faces = vi.faces;
        for (var i = 0; i < faces.length; i++) {
            var dicePoint = faces[i];
            this.dices[i].spriteFrame = this.diceSprite[dicePoint - 1];
        }
        if (!this.toggleHold.isChecked) {
            this.showResult();
        }
    },

    showResult() {
        // var self = this;
        // var data = this.resultData;
        // cc.log("===========RESULT: ", data);
        // var vi = data.vi;
        // var faces = vi.faces;
        // var point = vi.point;
        // var matchId = vi.matchId;
        // var currentBalance = data.chip;
        // var chipChange = data.chipChange; //tong so tien thua        
        // self.setBalance(currentBalance);
        // UIController.instance.taixiuBet = 0;
        // switch (GM.Stay) {
        //     case 0:
        //         cc.log("Playing on Lobby");
        //         self.setBalance(currentBalance);
        //         break;
        //     case 1:
        //         cc.log("Playing on Slot 1");
        //         GM.Gold_Lastest = currentBalance;
        //         Slot1.scope.notify_Gold_State_Change();
        //         break;
        //     case 2:
        //         cc.log("Playing  on Slot 2");
        //         GM.Gold_Lastest = currentBalance;
        //         Slot2.scope.notify_Gold_State_Change();
        //         break;
        //     default:
        //         break;
        // }

        // if (chipChange > 0) {
        //     self.refundLabel.string = "THẮNG " + StringUtil.formatNumber(chipChange);
        //     self.refundLabel.node.color = cc.color(255, 200, 0, 255);
        // } else {
        //     self.refundLabel.node.color = cc.color(255, 255, 255, 255);
        //     if (self.myBigBet != 0) {
        //         self.refundLabel.string = "THUA: -" + StringUtil.formatNumber(self.myBigBet);
        //     } else if (self.mySmallBet != 0) {
        //         self.refundLabel.string = "THUA: -" + StringUtil.formatNumber(self.mySmallBet);
        //     }

        //     if (point > 10) {
        //         self.bigGateAnim.enabled = true;
        //         self.statusLabel.string = "KẾT QUẢ - TÀI : " + point + " ĐIỂM";
        //     } else {
        //         self.smallGateAnim.enabled = true;
        //         self.statusLabel.string = "KẾT QUẢ - XỈU : " + point + " ĐIỂM";
        //     }

        //     this.hideBowl();

        //     cc.log("HIS DATA: ", self.hisData);

        //     if (self.currentId != self.prevId) {
        //         self.getHistory();
        //         // if (self.hisData.length > 99) {
        //         //     self.hisData.splice(0, 1);
        //         // }
        //         // self.hisData.push({
        //         //     faces: faces,
        //         //     point: point,
        //         //     matchId: matchId
        //         // });
        //         // self.updateHisItemInGame(self.hisData);
        //         self.prevId = self.currentId;
        //     }
        //     // self.hisData.push({
        //     //     faces: faces,
        //     //     point: point,
        //     //     matchId: matchId
        //     // });
        //     // self.updateHisItemInGame(self.hisData);
        // }

        var self = this;
        var data = this.resultData;
        var vi = data.vi;
        var faces = vi.faces;
        var point = vi.point;
        var matchId = vi.matchId;
        var currentBalance = data.chip;
        var chipChange = data.chipChange;
        cc.log("TX RESULT: ", chipChange);
        self.setBalance(currentBalance);
        switch (GM.Stay) {
            case 0:
                cc.log("Playing on Lobby");
                self.setBalance(currentBalance);
                break;
            case 1:
                cc.log("Playing on Slot 1");
                GM.Gold_Lastest = currentBalance;
                Slot1.scope.notify_Gold_State_Change();
                break;
            case 2:
                cc.log("Playing  on Slot 2");
                GM.Gold_Lastest = currentBalance;
                Slot2.scope.notify_Gold_State_Change();
                break;
            case 3:
                cc.log("Playing on Slot 3");
                GM.Gold_Lastest = currentBalance;
                Slot3.scope.notify_Gold_State_Change();
                break;
            case 4:
                cc.log("Playing  on Slot 4");
                GM.Gold_Lastest = currentBalance;
                Slot4.scope.notify_Gold_State_Change();
                break;
            default:
                break;
        }

        if (chipChange > 0) {
            self.refundLabel.string = "THẮNG " + StringUtil.formatNumber(chipChange);
            self.refundLabel.node.color = cc.color(255, 200, 0, 255);
        } else if (chipChange < 0) {
            self.refundLabel.node.color = cc.color(255, 255, 255, 255);
            self.refundLabel.string = "THUA: " + StringUtil.formatNumber(chipChange);
        }

        if (point > 10) {
            self.bigGateAnim.enabled = true;
            self.statusLabel.string = "KẾT QUẢ - TÀI : " + point + " ĐIỂM";
        } else {
            self.smallGateAnim.enabled = true;
            self.statusLabel.string = "KẾT QUẢ - XỈU : " + point + " ĐIỂM";
        }

        this.hideBowl();

        cc.log("HIS DATA:  ", self.hisData);

        if (self.currentId != self.prevId) {
            self.getHistory();
            self.prevId = self.currentId;
        }

    },

    showBowl() {
        this.bowlImage.active = true;
        this.dragScript.enabled = false;
    },

    hideBowl() {
        this.bowlImage.active = false;
        this.bowlImage.position = cc.v2(0, 0);
        this.dragScript.enabled = true;
    },
    //SET MONEY
    //minus
    changeBalance(balanceChange) {
        LobbyCtl.instance.addUserGold(balanceChange);
    },
    setBalance(balance) {
        LobbyCtl.instance.setUserGold(balance);
    }

});