
cc.Class({
    extends: cc.Component,

    properties: {
        label: cc.Label,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

    },

    //
    start() {

    },
    setNumber(num) {
        if (num != 0) {
            if (num > 10) {
                this.node.color = cc.Color.BLACK;
            } else {
                this.node.color = cc.Color.WHITE;
            }
            this.label.string = num;
        } else {
            this.label.string = "";
        }
    }

    // update (dt) {},
});
