var StringUtil = require('StringUtil');
cc.Class({
    extends: cc.Component,

    properties: {
        evenRowSprite: cc.SpriteFrame,
        oddRowSprite: cc.SpriteFrame,
        idBox: cc.Sprite,
        timeBox: cc.Sprite,
        gateBox: cc.Sprite,
        moneyBox: cc.Sprite,
        resultBox: cc.Sprite,
        refundBox: cc.Sprite,

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {

    },

    setInfor(index, id, time, gate, money, result, refund) {
        var isEven = index % 2 === 0;
        this.idBox.spriteFrame = isEven ? this.evenRowSprite : this.oddRowSprite;
        this.timeBox.spriteFrame = isEven ? this.evenRowSprite : this.oddRowSprite;
        this.gateBox.spriteFrame = isEven ? this.evenRowSprite : this.oddRowSprite;
        this.moneyBox.spriteFrame = isEven ? this.evenRowSprite : this.oddRowSprite;
        this.resultBox.spriteFrame = isEven ? this.evenRowSprite : this.oddRowSprite;
        this.refundBox.spriteFrame = isEven ? this.evenRowSprite : this.oddRowSprite;

        this.idBox.node.children[0].getComponent(cc.Label).string = "#" + id;
        this.timeBox.node.children[0].getComponent(cc.Label).string = time;
        this.gateBox.node.children[0].getComponent(cc.Label).string = gate;
        this.moneyBox.node.children[0].getComponent(cc.Label).string = StringUtil.formatNumber(money);
        this.resultBox.node.children[0].getComponent(cc.Label).string = result;
        this.refundBox.node.children[0].getComponent(cc.Label).string = StringUtil.formatNumber(refund);

    },

    // update (dt) {},
});
