cc.Class({
    extends: cc.Component,

    properties: {
        Item_BG: {
            default: [],
            type: [cc.Node]
        },
        sprite_List: {
            default: [],
            type: [cc.SpriteFrame]
        },
        label_Sess: {
            default: null,
            type: cc.Label
        },
        label_Time: {
            default: null,
            type: cc.Label
        },
        label_Bet: {
            default: null,
            type: cc.Label
        },
        label_Result: {
            default: null,
            type: cc.Label
        },
        label_Class: {
            default: null,
            type: cc.Label
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {},

    start() {

    },

    update(dt) {},

    init_Item_History(data) {
        for (let index = 0; index < this.Item_BG.length; index++) {
            this.Item_BG[index].getComponent(cc.Sprite).spriteFrame = this.sprite_List[data.bg_index];
        }
        this.label_Sess.string = "#" + data.sess;
        this.label_Time.string = data.time;
        this.label_Bet.string = this.convert_money(data.bet);
        this.label_Result.string = this.convert_money(data.gold);
        this.label_Class.string = data.class;
    },

    convert_money(price) {
        return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " ";
    }
});