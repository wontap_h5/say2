var WarpConst = require("WarpConst");
var WebsocketClient = require("WebsocketClient");

var LobbyCtl = require("LobbyCtl");
var Slot1 = require("Pirate_Main");
var Slot2 = require("Slot_2_Main");
var Slot3 = require("Slot_3_Main");
var Slot4 = require("Slot_4_Main");
var GM = require("GM");
var Mini_Poker = cc.Class({
    extends: cc.Component,

    statics: {
        scope: null
    },

    properties: {
        Bet_selector: {
            default: [],
            type: cc.Toggle
        },
        GP_Column_1: {
            default: null,
            type: cc.Node
        },
        GP_Column_2: {
            default: null,
            type: cc.Node
        },
        GP_Column_3: {
            default: null,
            type: cc.Node
        },
        GP_Column_4: {
            default: null,
            type: cc.Node
        },
        GP_Column_5: {
            default: null,
            type: cc.Node
        },
        Spin_Result: {
            default: null,
            type: cc.Node
        },
        label_Gold_Add: {
            default: null,
            type: cc.Label
        },
        label_Jackpot: {
            default: null,
            type: cc.Label
        },
        Popup_Rank: {
            default: null,
            type: cc.Node
        },
        Popup_Guide: {
            default: null,
            type: cc.Node
        },
        Popup_History: {
            default: null,
            type: cc.Node
        },
        Rank_Content: {
            default: null,
            type: cc.Node
        },
        Item_Rank_Prefab: {
            default: null,
            type: cc.Prefab
        },
        History_Content: {
            default: null,
            type: cc.Node
        },
        Item_History_Prefab: {
            default: null,
            type: cc.Prefab
        },
        Btn_Auto_Spin: {
            default: null,
            type: cc.Toggle
        },
        Btn_Flash_Spin: {
            default: null,
            type: cc.Toggle
        },
        Popup_Error_Action: {
            default: null,
            type: cc.Node
        },
        label_popup_error_action_mes: {
            default: null,
            type: cc.Label
        },
        sprite_Card: {
            default: [],
            type: [cc.SpriteFrame]
        },
        Hand_Spin: {
            default: null,
            type: cc.Node
        },
        Game_Drag : {
            default: null,
            type: cc.Node
        }
    },

    Spin_Count: null,
    Spining: null,
    Result_Spin_Server: null,
    SPIN_AUTO_ONLINE: null,
    SPIN_FLASH_ONLINE: null,

    Server_Data_Room: null,
    Spin_Bet: null,
    Last_Jackpot: null,

    // LIFE-CYCLE CALLBACKS:

    OnMiniPokerSubDone(status, res) {
        cc.log("OnMiniPokerSubDone : ", status + " _ " + res);
        var data = JSON.parse(res);
        if (status == WarpConst.WarpResponseResultCode.SUCCESS) {
            if ("undefined" !== typeof data.type) {
                if (data.type === 23) {
                    Mini_Poker.scope.effect_Update_Gold(0, data.jackpot, Mini_Poker.scope.label_Jackpot.node, Mini_Poker.scope.label_Jackpot);
                    Mini_Poker.scope.Last_Jackpot = data.jackpot;
                    Mini_Poker.scope.Spin_Bet = data.bet;
                    cc.log("Spin_Bet new : ", Mini_Poker.scope.Spin_Bet);
                } else {
                    Mini_Poker.scope.Show_Popup_Error_Action(" Kết nối thất bại \n Vui lòng thử lại sau ! ");
                    Mini_Poker.scope.f5_bet();
                }
            } else {
                Mini_Poker.scope.Show_Popup_Error_Action(" Kết nối thất bại \n Vui lòng thử lại sau ! ");
                Mini_Poker.scope.f5_bet();
            }
        } else {
            Mini_Poker.scope.Show_Popup_Error_Action(" Đổi mức cược không thành công. \n Vui lòng thử lại sau. ");
            Mini_Poker.scope.f5_bet();
        }
    },

    f5_bet() {
        switch (Mini_Poker.scope.Spin_Bet) {
            case 100:
                Mini_Poker.scope.Bet_selector[0].isChecked = true;
                break;
            case 1000:
                Mini_Poker.scope.Bet_selector[1].isChecked = true;
                break;
            case 10000:
                Mini_Poker.scope.Bet_selector[2].isChecked = true;
                break;
        }
    },

    OnMiniPokerStartDone(res) {
        var raw_data = JSON.parse(res);
        if ("undefined" !== typeof raw_data.result) {
            var data = JSON.parse(raw_data.result);
            cc.log("OnMiniPokerStartDone : ", JSON.stringify(data));
            Mini_Poker.scope.Result_Spin_Server = data;

            Mini_Poker.scope.Manager_Spin_Anim();

            var time_update_money = 1600; // 3500
            if (Mini_Poker.scope.SPIN_AUTO_ONLINE) {
                cc.log("SPIN_AUTO");
                Mini_Poker.scope.Machine_Spin(1700, 1200); //  time_update_money + delaytime
            } else if (Mini_Poker.scope.SPIN_FLASH_ONLINE) {
                cc.log("SPIN_FLASH");
                Mini_Poker.scope.Machine_Spin(800, 900);
                time_update_money = 750;
            } else {
                cc.log("SPIN_MANUAL");
                Mini_Poker.scope.Machine_Spin(1700, 1500);
            }

            setTimeout(function () {
                switch (GM.Stay) {
                    case 0:
                        cc.log("Playing on Lobby");
                        Mini_Poker.scope.setBalance(data.gold);
                        break;
                    case 1:
                        cc.log("Playing on Slot 1");
                        GM.Gold_Lastest = data.gold;
                        Slot1.scope.notify_Gold_State_Change();
                        break;
                    case 2:
                        cc.log("Playing on Slot 2");
                        GM.Gold_Lastest = data.gold;
                        Slot2.scope.notify_Gold_State_Change();
                        break;
                    case 3:
                        cc.log("Playing on Slot 3");
                        GM.Gold_Lastest = data.gold;
                        Slot3.scope.notify_Gold_State_Change();
                        break;
                    case 4:
                        cc.log("Playing on Slot 4");
                        GM.Gold_Lastest = data.gold;
                        Slot4.scope.notify_Gold_State_Change();
                        break;
                    default:
                        break;
                }
            }, time_update_money);
        }
    },

    OnMiniPokerGetUserHistoryDone(res) {
        cc.log("OnMiniPokerGetUserHistoryDone : ", res);
        var data = JSON.parse(res);
        Mini_Poker.scope.Popup_History.active = true;
        Mini_Poker.scope.Popup_History.position = cc.v2(-Mini_Poker.scope.Game_Drag.position.x, -Mini_Poker.scope.Game_Drag.position.y);
        Mini_Poker.scope.History_Content.removeAllChildren(true);
        var Data_History = data.history;
        for (var i = 0; i < Data_History.length; i++) {
            var item = cc.instantiate(Mini_Poker.scope.Item_History_Prefab);
            var data = Data_History[i];
            item.getComponent('Mini_Poker_Item_History').init_Item_History({
                bg_index: i % 2,
                sess: data.id,
                time: data.time.split(" ")[1] + "\n" + data.time.split(" ")[0],
                bet: data.bet,
                gold: data.add,
                class: decodeURIComponent(escape(data.desc)),
            });
            Mini_Poker.scope.History_Content.addChild(item);
        }
    },

    OnMiniPokerGetGloryDone(res) {
        cc.log("OnMiniPokerGetGloryDone : ", res);
        var data = JSON.parse(res);
        Mini_Poker.scope.Popup_Rank.active = true;
        Mini_Poker.scope.Popup_Rank.position = cc.v2(-Mini_Poker.scope.Game_Drag.position.x, -Mini_Poker.scope.Game_Drag.position.y);
        Mini_Poker.scope.Rank_Content.removeAllChildren(true);
        var Data_Rank = data.history;
        for (var i = 0; i < Data_Rank.length; i++) {
            var item = cc.instantiate(Mini_Poker.scope.Item_Rank_Prefab);
            var data = Data_Rank[i];
            var name = decodeURIComponent(escape(data.displayName));
            if (name.length > 15) {
                name = name.substring(0, 11) + "...";
            }
            item.getComponent('Mini_Poker_Item_Rank').init_Item_Rank({
                bg_index: i % 2,
                time: data.time.split(" ")[1] + "\n" + data.time.split(" ")[0],
                account: name,
                bet: data.bet,
                gold: data.win,
                class: decodeURIComponent(escape(data.desc)),
            });
            Mini_Poker.scope.Rank_Content.addChild(item);
        }
    },

    OnJackpotChange(res) {
        cc.log("OnJackpotChange :  ", res);
        var data = JSON.parse(res);
        var old_jackpot = Mini_Poker.scope.Last_Jackpot;
        switch (Mini_Poker.scope.Spin_Bet) {
            case 100:
                if (data.roomId === 40) {
                    Mini_Poker.scope.effect_Update_Jackpot(old_jackpot, data.jackpot, Mini_Poker.scope.label_Jackpot.node, Mini_Poker.scope.label_Jackpot);
                    Mini_Poker.scope.Last_Jackpot = data.jackpot;
                }
                break;
            case 1000:
                if (data.roomId === 41) {
                    Mini_Poker.scope.effect_Update_Jackpot(old_jackpot, data.jackpot, Mini_Poker.scope.label_Jackpot.node, Mini_Poker.scope.label_Jackpot);
                    Mini_Poker.scope.Last_Jackpot = data.jackpot;
                }
                break;
            case 10000:
                if (data.roomId === 42) {
                    Mini_Poker.scope.effect_Update_Jackpot(old_jackpot, data.jackpot, Mini_Poker.scope.label_Jackpot.node, Mini_Poker.scope.label_Jackpot);
                    Mini_Poker.scope.Last_Jackpot = data.jackpot;
                }
                break;
        }
    },

    onLoad() {},

    start() {

    },

    update(dt) {

    },

    fake_5_card() {
        var card_1_id = Mini_Poker.scope.randomBetween(1, 52);
        Mini_Poker.scope.GP_Column_1.children[0].getComponent(cc.Sprite).spriteFrame = Mini_Poker.scope.sprite_Card[card_1_id - 1];
        var card_2_id = Mini_Poker.scope.randomBetween(1, 52);
        Mini_Poker.scope.GP_Column_2.children[0].getComponent(cc.Sprite).spriteFrame = Mini_Poker.scope.sprite_Card[card_2_id - 1];
        var card_3_id = Mini_Poker.scope.randomBetween(1, 52);
        Mini_Poker.scope.GP_Column_3.children[0].getComponent(cc.Sprite).spriteFrame = Mini_Poker.scope.sprite_Card[card_3_id - 1];
        var card_4_id = Mini_Poker.scope.randomBetween(1, 52);
        Mini_Poker.scope.GP_Column_4.children[0].getComponent(cc.Sprite).spriteFrame = Mini_Poker.scope.sprite_Card[card_4_id - 1];
        var card_5_id = Mini_Poker.scope.randomBetween(1, 52);
        Mini_Poker.scope.GP_Column_5.children[0].getComponent(cc.Sprite).spriteFrame = Mini_Poker.scope.sprite_Card[card_5_id - 1];
    },

    onEnable() {
        cc.log("onEnable");
        Mini_Poker.scope = this;

        Mini_Poker.scope.SPIN_AUTO_ONLINE = false;
        Mini_Poker.scope.SPIN_FLASH_ONLINE = false;
        Mini_Poker.scope.Spining = false;
        Mini_Poker.scope.Spin_Bet = 100;
        Mini_Poker.scope.Bet_selector[0].isChecked = true;

        WebsocketClient.OnMiniPokerSubDone = Mini_Poker.scope.OnMiniPokerSubDone;
        WebsocketClient.OnMiniPokerStartDone = Mini_Poker.scope.OnMiniPokerStartDone;
        WebsocketClient.OnMiniPokerGetUserHistoryDone = Mini_Poker.scope.OnMiniPokerGetUserHistoryDone;
        WebsocketClient.OnMiniPokerGetGloryDone = Mini_Poker.scope.OnMiniPokerGetGloryDone;
        WebsocketClient.OnJackpotChange = Mini_Poker.scope.OnJackpotChange;

        Mini_Poker.scope.fake_5_card();
        Mini_Poker.scope.subMiniPoker(100);
    },

    onDisable() {
        cc.log("onDisable");
    },

    choose_Bet(toggle) {
        if (!Mini_Poker.scope.Spining) {
            if (Mini_Poker.scope.SPIN_AUTO_ONLINE || Mini_Poker.scope.SPIN_FLASH_ONLINE) {
                cc.log("Can not choose Bet because Auto or Flash mode is Active");
                Mini_Poker.scope.Show_Popup_Error_Action(" Không thể chuyển mức cược \n khi đang quay ! ");
                Mini_Poker.scope.f5_bet();
            } else {
                cc.log("Can choose Bet ");
                var index = Mini_Poker.scope.Bet_selector.indexOf(toggle);
                switch (index) {
                    case 0:
                        Mini_Poker.scope.subMiniPoker(100);
                        break;
                    case 1:
                        Mini_Poker.scope.subMiniPoker(1000);
                        break;
                    case 2:
                        Mini_Poker.scope.subMiniPoker(10000);
                        break;
                }
                cc.log("Spin_Bet : ", Mini_Poker.scope.Spin_Bet);
            }
        } else {
            cc.log("Can not choose Bet");
            Mini_Poker.scope.Show_Popup_Error_Action(" Không thể chuyển mức cược \n khi đang quay ! ");
            Mini_Poker.scope.f5_bet();
        }
    },

    Game_Spin_Manual() {
        if (!Mini_Poker.scope.Spining && !Mini_Poker.scope.SPIN_AUTO_ONLINE && !Mini_Poker.scope.SPIN_FLASH_ONLINE) {
            // init Data and Spin
            cc.log("LobbyCtl.userInfo : ", WarpConst.GameBase.userInfo.desc.gold);
            cc.log("Spin_Bete_Bet : ", Mini_Poker.scope.Spin_Bet);
            if (GM.Stay === 0) {
                if (WarpConst.GameBase.userInfo.desc.gold >= Mini_Poker.scope.Spin_Bet) {
                    Mini_Poker.scope.Spin();
                    Mini_Poker.scope.Hand_Spin.getComponent(cc.Animation).play();
                } else {
                    Mini_Poker.scope.Show_Popup_Error_Action(" Không đủ tiền ! ");
                }
            } else {
                if (GM.Gold_Lastest >= Mini_Poker.scope.Spin_Bet) {
                    Mini_Poker.scope.Spin();
                    Mini_Poker.scope.Hand_Spin.getComponent(cc.Animation).play();
                } else {
                    Mini_Poker.scope.Show_Popup_Error_Action(" Không đủ tiền ! ");
                }
            }

        }
    },

    Spin() {
        Mini_Poker.scope.Spin_Count += 1;
        Mini_Poker.scope.Spining = true;

        switch (GM.Stay) {
            case 0:
                cc.log("Playing on Lobby");
                Mini_Poker.scope.changeBalance(-Mini_Poker.scope.Spin_Bet);
                break;
            case 1:
                cc.log("Playing on Slot 1");
                GM.Gold_Lastest -= Mini_Poker.scope.Spin_Bet;
                Slot1.scope.notify_Gold_State_Change();
                break;
            case 2:
                cc.log("Playing on Slot 2");
                GM.Gold_Lastest -= Mini_Poker.scope.Spin_Bet;
                Slot2.scope.notify_Gold_State_Change();
                break;
            case 3:
                cc.log("Playing on Slot 3");
                GM.Gold_Lastest -= Mini_Poker.scope.Spin_Bet;
                Slot3.scope.notify_Gold_State_Change();
                break;
            case 4:
                cc.log("Playing on Slot 4");
                GM.Gold_Lastest -= Mini_Poker.scope.Spin_Bet;
                Slot4.scope.notify_Gold_State_Change();
                break;
            default:
                break;
        }

        Mini_Poker.scope.spinMiniPoker(Mini_Poker.scope.Spin_Bet);
    },

    Manager_Spin_Anim() {
        var time_init_icon = 1000;
        var time_Column_move = 1.5;
        var time_C1_delay = 0.02;
        var time_C2_delay = 0.04;
        var time_C3_delay = 0.06;
        var time_C4_delay = 0.08;
        var time_C5_delay = 0.1;

        if (Mini_Poker.scope.SPIN_FLASH_ONLINE) {
            cc.log("SPIN_FLASH_ONLINE");
            time_init_icon = 500;
            time_Column_move = 0.5;
            time_C1_delay = 0.05;
            time_C2_delay = 0.1;
            time_C3_delay = 0.15;
            time_C4_delay = 0.2;
            time_C5_delay = 0.25;
        } else {
            cc.log("SPIN Other Mode");
        }

        // init Icon result
        setTimeout(function () {
            var card_1_id = Mini_Poker.scope.Result_Spin_Server.ids[0];
            Mini_Poker.scope.GP_Column_1.children[0].getComponent(cc.Sprite).spriteFrame = Mini_Poker.scope.sprite_Card[card_1_id - 1];

            var card_2_id = Mini_Poker.scope.Result_Spin_Server.ids[1];
            Mini_Poker.scope.GP_Column_2.children[0].getComponent(cc.Sprite).spriteFrame = Mini_Poker.scope.sprite_Card[card_2_id - 1];

            var card_3_id = Mini_Poker.scope.Result_Spin_Server.ids[2];
            Mini_Poker.scope.GP_Column_3.children[0].getComponent(cc.Sprite).spriteFrame = Mini_Poker.scope.sprite_Card[card_3_id - 1];

            var card_4_id = Mini_Poker.scope.Result_Spin_Server.ids[3];
            Mini_Poker.scope.GP_Column_4.children[0].getComponent(cc.Sprite).spriteFrame = Mini_Poker.scope.sprite_Card[card_4_id - 1];

            var card_5_id = Mini_Poker.scope.Result_Spin_Server.ids[4];
            Mini_Poker.scope.GP_Column_5.children[0].getComponent(cc.Sprite).spriteFrame = Mini_Poker.scope.sprite_Card[card_5_id - 1];
        }, time_init_icon);

        // Scroll 5 Column
        Mini_Poker.scope.GP_Column_1.runAction(
            cc.sequence(
                cc.delayTime(time_C1_delay),
                cc.moveTo(0, -293, 3000),
                cc.moveTo(time_Column_move, -293, 105).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 1");
                }))
        );
        Mini_Poker.scope.GP_Column_2.runAction(
            cc.sequence(
                cc.delayTime(time_C2_delay),
                cc.moveTo(0, -147, 3000),
                cc.moveTo(time_Column_move, -147, 105).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 2");
                }))
        );
        Mini_Poker.scope.GP_Column_3.runAction(
            cc.sequence(
                cc.delayTime(time_C3_delay),
                cc.moveTo(0, 0, 3000),
                cc.moveTo(time_Column_move, 0, 105).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 3");
                }))
        );
        Mini_Poker.scope.GP_Column_4.runAction(
            cc.sequence(
                cc.delayTime(time_C4_delay),
                cc.moveTo(0, 147, 3000),
                cc.moveTo(time_Column_move, 147, 105).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 4");
                }))
        );
        Mini_Poker.scope.GP_Column_5.runAction(
            cc.sequence(
                cc.delayTime(time_C5_delay),
                cc.moveTo(0, 293, 3000),
                cc.moveTo(time_Column_move, 293, 105).easing(cc.easeCircleActionOut()),
                cc.callFunc(function () {
                    cc.log("End Scroll Column 5 ");
                    Mini_Poker.scope.effect_Update_Jackpot(Mini_Poker.scope.Last_Jackpot, Mini_Poker.scope.Result_Spin_Server.jackpot, Mini_Poker.scope.label_Jackpot.node, Mini_Poker.scope.label_Jackpot);
                    Mini_Poker.scope.Last_Jackpot = Mini_Poker.scope.Result_Spin_Server.jackpot;
                }))
        );
    },

    Machine_Spin(time_spin, time_hide_result) {
        setTimeout(function () {
            if (Mini_Poker.scope.Result_Spin_Server.win_chips > 0) {
                Mini_Poker.scope.Spin_Result.active = true;
                Mini_Poker.scope.label_Gold_Add.string = decodeURIComponent(escape(Mini_Poker.scope.Result_Spin_Server.special)) + " +" + GM.instance.convert_money(Mini_Poker.scope.Result_Spin_Server.win_chips);
            } else {
                time_hide_result = 0;
            }

            setTimeout(function () {
                Mini_Poker.scope.Spin_Result.active = false;
                Mini_Poker.scope.Spining = false;
                cc.log("Can Spins");
                if (Mini_Poker.scope.SPIN_FLASH_ONLINE) {
                    if (GM.Stay === 0) {
                        if (WarpConst.GameBase.userInfo.desc.gold >= Mini_Poker.scope.Spin_Bet) {
                            Mini_Poker.scope.Flash_Spin();
                        } else {
                            Mini_Poker.scope.Btn_Flash_Spin.isChecked = false;
                            Mini_Poker.scope.Spining = false;
                            Mini_Poker.scope.SPIN_FLASH_ONLINE = false;
                            Mini_Poker.scope.Show_Popup_Error_Action(" Không đủ tiền ! ");
                        }
                    } else {
                        if (GM.Gold_Lastest >= Mini_Poker.scope.Spin_Bet) {
                            Mini_Poker.scope.Flash_Spin();
                        } else {
                            Mini_Poker.scope.Btn_Flash_Spin.isChecked = false;
                            Mini_Poker.scope.Spining = false;
                            Mini_Poker.scope.SPIN_FLASH_ONLINE = false;
                            Mini_Poker.scope.Show_Popup_Error_Action(" Không đủ tiền ! ");
                        }
                    }
                } else if (Mini_Poker.scope.SPIN_AUTO_ONLINE) {
                    if (GM.Stay === 0) {
                        if (WarpConst.GameBase.userInfo.desc.gold >= Mini_Poker.scope.Spin_Bet) {
                            Mini_Poker.scope.Auto_Spin();
                        } else {
                            Mini_Poker.scope.Btn_Auto_Spin.isChecked = false;
                            Mini_Poker.scope.Spining = false;
                            Mini_Poker.scope.SPIN_AUTO_ONLINE = false;
                            Mini_Poker.scope.Show_Popup_Error_Action(" Không đủ tiền ! ");
                        }
                    } else {
                        if (GM.Gold_Lastest >= Mini_Poker.scope.Spin_Bet) {
                            Mini_Poker.scope.Auto_Spin();
                        } else {
                            Mini_Poker.scope.Btn_Auto_Spin.isChecked = false;
                            Mini_Poker.scope.Spining = false;
                            Mini_Poker.scope.SPIN_AUTO_ONLINE = false;
                            Mini_Poker.scope.Show_Popup_Error_Action(" Không đủ tiền ! ");
                        }
                    }
                }
            }, time_hide_result);
        }, time_spin);
    },

    Game_Flash_Spin(toggle) {
        if (Mini_Poker.scope.SPIN_AUTO_ONLINE) {
            Mini_Poker.scope.Btn_Flash_Spin.isChecked = false;
            Mini_Poker.scope.Show_Popup_Error_Action(" Không thể chọn chế độ Siêu tốc \n khi đang ở chế độ Tự quay ");
            return;
        }
        cc.log("Van qua :))");

        if (toggle.isChecked) {
            Mini_Poker.scope.SPIN_FLASH_ONLINE = true;
        } else {
            Mini_Poker.scope.SPIN_FLASH_ONLINE = false;
        }

        if (!Mini_Poker.scope.Spining) {
            cc.log("Pre Spin");
            Mini_Poker.scope.Spining = true;
            if (Mini_Poker.scope.SPIN_FLASH_ONLINE) {
                cc.log("LobbyCtl.userInfo : ", WarpConst.GameBase.userInfo.desc.gold);
                cc.log("Spin_Bete_Bet : ", Mini_Poker.scope.Spin_Bet);
                if (GM.Stay === 0) {
                    if (WarpConst.GameBase.userInfo.desc.gold >= Mini_Poker.scope.Spin_Bet) {
                        Mini_Poker.scope.Flash_Spin();
                    } else {
                        Mini_Poker.scope.Btn_Flash_Spin.isChecked = false;
                        Mini_Poker.scope.Spining = false;
                        Mini_Poker.scope.SPIN_FLASH_ONLINE = false;
                        Mini_Poker.scope.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                } else {
                    if (GM.Gold_Lastest >= Mini_Poker.scope.Spin_Bet) {
                        Mini_Poker.scope.Flash_Spin();
                    } else {
                        Mini_Poker.scope.Btn_Flash_Spin.isChecked = false;
                        Mini_Poker.scope.Spining = false;
                        Mini_Poker.scope.SPIN_FLASH_ONLINE = false;
                        Mini_Poker.scope.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                }

            } else {
                Mini_Poker.scope.Spining = false;
            }
        } else {
            cc.log("Spinning");
        }
    },

    Flash_Spin() {
        // init Data and Spin
        Mini_Poker.scope.Spin();
    },

    Game_Auto_Spin(toggle) {
        if (Mini_Poker.scope.SPIN_FLASH_ONLINE) {
            Mini_Poker.scope.Btn_Auto_Spin.isChecked = false;
            Mini_Poker.scope.Show_Popup_Error_Action(" Không thể chọn chế độ Tự quay \n khi đang ở chế độ quay Siêu tốc ");
            return;
        }
        cc.log("Van qua :))");

        if (toggle.isChecked) {
            Mini_Poker.scope.SPIN_AUTO_ONLINE = true;
        } else {
            Mini_Poker.scope.SPIN_AUTO_ONLINE = false;
        }

        if (!Mini_Poker.scope.Spining) {
            cc.log("Pre Spin");
            Mini_Poker.scope.Spining = true;
            if (Mini_Poker.scope.SPIN_AUTO_ONLINE) {
                cc.log("LobbyCtl.userInfo : ", WarpConst.GameBase.userInfo.desc.gold);
                cc.log("Spin_Bete_Bet : ", Mini_Poker.scope.Spin_Bet);

                if (GM.Stay === 0) {
                    if (WarpConst.GameBase.userInfo.desc.gold >= Mini_Poker.scope.Spin_Bet) {
                        Mini_Poker.scope.Auto_Spin();
                    } else {
                        Mini_Poker.scope.Btn_Auto_Spin.isChecked = false;
                        Mini_Poker.scope.Spining = false;
                        Mini_Poker.scope.SPIN_AUTO_ONLINE = false;
                        Mini_Poker.scope.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                } else {
                    if (GM.Gold_Lastest >= Mini_Poker.scope.Spin_Bet) {
                        Mini_Poker.scope.Auto_Spin();
                    } else {
                        Mini_Poker.scope.Btn_Auto_Spin.isChecked = false;
                        Mini_Poker.scope.Spining = false;
                        Mini_Poker.scope.SPIN_AUTO_ONLINE = false;
                        Mini_Poker.scope.Show_Popup_Error_Action(" Không đủ tiền ! ");
                    }
                }
            } else {
                Mini_Poker.scope.Spining = false;
            }
        } else {
            cc.log("Spinning");
        }
    },

    Auto_Spin() {
        // init Data and Spin
        Mini_Poker.scope.Spin();
    },


    randomBetween(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    },

    Close_Popup_Error_Action() {
        Mini_Poker.scope.Popup_Error_Action.active = false;
    },

    effect_Update_Gold(gold_start, gold_end, node, label) {
        var gold_add = gold_end - gold_start;
        label.string = gold_start;

        var steps = 40;
        var delta_gold_add = -1;

        if (gold_add < 40) {
            steps = 10;
        }

        delta_gold_add = Math.floor(gold_add / steps);

        var add = cc.sequence(
            cc.delayTime(0.03),
            cc.callFunc(function () {
                gold_start += delta_gold_add;
                label.string = GM.instance.convert_money(gold_start);
            })
        );

        if (steps === 40) {
            node.runAction(
                cc.sequence(
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    cc.callFunc(function () {
                        gold_start = gold_end;
                        label.string = GM.instance.convert_money(gold_start);
                    })
                )
            );
        } else {
            node.runAction(
                cc.sequence(
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    cc.callFunc(function () {
                        gold_start = gold_end;
                        label.string = GM.instance.convert_money(gold_start);
                    })
                )
            );
        }

    },

    effect_Update_Jackpot(gold_start, gold_end, node, label) {
        var gold_add = gold_end - gold_start;
        label.string = GM.instance.convert_money(gold_start);

        var steps = 5;
        var delta_gold_add = -1;

        delta_gold_add = Math.floor(gold_add / steps);

        var add = cc.sequence(
            cc.delayTime(0.1),
            cc.callFunc(function () {
                gold_start += delta_gold_add;
                label.string = GM.instance.convert_money(gold_start);
            })
        );

        node.runAction(
            cc.sequence(
                add,
                add,
                add,
                add,
                cc.callFunc(function () {
                    gold_start = gold_end;
                    label.string = GM.instance.convert_money(gold_start);
                })
            )
        );
    },

    Show_Rank() {
        Mini_Poker.scope.getMiniPokerGlory();
    },
    Show_Guide() {
        Mini_Poker.scope.Popup_Guide.active = true;
        Mini_Poker.scope.Popup_Guide.position = cc.v2(-Mini_Poker.scope.Game_Drag.position.x, -Mini_Poker.scope.Game_Drag.position.y);
    },
    Show_History() {
        Mini_Poker.scope.getMiniPokerHis();
    },

    Close_Rank() {
        Mini_Poker.scope.Popup_Rank.active = false;
    },

    Close_Guide() {
        Mini_Poker.scope.Popup_Guide.active = false;
    },

    Close_History() {
        Mini_Poker.scope.Popup_History.active = false;
    },


    Back_2_G_Main() {
        if (!Mini_Poker.scope.Spining && !Mini_Poker.scope.SPIN_AUTO_ONLINE && !Mini_Poker.scope.SPIN_FLASH_ONLINE) {
            cc.find("UIController").getComponent("UIController").hideMiniPoker();
        } else {
            cc.log("Can not close");
            Mini_Poker.scope.Show_Popup_Error_Action(" Không thể thoát trò chơi ! \n khi đang quay ");
        }
    },

    Show_Popup_Error_Action(mess) {
        cc.log("Show_Popup_Error_Action : ", mess);
        Mini_Poker.scope.Popup_Error_Action.active = true;
        Mini_Poker.scope.Popup_Error_Action.position = cc.v2(-Mini_Poker.scope.Game_Drag.position.x, -Mini_Poker.scope.Game_Drag.position.y);
        Mini_Poker.scope.label_popup_error_action_mes.string = mess;
    },

    // Connect Server
    subMiniPoker(game_bet) {
        var str = {
            type: WarpConst.MiniGame.SUBSCRIBE_ROOM,
            bet: game_bet,
            chipType: 1

        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_POKER;
        WebsocketClient.Socket.send(m);
    },
    spinMiniPoker(bet_selected) {
        var str = {
            type: WarpConst.MiniGame.START_MATCH,
            bet: bet_selected, //bet value: 100 | 1000 | 10000
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_POKER;
        WebsocketClient.Socket.send(m);
    },
    getMiniPokerHis() {
        var str = {
            type: WarpConst.MiniGame.GET_USER_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_POKER;
        WebsocketClient.Socket.send(m);
    },
    getMiniPokerGlory() {
        var str = {
            type: WarpConst.MiniGame.GET_JACKPOT_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_POKER;
        WebsocketClient.Socket.send(m);
    },

    //SET MONEY
    //minus
    changeBalance(balanceChange) {
        LobbyCtl.instance.addUserGold(balanceChange);
    },
    setBalance(balance) {
        LobbyCtl.instance.setUserGold(balance);
    }
});