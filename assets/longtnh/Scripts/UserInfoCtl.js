/**
 * By: longtnh
 * To do: Control USER INFO POPUP
 * Date: 2019.03.22
 */
var WebsocketClient = require("WebsocketClient");
var WarpConst = require("WarpConst");
var App = require("App");
var UserInfoCtl = cc.Class({
    extends: cc.Component,

    properties: {
        //0: Info|1: Change pass|2: Change ava
        Panels: {
            default: [],
            type: [cc.Node],
        },

        //TAB - Info
        infoUserNameText: cc.Label,
        infoUserIdText: cc.Label,
        infoUserMoneyText: cc.Label,
        infoUserPhoneText: cc.Label,
        infoAvaImage: cc.Sprite,

        //TAB - Change pass
        cpOldPassIpf: cc.EditBox,
        cpNewPassIpf: cc.EditBox,
        cpRePassIpf: cc.EditBox,

        btn_ChangePassword: cc.Node,
        btn_Validate: cc.Node,
        sprite_Dis: cc.SpriteFrame,
        sprite_On_FAK: cc.SpriteFrame,
        sprite_On_ChangePass: cc.SpriteFrame,

        //--- private values ---
        _newAvar: "",
    },
    statics: {
        instance: null,
        showDialog: null,
        changeAvaOnLobby: null,
    },
    onLoad() {
        UserInfoCtl.instance = this;
    },
    onEnable() {
        WebsocketClient.OnChangePassDone = this.OnChangePassDone;
        WebsocketClient.OnChangeAvaDone = this.OnChangeAvaDone;
        if (this.Panels[0].active) {
            if (WarpConst.GameBase.userInfo) {
                var data = WarpConst.GameBase.userInfo.desc;
                if (data.mobile == null || data.mobile.length == 0) {
                    this.btn_Validate.getComponent(cc.Sprite).spriteFrame = this.sprite_On_FAK;
                } else {
                    this.btn_Validate.getComponent(cc.Sprite).spriteFrame = this.sprite_Dis;
                }
            }
        }
    },
    onDisable() {
        WebsocketClient.OnChangePassDone = null;
        WebsocketClient.OnChangeAvaDone = null;
    },
    show() {
        //cc.log(RechargeCtl.instance);
        UserInfoCtl.instance.node.active = true;
        this.ChangeActivePanel(null, 0);
    },
    close() {
        this.node.active = false;
    },
    ChangeActivePanel(event, index) {
        for (let i = 0; i < this.Panels.length; i++) {
            this.Panels[i].active = false;
        }

        switch (parseInt(index)) {
            case 0: //Info
                this.Panels[0].active = true;
                var data = WarpConst.GameBase.userInfo.desc;
                if (data.faceBookId !== null) {
                    this.btn_ChangePassword.getComponent(cc.Sprite).spriteFrame = this.sprite_Dis;
                } else {
                    this.btn_ChangePassword.getComponent(cc.Sprite).spriteFrame = this.sprite_On_ChangePass;
                }

                if (data.mobile == null || data.mobile.length == 0) {
                    this.btn_Validate.getComponent(cc.Sprite).spriteFrame = this.sprite_On_FAK;
                } else {
                    this.btn_Validate.getComponent(cc.Sprite).spriteFrame = this.sprite_Dis;
                }

                var userName = data.username.length > 20 ? (data.username.substring(0, 17) + "...") : data.username;
                if (data.displayName.length > 0) {
                    userName = App.formatUnicode(data.displayName);
                    userName = data.displayName.length > 20 ? (userName.substring(0, 17) + "...") : userName;
                }
                this.infoUserNameText.string = userName;
                //this.infoUserNameText.string = data.username.length > 20 ? data.username.substring(0, 17) + "..." : data.username;
                this.infoUserIdText.string = "ID: " + data.id;
                this.infoUserMoneyText.string = App.formatMoney(data.gold);
                this.infoUserPhoneText.string = (data.mobile == null || data.mobile.length == 0) ? " . . . " : " +84" + data.mobile + " ";

                this.infoAvaImage.spriteFrame = WarpConst.GameBase.playerAvaSpriteFrame;

                // for (var i = 0; i < WarpConst.GameBase.playerAva.length; i++) {
                //     if (WarpConst.GameBase.playerAva[i]._name.includes(data.avatar)) {
                //         this.infoAvaImage.spriteFrame = WarpConst.GameBase.playerAva[i];
                //         break;
                //     }
                // }

                break;
            case 1: //Change pass
                var data = WarpConst.GameBase.userInfo.desc;
                if (data.faceBookId !== null) {
                    this.Panels[0].active = true;
                } else {
                    this.Panels[1].active = true;
                    UserInfoCtl.instance.cpOldPassIpf.string = UserInfoCtl.instance.cpNewPassIpf.string = UserInfoCtl.instance.cpRePassIpf.string = "";
                }
                break;
            case 2: //Change ava
                this.Panels[2].active = true;
                break;
        }

        this.Panels[parseInt(index)].active = true;
    },
    //#region  Request - Responese with server
    ChangePass() {
        if (this.cpOldPassIpf.string.length == 0 || this.cpNewPassIpf.string.length == 0 || this.cpRePassIpf.string.length == 0) {
            UserInfoCtl.showDialog("Vui lòng điền đầy đủ thông tin.");
            return;
        }
        if (this.cpOldPassIpf.string.length < 6 || this.cpNewPassIpf.string.length < 6 || this.cpRePassIpf.string.length < 6) {
            UserInfoCtl.showDialog("Mật khẩu phải dài hơn 6 ký tự");
            return;
        }
        if (this.cpNewPassIpf.string != this.cpRePassIpf.string) {
            UserInfoCtl.showDialog("Mật khẩu không trùng khớp.");
            return;
        }

        var str = {
            oldPassword: this.cpOldPassIpf.string,
            newPassword: this.cpNewPassIpf.string
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.CHANGE_PASS;
        WebsocketClient.Socket.send(m);
    },
    OnChangePassDone(status, data) {
        if (status == WarpConst.WarpResponseResultCode.SUCCESS) {
            UserInfoCtl.showDialog("Đổi mật khẩu thành công.");
        } else {
            UserInfoCtl.showDialog("Mật khẩu sai. Vui lòng thử lại.");
        }
        UserInfoCtl.instance.cpOldPassIpf.string = UserInfoCtl.instance.cpNewPassIpf.string = UserInfoCtl.instance.cpRePassIpf.string = "";
    },
    ChangeAva(event, data) {
        UserInfoCtl.instance._newAvar = data;
        var str = {
            avatar: data
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.UPDATE_AVATAR;
        WebsocketClient.Socket.send(m);
    },
    OnChangeAvaDone(status, data) {
        if (status == WarpConst.WarpResponseResultCode.SUCCESS) {
            UserInfoCtl.showDialog("Đổi avatar thành công.");
            for (var i = 0; i < WarpConst.GameBase.playerAva.length; i++) {
                if (WarpConst.GameBase.playerAva[i]._name.includes(UserInfoCtl.instance._newAvar)) {
                    UserInfoCtl.instance.infoAvaImage.spriteFrame = WarpConst.GameBase.playerAva[i];
                    if (UserInfoCtl.changeAvaOnLobby)
                        UserInfoCtl.changeAvaOnLobby(UserInfoCtl.instance.infoAvaImage.spriteFrame);
                    break;
                }
            }


        } else {
            UserInfoCtl.showDialog("Đổi avatar không thành công. Vui lòng thử lại.");
        }
    },
    AuthenAccount() {
        UserInfoCtl.showDialog("Tính năng đang phát triển.");
    }
    //#endregion
});