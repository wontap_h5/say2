var WarpConst = require("WarpConst");
var App = require("App");

var ExchangeCtl = cc.Class({
    extends: cc.Component,

    properties: {
        loadingIcon: cc.Node,
        tabEle: {
            default: [],
            type: [cc.Node],
        },
        tabText: {
            default: [],
            type: [cc.Node],
        },
        tabTextColor: {
            default: [],
            type: [cc.Color],
        },

        //TAB - Server his
        ServerHisContentPanel: cc.Node,
        ServerHisEle: {
            default: [],
            type: [cc.Node],
        },

        //TAB - Exchange item - ex
        ExchangeContentPanel: cc.Node,
        ExCardNetBtn: {
            default: [],
            type: [cc.Node],
        },
        ExEgbreakAnim: {
            default: [],
            type: [cc.Animation],
        },
        ExEgbreakToCard: {
            default: [],
            type: [cc.Node],
        },
        ExEgbreakToMoney: {
            default: [],
            type: [cc.Node],
        },
        ExValueEle: cc.Node,

        //TAB - My his
        MyhisContentPanel: cc.Node,
        MyhisEle: {
            default: [],
            type: [cc.Node],
        },

        //Private variable
        _SpawnedEles: [],
        _currentSelectedTelco: null,
        _exchangeConfig: null,
        _currentExSelectedEgg: null,
        _lastEggBreakResult: null,
        _isOpeningEgg: false,
        _currentSelectedValue: null,
        _exchangeValueActiveList: []
    },
    statics: {
        instance: null,
        showDialog: null,
    },
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        ExchangeCtl.instance = this;
        this.ServerHisEle[0].active = false;
        this.ServerHisEle[1].active = false;
        this.MyhisEle[0].active = false;
        this.MyhisEle[1].active = false;
        this.ExValueEle.active = false;
    },

    onEnable() {
        ExchangeCtl.instance._exchangeConfig = WarpConst.GameBase.config_exchange;
        cc.log("__Giang__ _exchangeConfig :  ", ExchangeCtl.instance._exchangeConfig);
    },

    show() {
        //cc.log(ExchangeCtl.instance);
        ExchangeCtl.instance.node.active = true;
        ExchangeCtl.instance.onClickTabBtn(null, "0");
    },
    close() {
        if (this._isOpeningEgg) {
            ExchangeCtl.showDialog("Vui lòng đợi cho tới khi hoàn thành Mở Trứng.");
            return;
        }
        this.node.active = false;
    },

    onClickTabBtn(event, index) {
        for (let i = 0; i < this.tabEle.length; i++) {
            this.tabEle[i].active = false;
            this.tabEle[i + 1].active = true;
            this.tabText[i / 2].color = this.tabTextColor[0];
            i++;
        }
        //cc.log(index);
        this.tabEle[parseInt(index * 2)].active = true;
        this.tabText[parseInt(index)].color = this.tabTextColor[1];

        this.ServerHisContentPanel.active = false;
        this.ExchangeContentPanel.active = false;
        this.MyhisContentPanel.active = false;
        switch (index) {
            case "0": //Server His
                this.ServerHisContentPanel.active = true;
                for (var i = this._SpawnedEles.length - 1; i > -1; i--) {
                    this._SpawnedEles[i].destroy();
                    this._SpawnedEles.pop();
                }

                this.loadServerHis(true);
                break;
            case "1": //Exchange
                this.ExchangeContentPanel.active = true;
                //this.onClickCardNetBtn(null, 0);
                for (var j = 0; j < ExchangeCtl.instance.ExEgbreakToCard.length; j++) {
                    ExchangeCtl.instance.ExEgbreakToCard[j].active = false;
                }
                for (var j = 0; j < ExchangeCtl.instance.ExEgbreakToMoney.length; j++) {
                    ExchangeCtl.instance.ExEgbreakToMoney[j].active = false;
                }

                if (this._exchangeConfig == null) {
                    for (var j = 0; j < ExchangeCtl.instance.ExCardNetBtn.length; j++) {
                        ExchangeCtl.instance.ExCardNetBtn[j].parent.active = false;
                    }

                    //Load data
                    //{"provider":[{"name":"Viettel","code":"VTT"},{"name":"Mobifone","code":"VMS"},{"name":"Vinaphone","code":"VNP"}],"card":[{"gold":"25000","money":"20000"},{"gold":"62500","money":"50000"},{"gold":"125000","money":"100000"},{"gold":"250000","money":"200000"},{"gold":"625000","money":"500000"}]}
                    this.loadingIcon.active = true;
                    var jsonData = {
                        api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1"
                    }

                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', WarpConst.Links.outSideHttpRequest + 'dap-trung/get-config', true);
                    xhr.onload = function () {
                        // cc.log(App.dycryptAes256Cbc(this.responseText));
                        ExchangeCtl.instance.loadingIcon.active = false;
                        var data = App.dycryptAes256Cbc(this.responseText);
                        // cc.log(data);
                        // return;

                        //Set telco btns active
                        var json = JSON.parse(data);
                        ExchangeCtl.instance._exchangeConfig = json;

                        for (var i = 0; i < json.provider.length; i++) {
                            for (var j = 0; j < ExchangeCtl.instance.ExCardNetBtn.length; j++) {
                                if (ExchangeCtl.instance.ExCardNetBtn[j].parent.name == json.provider[i].code) {
                                    ExchangeCtl.instance.ExCardNetBtn[j].parent.active = true;
                                }
                            }
                        }
                        for (var j = 0; j < ExchangeCtl.instance.ExCardNetBtn.length; j++) {
                            //cc.log(ExchangeCtl.instance.RbcCardNetBtn[j].parent);
                            if (ExchangeCtl.instance.ExCardNetBtn[j].parent.active) {
                                ExchangeCtl.instance.onClickCardNetBtn(null, j.toString());
                                break;
                            }

                        }

                        //Fake Spawn values
                        for (var i = 0; i < json.card.length; i++) {
                            var obj = cc.instantiate(ExchangeCtl.instance.ExValueEle);
                            obj.parent = ExchangeCtl.instance.ExValueEle.parent;
                            obj.getComponentInChildren(cc.Label).string = App.formatMoney(json.card[i].gold);

                            var button = obj.getComponent(cc.Button);
                            button.clickEvents[0].customEventData = i;
                            cc.log(obj);
                            ExchangeCtl.instance._exchangeValueActiveList.push(obj.children[0]);
                            obj.active = true;
                        }
                        ExchangeCtl.instance.onClickValueCallback(null, '0');
                    };
                    xhr.send(JSON.stringify({
                        data: App.encryptAes256Cbc(jsonData)
                    }));
                } else {
                    for (var j = 0; j < ExchangeCtl.instance.ExCardNetBtn.length; j++) {
                        ExchangeCtl.instance.ExCardNetBtn[j].parent.active = false;
                    }

                    var json = ExchangeCtl.instance._exchangeConfig;

                    for (var i = 0; i < json.provider.length; i++) {
                        for (var j = 0; j < ExchangeCtl.instance.ExCardNetBtn.length; j++) {
                            if (ExchangeCtl.instance.ExCardNetBtn[j].parent.name == json.provider[i].code) {
                                ExchangeCtl.instance.ExCardNetBtn[j].parent.active = true;
                            }
                        }
                    }
                    for (var j = 0; j < ExchangeCtl.instance.ExCardNetBtn.length; j++) {
                        //cc.log(ExchangeCtl.instance.RbcCardNetBtn[j].parent);
                        if (ExchangeCtl.instance.ExCardNetBtn[j].parent.active) {
                            ExchangeCtl.instance.onClickCardNetBtn(null, j.toString());
                            break;
                        }

                    }

                    if (ExchangeCtl.instance._exchangeValueActiveList.length == 0) {
                        //Fake Spawn values
                        for (var i = 0; i < json.card.length; i++) {
                            var obj = cc.instantiate(ExchangeCtl.instance.ExValueEle);
                            obj.parent = ExchangeCtl.instance.ExValueEle.parent;
                            obj.getComponentInChildren(cc.Label).string = App.formatMoney(json.card[i].gold);

                            var button = obj.getComponent(cc.Button);
                            button.clickEvents[0].customEventData = i;
                            cc.log(obj);
                            ExchangeCtl.instance._exchangeValueActiveList.push(obj.children[0]);
                            obj.active = true;
                        }
                        ExchangeCtl.instance.onClickValueCallback(null, '0');
                    }


                    // for (var j = 0; j < ExchangeCtl.instance.ExCardNetBtn.length; j++) {
                    //     //cc.log(ExchangeCtl.instance.RbcCardNetBtn[j].parent);
                    //     if (ExchangeCtl.instance.ExCardNetBtn[j].parent.active) {
                    //         this.onClickCardNetBtn(null, j.toString());
                    //         break;
                    //     }
                    // }
                }
                break;
            case "2": //My his
                this.MyhisContentPanel.active = true;
                for (var i = this._SpawnedEles.length - 1; i > -1; i--) {
                    this._SpawnedEles[i].destroy();
                    this._SpawnedEles.pop();
                }

                this.loadServerHis(false);
                break;
        }
    },
    //#region Server His
    loadServerHis(isServer) {
        this.loadingIcon.active = true;
        var jsonData = {
            api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1",
            page: 1,
            limit: 30
        }
        if (!isServer) {
            jsonData = {
                api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1",
                user_id: WarpConst.GameBase.userInfo.desc.id, //3,
                page: 1,
                limit: 20
            }
        }

        var xhr = new XMLHttpRequest();
        xhr.open('POST', WarpConst.Links.outSideHttpRequest + "/dap-trung/history", true);
        xhr.onload = function () {
            //cc.log(App.dycryptAes256Cbc(this.responseText));
            ExchangeCtl.instance.loadingIcon.active = false;
            ExchangeCtl.instance.setServerHisData(App.dycryptAes256Cbc(this.responseText), isServer);
        };
        xhr.send(JSON.stringify({
            data: App.encryptAes256Cbc(jsonData)
        }));
    },
    setServerHisData(jsonString, isServer) {
        //{"status":1,"message":"Success","data":[{"user_name":"Em Gai Mua","datetime":"2019-03-16 08:26:03","money":"20,000","serial":"10003705334998","pin":"918105182525516","net":"Viettel"},{"user_name":"Đi Qua Cơn Mê","datetime":"2019-03-16 06:34:04","money":"20,000","serial":"10003705334997","pin":"315905281025226","net":"Viettel"}]}
        var data = JSON.parse(jsonString).data;
        var len = data.length;
        cc.log("length = " + len);
        // if(len > 20)
        // len = 20;

        // cc.log(jsonString);
        // return;
        for (var i = 0; i < len; i++) {
            if (isServer) {
                var obj = cc.instantiate(this.ServerHisEle[i % 2]);
                obj.parent = this.ServerHisEle[0].parent;
                var textArr = obj.getComponentsInChildren(cc.Label);

                var dateTime = data[i].datetime.split(" ");
                textArr[0].string = dateTime[0] + "\n" + dateTime[1];
                textArr[1].string = decodeURIComponent(unescape(data[i].user_name));
                textArr[2].string = data[i].net;
                textArr[3].string = data[i].money;
            } else {
                var obj = cc.instantiate(this.MyhisEle[i % 2]);
                obj.parent = this.MyhisEle[0].parent;
                var textArr = obj.getComponentsInChildren(cc.Label);

                var dateTime = data[i].datetime.split(" ");
                textArr[0].string = dateTime[0] + "\n" + dateTime[1];
                //textArr[0].string = data[i].datetime;
                textArr[1].string = data[i].serial;
                textArr[2].string = data[i].pin;
                textArr[3].string = data[i].net;
                textArr[4].string = decodeURIComponent(unescape(data[i].money));
            }
            obj.active = true;
            this._SpawnedEles.push(obj);
        }
        //cc.log(this.RhlEles[0].getComponentsInChildren(cc.Label).length);
    },
    //#endregion
    //#region Exchange item
    onClickCardNetBtn(event, index) {
        for (let i = 0; i < this.ExCardNetBtn.length; i++) {
            this.ExCardNetBtn[i].active = false;
        }
        this.ExCardNetBtn[parseInt(index)].active = true;
        this._currentSelectedTelco = this.ExCardNetBtn[parseInt(index)].parent.name;
        cc.log(this._currentSelectedTelco);
    },
    onClickEgg(event, index) {
        // cc.log(this._currentSelectedValue);
        // return;
        if (this._isOpeningEgg) {
            ExchangeCtl.showDialog("Đang mở trứng. Vui lòng chờ tới lượt tiếp theo.");
            return;
        }
        this._isOpeningEgg = true;

        this._currentExSelectedEgg = parseInt(index);
        this.loadingIcon.active = true;
        var jsonData = {
            api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1",
            user_id: WarpConst.GameBase.userInfo.desc.id,
            gold: ExchangeCtl.instance._currentSelectedValue,
            provider: ExchangeCtl.instance._currentSelectedTelco
        }

        cc.log("__ GG dap trung : ", JSON.stringify(jsonData));

        var xhr = new XMLHttpRequest();
        xhr.open('POST', WarpConst.Links.outSideHttpRequest + 'dap-trung/', true);
        xhr.onload = function () {
            // cc.log(App.dycryptAes256Cbc(this.responseText));
            ExchangeCtl.instance.loadingIcon.active = false;
            var data = App.dycryptAes256Cbc(this.responseText);
            //cc.log(data);
            //{"status":-2,"message":"Số dư sau khi mở còn 30000 Gold"}
            //{"status":1,"message":"Chúc mừng bạn đập được Thẻ Viettel 20.000. Hệ thống đang xử lý, xin vui lòng đợi.","change":{"total":"4975000","change":"25000"},"type":"Gold"}
            //data = '{"status":1,"message":"Chúc mừng bạn đập được Thẻ Viettel 20.000. Hệ thống đang xử lý, xin vui lòng đợi.","change":{"total":"4975000","change":"25000"},"type":"Gold"}';
            // return;
            try {
                var json = JSON.parse(data);

                if (json.status < 1) {
                    //Error
                    ExchangeCtl.showDialog(decodeURIComponent(unescape(json.message)));
                    ExchangeCtl.instance._isOpeningEgg = false;
                } else {
                    ExchangeCtl.instance._lastEggBreakResult = json;
                    ExchangeCtl.instance.ExEgbreakAnim[ExchangeCtl.instance._currentExSelectedEgg].play('LobbyEggBreak');
                }
                /*
                else if(json.status == 1){
                    ExchangeCtl.instance.ExEgbreakAnim[0].play('LobbyEggBreak');
                }else{
                    //Open egg false - return money
                    ExchangeCtl.instance.ExEgbreakAnim[0].play('LobbyEggBreak');
                }
                */
            } catch (Err) {
                cc.log(Err);
                ExchangeCtl.showDialog("Đã xảy ra lỗi. Vui lòng thử lại sau.");
            }

        };
        xhr.send(JSON.stringify({
            data: App.encryptAes256Cbc(jsonData)
        }));
    },
    onEggbreakEnd(string) {
        cc.log("onEggbreakEnd complete " + string);
        //ExchangeCtl.instance.ExEgbreakAnim[0].play('LobbyEggBreakScale');
        if (string == "show_resutl") {
            if (ExchangeCtl.instance._lastEggBreakResult.status == 1) { //return card
                ExchangeCtl.instance.ExEgbreakToCard[ExchangeCtl.instance._currentExSelectedEgg].active = true;
            } else if (ExchangeCtl.instance._lastEggBreakResult.status > 1) //return money
            {
                ExchangeCtl.instance.ExEgbreakToMoney[ExchangeCtl.instance._currentExSelectedEgg].getComponent(cc.Label).string = App.formatMoney(ExchangeCtl.instance._lastEggBreakResult.change);
                ExchangeCtl.instance.ExEgbreakToMoney[ExchangeCtl.instance._currentExSelectedEgg].active = true;
            }

        } else if (string == "show_message") {
            try {
                ExchangeCtl.showDialog(decodeURIComponent(unescape(ExchangeCtl.instance._lastEggBreakResult.message)));
            } catch (Err) {
                cc.log(Err);
            }
        } else if (string == "end") {
            for (var j = 0; j < ExchangeCtl.instance.ExEgbreakToCard.length; j++) {
                ExchangeCtl.instance.ExEgbreakToCard[j].active = false;
            }
            for (var j = 0; j < ExchangeCtl.instance.ExEgbreakToMoney.length; j++) {
                ExchangeCtl.instance.ExEgbreakToMoney[j].active = false;
            }
            ExchangeCtl.instance.ExEgbreakAnim[ExchangeCtl.instance._currentExSelectedEgg].play('LobbyEggBreakScale');
            //ExchangeCtl.instance.ExEgbreakToCard[0].active = false;
            ExchangeCtl.instance._isOpeningEgg = false;
        }
    },
    onClickValueCallback(event, customEventData) {
        //cc.log("Value = " + customEventData);
        this._currentSelectedValue = this._exchangeConfig.card[parseInt(customEventData)].gold;
        for (var i = 0; i < this._exchangeValueActiveList.length; i++) {
            this._exchangeValueActiveList[i].active = false;
        }
        this._exchangeValueActiveList[customEventData].active = true;
    },
    //#endregion
});