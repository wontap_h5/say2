/**
 * By: longtnh
 * To do: Control Setting Pannel
 * Date: 2019.03.25
 */
var GM = require("GM");
var WarpConst = require("WarpConst");
var FeedbackCtl = require("FeedbackCtl");
var SettingCtl = cc.Class({
    extends: cc.Component,

    properties: {
        btn_Music: {
            default: null,
            type: cc.Node
        },
        sprite_Music: {
            default: [],
            type: [cc.SpriteFrame]
        }
    },
    statics: {
        instance: null,
    },
    onLoad() {
        SettingCtl.instance = this;
    },

    start() {

    },
    show() {
        this.node.active = true;
        this.change_State();
    },
    change_State(){
        if (GM.instance.music_Lobby_State === 1) {
            this.btn_Music.getComponent(cc.Sprite).spriteFrame = this.sprite_Music[1];
        } else {
            this.btn_Music.getComponent(cc.Sprite).spriteFrame = this.sprite_Music[0];
        }
    },
    close() {
        this.node.active = false;
    },
    OpenFeedback() {
        FeedbackCtl.instance.show();
        this.close();
    },
    OpenFanpage() {
        cc.sys.openURL(WarpConst.GameBase.gameConfig.data.fanpage);
    }
});