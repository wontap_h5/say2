/**
 * By: longtnh
 * To do: Control USER REG
 * Date: 2019.03.24
 */
var App = require("App");
var WarpConst = require("WarpConst");
var WebsocketClient = require("WebsocketClient");
var InboxCtl = cc.Class({
    extends: cc.Component,

    properties: {
        InboxEle: cc.Node,
        //0: sender|1: time|2: title|3: content
        InboxDetailText: {
            default: [],
            type: [cc.Label],
        },
        messageDetailPop: cc.Node,

        //--- private variables ---
        _spawnedEles: [],
        _messageData: null,

        label_Mess_indicator: cc.Label,
        bg_Inbox: cc.Node
    },
    statics: {
        instance: null,
        showDialog: null,
        loginAfterReg: null,
    },

    messCount: null,

    onLoad() {
        InboxCtl.instance = this;
        this.InboxEle.active = false;
        this.messCount = 0;
    },
    show() {
        this.node.active = true;
        this.GetMessage();
    },
    close() {
        this.node.active = false;
    },
    onEnable() {
        WebsocketClient.OnGetMessageDone = this.OnGetMessageDone;
        WebsocketClient.OnDelOneMessageDone = this.OnDelOneMessageDone;
    },
    onDisable() {
        WebsocketClient.OnGetMessageDone = null;
        WebsocketClient.OnDelOneMessageDone = null;
        for (var i = this._spawnedEles.length - 1; i > -1; i--) {
            this._spawnedEles[i].destroy();
            this._spawnedEles.pop();
        }
    },
    CloseMessageDetailPop() {
        this.messageDetailPop.active = false;
    },
    //#region request - response with server
    GetMessage() {
        var str = {
            status: -1,
            page: 0
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.GET_MESSAGES;
        WebsocketClient.Socket.send(m);
    },
    OnGetMessageDone(status, jsonString) {
        //{"messages":[{"id":83917,"content":"Báº¡n ÄÃ£ nháº­n 1900 tá»« tÃ i khoáº£n longtnh12345. LÃ½ do: Chuyï¿½n tiï¿½n cho ngï¿½ï¿½i chï¿½i longtnh123, id = 3","createdDate":"03/22/2019 11:33:36","status":0,"type":0,"value":0,"title":"longtnh12345","senderId":1,"senderName":"ADMIN"},{"id":83915,"content":"Báº¡n ÄÃ£ nháº­n 1900 tá»« tÃ i khoáº£n longtnh12345. LÃ½ do: Chuyï¿½n tiï¿½n cho ngï¿½ï¿½i chï¿½i longtnh123, id = 3","createdDate":"03/22/2019 11:33:33","status":0,"type":0,"value":0,"title":"longtnh12345","senderId":1,"senderName":"ADMIN"},{"id":83913,"content":"Báº¡n ÄÃ£ nháº­n 1900 tá»« tÃ i khoáº£n longtnh12345. LÃ½ do: Chuyï¿½n tiï¿½n cho ngï¿½ï¿½i chï¿½i longtnh123, id = 3","createdDate":"03/22/2019 11:33:30","status":0,"type":0,"value":0,"title":"longtnh12345","senderId":1,"senderName":"ADMIN"},{"id":83897,"content":"Báº¡n ÄÃ£ nháº­n 500. LÃ½ do: tÃ i khoáº£n longtnh12345 ÄÃ£ nháº­p CODE giá»i thiá»u","createdDate":"03/21/2019 10:57:02","status":0,"type":0,"value":0,"title":"longtnh12345","senderId":1,"senderName":"ADMIN"}]}
        if (status == WarpConst.WarpResponseResultCode.SUCCESS) {
            /**
             * for (var i = 0; i < json.card.length; i++){
                            var obj = cc.instantiate(ExchangeCtl.instance.ExValueEle);
                            obj.parent = ExchangeCtl.instance.ExValueEle.parent;
                            obj.getComponentInChildren(cc.Label).string = App.formatMoney(json.card[i].gold);
                            
                            var button = obj.getComponent(cc.Button);
                            button.clickEvents[0].customEventData = i;
                            cc.log(obj);
                            ExchangeCtl.instance._exchangeValueActiveList.push(obj.children[0]);
                            obj.active = true;
                        }
             */

            for (var i = InboxCtl.instance._spawnedEles.length - 1; i > -1; i--) {
                InboxCtl.instance._spawnedEles[i].destroy();
                InboxCtl.instance._spawnedEles.pop();
            }

            var data = JSON.parse(jsonString).messages;
            InboxCtl.instance._messageData = data;
            cc.log("there are " + data.length + " mess.");

            InboxCtl.instance.messCount = data.length;
            InboxCtl.instance.bg_Inbox.active = false;
            InboxCtl.instance.label_Mess_indicator.node.active = false;

            var not_read_count = 0;

            for (var i = 0; i < data.length; i++) {
                var obj = cc.instantiate(InboxCtl.instance.InboxEle);
                obj.parent = InboxCtl.instance.InboxEle.parent;

                var textArr = obj.getComponentsInChildren(cc.Label);
                textArr[0].string = data[i].senderName; //title
                textArr[1].string = decodeURIComponent(escape(data[i].content)); //content
                textArr[2].string = data[i].createdDate; //time

                var btnArr = obj.getComponentsInChildren(cc.Button);
                btnArr[0].clickEvents[0].customEventData = data[i];
                btnArr[1].clickEvents[0].customEventData = data[i];

                if (data[i].status == 1) {
                    textArr[3].node.active = false;;
                } else {
                    not_read_count += 1;
                    InboxCtl.instance.bg_Inbox.active = true;
                    InboxCtl.instance.label_Mess_indicator.node.active = true;
                    InboxCtl.instance.label_Mess_indicator.string = "" + not_read_count;
                }

                InboxCtl.instance._spawnedEles.push(obj);
                obj.active = true;
            }
            //cc.log("OnGetMessageDone from InboxCtl " + InboxCtl.instance._spawnedEles.length);
        } else {
            UserInfoCtl.showDialog("Đã xảy ra lỗi khi lấy danh sách thư. Vui lòng thử lại sau.");
        }
    },
    SetOneMessageRead(event, data) {
        var index = (-1 * data.id).toString();
        //0: sender|1: time|2: title|3: content
        this.InboxDetailText[0].string = data.senderName;
        this.InboxDetailText[1].string = data.createdDate;
        this.InboxDetailText[2].string = data.title == null ? "" : decodeURIComponent(escape(data.title));
        this.InboxDetailText[3].string = decodeURIComponent(escape(data.content));
        this.messageDetailPop.active = true;

        for (var i = 0; i < InboxCtl.instance._messageData.length; i++) {
            if (InboxCtl.instance._messageData[i].id == data.id) {
                InboxCtl.instance._messageData[i].status = 1;
                break;
            }
        }

        InboxCtl.instance.messCount -= 1;
        if (InboxCtl.instance.messCount === 0) {
            InboxCtl.instance.bg_Inbox.active = false;
            InboxCtl.instance.label_Mess_indicator.node.active = false;
        } else {
            InboxCtl.instance.bg_Inbox.active = true;
            InboxCtl.instance.label_Mess_indicator.node.active = true;
            InboxCtl.instance.label_Mess_indicator.string = InboxCtl.instance.messCount;
        }

        var str = {
            msgIds: index, //string data
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.DELETE_MESSAGES;
        WebsocketClient.Socket.send(m);
    },
    DelOneMessage(event, data) {
        var index = (data.id).toString();
        var str = {
            msgIds: index, //string data
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.DELETE_MESSAGES;
        WebsocketClient.Socket.send(m);
    },
    DelReadMessage() {
        var index = "";
        for (var i = 0; i < InboxCtl.instance._messageData.length; i++) {
            if (InboxCtl.instance._messageData[i].status == 1) {
                index += (InboxCtl.instance._messageData[i].id).toString() + ",";
            }
        }
        if (index.length == 0) {
            InboxCtl.showDialog("Bạn không có thư nào đã đọc");
            return;
        }
        index = index.substring(0, index.length - 1);
        cc.log(index);
        var str = {
            msgIds: index, //string data
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.DELETE_MESSAGES;
        WebsocketClient.Socket.send(m);
    },
    OnDelOneMessageDone(status, jsonString) {
        if (status == WarpConst.WarpResponseResultCode.SUCCESS) {
            //Set Read: {"isRead":true,"desc":"Success"}
            //Del: {"desc":"Success"}
            //InboxCtl.showDialog("Xóa thư thành công.");
            var data = JSON.parse(jsonString);
            if (!data.isRead) {
                InboxCtl.showDialog("Xóa thư thành công.");
            }
            InboxCtl.instance.close();
            InboxCtl.instance.show();
        } else {
            InboxCtl.showDialog("Có lỗi xảy ra. Vui lòng thử lại sau.");
        }
    },
    //#endregion
});