/**
 * By: longtnh
 * To do: Save request-response code and some important variables
 * Date: 2019.03.02
 */
var WarpConst = {
     REQUEST: 0,
     RESPONSE: 1,
     UPDATE: 2
};
var WarpConst2 = {
     REQUEST: 3,
     RESPONSE: 4,
     UPDATE: 5
};

var WarpRequestTypeCode = {
     AUTH: 1,
     SIGNOUT: 4,
     GET_USER_INFO: 5,
     UPDATE_AVATAR: 6,
     JOIN_LOBBY: 10,
     SUBSCRIBE_LOBBY: 11,
     UNSUBSCRIBE_LOBBY: 12,

     JOIN_ROOM: 22,

     GET_ROOMS: 27,
     SEND_KEEP_ALIVE: 30,
     UPDATE_PEERS: 28,
     GET_MESSAGES: 44,
     CHANGE_PASS: 46,
     UPDATE_INFO: 47,
     DELETE_MESSAGES: 50,
     GET_SYSTEM_MESSAGE: 53,
     GET_KOIN_EXCHANGE: 56,
     FEEDBACK: 72,
     GLOBAL_CHAT: 73,
     IAP_TOPUP: 80,
     UPDATE_USER_MOBILE: 88,

     SUBSCRIBE_GAME: 94,
     TAI_XIU: 96,
     TRANSFER_GOLD: 101,
     GIFT_CODE: 103,
     DEALER_CONFIG: 113,
     REF_CODE: 114,
     GET_ALL_JACKPOT_AMOUNT: 115,
     START_MATCH: 202,
};

var WarpResponseResultCode = {
     SUCCESS: 0,
     GIFT_CODE_NOT_EXITS: 43,
     GIFT_CODE_USED: 44,
}

var WarpNotifyTypeCode = {
     UPDATE_MONEY: 8,
     UPDATE_JACKPOT: 14,
     NOTIFY_SESSION_EXPIRED: 47,
     WORLD_CHAT_NOTIFICATION: 53,
     TAI_XIU: 59,
     NOTIFY_ALL: 73,
};

var LobbyId = {
     SLOT: 18,
     SLOT2: 36,
     SLOT3: 37,
     SLOT4: 38,
     MINI_TAIXIU: 96,
     MINI_POKER: 98,
     MINI_GOLDRUSH: 111,
     MINI_SLOT: 112,
     MINI_SLOT_2: 116,
};

var MiniGame = {
     UNSUB: 6,
     SUBSCRIBE_ROOM: 23,
     START_MATCH: 202,
     ADD_BET: 214,
     GET_JACKPOT_HISTORY: 221,
     GET_USER_HISTORY: 222,
     GOLDRUSH_FIND_MATCH: 234,
     GOLDRUSH_RUSH_ONE: 235,
     GOLDRUSH_HIDE: 236,
     GOLDRUSH_DELETE_MATCH: 237,
};

var MoneyChangeType = {
     BY_SMS: 1,
     BY_CARD: 2,
     BY_APPLE_ITEM: 3,
     BY_SYSTEM_ADD: 4,
     BY_VIDEO_ADS: 6,
     BY_TRANSFER_GOLD_GOLD: 8,
     BY_GIFT_CODE: 12,
     BY_BANNER_ADS: 14,
     BY_REF_CODE: 16,
     BY_GOLD_RUSH: 17,
     BY_UPDATE_USER_MOBILE: 18
};

var Links = {
     outSideHttpRequest: "https://api.huto.top/",
};

var setup_platform = "";
var setup_model = "";
var setup_provider = "";
switch (BUILD_TYPE) {
     case BUILD_FACEBOOK:
          setup_platform = "Windows";
          setup_model = "Chrome";
          setup_provider = "iAHya"; // ok
          break;
     case BUILD_APPLE_STORE:
          setup_platform = "iOS";
          setup_model = "Chrome";
          setup_provider = "iAHya";
          break;
     case BUILD_APPLE_STORE_AND_GAME_FAKE:
          setup_platform = "iOS";
          setup_model = "Chrome";
          setup_provider = "iAHya";
          break;
     case BUILD_IPA_INSTALL:
          setup_platform = "iOS";
          setup_model = "Chrome";
          setup_provider = "iAHya";
          break;
     case BUILD_GOOGLE_PLAY:
          setup_platform = "Android";
          setup_model = "Chrome";
          setup_provider = "iAHya";
          break;
     case BUILD_GOOGLE_PLAY_AND_GAME_FAKE:
          setup_platform = "Android";
          setup_model = "Chrome";
          setup_provider = "oYasO"; // ok
          break;
     case BUILD_APK_INSTALL:
          setup_platform = "Android";
          setup_model = "Chrome";
          setup_provider = "iAHya"; // ok
          break;
     case BUILD_WEB_MOBILE:
          setup_platform = "Windows";
          setup_model = "Chrome";
          setup_provider = "iAHya"; // ok
          break;
     default:
          break;
}


var GameBase = {
     api_key: "ede9fgd469e42fb6f064232dfgdfg",
     MoneyName: " GOLD",
     providerCode: setup_provider,
     platform: setup_platform,
     model: setup_model,
     device_uuid: "sdjklhfjklasdcndsjkcbvd",
     clientVersion: "2.0.0",
     alphabet: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
     userInfo: null,
     gameConfig: null,
     apiToken: null,
     playerAva: null,
     playerAvaSpriteFrame: null,
     disconnectByLogoutBtn: false,
     config_recharge : null,
     config_exchange : null,
};

module.exports = {
     WarpConst,
     WarpConst2,
     WarpRequestTypeCode,
     WarpNotifyTypeCode,
     LobbyId,
     MiniGame,
     Links,
     WarpResponseResultCode,
     GameBase,
     MoneyChangeType
}