var JSaes = require("aes-js");

var App = {
    formatMoney: function(value){
        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " ";
    },
    dycryptAes256Cbc(dataURI){
        //var key_256_array = new Uint8Array("9f383e507e39122df4b2e94c06ab761a");
        var key = [57,102,51,56,51,101,53,48,55,101,51,57,49,50,50,100,102,52,98,50,101,57,52,99,48,54,97,98,55,54,49,97];
        var iv = [0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0];
        //var dataURI = "8Hkkb7rKuXmqFSKhslvIcAG9jGu5Mt3/N9VPU0nsVj1N8YjZzfczr/biac9NxBlv27LGiqxY39goiQpKuRVJRP2wI9uoAbegdHUuIXfIu2JUdwyonm9Ok1JZIWItxw86YDhB37ylEHRe8Z1xerVavsHYelk2dRf6GJWUfoUVwpH8pHmqp/lMqRu5u48Fnnc+zE5VSRADCu5nddqiTYslTne8YVvTOo2Qr4TQo66SnzNY/t0N+bHnOuzEGeotaoyuAKvQIvr5Mex8dd8hFjs/8qb/aW5pdug9Ga8VNJL3CsYniZxVhOuUqb/zENuj65kLbDIhu9tjgunnlg/FtaUjW9MsQNsJCg4pb7tOvPcQfuYh9RqTNKC4Lk6BftBw66YDhuYVJZZEXLc48cz1exwzdCIsb1KO2RhdqHqqZ6cv53wEop6OnXrriSIBbWeC2l4aFQE2SXbJysdR2FdKwQBcVhz5yJgtW8VgNOwLV+IaMbzLeEYjI77GXADaXLoODq2GHUyPtydQezYngEJNGGMcQlnhkHeKpW0McSvwjbqTKUk8Jur0zSAPkurku2PIINbxq/XGwNPH8l8vsnp/h7P7bQLIlqMGG1Xm+O1DBOBA4IZTrxvC1Ld83jKiiLgHfMLiFS7hI2dqvRedhrNJaDhrgyssEjdoNm+zApWWxAeyu2cDWFz2r4CRlrXAjj8SAxk+NAuOHc0ikc77ebCvRDyW5zJanm8vj0CI7lYtBVRlxOIg2X00ommuxqRBzgv/zr/Uq3xKctEjHxCzxeXzhQlXzjwqdyX6EoSP/hW/n6mV/paJoFmnX7mhvPerNKaaeVVfXsspUbun64lM84TTT+f5YmOeQf8rYOy0hm1oB+wVqEYSb6Zk3fPgj10qeShasROvENhe6eYyLSIAB+ANa+dK2FxlHh0WhqYA4aYv7mr426jkPUQDva/mMU6J5WSivUt5oadDUH4MI8uSVOyT5woeyg==";
        //var dataURI = "h7lJBzWSOJaAc5nC/sRcdFNnXLbTtbR7qWBZRB9sda+wN99Zb7Dn5hzmFEUqz6ekK3xLVSc+SBrTgVvpNqpJfT2PvNX1TT5iQ1NDYQrjJR/z3VYH42YVVB1HWBFHZK8RCseZxGYkxiYGGW33SZvjpMGeXsmoTQ7QOPXj6NpoHmM1vO7BufTRHt0MGYXl65P8Z0uTb9rwf/RBEP6veVdmRyCdj7yL24VKabaratAVkVJ9O6wjuJCwvDcdxfKOsRY+ELRLlQD85v7S5ePczQ6SiKm8xZfnqYLYhsy7fI01ndqew9daU4PW2gClLIDsS/Lsn18w2E7O0zjCE2Atsp8B/xG7YnjLZhCHQYFdn2dyyNZ7sTP+h6aHHDN0NLa56I3CmPCy/fKM6bsi4sBPNQe1j+XzuHCpGPWNCyGWD82t4lbMcfh9fs2+sm8S+d8uJEXgQyazHVLpJEGJuI8xP1PiyD/LuypYz/cr2pr640gwY4E+v9F8nDDDV7BUq7njeB2+6De3CNUn1ep1b6l4VYt6trGoJsyZu5LoZk6znJNBigT6ShOrqo8T91f1AgSg95KGa1Jrqsnc0NF8sErU8zhs/YQghkNo8ORZa/rfPwzPRiSzrjovb2FqOxwoEWx5A8C4hginslp7xbJopWAaOZJLOJAPrc4B4AsAxbgEbaNEcbkPpgoyqvDJpD5GQB3WqJgyGk3+BBvRZeAwSEgGrNyutXIjy6vNF3X1qUnQGMrMIZSedfxaopByk+TH0LSBQEvduOqWcLJu3YqkAt2m6EFM6S1OU7ycesLC0Mgi3Hq2Er1TSH8GQjhoo+pCDEc7w5zXGlT5b8AtyWQWEE99FhHY6ukujmlo3U4DTvTLXMnxDlgxdN9m55tbk9hMsYYCok+XxnqL6M5TJ40ywYQq0du2rHY2eLxBVEKSBVnGG8BZgpDKGmlN/srxIq5lOWAC0ATnlpPRZIC942p1TgL2fmxk06j2J7T64ficSfASSIRITKbE+3WqpzCuS30QFOB4asdrHO3KkbMSUxQuQNIY5d1MV76ddJo3nWXL3WZsQfUNVZjD9HllJhqYjK8q4C2PJDlE0NKLDWtwyqabsqyG/oqQHopBsOZMbnrx7f3b2YZP3JdTRK+WpvmYid8HO2BtkbRDFUko84djsHsTYmrbHX8pXEiXJcAJ4RjJ36IdD3d+jCvc2q+UYPg2oOfkj2+6uD3ipj3UvfRBPiwqyQC0WwMzghpNOZoyC1NOIOFTjVSr7TXU/e8n3DkdAc8Qfo0er7A0RCT7WcrAAl6duCKrxbuY/WjsZNV+fVtKs1Fl8/oMJfmOTVlxmmq0gEGG5riSiQG8phpsEV8Awbsoq8De3ABQgaVRGSqdx+wpkdKxpO2POXaWMZ+NCR+Cfy5K6LPHt2u1Pk+rz5j6ZU1FdX4Q6wyvSm4Afz7NSEFtL1gZsV3hAcvNvF4QO+0+mG8RS2F2/zEY24EVlNlrFg5zk0E8yOfFBdKnS2ARBlhYmfit9s6GhtgPN0JefCjOtRLGo/fm23VD5iDdZ9qMhOpmj7mleGVmTxYPH+41HpMY2vtWpHRGU9NzF8R3DSbNcG2n0nAceiE1A6hQXjXbS73dDtznXzNwgu2P3b2mhtYxmzuq7lSyH5aslQibFCF+r/w/yLZGD3RX9Vb6+MB1UBFSxxZXl5livGU9yJa0AyTqKOdfE/Bq2K/7+Qww0ge8k73T3yOZqPHIRaSC6Pm1j+DyvpIoUoLs9uiqduVcrIpAqhivDC2dc5MgKiw4ZQpS6cpOy3X0H+M7S/wvIWHhkAH2IShFZ7SPIMuHAobOb2dt30W5T+Lbq0rcAglksAinyvj9bZaUrah1NXw+q2dkV1WiqVA9CqGn17MWQpSWi6rMg95KjelSfVsUa3uiZziEvEzjiipPJDBL3caT5IRPTnXnLqpMAY5tPx0e1sbO+QXfI8HerBu+qRAVMegjXAaw8FR8Uz40bB65JAJ8S6+fv0YLxxxt5CwI+ScZwJThSdeFZ6dvJFF4NbHo/PhQX/0XkQgBcsl4M4fVUeWq+kh3utJ4hI0TQjM2jI4tvoC6YJMCpW2AFSb6RF4/BzhgEW+LTQrZ86qRiX4/JSTwKh9GpJEuSNIJS3l7T/Llayidx8ltbCp7q/DT7HD4+XXpQ4rpejjXb84/mqrR8uSg/FDw6Npjh48P+aF1fTVQHhknirWyfgkvPV2pNomDiGkqw7CfZn0Pf68h8HxMBehOi+Tr8DMqen9NWQVhMbS0A6FEYB483e+wYS4080vXqKL5Ihq+y3tjubl0DSSVwS/eVZCdSyAgRgJNo+2YrDBaCSpVEkf5xZfdXuaMwhsXoM+Zes3HDX3PY+HbcLI3sxzQWmswxvX6X5fPhcAlGAP06lEzSAuvvuiXU2b0zqJgEwYcvvfixFVcykUd7CgjSHIu1ij6nEIhMj9TKq/ij/3A5v7jdYLiFQSwuRIY2hqUTYkCKOXzvxDgMaoUI9QfoRmHPdpiEumVWKSRDLQKgVeHJiTL2iBy8+JwYaFcmVg=";
        //var dataURI = "QOxBMUIu2jr0jsm0GoBYH6ks9vAk2EAcouiuackd6oqSRzKy4jsFPzQWBSY2IWFpXjeL67wBynV9ff4RBbRk/XOP3XLUuzMD54+Nx0ITj3VGQR+XkI222ts7m7I1SNp7RyW1hE1J36/YHN/zbRxB69713xr9oQFxhbTlO3wqCDCjfCNH7gltuiwGbeEQeUgwdYQo0fJvxtFISauu+V6BCcwt2Nn4uHBRX7IcW5ud366lL0fC6YLYGAgmgM6CBFtWkIxY5cLWM0bUOSnJ+jeCu6qYHvbtdKPncFosq2UfLSW6/zHSfzNNlkJHUzVYweLpYFpVn3pg/qiDF5lMScg0FglOi3UyDVJsEgbTGpeMyLo8fZGiVD7DQR3O3AprWOKDKFdVW4BYclE/0aNORSoXpwSYSkaJ4YxZXPDBkHb+w1B/wJNBstRY0QIeec2/ZMxf1afmDRKHHZMTgsGxa78M70KMb9R/iGxSk+m24J0oZIXr204HFMAQth6rst9Cya2VCAMEBn1CZN0yJF/Gb17elNsOxwH2PJhzjbdATw2ASyWQSggTlbGME2I8U+ckcUOR2sMsDPxAVPa+sBqvcpLqtZL8E5/j50qr46dNgKm8DDvGOEaqV2/BmvBVcVploBMvT7ZGEqA3UT79yKBFVHlI6kRKMBrxLm2kVUI6sRES+nuj2lgxtHXP3K6EsfLbshCe9mQfY9kIVgJSogXWakry1iSWchOb6V2rZEbGYq5qooWzrT0Qeymjgf/7401lSgFk6b/Z8+KPUlkfnyyHYF9N2A4hgF6HtGIjRtah8wfi4wNF7QG+n8bMb2t65qY0suOlF3qsp+FHoZGQJCxMT7Dk7z7Ib9rTi2ydWDjzKIQpehsmstNy/WVSIvniOoho2FudJ8t6KXeXP2RthMIhx0n14S3EE5LzIjCB5CE4MUDC4XSRjjNQ8Ta+gDlu+lXer2II91/3V/O3uegrxVPB7kSo6QOaCLR2VeyQ3dJGGnSvaBEqTvsjCnRq339fm+f+xPTHy3FbJPYTFWJU/aaw0F5ELH/oS4w9BAwWV0EYMPESyHZgCxFVZonq2VGRH/QwAZDycch7XpY3IdGDoI4ok0DtFD0akgDZMVdLNVrWicY9Y5T6rHoW5vbV7h/rlDfkLsXmkMbDHEQ8XBWqpi/zID/UzvSP14BchGr/XhwoCvi9ao6B8Ptn+2C9Fp69o8/MkrqJwQhHC+F/O0mfsPHfqz27t3UH4Su5ixASRhAqM51ZciRKr1pKAgRs+5PXPFDlj7C+LUQfe9D2uhKfF6wuQB1Pndi9QTZ/jonsj31Wppxh4l9CdaRZR7Yx8lyehE43w6tMyTOr+chSJBpH62UcCWiO5kX9H8ORDAj+3ynRk5rrlBWJwObEFrC9zvkHvZFg7k15dGm1ApgBk61NuB5zX9e4Ox21ktoRANk306GwT87Z0XojirpJX8QIaOQkEH9W55yXjXF/p1UU7FpdIHH8qHKQuwWCzvk6eCgQuZ2TNxxNk71zSA5/+fWCzjN+x/1i67otrN7mluFgXYniJ+nfAfMGJMUzeNt76qRJP5jjJjgX/Q7uyJZwIJRzKO48jQKDoGgnlHHHSq9daEI49KKovLiAd9rPB5j/3nBJ45XsOiuCVkXQzMSpbTsq+oJLF+2cFw7ocIavAMPZALBhiKj4YXSCqibi7Zy9vadCtSsi4elUw/HeKsj4Adb8tbWJane0FrNM65WDV26+U4wIfMxr/CyUljIKnWj85sePL37Orsj0HOXleNrnW8yn/J+Gfx1kSOHeHCsnqaa6Dr4g8XAdNWne0gLn+F0Zn4BkTajhsoZfzNPxbGF4G5GMfguRnIrWdyiijVUHkzlTdyW+fNPpWLWe3Q0yfAHFvdgV8YsmsU4jzhcaz50vQ1YHUxt/ifbAyU64ngBdvgVSFkUS8r6nqsPXfvArvVWwbO314qoZC83AUkzInGBhxpI2WVRxVhXwzf8rqBouMYEZwh8jGPmNHDMTt/xRBSydq/FidxafxjWEcWfrUWEvqUfIDgtOU2btGY0CcUl1mRJWYVbmaccp9rVmoShlhewMY0lxrGYPaetfQAn71N7SZt6SGToLbOh3nL7Z3EPN8MzZAkRRXhgUHKYCh1NdmTBjhQvPvOnu5L3QPA7097jkVgn3AYWSB8jT5890o2eVuKPkpu5XQbpu3vkvxfEU104MHNhigMyj5I2aIx6byAjrx+eSpabr1ueSUzue6ywC+xDBkOt+xppb2jO2bfind3AmGjdncaiDou3z4MKEXH0JxoeZ5CzFtvR49Ev7Ek1VL0Akd9GsgUZaovEu6ZesHqd+UiEHy10XOeohl3VCjU9zMXV0MnfMWHUiNnY77iMxlPMMSxjV6YJTqWJG/ekc2JXolxkAFelAU4ShpHv55YCr9dTJDIFJNVaCm5VmNluSMlUGH5uC44NbhRKCBFhlxyw6bjAiUkiQTX0zZARHmB/2WqvjNdBlzK54cqVATqAdMZcyr1LvCIEmVfsqRGO6P9fqghx5NIOeq0JTNJHvyHJlF/dzhUhv4pGnX7v999c/lJDQFLUauFztahBSmAdi4Upofxst0RGwhiA89r7YRbikDQqYqqkxi2bFAA/2B7lRTurJEbPCYdWc3vUMPmC70dE/wt2Zd9aqQq3FClxf18gpCNCvgNAEpSzHBNCL4iWCjJWpGLND1c1/u0ucGjZNfmXPfjPVXA8IaT6OWpJKL7w5V+CGPUBBXjPeTk+r1W8BH/AdYSizdihO/fM3qDq2U81Ce1To9h3RN5X9hs+C7ukajuiQimAlZPDrveg6XhVDgS5oAwR2GZeQ4S04Y1zrclwH0YFPJ3e6cB2kPCUhdWfu9ipc9Bq03496QdlfUSBpDqwxRrLAFTEiN5n46qc7ATcrPT402PMwz28AMnp38VzWyztVVlJ7NHYltieoTT/itpcRczChIgcgMJhZVcnRG9I1SMS+cP347Z21jmgHTm6JDJfgCjjfxufjffkBoO7Hz7YElvG67eRi2ChDeVN8/krFadFTnlVpJxgMNF+pnzc4ss8mkcW4Sdj1gnv+E3Ro+p2Vgo7N2STKfqE7WSwkhRvylOV9KxzjMY1lA+7DMEvpOGVtmAWB0NEu53lPFjNyeYCkx031J/yVC2huWh+dPtJAF4cJ9t7DMufi+KGG6jWYvRXDZk62mYwtUEjVpQz8iKDopOmefnXEBXY3DUUBqYdjE5SxTa14/FGhfxDsdZSWqmHWDzs+X4OLedOHBfM4zBIcR3wgYuFAlFafUuK90AtjULPCFUgzZJuDv8NFOR5Gfpz5YyU9de06rYQKceH1XYKRud1jbHNHywBXEACsMMUD9LRHm4SycoQcexyxBO8hHPdONcy6k99KEdmgud0U3OfrIlxHvtAmGKpNavrkO0jF5z5SqIvsWSkSVHpZApwICBHEGBqC+lBYBzKRgEqxUZPv2C9xfCGYoERiggIIrVVeO0ZMHvXtOU7SvnvApJsePJtcQ5BHHYi6Ab3YpIixxPOIBbZ7wewApse+QSqEt97gq6tUrRP2MBChk7EFpkdpLmy/xOOXd/vQIEVLuzIaFaI9ha+y2TJiXlCv0mmzfwSiu6KlkJ0XvrrgoJx3uFRqBw5VfuEo0KOZ1LYJVEjr0Fg9TPA3TVlvmSgcr7YCh+CyFLnWGpvaFu7Dw3CqmpCX83peFg85SGdw3wapUnNIKn9ZBQFl/mwQVPdFSh7xkzjsenGmH5P3GptQzCtOZMtZTFGttIj6RieILQ/qyju8CjZKCV1G35GwFI+5KwTCMr6pJUEiOkuLIlqplhQ8NxLpp5Zp90nH/6oE6AAR/JL+FCvs9V5eDXcLsB3Cyjo3PhOa80g0P1lROcWkqb1NjPG+MmlzKd4TJIrlh7OTsNG4mQPesMMkRI2D5kNeaWPbGaE66G1buVSf61y5njbwWUf65bSfDmSpowKUUBe7alUhKgSquuX4716zY7q/Jg==";
        var binary_string =  window.atob(dataURI);
        var len = binary_string.length;
        //cc.log("leng = " + len);
        var bytes = new Uint8Array( len );
        for (var i = 0; i < len; i++)        {
            bytes[i] = binary_string.charCodeAt(i);
        }
        
        var aesCbc = new JSaes.ModeOfOperation.cbc(key, iv);
        var decryptedBytes = aesCbc.decrypt(bytes);
        // Convert our bytes back into text
        var decryptedText = JSaes.utils.utf8.fromBytes(decryptedBytes);
        //cc.log(decryptedText.substring(0, decryptedText.lastIndexOf("}") + 1));
        //cc.log(decryptedText);
        return decryptedText.substring(0, decryptedText.lastIndexOf("}") + 1);
    },
    encryptAes256Cbc(jsonData){
        // var data = {
        //     api_key:"b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1"
        // }
        var text = JSON.stringify(jsonData);
        
        
        var key = [57,102,51,56,51,101,53,48,55,101,51,57,49,50,50,100,102,52,98,50,101,57,52,99,48,54,97,98,55,54,49,97];
        var iv = [0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0];
        var textBytes = JSaes.utils.utf8.toBytes(text);

        var aesCbc = new JSaes.ModeOfOperation.cbc(key, iv);
        var encryptedBytes = aesCbc.encrypt(JSaes.padding.pkcs7.pad(textBytes));
        //cc.log(encryptedBytes.toString());
        var t = String.fromCharCode.apply(String, new Uint8Array(encryptedBytes));
        
        var binary = '';
        var len = encryptedBytes.length;
        for (var i = 0; i < len; i++) {
            binary += String.fromCharCode( encryptedBytes[ i ] );
        }
        //cc.log(window.btoa( binary ));
        return window.btoa( binary );
        
    },
    formatUnicode(data){
        return decodeURIComponent(escape(data));
    },
    formatUnicode2(data){
        return decodeURIComponent(unescape(data));
    },
    getRandomNum(fromNum, toNum){
        return Math.floor(Math.random() * toNum) + fromNum;
    }
};

module.exports = App;