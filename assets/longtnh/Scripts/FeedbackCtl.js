/**
 * By: longtnh
 * To do: Control Feedback Pannel
 * Date: 2019.03.25
 */
var WebsocketClient = require("WebsocketClient");
var WarpConst = require("WarpConst");
var App = require("App");
var FeedbackCtl = cc.Class({
    extends: cc.Component,

    properties: {
        feedbackIpf: cc.EditBox,
    },
    statics: {
        instance: null,
        showDialog: null,
    },
    onLoad () {
        FeedbackCtl.instance = this;
    },
    onEnable(){
        WebsocketClient.OnSendFeedBack = this.OnSendFeedBack;
    },
    onDisable(){
        WebsocketClient.OnSendFeedBack = null;
    },
    show(){
        this.node.active = true;
        FeedbackCtl.instance.feedbackIpf.string = "";
    },
    close(){
        this.node.active = false;
    },
    //#region request - response with server
    SendFeedBack(){
        if(this.feedbackIpf.string.length == 0){
            FeedbackCtl.showDialog(" Vui lòng điền nội dung góp ý ");
            return;
        }

        var str = {
            userId: WarpConst.GameBase.userInfo.desc.id,
            clientVersion: WarpConst.GameBase.clientVersion,
            platform: WarpConst.GameBase.platform,
            model: WarpConst.GameBase.model,
            type: 3,
            content: this.feedbackIpf.string
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.FEEDBACK;
        WebsocketClient.Socket.send(m);
    },
    OnSendFeedBack(status, strJson){
        if(status == WarpConst.WarpResponseResultCode.SUCCESS){
            //var data = JSON.parse(strJson);
            FeedbackCtl.instance.node.active = false;
            FeedbackCtl.showDialog("Gửi phản hồi thành công.");
        }else{
            FeedbackCtl.showDialog("Có lỗi xảy ra. Vui lòng thử lại sau.");
        }
    }
    //#endregion
});
