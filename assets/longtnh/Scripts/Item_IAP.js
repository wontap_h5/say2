var RechargeCtl = require("RechargeCtl");
cc.Class({
    extends: cc.Component,

    properties: {
        label_Price: {
            default: null,
            type: cc.Label
        },
        label_Value: {
            default: null,
            type: cc.Label
        },
        btn_Buy: {
            default: null,
            type: cc.Node
        }
    },

    id_Item: null,

    // LIFE-CYCLE CALLBACKS:

    onLoad() {},

    start() {},

    setup_Item(data) {
        this.label_Price.string = this.convert_money(data.price);
        this.label_Value.string = this.convert_money(data.value) + " GOLD";

        this.id_Item = data.id;
    },

    buy_Item() {
        // index : gold_pack_1
        RechargeCtl.instance.buy_Item_IAP(this.id_Item);
    },

    convert_money(price) {
        return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " ";
    }

    // update (dt) {},
});