/**
 * By: longtnh
 * To do: Control Lobby
 * Date: 2019.03.02
 */

/*
GameID HotUpdate
1 : Slot 1
2 : Slot 2
3 : Slot 3
4 : Slot 4
 */

var typeUpload = "";

switch (BUILD_TYPE) {
    case BUILD_FACEBOOK:
        typeUpload = "__________";
        break;
    case BUILD_APPLE_STORE:
        typeUpload = "applestore";
        break;
    case BUILD_APPLE_STORE_AND_GAME_FAKE:
        typeUpload = "applestore_fake";
        break;
    case BUILD_IPA_INSTALL:
        typeUpload = "ipa";
        break;
    case BUILD_GOOGLE_PLAY:
        typeUpload = "googleplay";
        break;
    case BUILD_GOOGLE_PLAY_AND_GAME_FAKE:
        typeUpload = "googleplay_fake";
        break;
    case BUILD_APK_INSTALL:
        typeUpload = "apk";
        break;
    case BUILD_WEB_MOBILE:
        typeUpload = "__________";
        break;
    default:
        break;
}

var Slot_1_Custom_Manifest = JSON.stringify({
    "packageUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/slot_1/remote-assets/",
    "remoteManifestUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/slot_1/remote-assets/project.manifest",
    "remoteVersionUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/slot_1/remote-assets/version.manifest",
    "version": "1.0",
    "assets": {},
    "searchPaths": []
});
var Slot_2_Custom_Manifest = JSON.stringify({
    "packageUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/slot_2/remote-assets/",
    "remoteManifestUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/slot_2/remote-assets/project.manifest",
    "remoteVersionUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/slot_2/remote-assets/version.manifest",
    "version": "1.0",
    "assets": {},
    "searchPaths": []
});

var Slot_3_Custom_Manifest = JSON.stringify({
    "packageUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/slot_3/remote-assets/",
    "remoteManifestUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/slot_3/remote-assets/project.manifest",
    "remoteVersionUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/slot_3/remote-assets/version.manifest",
    "version": "1.0",
    "assets": {},
    "searchPaths": []
});
var Slot_4_Custom_Manifest = JSON.stringify({
    "packageUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/slot_4/remote-assets/",
    "remoteManifestUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/slot_4/remote-assets/project.manifest",
    "remoteVersionUrl": "http://creatorg.huto.top/" + typeUpload + "/say2/slot_4/remote-assets/version.manifest",
    "version": "1.0",
    "assets": {},
    "searchPaths": []
});

var default_PotAll = [
    5024562,
    17427954,
    63214690,
    3654785,
    24853157,
    58746348,
    6582143,
    13596423,
    40215735,
    5002562,
    24153157,
    40264735,
    1204862,
    19153657,
    50212735,
    1022962,
    26427624,
    38214790,
    1202562,
    32153157,
    57264735
];

var GM = require("GM");
var WarpConst = require("WarpConst");
var WebsocketClient = require("WebsocketClient");
var RechargeCtl = require("RechargeCtl");
var ExchangeCtl = require("ExchangeCtl");
var FreeGoldCtl = require("FreeGoldCtl");
var TransferGoldCtl = require("TransferGoldCtl");
var BannerCtl = require("BannerCtl");
var UserInfoCtl = require("UserInfoCtl");
var UserInfoCtl = require("UserInfoCtl");
var RegCtl = require("RegCtl");
var InboxCtl = require("InboxCtl");
var PotAllCtl = require("PotAllCtl");
var SettingCtl = require("SettingCtl");
var FeedbackCtl = require("FeedbackCtl");
var App = require("App");
var JSaes = require("aes-js");
var UIController = require('UIController');
var LobbyCtl = cc.Class({
    extends: cc.Component,

    properties: {
        //--- Sprites ---
        userAvaImage: cc.Sprite,
        loadingGameImage: cc.Sprite,
        // ---Texts ---
        userNameText: cc.Label,
        userIdText: cc.Label,
        userMoneyText: cc.Label,
        headLineText: cc.RichText,
        dialogContentText: cc.RichText,
        inboxCountText: cc.Label,
        notifyAllText: cc.RichText,
        //--- Btns ---
        dialogBtn: cc.Node,

        //0-2: slot1|3-5: slot2
        lobbyChestValueText: {
            default: [],
            type: [cc.Label]
        },

        //--- Input ---
        userNameIpf: cc.EditBox,
        passIpf: cc.EditBox,

        //--- Anim ---
        notifyAllAnim: cc.Node,

        // --- Group ---
        beforeLoginGroup: cc.Node,
        afterLoginGroup: cc.Node,
        bannerEle: cc.Node,
        bannerEle2: cc.Node,
        //--- POPS ---
        /**
         * 0: reg|1: recharge|2: loading|3: dialog|4: exchange|5: freeGold|6: transferGold|7: userInfo|8: inbox|9: messagedetail|10: setting|11: feedback|12: imageLoadingGame
         */
        popUps: {
            default: [],
            type: [cc.Node],
        },

        //--- RESOURCE ---
        avaResource: {
            default: [],
            type: [cc.SpriteFrame],
        },

        //--- Private variables ---
        _userInfo: null,
        _spawedBanner: [],
        _currentBannerIndex: 0,
        _currrentLoadedBanner: false,
        _fakePotValues: [],

        music_background: {
            default: null,
            type: cc.AudioClip
        },
        Popup_News: {
            default: null,
            type: cc.Node
        },
        inbox_Indicator: cc.Node,
        inbox_BG: cc.Node,
        UI_Canvas: cc.Node,
        Icon_Game: cc.Node,
        UI_PotAll: cc.Node,
        Slot_1_Download_Frame: {
            default: null,
            type: cc.Node
        },
        Slot_1_Download_Progress: {
            default: null,
            type: cc.Sprite
        },
        Slot_1_Download_Text: {
            default: null,
            type: cc.Label
        },
        Slot_2_Download_Frame: {
            default: null,
            type: cc.Node
        },
        Slot_2_Download_Progress: {
            default: null,
            type: cc.Sprite
        },
        Slot_2_Download_Text: {
            default: null,
            type: cc.Label
        },
        Slot_3_Download_Frame: {
            default: null,
            type: cc.Node
        },
        Slot_3_Download_Progress: {
            default: null,
            type: cc.Sprite
        },
        Slot_3_Download_Text: {
            default: null,
            type: cc.Label
        },
        Slot_4_Download_Frame: {
            default: null,
            type: cc.Node
        },
        Slot_4_Download_Progress: {
            default: null,
            type: cc.Sprite
        },
        Slot_4_Download_Text: {
            default: null,
            type: cc.Label
        },
        Btn_Slot_1: {
            default: null,
            type: cc.Node
        },
        Btn_Slot_2: {
            default: null,
            type: cc.Node
        },
        Btn_Slot_3: {
            default: null,
            type: cc.Node
        },
        Btn_Slot_4: {
            default: null,
            type: cc.Node
        },
        Btn_Shop: {
            default: null,
            type: cc.Node
        },
        label_News: {
            default: null,
            type: cc.RichText
        },
        feature_in_footer: {
            default: null,
            type: cc.Node
        },
        Popup_Submit: {
            default: null,
            type: cc.Node
        },
        Popup_Need_Verify: {
            default: null,
            type: cc.Node
        },
        Spine_Anim: {
            default: [],
            type: [cc.Node]
        },
    },
    statics: {
        instance: null,
        mCount: 0,
        userInfo: null,
        need_Open_Feature_ID: -1,
    },

    update_PotAll_Data: null,
    last_Pot: null,
    cache_phonenumber: null,

    _updating: false,
    _canRetry: false,
    Slot_1_StoragePath: '',
    Slot_2_StoragePath: '',
    Slot_3_StoragePath: '',
    Slot_4_StoragePath: '',
    Game_Enter: '',

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        LobbyCtl.instance = this;

        var is16_9 = 1;
        var is18_9 = 2;
        var is185_9 = 3;
        var is19_9 = 4;
        var is195_9 = 5;
        var is20_9 = 6;
        var is4_3 = 7;

        var ratio = this.UI_Canvas.width / this.UI_Canvas.height;
        cc.log("GGGG Ratio : ", ratio);

        if (ratio > 1.70 && ratio < 1.80) {
            GM.Screen = is16_9;
            cc.log("GGGG is16_9");
        } else if (ratio > 1.95 && ratio < 2.02) {
            GM.Screen = is18_9;
            cc.log("GGGG is18_9");
        } else if (ratio > 2.03 && ratio < 2.08) {
            GM.Screen = is185_9;
            cc.log("GGGG is185_9");
        } else if (ratio > 2.09 && ratio < 2.13) {
            GM.Screen = is19_9;
            cc.log("GGGG is19_9");
        } else if (ratio > 2.14 && ratio < 2.18) {
            GM.Screen = is195_9;
            cc.log("GGGG is195_9");
        } else if (ratio > 2.20 && ratio < 2.25) {
            GM.Screen = is20_9;
            cc.log("GGGG is20_9");
        } else if (ratio > 1.25 && ratio < 1.40) {
            GM.Screen = is4_3;
            cc.log("GGGG is4_3");
        } else {
            GM.Screen = is16_9;
            cc.log("GGGG is unknown");
        }
        cc.log("GGGG GM.Screen : ", GM.Screen);

        switch (GM.Screen) {
            case is4_3:
                this.bannerEle.parent.runAction(cc.repeatForever(
                    cc.sequence(
                        cc.scaleTo(1, 0.96, 0.96),
                        cc.scaleTo(1, 1, 1),
                        cc.callFunc(function () {
                            LobbyCtl.instance.changeBanner();
                        })
                    )
                ));
                break;
            case is16_9:
                this.bannerEle.parent.runAction(cc.repeatForever(
                    cc.sequence(
                        cc.scaleTo(1, 0.98, 0.98),
                        cc.scaleTo(1, 1, 1),
                        cc.callFunc(function () {
                            LobbyCtl.instance.changeBanner();
                        })
                    )
                ));
                break;
            case is18_9:
                for (let index = 0; index < this.feature_in_footer.childrenCount; index++) {
                    this.feature_in_footer.children[index].scale = 0.8;
                }
                this.Icon_Game.scale = 0.9;
                this.Icon_Game.y -= 20;
                this.UI_PotAll.scale = 0.9;
                this.UI_PotAll.x += 15;
                this.bannerEle.parent.x += 15;
                this.bannerEle.parent.runAction(cc.repeatForever(
                    cc.sequence(
                        cc.scaleTo(1, 0.88, 0.88),
                        cc.scaleTo(1, 0.9, 0.9),
                        cc.callFunc(function () {
                            LobbyCtl.instance.changeBanner();
                        })
                    )
                ));
                break;
            case is185_9:
                for (let index = 0; index < this.feature_in_footer.childrenCount; index++) {
                    this.feature_in_footer.children[index].scale = 0.8;
                }
                this.Icon_Game.scale = 0.85;
                this.Icon_Game.y -= 25;
                this.UI_PotAll.scale = 0.85;
                this.UI_PotAll.x += 15;
                this.bannerEle.parent.x += 15;
                this.bannerEle.parent.runAction(cc.repeatForever(
                    cc.sequence(
                        cc.scaleTo(1, 0.83, 0.83),
                        cc.scaleTo(1, 0.85, 0.85),
                        cc.callFunc(function () {
                            LobbyCtl.instance.changeBanner();
                        })
                    )
                ));
                break;
            case is19_9:
                for (let index = 0; index < this.feature_in_footer.childrenCount; index++) {
                    this.feature_in_footer.children[index].scale = 0.8;
                }
                this.Icon_Game.scale = 0.8;
                this.Icon_Game.y -= 30;
                this.UI_PotAll.scale = 0.8;
                this.UI_PotAll.x += 15;
                this.bannerEle.parent.x += 15;
                this.bannerEle.parent.runAction(cc.repeatForever(
                    cc.sequence(
                        cc.scaleTo(1, 0.78, 0.78),
                        cc.scaleTo(1, 0.8, 0.8),
                        cc.callFunc(function () {
                            LobbyCtl.instance.changeBanner();
                        })
                    )
                ));
                break;
            case is195_9:
                for (let index = 0; index < this.feature_in_footer.childrenCount; index++) {
                    this.feature_in_footer.children[index].scale = 0.8;
                }
                this.Icon_Game.scale = 0.75;
                this.Icon_Game.y -= 35;
                this.UI_PotAll.scale = 0.75;
                this.UI_PotAll.x += 15;
                this.bannerEle.parent.x += 15;
                this.bannerEle.parent.runAction(cc.repeatForever(
                    cc.sequence(
                        cc.scaleTo(1, 0.73, 0.73),
                        cc.scaleTo(1, 0.75, 0.75),
                        cc.callFunc(function () {
                            LobbyCtl.instance.changeBanner();
                        })
                    )
                ));
                break;
            case is20_9:
                for (let index = 0; index < this.feature_in_footer.childrenCount; index++) {
                    this.feature_in_footer.children[index].scale = 0.8;
                }
                this.Icon_Game.scale = 0.73;
                this.Icon_Game.y -= 40;
                this.UI_PotAll.scale = 0.73;
                this.UI_PotAll.x += 15;
                this.bannerEle.parent.x += 15;
                this.bannerEle.parent.runAction(cc.repeatForever(
                    cc.sequence(
                        cc.scaleTo(1, 0.71, 0.71),
                        cc.scaleTo(1, 0.73, 0.73),
                        cc.callFunc(function () {
                            LobbyCtl.instance.changeBanner();
                        })
                    )
                ));
                break;
            default:
                this.bannerEle.parent.runAction(cc.repeatForever(
                    cc.sequence(
                        cc.scaleTo(1, 0.98, 0.98),
                        cc.scaleTo(1, 1, 1),
                        cc.callFunc(function () {
                            LobbyCtl.instance.changeBanner();
                        })
                    )
                ));
                break;
        }

        // Setup Plugin 
        switch (BUILD_TYPE) {
            case BUILD_FACEBOOK:

                break;
            case BUILD_APPLE_STORE:
            case BUILD_APPLE_STORE_AND_GAME_FAKE:
            case BUILD_IPA_INSTALL:
            case BUILD_GOOGLE_PLAY:
            case BUILD_GOOGLE_PLAY_AND_GAME_FAKE:
            case BUILD_APK_INSTALL:
                // Onesignal
                sdkbox.PluginOneSignal.init();
                sdkbox.PluginOneSignal.setSubscription(true);
                sdkbox.PluginOneSignal.enableInAppAlertNotification(true);
                // FB SDK
                sdkbox.PluginFacebook.init();
                sdkbox.PluginFacebook.setListener({
                    onLogin: function (isLogin, msg) {
                        if (isLogin) {
                            var data = {
                                id: sdkbox.PluginFacebook.getUserID(),
                                token: sdkbox.PluginFacebook.getAccessToken()
                            }
                            cc.log("GGGG PluginFacebook : ", JSON.stringify(data));
                            LobbyCtl.instance.LoginWithFacebookData(data);
                        } else {
                            cc.log("GGGG onLogin :  login failed");
                        }
                    }
                });
                //FB AccountKit
                window.OnFacebookAccountKit = function (access_token) {
                    cc.log("GGGG OnFacebookAccountKit :  ", access_token);
                    const graph_URL = "https://graph.accountkit.com/v1.3/me/?access_token=" + access_token;

                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
                            var response = xhr.responseText;
                            cc.log("GGGG response : ", response);
                            var phone = JSON.parse(response).phone.number;
                            cc.log("GGGG phone  : ", phone);
                            if (phone.length > 0) {
                                cc.log("GGGG Send");
                                if (phone.substring(0, 3) !== "+84") {
                                    LobbyCtl.instance.showDialog(" Xác thực thất bại. \n Vui lòng thử lại sau ! ");
                                    return;
                                }
                                var raw_phone = phone.substring(3);
                                LobbyCtl.instance.cache_phonenumber = raw_phone;
                                var str = {
                                    mobile: raw_phone,
                                    passport: 0
                                };
                                var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
                                m[1] = WarpConst.WarpRequestTypeCode.UPDATE_USER_MOBILE;
                                WebsocketClient.Socket.send(m);
                            }
                        }
                    };
                    xhr.open("GET", graph_URL, true);
                    xhr.send();
                };
                break;
            case BUILD_WEB_MOBILE:

                break;
            default:
                break;
        }
    },

    start() {
        for (let index = 0; index < this.Spine_Anim.length; index++) {
            if (index !== 7) {
                this.Spine_Anim[index].getComponent("sp.Skeleton").setAnimation(0, 'animation', true);
            }
        }

        // set checkRealUser
        cc.sys.localStorage.setItem("checkRealUser", "creatorg");
        this.cache_phonenumber = "";
        this.last_Pot = [];
        for (let index = 0; index < 15; index++) {
            this.last_Pot.push(0);
        }

        cc.audioEngine.playMusic(this.music_background, true);

        //--- Default set---
        LobbyCtl.instance.beforeLoginGroup.active = true;
        LobbyCtl.instance.afterLoginGroup.active = false;

        //--- Set pos for popups and hide ---
        for (var i = 0; i < LobbyCtl.instance.popUps.length; i++) {
            LobbyCtl.instance.popUps[i].x = 0;
            LobbyCtl.instance.popUps[i].y = 0;
            LobbyCtl.instance.popUps[i].active = false;
        }

        //--- Set hanler for response from server
        WebsocketClient.OnLoginDone = LobbyCtl.instance.OnLoginDone;
        WebsocketClient.OnGetHeadLineDone = LobbyCtl.instance.OnGetHeadLineDone;
        WebsocketClient.OnGetGameConfigDone = LobbyCtl.instance.OnGetGameConfigDone;
        WebsocketClient.OnMoneyChangeDone = LobbyCtl.instance.OnMoneyChangeDone;
        WebsocketClient.OnLogOutDone = LobbyCtl.instance.OnLogOutDone;
        WebsocketClient.OnLoginOnOtherDevice = LobbyCtl.instance.OnLoginOnOtherDevice;
        WebsocketClient.OnDisconnect = LobbyCtl.instance.OnDisconnect;
        WebsocketClient.OnGetMessageDone = LobbyCtl.instance.OnGetMessageDone;
        WebsocketClient.OnNotifyAll = LobbyCtl.instance.OnNotifyAll;
        WebsocketClient.OnUpdateUserInfoDone = LobbyCtl.instance.OnUpdateUserInfoDone;
        WebsocketClient.OnUpdateUserPhoneDone = LobbyCtl.instance.OnUpdateUserPhoneDone;
        WebsocketClient.OnGetAllJackpotAmount = LobbyCtl.instance.OnGetAllJackpotAmount;

        //--- Set some static method to use from other Controller without require
        RechargeCtl.showDialog = LobbyCtl.instance.showDialog;
        RechargeCtl.show_Popup_Need_Verify = LobbyCtl.instance.show_Popup_Need_Verify;
        ExchangeCtl.showDialog = LobbyCtl.instance.showDialog;
        FreeGoldCtl.showDialog = LobbyCtl.instance.showDialog;
        FreeGoldCtl.show_Popup_Need_Verify = LobbyCtl.instance.show_Popup_Need_Verify;
        TransferGoldCtl.showDialog = LobbyCtl.instance.showDialog;
        TransferGoldCtl.setUserGold = LobbyCtl.instance.setUserGold;
        TransferGoldCtl.show_Popup_Need_Verify = LobbyCtl.instance.show_Popup_Need_Verify;
        UserInfoCtl.showDialog = LobbyCtl.instance.showDialog;
        RegCtl.showDialog = LobbyCtl.instance.showDialog;
        RegCtl.loginAfterReg = LobbyCtl.instance.loginAfterReg;
        UserInfoCtl.changeAvaOnLobby = LobbyCtl.instance.changeAvaOnLobby;
        BannerCtl.changeBanner = LobbyCtl.instance.changeBanner;
        InboxCtl.showDialog = LobbyCtl.instance.showDialog;
        FeedbackCtl.showDialog = LobbyCtl.instance.showDialog;
        UIController.openPopup = LobbyCtl.instance.openPopup;
        WarpConst.GameBase.playerAva = LobbyCtl.instance.avaResource;

        LobbyCtl.instance.bannerEle.active = false;
        LobbyCtl.instance._spawedBanner.push(LobbyCtl.instance.bannerEle2);

        BannerCtl.instance.node.active = false;
        LobbyCtl.instance.openPopup(null, "potAll");

        //Set layer for dialog popup
        LobbyCtl.instance.popUps[3].zIndex = 999;

        if (WarpConst.GameBase.userInfo != null) {
            LobbyCtl.instance.beforeLoginGroup.active = false;
            LobbyCtl.instance.afterLoginGroup.active = true;
            LobbyCtl.instance.OnLoginDone(WarpConst.WarpResponseResultCode.SUCCESS, JSON.stringify(WarpConst.GameBase.userInfo));

            UIController.instance.showTaiXiu(null, "false");

            var PotAll_Data = [];
            var get_Pot_LocalStorage = cc.sys.localStorage.getItem("potall");
            if (get_Pot_LocalStorage) {
                cc.log("GGGG LocalStorage PotAll is available ");
                if (JSON.parse(get_Pot_LocalStorage).length == 21) {
                    cc.log("GGGG LocalStorage PotAll is available : Data ok ");
                    PotAll_Data = JSON.parse(get_Pot_LocalStorage);
                } else {
                    cc.log("GGGG LocalStorage PotAll is available : Data ver old -> use default ");
                    PotAll_Data = default_PotAll;
                }
            } else {
                cc.log("GGGG LocalStorage PotAll is not available");
                PotAll_Data = default_PotAll;
            }

            for (let index = 0; index < this.lobbyChestValueText.length; index++) { // 12
                if (index > 5) {
                    this.lobbyChestValueText[index].string = App.formatMoney(PotAll_Data[index + 3]);
                } else {
                    this.lobbyChestValueText[index].string = App.formatMoney(PotAll_Data[index]);
                }
            }
        } else {
            LobbyCtl.instance.popUps[2].active = true;

            //--- Headline ---
            LobbyCtl.instance.headLineText.string = "Chào mừng bạn đến với cổng game slot Say 2";
            var headLineTextLength = LobbyCtl.instance.headLineText.node.width;
            var headLineTextLeftPos = 0 - 800 - headLineTextLength;
            var headLineTextTime = headLineTextLength * 0.015;
            LobbyCtl.instance.headLineText.node.x = headLineTextLeftPos;
            var headLineSeq = cc.repeatForever(
                cc.sequence(
                    cc.moveTo(0, 800, 0),
                    cc.moveTo(headLineTextTime, headLineTextLeftPos, 0)
                ));
            LobbyCtl.instance.headLineText.node.runAction(headLineSeq);

            var PotAll_Data = [];
            var get_Pot_LocalStorage = cc.sys.localStorage.getItem("potall");
            if (get_Pot_LocalStorage) {
                cc.log("GGGG LocalStorage PotAll is available");
                if (JSON.parse(get_Pot_LocalStorage).length == 21) { // 3x7
                    cc.log("GGGG LocalStorage PotAll is available : Data ok ");
                    PotAll_Data = JSON.parse(get_Pot_LocalStorage);
                } else {
                    cc.log("GGGG LocalStorage PotAll is available : Data ver old -> use default ");
                    PotAll_Data = default_PotAll;
                }
            } else {
                cc.log("GGGG LocalStorage PotAll is not available");
                PotAll_Data = default_PotAll;
            }

            for (let index = 0; index < this.lobbyChestValueText.length; index++) { // 12
                if (index > 5) {
                    this.lobbyChestValueText[index].string = App.formatMoney(PotAll_Data[index + 3]);
                } else {
                    this.lobbyChestValueText[index].string = App.formatMoney(PotAll_Data[index]);
                }

            }
        }
    },
    login: function () {
        cc.log("GGGG Start login");
        if (LobbyCtl.instance.userNameIpf.string.length == 0 || LobbyCtl.instance.passIpf.string.length == 0) {
            LobbyCtl.instance.showDialog(" Vui lòng điền đầy đủ Tài khoản, Mật khẩu ");
            return;
        }
        var data = {
            loginType: 1,
            userName: LobbyCtl.instance.userNameIpf.string,
            pass: LobbyCtl.instance.passIpf.string,
            platform: WarpConst.GameBase.platform,
            model: WarpConst.GameBase.model,
            provider_code: WarpConst.GameBase.providerCode,
            apiKey: WarpConst.GameBase.api_key,
            client_version: WarpConst.GameBase.clientVersion,
        };
        LobbyCtl.instance.popUps[2].active = true;
        WebsocketClient.instance.connectToServer(data);
    },
    loginAfterReg(id, pass) {
        LobbyCtl.instance.userNameIpf.string = id;
        LobbyCtl.instance.passIpf.string = pass;
        LobbyCtl.instance.login();
    },

    // update (dt) {},

    //#region //Request - Response with server
    OnLoginOnOtherDevice(data) {
        LobbyCtl.instance.showDialog("Tài khoản vừa được đăng nhập ở nơi khác. Vui lòng khởi động lại trò chơi", true);
        //hide all minigame when dialog disable ok button
        UIController.instance.closeAllMiniGame();
    },
    OnDisconnect() {
        LobbyCtl.instance.showDialog("Mất kết nối Vui lòng kiểm tra lại.", true);
        WarpConst.GameBase.userInfo = LobbyCtl.instance._userInfo = LobbyCtl.userInfo = null;
        //hide all minigame when dialog disable ok button
        UIController.instance.closeAllMiniGame();
    },
    OnMoneyChangeDone(jsonString) {
        var data = JSON.parse(jsonString);
        switch (data.type) {
            case WarpConst.MoneyChangeType.BY_GOLD_RUSH:
                LobbyCtl.instance.showDialog(" Bạn có thông báo từ game Đào Vàng ");
                break;
            case WarpConst.MoneyChangeType.BY_UPDATE_USER_MOBILE:
                LobbyCtl.instance.showDialog(" Xác thực số điện thoại thành công. \n Tài khoản nhận được " + App.formatMoney(data.change) + WarpConst.GameBase.MoneyName, false, true);
                break;
            case WarpConst.MoneyChangeType.BY_CARD:
                LobbyCtl.instance.showDialog("Nạp thẻ thành công, tài khoản được cộng " + App.formatMoney(data.change) + WarpConst.GameBase.MoneyName);
                break;
            case WarpConst.MoneyChangeType.BY_APPLE_ITEM:
                LobbyCtl.instance.showDialog("Nạp IAP thành công, tài khoản được cộng " + App.formatMoney(data.change) + WarpConst.GameBase.MoneyName);
                break;
            case WarpConst.MoneyChangeType.BY_BANNER_ADS:
                LobbyCtl.instance.showDialog("Xem banner thành công, tài khoản được cộng " + App.formatMoney(data.change) + WarpConst.GameBase.MoneyName);
                break;
            case WarpConst.MoneyChangeType.BY_VIDEO_ADS:
                LobbyCtl.instance.showDialog("Xem video thành công, tài khoản được cộng " + App.formatMoney(data.change) + WarpConst.GameBase.MoneyName);
                break;
            case WarpConst.MoneyChangeType.BY_TRANSFER_GOLD_GOLD:
                LobbyCtl.instance.showDialog("Tài khoản nhận được " + App.formatMoney(data.change) + WarpConst.GameBase.MoneyName + " từ giao dịch chuyển " + WarpConst.GameBase.MoneyName + ".");
                break;
            case WarpConst.MoneyChangeType.BY_REF_CODE:
                LobbyCtl.instance.showDialog("Tài khoản nhận được " + App.formatMoney(data.change) + WarpConst.GameBase.MoneyName + " từ nhập mã giới thiệu.");
                break;
            case WarpConst.MoneyChangeType.BY_SYSTEM_ADD:
                LobbyCtl.instance.showDialog("Tài khoản nhận được " + App.formatMoney(data.change) + WarpConst.GameBase.MoneyName + " từ HỆ THỐNG.");
                break;
        }

        LobbyCtl.instance.setUserGold(data.total);
    },
    OnGetGameConfigDone() {
        cc.log("GGGG OnGetGameConfigDone : ", WarpConst.GameBase.gameConfig.data);
        LobbyCtl.instance.popUps[2].active = false;
        //Fill banner
    },

    OnUpdateUserInfoDone(resultCode, jsonStr) {
        if (resultCode == WarpConst.WarpResponseResultCode.SUCCESS) {
            //Do stuff
            LobbyCtl.instance.showDialog(" Cập nhật thành công ! ");
        } else {
            LobbyCtl.instance.showDialog(" Cập nhật thất bại ! ");
        }
    },

    OnUpdateUserPhoneDone(resultCode, jsonStr) {
        cc.log("GGGG OnUpdateUserPhoneDone : ", resultCode);
        cc.log("GGGG OnUpdateUserPhoneDone :  ", jsonStr);
        if (resultCode == WarpConst.WarpResponseResultCode.SUCCESS) {
            var raw_data = JSON.parse(jsonStr);

            /*
              allowExchange    : Btn Shop 
              allowRechargeSc  : Tab Nạp thẻ cào
              allowRechargeIap : Tab IAP
              allowAgency      : Tab Đại lý
            */

            WarpConst.GameBase.userInfo.desc.allowExchange = raw_data.allowExchange;
            WarpConst.GameBase.userInfo.desc.allowRechargeSc = raw_data.allowRechargeSc;
            WarpConst.GameBase.userInfo.desc.allowRechargeIap = raw_data.allowRechargeIap;
            WarpConst.GameBase.userInfo.desc.allowAgency = raw_data.allowAgency;

            if (WarpConst.GameBase.userInfo.desc.custom_exchange.enable && WarpConst.GameBase.userInfo.desc.allowExchange) {
                LobbyCtl.instance.Btn_Shop.active = true;
            } else {
                LobbyCtl.instance.Btn_Shop.active = false;
            }

            WarpConst.GameBase.userInfo.desc.mobile = LobbyCtl.instance.cache_phonenumber;
            cc.log("GGGG VietNam : ", WarpConst.GameBase.userInfo.desc.mobile);
            UserInfoCtl.instance.infoUserPhoneText.string = " +84" + LobbyCtl.instance.cache_phonenumber;
        } else {
            var raw_data = JSON.parse(jsonStr);
            if (raw_data.type == 3) {
                LobbyCtl.instance.showDialog(" Xác thực thất bại. \n Số điện thoại này đã xác minh cho tài khoản khác ");
            }
        }
    },

    getPotAll() {
        var str = {};
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.GET_ALL_JACKPOT_AMOUNT;
        WebsocketClient.Socket.send(m);
    },
    OnGetAllJackpotAmount(res) {
        cc.log("GGGG OnGetAllJackpotAmount : ", res);
        var raw_data = JSON.parse(res);

        var Pot_Slot = [];

        for (let a = 0; a < 3; a++) {
            Pot_Slot.push(raw_data.slot1[a][1]);
        }

        for (let b = 0; b < 3; b++) {
            Pot_Slot.push(raw_data.slot2[b][1]);
        }

        for (let c = 0; c < 3; c++) {
            Pot_Slot.push(raw_data.mini_slot[c][1]);
        }

        for (let d = 0; d < 3; d++) {
            Pot_Slot.push(raw_data.slot3[d][1]);
        }

        for (let e = 0; e < 3; e++) {
            Pot_Slot.push(raw_data.slot4[e][1]);
        }

        for (let e = 0; e < 3; e++) {
            Pot_Slot.push(raw_data.mini_poker[e][1]);
        }

        for (let f = 0; f < 3; f++) {
            Pot_Slot.push(raw_data.mini_slot_2[f][1]);
        }

        for (let index = 0; index < LobbyCtl.instance.lobbyChestValueText.length; index++) { // 12
            if (index > 5) {
                LobbyCtl.instance.updateChestValue(LobbyCtl.instance.lobbyChestValueText[index], LobbyCtl.instance.last_Pot[index + 3], Pot_Slot[index + 3]);
            } else {
                LobbyCtl.instance.updateChestValue(LobbyCtl.instance.lobbyChestValueText[index], LobbyCtl.instance.last_Pot[index], Pot_Slot[index]);
            }
        }

        PotAllCtl.instance.notifyPotAllDataStateChange(Pot_Slot);

        LobbyCtl.instance.last_Pot = Pot_Slot;
        cc.log("GGGG Lobby copy last_Pot :  ", LobbyCtl.instance.last_Pot);

    },

    updateChestValue(label, fromNum, toNum) {
        var steps = 10;
        if ((toNum - fromNum) > 0) {
            var addNum = Math.floor((toNum - fromNum) / steps);
            var add = cc.sequence(
                cc.delayTime(0.05),
                cc.callFunc(function () {
                    fromNum += addNum;
                    label.string = App.formatMoney(fromNum);
                })
            );
            label.node.runAction(
                cc.sequence(
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    cc.callFunc(function () {
                        label.string = App.formatMoney(toNum);
                    })
                )
            );
        }
    },

    OnLoginDone: function (status, data) {
        /**
         * {"sessionid":108444808,"desc":{"id":3,"username":"longtnh123","order":0,"seatOrder":0,"owner":false,"sessionId":108444808,"isPlayer":true,"remainCardCount":0,"chipChange":0,"displayName":"longtnh123","email":null,"mobile":"","address":null,"avatar":"m3","faceBookId":null,"lobbyId":0,"gender":1,"koin":100,"gold":2330262,"exp":0,"expTarget":0,"level":0,"allLevel":0,"win":0,"loss":0,"draw":0,"providerCode":"nAoT2","online":false,"isReady":false,"extra":{},"isFirstLoginToday":false,"expired":false,"kicked":false,"link_fb":false,"link_acc":true,"isFirstPurchase":false,"isLikeReward":true,"isRateReward":true,"properties":{"user_bet_prev":0,"client_platform":"Android","client_version":"1.0.1","user_bet":0},"playedCards":[],"acquiredCards":[]}}
         */

        cc.log("GGGG OnLoginDone : ", data);
        LobbyCtl.instance.popUps[2].active = false;
        if (status != WarpConst.WarpResponseResultCode.SUCCESS) {
            LobbyCtl.instance.showDialog("Tài khoản hoặc mật khẩu sai.");
            return;
        }
        LobbyCtl.instance.beforeLoginGroup.active = false;

        var json = JSON.parse(data);

        WarpConst.GameBase.userInfo = LobbyCtl.userInfo = LobbyCtl.instance._userInfo = json;
        LobbyCtl.instance.afterLoginGroup.active = true;

        /*
          allowExchange    : Btn Shop
          allowRechargeSc  : Tab Nạp thẻ cào
          allowRechargeIap : Tab IAP
          allowAgency      : Tab Đại lý
        */

        if (WarpConst.GameBase.gameConfig.data.custom_exchange.enable && WarpConst.GameBase.userInfo.desc.allowExchange) {
            LobbyCtl.instance.Btn_Shop.active = true;
        } else {
            LobbyCtl.instance.Btn_Shop.active = false;
        }

        var userName = json.desc.username.length > 13 ? (json.desc.username.substring(0, 13) + "...") : json.desc.username;
        if (json.desc.displayName.length > 0) {
            userName = App.formatUnicode(json.desc.displayName);
            userName = json.desc.displayName.length > 13 ? (userName.substring(0, 13) + "...") : userName;
        }

        LobbyCtl.instance.userNameText.string = userName;
        LobbyCtl.instance.userIdText.string = "ID: " + json.desc.id;
        // LobbyCtl.instance.userMoneyText.string = LobbyCtl.instance.formatMoney(json.desc.gold);
        LobbyCtl.instance.setUserGold(json.desc.gold, true);
        WebsocketClient.instance.sessionid = json.sessionid;
        if (WarpConst.GameBase.userInfo.desc.faceBookId !== null) {
            var avar_URL = "https://graph.facebook.com/" + WarpConst.GameBase.userInfo.desc.faceBookId + "/picture?width=150&height=150";
            cc.log("GGGG Avatar FB : ", avar_URL);

            cc.loader.load({
                url: avar_URL,
                type: 'png'
            }, function (err, texture) {
                LobbyCtl.instance.userAvaImage.spriteFrame = new cc.SpriteFrame(texture);
                WarpConst.GameBase.playerAvaSpriteFrame = new cc.SpriteFrame(texture);
            });

        } else {
            for (var i = 0; i < WarpConst.GameBase.playerAva.length; i++) {
                if (WarpConst.GameBase.playerAva[i]._name.includes(json.desc.avatar)) {
                    LobbyCtl.instance.userAvaImage.spriteFrame = WarpConst.GameBase.playerAva[i];
                    WarpConst.GameBase.playerAvaSpriteFrame = WarpConst.GameBase.playerAva[i];
                    break;
                }
            }
        }

        if (UIController.instance != null) {
            UIController.instance.subMsg();
        }
        LobbyCtl.instance.GetMessage();
        LobbyCtl.instance.show_Popup_News();

        LobbyCtl.instance.update_PotAll_Data = setInterval(function () {
            cc.log("GGGG Lobby setInterval update_PotAll_Data");
            cc.log("GGGG Lobby Updating PotAll Data");
            LobbyCtl.instance.getPotAll();
            LobbyCtl.instance.GetMessage();
        }, 5000);

        //Get headline
        var str = {
            type: 3
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.GET_SYSTEM_MESSAGE;
        WebsocketClient.Socket.send(m);

        LobbyCtl.instance.keepAlive = setInterval(function () {
            if (WarpConst.GameBase.userInfo != null) {
                cc.log("GGGG keepAlive");
                var str = {};
                var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
                m[1] = WarpConst.WarpRequestTypeCode.SEND_KEEP_ALIVE;
                WebsocketClient.Socket.send(m);
            }
        }, 3000);
    },
    OnGetHeadLineDone(data) {
        /** 
         * {"messages":[{"id":341,"content":"Theo dõi Fanpage săn Giftcode free hàng ngày ","contentDisplayType":0,"createdDate":"02/05/2018 15:23:09","title":"Fanpage","type":3,"status":0,"typeMessage":0,"contentData":"Theo dõi Fanpage săn Giftcode free hàng ngày "}]}
         */
        cc.log("[RECV] OnGetHeadLineDone");
        var json = JSON.parse(data);
        cc.log("GGGG OnGetHeadLineDone :  ", data);
        LobbyCtl.instance.headLineText.node.stopAllActions();
        var count = 0;
        LobbyCtl.instance.headLineText.string = decodeURIComponent(escape(json.messages[count].content));
        LobbyCtl.instance.headLineText.node.runAction(
            cc.repeatForever(
                cc.sequence(
                    cc.moveTo(0, 800, 0),
                    cc.moveTo(20, -3500, 0),
                    cc.callFunc(function () {
                        count += 1;
                        if (count < json.messages.length) {
                            LobbyCtl.instance.headLineText.string = decodeURIComponent(escape(json.messages[count].content));
                        } else {
                            count = 0;
                            LobbyCtl.instance.headLineText.string = decodeURIComponent(escape(json.messages[count].content));
                        }
                    })
                )
            )
        );
    },
    Logout() {
        this.Popup_Submit.active = false;
        LobbyCtl.instance.popUps[2].active = true;
        var str = {}
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.SIGNOUT;
        WebsocketClient.Socket.send(m);
        WarpConst.GameBase.disconnectByLogoutBtn = true;

        //hide all minigame when dialog disable ok button
        UIController.instance.closeAllMiniGame();

    },
    OnLogOutDone(resultCode, jsonStr) {
        LobbyCtl.instance.popUps[2].active = false;
        if (resultCode == WarpConst.WarpResponseResultCode.SUCCESS) {
            WebsocketClient.Socket = null;
            WarpConst.GameBase.userInfo = LobbyCtl.instance._userInfo = LobbyCtl.userInfo = null;
            LobbyCtl.instance.afterLoginGroup.active = false;
            LobbyCtl.instance.beforeLoginGroup.active = true;
            clearInterval(LobbyCtl.instance.update_PotAll_Data);
            // use if user spam login
            //  cc.game.restart();
        } else {
            LobbyCtl.instance.showDialog("Có lỗi xảy ra. Vui lòng thử lại sau.");
        }
    },
    GetMessage() {
        var str = {
            status: -1,
            page: 0
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.GET_MESSAGES;
        WebsocketClient.Socket.send(m);
    },
    OnGetMessageDone(resultCode, jsonStr) {
        var data = JSON.parse(jsonStr);
        var count = 0;
        for (var i = 0; i < data.messages.length; i++) {
            if (data.messages[i].status == 0)
                count++;
        }

        if (count === 0) {
            LobbyCtl.instance.inbox_BG.active = false;
            LobbyCtl.instance.inbox_Indicator.active = false;
        } else {
            LobbyCtl.instance.inbox_BG.active = true;
            LobbyCtl.instance.inbox_Indicator.active = true;
            LobbyCtl.instance.inboxCountText.string = "" + count;
        }
    },
    OnNotifyAll(jsonStr) {
        var data = JSON.parse(jsonStr);
        var game = decodeURIComponent(escape(data.gameName));
        switch (game) {
            case "MINI_SLOT":
                game = "DINO EGG";
                break;
            case "SLOT":
                game = "PIRATES";
                break;
            case "SLOT_2":
                game = "NOTHING TO FEAR";
                break;
            case "SLOT_3":
                game = "AQUAMEN";
                break;
            case "SLOT_4":
                game = "ALADDIN'S ADVENTURE";
                break;
            case "MINI_POKER":
                game = "MINI POKER";
                break;
            case "MINI_SLOT_2":
                game = "HALLOWEEN";
                break;
            default:
                break;
        }
        LobbyCtl.instance.notifyAllText.string = "<color=#ffffff> Chúc mừng người chơi</c> <color=#1bdd00> " + decodeURIComponent(escape(data.userName)) + " </c> <color=#ffffff> đã nổ hũ game</c> <color=#ffc800> " + game + "</c> <color=#ffffff> trị giá</c> <color=#ffc800> " + GM.instance.convert_money(data.jackpot) + " </c> <color=#ffffff> Gold </c>";
        if (LobbyCtl.instance.JacpotAnimRunning !== null) {
            clearTimeout(LobbyCtl.instance.JacpotAnimRunning);
        }
        LobbyCtl.instance.notifyAllAnim.active = true;
        LobbyCtl.instance.JacpotAnimRunning = setTimeout(function () {
            LobbyCtl.instance.notifyAllAnim.active = false;
            LobbyCtl.instance.JacpotAnimRunning = null;
        }, 3000);

    },
    //#endregion

    //#region POP UPS
    OpenRegPop(event, isOpen) {
        if (isOpen == "false") {
            this.popUps[0].active = false;
            return;
        }
        this.popUps[0].active = true;
    },
    openPopup(event, popUpName) {
        if (popUpName == "reg") {
            RegCtl.instance.show();
            return;
        } else if (popUpName == "potAll") {
            if (BannerCtl.instance.node.active) {
                BannerCtl.instance.node.active = false;
                PotAllCtl.instance.show();
            } else {
                BannerCtl.instance.node.active = true;
                PotAllCtl.instance.close();
            }
            return;
        }

        if (LobbyCtl.userInfo == null) {
            LobbyCtl.instance.showDialog("Vui lòng đăng nhập để thực hiện tính năng này.");
            return;
        }

        switch (popUpName) {
            case "recharge":
                RechargeCtl.instance.show();
                break;
            case "exchange":
                ExchangeCtl.instance.show();
                break;
            case "freeGold":
                FreeGoldCtl.instance.show();
                break;
            case "transfer":
                TransferGoldCtl.instance.show();
                break;
            case "userInfo":
                UserInfoCtl.instance.show();
                break;
            case "setting":
                SettingCtl.instance.show();
                break;
            case "showMiniGame":
                UIController.instance.showMiniGame();
                break;
            case "developing":
                this.showDialog("Tính năng đang phát triển.");
                break;
            case "Slot1":
                if (!WarpConst.GameBase.gameConfig.data.custom_active_games.slot1) {
                    LobbyCtl.instance.showDialog(" Game Private hiện đang bảo trì. \n Vui lòng quay lại sau ! ");
                    return;
                }
                this.loadSlot("Slot1");
                break;
            case "Slot2":
                if (!WarpConst.GameBase.gameConfig.data.custom_active_games.slot2) {
                    LobbyCtl.instance.showDialog(" Game Nothing To Fear hiện đang bảo trì. \n Vui lòng quay lại sau ! ");
                    return;
                }
                this.loadSlot("Slot2");
                break;
            case "Slot3":
                if (!WarpConst.GameBase.gameConfig.data.custom_active_games.slot3) {
                    LobbyCtl.instance.showDialog(" Game Aquamen hiện đang bảo trì. \n Vui lòng quay lại sau ! ");
                    return;
                }
                this.loadSlot("Slot3");
                break;
            case "Slot4":
                if (!WarpConst.GameBase.gameConfig.data.custom_active_games.slot4) {
                    LobbyCtl.instance.showDialog(" Game Aladdin's Adventure hiện đang bảo trì. \n Vui lòng quay lại sau ! ");
                    return;
                }
                this.loadSlot("Slot4");
                break;
            case "Tai_Xiu":
                UIController.instance.showTaiXiu(null, "true");
                break;
            case "Mini_Poker":
                UIController.instance.showMiniPoker();
                break;
            case "Mini_Slot_1":
                UIController.instance.show_Mini_Slot_1();
                break;
            case "Mini_Slot_2":
                UIController.instance.show_Mini_Slot_2();
                break;
            case "Dao_Vang":
                UIController.instance.showGoldRush();
                break;
            default:
                break;
        };
    },
    //#endregion

    //#region TEST TAI XIU
    subTaiXiu: function () {
        var str = {
            type: 23,
            bet: 0,
            chipType: 1

        };
        cc.log("sub tx ");
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_TAIXIU;
        WebsocketClient.Socket.send(m);
    },
    addBetTaiXiu: function () {
        var str = {
            type: WarpConst.MiniGame.ADD_BET,
            bet: 5000, //bet amount
            pot: 1 //betSide: 1-TAI | 0: XIU
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.TAI_XIU;
        WebsocketClient.Socket.send(m);
    },
    getUserHistoryTaiXiu: function () {
        var str = {
            type: WarpConst.MiniGame.GET_USER_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.TAI_XIU;
        WebsocketClient.Socket.send(m);
    },
    //#endregion
    loadTaiXiu: function () {

        cc.director.loadScene("demo");
    },
    //#region //SLOT
    loadSLotDemo: function () {
        switch (LobbyCtl.mCount) {
            case 0:
                this.subSlotGame();
                break;
            case 1:
                this.joinLobby();
                break;
            case 2:
                this.subLobby();
                break;
            case 3:
                this.getRoom();
                break;
            case 4:
                this.joinRoom();
                break;
            default:
                this.spin();
                break;
        }
        LobbyCtl.mCount += 1;
    },
    subSlotGame: function () {
        //{"desc":"Success"}
        var str = {
            gameId: 4
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.SUBSCRIBE_GAME;
        WebsocketClient.Socket.send(m);
    },
    joinLobby: function () {
        //{"desc":"Success"}
        var str = {
            lobbyId: WarpConst.LobbyId.SLOT
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.JOIN_LOBBY;
        WebsocketClient.Socket.send(m);
    },
    subLobby: function () {
        /*
        {"listBet":{"koin":[1000,2000,5000,10000,20000,50000,100000,200000,500000,1000000,2000000,5000000],"gold":[5,100,200,500,1000,2000,5000,10000,20000,50000,100000,200000]},"listBetAvailable":{"koin":[],"gold":[10000,5000,2000,1000,500,200,100,5]},"desc":"Success"}
        */
        var str = {
            lobbyId: WarpConst.LobbyId.SLOT
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.SUBSCRIBE_LOBBY;
        WebsocketClient.Socket.send(m);
    },
    getRoom: function () {
        //{"rooms":[{"timeCountDown":0,"timeCountDownNew":10,"stateNew":0,"id":1,"name":null,"type":null,"chipType":1,"bet":100,"maxUsers":0,"max_player":10000,"curr_num_of_player":973,"locked":false,"started":true,"quickplay":false,"funds":3024114,"reserve":45578711},{"timeCountDown":0,"timeCountDownNew":10,"stateNew":-1,"id":2,"name":null,"type":null,"chipType":1,"bet":1000,"maxUsers":0,"max_player":10000,"curr_num_of_player":104,"locked":false,"started":true,"quickplay":false,"funds":5155784,"reserve":6619082},{"timeCountDown":0,"timeCountDownNew":10,"stateNew":0,"id":3,"name":null,"type":null,"chipType":1,"bet":10000,"maxUsers":0,"max_player":10000,"curr_num_of_player":79,"locked":false,"started":true,"quickplay":false,"funds":56855202,"reserve":9793833}]}
        var str = {
            lobbyId: WarpConst.LobbyId.SLOT
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.GET_ROOMS;
        WebsocketClient.Socket.send(m);
    },
    joinRoom: function () {
        //{"freeSpin":0,"users":[{"id":3,"username":"longtnh123","order":973,"seatOrder":973,"owner":false,"sessionId":179317170,"isPlayer":false,"remainCardCount":0,"chipChange":0,"displayName":"longtnh123","email":null,"mobile":"","address":null,"avatar":"m3","faceBookId":null,"lobbyId":18,"gender":1,"koin":100,"gold":107880,"exp":0,"expTarget":0,"level":0,"allLevel":0,"win":0,"loss":0,"draw":0,"providerCode":"nAoT2","online":false,"isReady":true,"extra":{},"isFirstLoginToday":true,"expired":false,"kicked":false,"link_fb":false,"link_acc":true,"isFirstPurchase":false,"isLikeReward":true,"isRateReward":true,"properties":{},"playedCards":[],"acquiredCards":[]}],"room":{"timeCountDown":0,"timeCountDownNew":10,"stateNew":0,"id":1,"name":null,"type":null,"chipType":1,"bet":100,"maxUsers":0,"max_player":10000,"curr_num_of_player":974,"locked":false,"started":true,"quickplay":false,"funds":3024114,"reserve":45578711}}

        //type = 1: gold| 0: koin - room bet
        var str = {
            bet: 100,
            type: 1
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.JOIN_ROOM;
        WebsocketClient.Socket.send(m);
    },
    spin: function () {
        //send: {"type":202,"deal":1,"line":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]}
        //recv: {"free_spins":0,"chips":106480,"jackpot":3031150,"result_full":["icon 4","icon 5","icon 4","icon 3","icon 5","icon 6","icon 6","icon 5","icon 7","icon 5","icon 7","icon 3","icon 7","icon 7","icon 4"],"win_icon":[{"line_num":6,"icon":"3 icon 5","value":"3"},{"line_num":14,"icon":"3 icon 5","value":"3"}],"type":202,"pay_lines":[{"line_num":6,"icon":"3 icon 5","value":"3"},{"line_num":14,"icon":"3 icon 5","value":"3"}],"win_chips":600,"total_prize":6,"result_line":[["icon 3","icon 4","icon 4","icon 5","icon 5"],["icon 5","icon 5","icon 6","icon 6","icon 7"],["icon 3","icon 4","icon 7","icon 7","icon 7"],["icon 4","icon 5","icon 6","icon 6","icon 7"],["icon 5","icon 6","icon 6","icon 7","icon 7"],["icon 3","icon 4","icon 5","icon 5","icon 5"],["icon 3","icon 4","icon 5","icon 7","icon 7"],["icon 3","icon 4","icon 4","icon 5","icon 7"],["icon 3","icon 4","icon 5","icon 7","icon 7"],["icon 3","icon 5","icon 5","icon 6","icon 7"],["icon 4","icon 4","icon 6","icon 7","icon 7"],["icon 4","icon 5","icon 6","icon 7","icon 7"],["icon 3","icon 3","icon 5","icon 5","icon 6"],["icon 5","icon 5","icon 5","icon 6","icon 7"],["icon 4","icon 5","icon 6","icon 7","icon 7"],["icon 3","icon 4","icon 5","icon 5","icon 6"],["icon 4","icon 5","icon 5","icon 6","icon 7"],["icon 3","icon 5","icon 6","icon 7","icon 7"],["icon 3","icon 3","icon 5","icon 5","icon 7"],["icon 4","icon 4","icon 5","icon 5","icon 7"]]}
        var str = {
            type: WarpConst.WarpRequestTypeCode.START_MATCH,
            deal: 1, //Quay that
            line: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20] //selected line
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.UPDATE_PEERS;
        WebsocketClient.Socket.send(m);
    },
    getSlotUserHistory: function () {
        var str = {
            type: WarpConst.MiniGame.GET_USER_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.UPDATE_PEERS;
        WebsocketClient.Socket.send(m);
    },
    getSlotGlory: function () {
        var str = {
            type: WarpConst.MiniGame.GET_JACKPOT_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.UPDATE_PEERS;
        WebsocketClient.Socket.send(m);
    },
    //#endregion

    // load Slot_1
    loadSlot(data) {
        switch (BUILD_TYPE) {
            case BUILD_FACEBOOK:
            case BUILD_WEB_MOBILE:
                UIController.instance.show_Splash_Slot(data);
                LobbyCtl.instance.Stop_Game_Anim();
                cc.audioEngine.stopAll();
                clearInterval(LobbyCtl.instance.update_PotAll_Data);
                clearInterval(LobbyCtl.instance.keepAlive);
                LobbyCtl.instance.headLineText.node.stopAllActions();
                setTimeout(function () {
                    cc.director.preloadScene(
                        data,
                        function (completedCount, totalCount, item) {
                            UIController.instance.update_load_Scene(completedCount / totalCount);
                        },
                        function () {
                            cc.director.loadScene(data, function () {
                                UIController.instance.hide_Splash_Slot();
                            });
                        }
                    );
                }, 10);
                break;
            case BUILD_APPLE_STORE:
            case BUILD_APPLE_STORE_AND_GAME_FAKE:
            case BUILD_IPA_INSTALL:
            case BUILD_GOOGLE_PLAY:
            case BUILD_GOOGLE_PLAY_AND_GAME_FAKE:
            case BUILD_APK_INSTALL:
                this.Game_Enter = data;
                LobbyCtl.instance.Btn_Slot_1.getComponent(cc.Button).interactable = false;
                LobbyCtl.instance.Btn_Slot_2.getComponent(cc.Button).interactable = false;
                LobbyCtl.instance.Btn_Slot_3.getComponent(cc.Button).interactable = false;
                LobbyCtl.instance.Btn_Slot_4.getComponent(cc.Button).interactable = false;
                switch (data) {
                    case "Slot1":
                        this.HotUpdate_usingCustomManifest(1);
                        break;
                    case "Slot2":
                        this.HotUpdate_usingCustomManifest(2);
                        break;
                    case "Slot3":
                        this.HotUpdate_usingCustomManifest(3);
                        break;
                    case "Slot4":
                        this.HotUpdate_usingCustomManifest(4);
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    },

    //#region Mini Slot
    subMiniSlot() {
        var str = {
            type: WarpConst.MiniGame.SUBSCRIBE_ROOM,
            bet: 0,
            chipType: 1

        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_SLOT;
        WebsocketClient.Socket.send(m);
    },
    spinMiniSlot() {
        var str = {
            type: WarpConst.MiniGame.START_MATCH,
            bet: 100, //bet value: 100 | 1000 | 10000
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_SLOT;
        WebsocketClient.Socket.send(m);
    },
    getMiniSlotHis() {
        var str = {
            type: WarpConst.MiniGame.GET_USER_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_SLOT;
        WebsocketClient.Socket.send(m);
    },
    getMiniSlotGlory: function () {
        var str = {
            type: WarpConst.MiniGame.GET_JACKPOT_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_SLOT;
        WebsocketClient.Socket.send(m);
    },
    //#endregion

    sendChat() {
        var str = {
            message: "helo baby"

        };
        cc.log("sub tx");
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.GLOBAL_CHAT;
        WebsocketClient.Socket.send(m);
    },

    //#region Mini Gold Rush
    goldRushSub() {
        var str = {
            type: WarpConst.MiniGame.SUBSCRIBE_ROOM,
            bet: 0,
            chipType: 1

        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_GOLDRUSH;
        WebsocketClient.Socket.send(m);
    },
    goldRushHide() {
        var str = {
            type: WarpConst.MiniGame.GOLDRUSH_HIDE,
            userId: LobbyCtl.instance._userInfo.desc.id,
            username: LobbyCtl.instance._userInfo.desc.username,
            bet: 500, //bet: 500|5000|50000
            rs: '[1,2,3,4,5]', //string array created as: "[" + ids[0] + "," + ids[1] + "," + ids[2] + "," + ids[3] + "," + ids[4] + "]"
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_GOLDRUSH;
        WebsocketClient.Socket.send(m);
    },
    goldRushGetHis() {
        var str = {
            type: WarpConst.MiniGame.GET_USER_HISTORY
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_GOLDRUSH;
        WebsocketClient.Socket.send(m);
    },
    goldRushGetHideList() {
        var str = {
            type: WarpConst.MiniGame.GOLDRUSH_FIND_MATCH,
            bet: -1 * LobbyCtl.instance._userInfo.desc.id //negative user id to get hide list
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_GOLDRUSH;
        WebsocketClient.Socket.send(m);
    },
    goldRushDelOneMtach() {
        var str = {
            type: WarpConst.MiniGame.GOLDRUSH_DELETE_MATCH,
            bet: 500, //500|5000|50000
            idToDel: 3299 //id of the item from hide list
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_GOLDRUSH;
        WebsocketClient.Socket.send(m);
    },
    goldRushFindOneMatch() {
        var str = {
            type: WarpConst.MiniGame.GOLDRUSH_FIND_MATCH,
            bet: 500 //bet: 500|5000|50000
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_GOLDRUSH;
        WebsocketClient.Socket.send(m);

    },
    goldRushChooseOne() {
        var str = {
            type: WarpConst.MiniGame.GOLDRUSH_RUSH_ONE,
            chest_id: -1 //from 1 to 10
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.LobbyId.MINI_GOLDRUSH;
        WebsocketClient.Socket.send(m);
    },
    //#endregion

    //#region UTILS
    formatMoney: function (value) {
        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " ";
    },

    dycryptAes256Cbc(dataURI) {
        //var key_256_array = new Uint8Array("9f383e507e39122df4b2e94c06ab761a");
        var key = [57, 102, 51, 56, 51, 101, 53, 48, 55, 101, 51, 57, 49, 50, 50, 100, 102, 52, 98, 50, 101, 57, 52, 99, 48, 54, 97, 98, 55, 54, 49, 97];
        var iv = [0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0];
        //var dataURI = "8Hkkb7rKuXmqFSKhslvIcAG9jGu5Mt3/N9VPU0nsVj1N8YjZzfczr/biac9NxBlv27LGiqxY39goiQpKuRVJRP2wI9uoAbegdHUuIXfIu2JUdwyonm9Ok1JZIWItxw86YDhB37ylEHRe8Z1xerVavsHYelk2dRf6GJWUfoUVwpH8pHmqp/lMqRu5u48Fnnc+zE5VSRADCu5nddqiTYslTne8YVvTOo2Qr4TQo66SnzNY/t0N+bHnOuzEGeotaoyuAKvQIvr5Mex8dd8hFjs/8qb/aW5pdug9Ga8VNJL3CsYniZxVhOuUqb/zENuj65kLbDIhu9tjgunnlg/FtaUjW9MsQNsJCg4pb7tOvPcQfuYh9RqTNKC4Lk6BftBw66YDhuYVJZZEXLc48cz1exwzdCIsb1KO2RhdqHqqZ6cv53wEop6OnXrriSIBbWeC2l4aFQE2SXbJysdR2FdKwQBcVhz5yJgtW8VgNOwLV+IaMbzLeEYjI77GXADaXLoODq2GHUyPtydQezYngEJNGGMcQlnhkHeKpW0McSvwjbqTKUk8Jur0zSAPkurku2PIINbxq/XGwNPH8l8vsnp/h7P7bQLIlqMGG1Xm+O1DBOBA4IZTrxvC1Ld83jKiiLgHfMLiFS7hI2dqvRedhrNJaDhrgyssEjdoNm+zApWWxAeyu2cDWFz2r4CRlrXAjj8SAxk+NAuOHc0ikc77ebCvRDyW5zJanm8vj0CI7lYtBVRlxOIg2X00ommuxqRBzgv/zr/Uq3xKctEjHxCzxeXzhQlXzjwqdyX6EoSP/hW/n6mV/paJoFmnX7mhvPerNKaaeVVfXsspUbun64lM84TTT+f5YmOeQf8rYOy0hm1oB+wVqEYSb6Zk3fPgj10qeShasROvENhe6eYyLSIAB+ANa+dK2FxlHh0WhqYA4aYv7mr426jkPUQDva/mMU6J5WSivUt5oadDUH4MI8uSVOyT5woeyg==";
        //var dataURI = "h7lJBzWSOJaAc5nC/sRcdFNnXLbTtbR7qWBZRB9sda+wN99Zb7Dn5hzmFEUqz6ekK3xLVSc+SBrTgVvpNqpJfT2PvNX1TT5iQ1NDYQrjJR/z3VYH42YVVB1HWBFHZK8RCseZxGYkxiYGGW33SZvjpMGeXsmoTQ7QOPXj6NpoHmM1vO7BufTRHt0MGYXl65P8Z0uTb9rwf/RBEP6veVdmRyCdj7yL24VKabaratAVkVJ9O6wjuJCwvDcdxfKOsRY+ELRLlQD85v7S5ePczQ6SiKm8xZfnqYLYhsy7fI01ndqew9daU4PW2gClLIDsS/Lsn18w2E7O0zjCE2Atsp8B/xG7YnjLZhCHQYFdn2dyyNZ7sTP+h6aHHDN0NLa56I3CmPCy/fKM6bsi4sBPNQe1j+XzuHCpGPWNCyGWD82t4lbMcfh9fs2+sm8S+d8uJEXgQyazHVLpJEGJuI8xP1PiyD/LuypYz/cr2pr640gwY4E+v9F8nDDDV7BUq7njeB2+6De3CNUn1ep1b6l4VYt6trGoJsyZu5LoZk6znJNBigT6ShOrqo8T91f1AgSg95KGa1Jrqsnc0NF8sErU8zhs/YQghkNo8ORZa/rfPwzPRiSzrjovb2FqOxwoEWx5A8C4hginslp7xbJopWAaOZJLOJAPrc4B4AsAxbgEbaNEcbkPpgoyqvDJpD5GQB3WqJgyGk3+BBvRZeAwSEgGrNyutXIjy6vNF3X1qUnQGMrMIZSedfxaopByk+TH0LSBQEvduOqWcLJu3YqkAt2m6EFM6S1OU7ycesLC0Mgi3Hq2Er1TSH8GQjhoo+pCDEc7w5zXGlT5b8AtyWQWEE99FhHY6ukujmlo3U4DTvTLXMnxDlgxdN9m55tbk9hMsYYCok+XxnqL6M5TJ40ywYQq0du2rHY2eLxBVEKSBVnGG8BZgpDKGmlN/srxIq5lOWAC0ATnlpPRZIC942p1TgL2fmxk06j2J7T64ficSfASSIRITKbE+3WqpzCuS30QFOB4asdrHO3KkbMSUxQuQNIY5d1MV76ddJo3nWXL3WZsQfUNVZjD9HllJhqYjK8q4C2PJDlE0NKLDWtwyqabsqyG/oqQHopBsOZMbnrx7f3b2YZP3JdTRK+WpvmYid8HO2BtkbRDFUko84djsHsTYmrbHX8pXEiXJcAJ4RjJ36IdD3d+jCvc2q+UYPg2oOfkj2+6uD3ipj3UvfRBPiwqyQC0WwMzghpNOZoyC1NOIOFTjVSr7TXU/e8n3DkdAc8Qfo0er7A0RCT7WcrAAl6duCKrxbuY/WjsZNV+fVtKs1Fl8/oMJfmOTVlxmmq0gEGG5riSiQG8phpsEV8Awbsoq8De3ABQgaVRGSqdx+wpkdKxpO2POXaWMZ+NCR+Cfy5K6LPHt2u1Pk+rz5j6ZU1FdX4Q6wyvSm4Afz7NSEFtL1gZsV3hAcvNvF4QO+0+mG8RS2F2/zEY24EVlNlrFg5zk0E8yOfFBdKnS2ARBlhYmfit9s6GhtgPN0JefCjOtRLGo/fm23VD5iDdZ9qMhOpmj7mleGVmTxYPH+41HpMY2vtWpHRGU9NzF8R3DSbNcG2n0nAceiE1A6hQXjXbS73dDtznXzNwgu2P3b2mhtYxmzuq7lSyH5aslQibFCF+r/w/yLZGD3RX9Vb6+MB1UBFSxxZXl5livGU9yJa0AyTqKOdfE/Bq2K/7+Qww0ge8k73T3yOZqPHIRaSC6Pm1j+DyvpIoUoLs9uiqduVcrIpAqhivDC2dc5MgKiw4ZQpS6cpOy3X0H+M7S/wvIWHhkAH2IShFZ7SPIMuHAobOb2dt30W5T+Lbq0rcAglksAinyvj9bZaUrah1NXw+q2dkV1WiqVA9CqGn17MWQpSWi6rMg95KjelSfVsUa3uiZziEvEzjiipPJDBL3caT5IRPTnXnLqpMAY5tPx0e1sbO+QXfI8HerBu+qRAVMegjXAaw8FR8Uz40bB65JAJ8S6+fv0YLxxxt5CwI+ScZwJThSdeFZ6dvJFF4NbHo/PhQX/0XkQgBcsl4M4fVUeWq+kh3utJ4hI0TQjM2jI4tvoC6YJMCpW2AFSb6RF4/BzhgEW+LTQrZ86qRiX4/JSTwKh9GpJEuSNIJS3l7T/Llayidx8ltbCp7q/DT7HD4+XXpQ4rpejjXb84/mqrR8uSg/FDw6Npjh48P+aF1fTVQHhknirWyfgkvPV2pNomDiGkqw7CfZn0Pf68h8HxMBehOi+Tr8DMqen9NWQVhMbS0A6FEYB483e+wYS4080vXqKL5Ihq+y3tjubl0DSSVwS/eVZCdSyAgRgJNo+2YrDBaCSpVEkf5xZfdXuaMwhsXoM+Zes3HDX3PY+HbcLI3sxzQWmswxvX6X5fPhcAlGAP06lEzSAuvvuiXU2b0zqJgEwYcvvfixFVcykUd7CgjSHIu1ij6nEIhMj9TKq/ij/3A5v7jdYLiFQSwuRIY2hqUTYkCKOXzvxDgMaoUI9QfoRmHPdpiEumVWKSRDLQKgVeHJiTL2iBy8+JwYaFcmVg=";
        //var dataURI = "QOxBMUIu2jr0jsm0GoBYH6ks9vAk2EAcouiuackd6oqSRzKy4jsFPzQWBSY2IWFpXjeL67wBynV9ff4RBbRk/XOP3XLUuzMD54+Nx0ITj3VGQR+XkI222ts7m7I1SNp7RyW1hE1J36/YHN/zbRxB69713xr9oQFxhbTlO3wqCDCjfCNH7gltuiwGbeEQeUgwdYQo0fJvxtFISauu+V6BCcwt2Nn4uHBRX7IcW5ud366lL0fC6YLYGAgmgM6CBFtWkIxY5cLWM0bUOSnJ+jeCu6qYHvbtdKPncFosq2UfLSW6/zHSfzNNlkJHUzVYweLpYFpVn3pg/qiDF5lMScg0FglOi3UyDVJsEgbTGpeMyLo8fZGiVD7DQR3O3AprWOKDKFdVW4BYclE/0aNORSoXpwSYSkaJ4YxZXPDBkHb+w1B/wJNBstRY0QIeec2/ZMxf1afmDRKHHZMTgsGxa78M70KMb9R/iGxSk+m24J0oZIXr204HFMAQth6rst9Cya2VCAMEBn1CZN0yJF/Gb17elNsOxwH2PJhzjbdATw2ASyWQSggTlbGME2I8U+ckcUOR2sMsDPxAVPa+sBqvcpLqtZL8E5/j50qr46dNgKm8DDvGOEaqV2/BmvBVcVploBMvT7ZGEqA3UT79yKBFVHlI6kRKMBrxLm2kVUI6sRES+nuj2lgxtHXP3K6EsfLbshCe9mQfY9kIVgJSogXWakry1iSWchOb6V2rZEbGYq5qooWzrT0Qeymjgf/7401lSgFk6b/Z8+KPUlkfnyyHYF9N2A4hgF6HtGIjRtah8wfi4wNF7QG+n8bMb2t65qY0suOlF3qsp+FHoZGQJCxMT7Dk7z7Ib9rTi2ydWDjzKIQpehsmstNy/WVSIvniOoho2FudJ8t6KXeXP2RthMIhx0n14S3EE5LzIjCB5CE4MUDC4XSRjjNQ8Ta+gDlu+lXer2II91/3V/O3uegrxVPB7kSo6QOaCLR2VeyQ3dJGGnSvaBEqTvsjCnRq339fm+f+xPTHy3FbJPYTFWJU/aaw0F5ELH/oS4w9BAwWV0EYMPESyHZgCxFVZonq2VGRH/QwAZDycch7XpY3IdGDoI4ok0DtFD0akgDZMVdLNVrWicY9Y5T6rHoW5vbV7h/rlDfkLsXmkMbDHEQ8XBWqpi/zID/UzvSP14BchGr/XhwoCvi9ao6B8Ptn+2C9Fp69o8/MkrqJwQhHC+F/O0mfsPHfqz27t3UH4Su5ixASRhAqM51ZciRKr1pKAgRs+5PXPFDlj7C+LUQfe9D2uhKfF6wuQB1Pndi9QTZ/jonsj31Wppxh4l9CdaRZR7Yx8lyehE43w6tMyTOr+chSJBpH62UcCWiO5kX9H8ORDAj+3ynRk5rrlBWJwObEFrC9zvkHvZFg7k15dGm1ApgBk61NuB5zX9e4Ox21ktoRANk306GwT87Z0XojirpJX8QIaOQkEH9W55yXjXF/p1UU7FpdIHH8qHKQuwWCzvk6eCgQuZ2TNxxNk71zSA5/+fWCzjN+x/1i67otrN7mluFgXYniJ+nfAfMGJMUzeNt76qRJP5jjJjgX/Q7uyJZwIJRzKO48jQKDoGgnlHHHSq9daEI49KKovLiAd9rPB5j/3nBJ45XsOiuCVkXQzMSpbTsq+oJLF+2cFw7ocIavAMPZALBhiKj4YXSCqibi7Zy9vadCtSsi4elUw/HeKsj4Adb8tbWJane0FrNM65WDV26+U4wIfMxr/CyUljIKnWj85sePL37Orsj0HOXleNrnW8yn/J+Gfx1kSOHeHCsnqaa6Dr4g8XAdNWne0gLn+F0Zn4BkTajhsoZfzNPxbGF4G5GMfguRnIrWdyiijVUHkzlTdyW+fNPpWLWe3Q0yfAHFvdgV8YsmsU4jzhcaz50vQ1YHUxt/ifbAyU64ngBdvgVSFkUS8r6nqsPXfvArvVWwbO314qoZC83AUkzInGBhxpI2WVRxVhXwzf8rqBouMYEZwh8jGPmNHDMTt/xRBSydq/FidxafxjWEcWfrUWEvqUfIDgtOU2btGY0CcUl1mRJWYVbmaccp9rVmoShlhewMY0lxrGYPaetfQAn71N7SZt6SGToLbOh3nL7Z3EPN8MzZAkRRXhgUHKYCh1NdmTBjhQvPvOnu5L3QPA7097jkVgn3AYWSB8jT5890o2eVuKPkpu5XQbpu3vkvxfEU104MHNhigMyj5I2aIx6byAjrx+eSpabr1ueSUzue6ywC+xDBkOt+xppb2jO2bfind3AmGjdncaiDou3z4MKEXH0JxoeZ5CzFtvR49Ev7Ek1VL0Akd9GsgUZaovEu6ZesHqd+UiEHy10XOeohl3VCjU9zMXV0MnfMWHUiNnY77iMxlPMMSxjV6YJTqWJG/ekc2JXolxkAFelAU4ShpHv55YCr9dTJDIFJNVaCm5VmNluSMlUGH5uC44NbhRKCBFhlxyw6bjAiUkiQTX0zZARHmB/2WqvjNdBlzK54cqVATqAdMZcyr1LvCIEmVfsqRGO6P9fqghx5NIOeq0JTNJHvyHJlF/dzhUhv4pGnX7v999c/lJDQFLUauFztahBSmAdi4Upofxst0RGwhiA89r7YRbikDQqYqqkxi2bFAA/2B7lRTurJEbPCYdWc3vUMPmC70dE/wt2Zd9aqQq3FClxf18gpCNCvgNAEpSzHBNCL4iWCjJWpGLND1c1/u0ucGjZNfmXPfjPVXA8IaT6OWpJKL7w5V+CGPUBBXjPeTk+r1W8BH/AdYSizdihO/fM3qDq2U81Ce1To9h3RN5X9hs+C7ukajuiQimAlZPDrveg6XhVDgS5oAwR2GZeQ4S04Y1zrclwH0YFPJ3e6cB2kPCUhdWfu9ipc9Bq03496QdlfUSBpDqwxRrLAFTEiN5n46qc7ATcrPT402PMwz28AMnp38VzWyztVVlJ7NHYltieoTT/itpcRczChIgcgMJhZVcnRG9I1SMS+cP347Z21jmgHTm6JDJfgCjjfxufjffkBoO7Hz7YElvG67eRi2ChDeVN8/krFadFTnlVpJxgMNF+pnzc4ss8mkcW4Sdj1gnv+E3Ro+p2Vgo7N2STKfqE7WSwkhRvylOV9KxzjMY1lA+7DMEvpOGVtmAWB0NEu53lPFjNyeYCkx031J/yVC2huWh+dPtJAF4cJ9t7DMufi+KGG6jWYvRXDZk62mYwtUEjVpQz8iKDopOmefnXEBXY3DUUBqYdjE5SxTa14/FGhfxDsdZSWqmHWDzs+X4OLedOHBfM4zBIcR3wgYuFAlFafUuK90AtjULPCFUgzZJuDv8NFOR5Gfpz5YyU9de06rYQKceH1XYKRud1jbHNHywBXEACsMMUD9LRHm4SycoQcexyxBO8hHPdONcy6k99KEdmgud0U3OfrIlxHvtAmGKpNavrkO0jF5z5SqIvsWSkSVHpZApwICBHEGBqC+lBYBzKRgEqxUZPv2C9xfCGYoERiggIIrVVeO0ZMHvXtOU7SvnvApJsePJtcQ5BHHYi6Ab3YpIixxPOIBbZ7wewApse+QSqEt97gq6tUrRP2MBChk7EFpkdpLmy/xOOXd/vQIEVLuzIaFaI9ha+y2TJiXlCv0mmzfwSiu6KlkJ0XvrrgoJx3uFRqBw5VfuEo0KOZ1LYJVEjr0Fg9TPA3TVlvmSgcr7YCh+CyFLnWGpvaFu7Dw3CqmpCX83peFg85SGdw3wapUnNIKn9ZBQFl/mwQVPdFSh7xkzjsenGmH5P3GptQzCtOZMtZTFGttIj6RieILQ/qyju8CjZKCV1G35GwFI+5KwTCMr6pJUEiOkuLIlqplhQ8NxLpp5Zp90nH/6oE6AAR/JL+FCvs9V5eDXcLsB3Cyjo3PhOa80g0P1lROcWkqb1NjPG+MmlzKd4TJIrlh7OTsNG4mQPesMMkRI2D5kNeaWPbGaE66G1buVSf61y5njbwWUf65bSfDmSpowKUUBe7alUhKgSquuX4716zY7q/Jg==";
        var binary_string = window.atob(dataURI);
        var len = binary_string.length;
        cc.log("leng = " + len);
        var bytes = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }

        var aesCbc = new JSaes.ModeOfOperation.cbc(key, iv);
        var decryptedBytes = aesCbc.decrypt(bytes);
        // Convert our bytes back into text
        var decryptedText = JSaes.utils.utf8.fromBytes(decryptedBytes);
        //cc.log(decryptedText.substring(0, decryptedText.lastIndexOf("}") + 1));
        //cc.log(decryptedText);
        return decryptedText.substring(0, decryptedText.lastIndexOf("}") + 1);
    },
    encryptAes256Cbc(jsonData) {
        // var data = {
        //     api_key:"b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1"
        // }
        var text = JSON.stringify(jsonData);


        var key = [57, 102, 51, 56, 51, 101, 53, 48, 55, 101, 51, 57, 49, 50, 50, 100, 102, 52, 98, 50, 101, 57, 52, 99, 48, 54, 97, 98, 55, 54, 49, 97];
        var iv = [0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0];
        var textBytes = JSaes.utils.utf8.toBytes(text);

        var aesCbc = new JSaes.ModeOfOperation.cbc(key, iv);
        var encryptedBytes = aesCbc.encrypt(JSaes.padding.pkcs7.pad(textBytes));
        cc.log(encryptedBytes.toString());
        var t = String.fromCharCode.apply(String, new Uint8Array(encryptedBytes));

        var binary = '';
        var len = encryptedBytes.length;
        for (var i = 0; i < len; i++) {
            binary += String.fromCharCode(encryptedBytes[i]);
        }
        //cc.log(window.btoa( binary ));
        return window.btoa(binary);

    },
    sendHttpRequestDemo() {
        // var req = new XMLHttpRequest();
        // req.onreadystatechange = function() {
        //     if (this.readyState == 4 && this.status == 200) {
        //         // Typical action to be performed when the document is ready:
        //         cc.log(req.responseText);
        // }};
        // req.open("POST", "https://api.huto.club/card-type-config");
        // req.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
        // //req.setRequestHeader("Access-Control-Allow-Origin", "*");

        // var data = {
        //     "data":"W7jyRSqN2ahmwfAPTh65B7aAWsULcBfgh0i1d4rLqFMFiMMxoCnBvM8Z0WKNZLXUrglarJlcLlV5+tfuI6TzjA=="
        // };
        // req.send(data);
        var jsonData = {
            api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1"
        }

        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'https://api.huto.club/card-type-config', true);
        xhr.onload = function () {
            cc.log(LobbyCtl.instance.dycryptAes256Cbc(this.responseText));

        };
        xhr.send(JSON.stringify({
            data: this.encryptAes256Cbc(jsonData)
        }));

    },
    showDialog(content, disableOkBtn, OpenFeature) {
        // LobbyCtl.instance.popUps[3].x = 853;
        // LobbyCtl.instance.popUps[3].y = 480;

        if (!LobbyCtl.instance.dialogBtn) {
            return;
        }

        LobbyCtl.instance.dialogBtn.active = true;

        if (OpenFeature == true) {
            var button = LobbyCtl.instance.dialogBtn.getComponent(cc.Button);
            button.clickEvents[0].customEventData = "openFeature";
        }

        if (disableOkBtn == true) {
            var button = LobbyCtl.instance.dialogBtn.getComponent(cc.Button);
            button.clickEvents[0].customEventData = "true";
        }



        LobbyCtl.instance.dialogContentText.string = content;
        LobbyCtl.instance.popUps[3].active = true;


    },
    closeDialog(event, content) {
        cc.log(content);
        cc.log("GGGG VietNam closeDialog :   ", content);
        if (content == "true") {
            cc.game.end();
        }

        if (content == "openFeature") {
            var button = LobbyCtl.instance.dialogBtn.getComponent(cc.Button);
            button.clickEvents[0].customEventData = "";
            cc.log("GGGG VietNam closeDialog OK ");
            cc.log("GGGG VietNam closeDialog ", LobbyCtl.need_Open_Feature_ID);
            switch (LobbyCtl.need_Open_Feature_ID) {
                case "TransferGold":
                    TransferGoldCtl.instance.show("0");
                    break;
                case "Recharge":
                    RechargeCtl.instance.show("2");
                    break;
                case "FreeGold":
                    FreeGoldCtl.instance.show("0");
                    break;
                default:
                    break;
            }
        }

        LobbyCtl.instance.popUps[3].active = false;
    },
    TweenNum(fakePotValuesIndex, label, toNum, delayTimeNum) {
        //var steps = 20;
        var fromNum = toNum;
        var addNum = 20;
        var add = cc.sequence(
            cc.delayTime(0.05),
            cc.callFunc(function () {
                fromNum += addNum;
                //cc.log(fakePotValuesIndex + "|" + LobbyCtl.instance._fakePotValues[fakePotValuesIndex]);
                label.string = App.formatMoney(fromNum);
            })
        );
        label.node.runAction(
            cc.repeatForever(
                cc.sequence(
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    cc.callFunc(function () {
                        LobbyCtl.instance._fakePotValues[fakePotValuesIndex] = fromNum;
                    }),
                    cc.delayTime(delayTimeNum),
                )
            )
        );
    },
    //#endregion

    //#region OTHER
    addUserGold(value) {
        var currentGold = WarpConst.GameBase.userInfo.desc.gold;
        var newGold = currentGold + value;
        LobbyCtl.instance.setUserGold(newGold);
    },
    setUserGold(toNum, isFromZero) {
        cc.log("GGGG User money changed to " + toNum);
        var label = LobbyCtl.instance.userMoneyText;
        if (isFromZero == false || isFromZero == null || toNum <= WarpConst.GameBase.userInfo.desc.gold) {
            WarpConst.GameBase.userInfo.desc.gold = toNum;
            if (label) {
                label.string = App.formatMoney(toNum);
            }
            return;
        }

        var steps = 20;
        var fromNum = WarpConst.GameBase.userInfo.desc.gold;
        WarpConst.GameBase.userInfo.desc.gold = toNum;
        var addNum = Math.floor(toNum / steps);
        var add = cc.sequence(
            cc.delayTime(0.05),
            cc.callFunc(function () {
                fromNum += addNum;
                label.string = App.formatMoney(fromNum);
            })
        );
        label.node.runAction(
            cc.sequence(
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                cc.callFunc(function () {
                    label.string = App.formatMoney(toNum);
                })
            )
        );
    },
    getUserInfo() {
        //cc.log(this._userInfo);
        return LobbyCtl.instance._userInfo;
    },
    changeBanner() {
        //cc.log("thay banner");
        //LobbyCtl.instance.bannerEle.active =false;
        // if(!LobbyCtl.instance._currrentLoadedBanner){
        //     return;
        // }

        if (WarpConst.GameBase.gameConfig) {
            if (!LobbyCtl.instance._currrentLoadedBanner && LobbyCtl.instance._spawedBanner.length < WarpConst.GameBase.gameConfig.data.custom_event_banner.length + 1) {
                var i = LobbyCtl.instance._spawedBanner.length - 1;
                cc.log("GGGG changeBanner i " + i);
                LobbyCtl.instance._currrentLoadedBanner = true;
                var bannerData = WarpConst.GameBase.gameConfig.data.custom_event_banner;
                var obj = cc.instantiate(LobbyCtl.instance.bannerEle);
                obj.parent = LobbyCtl.instance.bannerEle.parent;
                var img = obj.getComponent(cc.Sprite);
                var ulr = bannerData[i].img_link;
                //https://gamebai.online/Banner/slot2.png
                //cc.loader.load(bannerData[i].img_link, function (err, texture) {

                cc.log("GGGG Banner : ", bannerData[i].img_link);
                cc.loader.load(bannerData[i].img_link, function (err, texture) {
                    // Use texture to create sprite frame
                    try {
                        img.spriteFrame = new cc.SpriteFrame(texture);
                        LobbyCtl.instance._currrentLoadedBanner = false;
                    } catch (error) {
                        cc.log(error);
                    }
                });

                var btn = obj.getComponent(cc.Button);
                btn.clickEvents[0].customEventData = i;
                LobbyCtl.instance._spawedBanner.push(obj);
                // obj.x += 200 * i;
                //obj.active = false;
            }

            if (LobbyCtl.instance._spawedBanner.length > 3) {
                LobbyCtl.instance._spawedBanner[LobbyCtl.instance._currentBannerIndex].active = false;
                LobbyCtl.instance._currentBannerIndex++;


                if (LobbyCtl.instance._currentBannerIndex == LobbyCtl.instance._spawedBanner.length)
                    if (LobbyCtl.instance._spawedBanner.length == WarpConst.GameBase.gameConfig.data.custom_event_banner.length + 1)
                        LobbyCtl.instance._currentBannerIndex = 1;
                    else
                        LobbyCtl.instance._currentBannerIndex = 0;
                //cc.log(LobbyCtl.instance._currentBannerIndex);
                LobbyCtl.instance._spawedBanner[LobbyCtl.instance._currentBannerIndex].active = true;
            }
        }


    },
    onClickBannerButton(event, data) {
        cc.log(data);
        cc.sys.openURL(WarpConst.GameBase.gameConfig.data.custom_event_banner[parseInt(data)].event_link);
    },
    changeAvaOnLobby(data) {
        WarpConst.GameBase.playerAvaSpriteFrame = data;
        LobbyCtl.instance.userAvaImage.spriteFrame = data;
    },

    state_music() {
        if (GM.instance.music_Lobby_State === 1) {
            GM.instance.music_Lobby_State = 0;
            cc.audioEngine.stopAll();
        } else {
            GM.instance.music_Lobby_State = 1;
            cc.audioEngine.playMusic(this.music_background, true);
        }
        SettingCtl.instance.change_State();
        cc.sys.localStorage.setItem("music_Lobby", "" + GM.instance.music_Lobby_State);
    },
    //#endregion

    FreeGold_Verify() {
        FreeGoldCtl.instance.close();
        this.verify_FAK();
    },

    show_Popup_News() {
        var data = WarpConst.GameBase.userInfo.desc;
        if (data.mobile == null || data.mobile.length == 0) {
            cc.log("GGGG Not Verify");
            var time = new Date();
            var day = time.getDate();
            var get_check = cc.sys.localStorage.getItem(data.username);
            if (get_check != null) {
                if (parseInt(get_check) !== day) {
                    this.Popup_News.active = true;
                    cc.sys.localStorage.setItem(data.username, "" + day);
                }
            } else {
                this.Popup_News.active = true;
                cc.sys.localStorage.setItem(data.username, "" + day);
            }
        } else {
            cc.log("GGGG Varified");
            this.Popup_News.active = false;
        }
    },

    close_Popup_News() {
        this.Popup_News.active = false;
    },

    Popup_New_Verify() {
        this.Popup_News.active = false;
        this.verify_FAK();
    },

    show_Popup_Need_Verify(feature_name) {
        LobbyCtl.instance.Popup_Need_Verify.active = true;
        LobbyCtl.need_Open_Feature_ID = feature_name;
    },

    close_Popup_Need_Verify() {
        LobbyCtl.instance.Popup_Need_Verify.active = false;
    },

    accept_Popup_Need_Verify() {
        LobbyCtl.instance.Popup_Need_Verify.active = false;
        LobbyCtl.instance.verify_FAK();
    },

    show_Popup_Submit() {
        this.Popup_Submit.active = true;
    },

    close_Popup_Submit() {
        this.Popup_Submit.active = false;
    },

    //#region Facebook
    SignInFb() {
        switch (BUILD_TYPE) {
            case BUILD_FACEBOOK:
                cc.log("Login FB on Mess ");

                cc.log("u" + FBInstant.player.getID());
                cc.log("p" + FBInstant.player.getID().substring(0, 6));

                var rand_device_uuid = "";
                for (let i = 0; i < 20; i++) {
                    rand_device_uuid += WarpConst.GameBase.alphabet.charAt(Math.floor(Math.random() * WarpConst.GameBase.alphabet.length));
                }

                var jsonData = {
                    api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1",
                    username: "u" + FBInstant.player.getID(),
                    password: "p" + FBInstant.player.getID().substring(0, 6),
                    mobile: "",
                    provider_code: WarpConst.GameBase.providerCode,
                    client_version: WarpConst.GameBase.clientVersion,
                    platform: WarpConst.GameBase.platform,
                    model: WarpConst.GameBase.model,
                    device_uuid: rand_device_uuid,
                    refCode: "",
                    token: WarpConst.GameBase.apiToken,
                }
                LobbyCtl.instance.popUps[2].active = true;

                var xhr = new XMLHttpRequest();
                xhr.open('POST', WarpConst.Links.outSideHttpRequest + 'game/register', true);
                xhr.onload = function () {
                    var data = App.dycryptAes256Cbc(this.responseText);
                    //Đã tồn tại: {"status":5,"message":"User existed","data":null}
                    //Thành công: {"status":0,"message":"Register successfully","data":1}
                    var json = JSON.parse(data);
                    if (json.status == 5) { //User exits
                        var data = {
                            loginType: 1,
                            userName: "u" + FBInstant.player.getID(),
                            pass: "p" + FBInstant.player.getID().substring(0, 6),
                            platform: WarpConst.GameBase.platform,
                            model: WarpConst.GameBase.model,
                            provider_code: WarpConst.GameBase.providerCode,
                            apiKey: WarpConst.GameBase.api_key,
                            client_version: WarpConst.GameBase.clientVersion,
                        };
                        WebsocketClient.instance.connectToServer(data);
                    } else if (json.status == 0) { //Reg success
                        LobbyCtl.instance.loginAfterReg("fb" + FBInstant.player.getID(), "pass" + FBInstant.player.getID());
                    } else { //Unknown
                        LobbyCtl.instance.popUps[2].active = false;
                        LobbyCtl.instance.showDialog("Đã xảy ra lỗi. Vui lòng thử lại.");
                    }

                };
                xhr.send(JSON.stringify({
                    data: App.encryptAes256Cbc(jsonData)
                }));
                break;
            case BUILD_APPLE_STORE:
            case BUILD_APPLE_STORE_AND_GAME_FAKE:
            case BUILD_IPA_INSTALL:
            case BUILD_GOOGLE_PLAY:
            case BUILD_GOOGLE_PLAY_AND_GAME_FAKE:
            case BUILD_APK_INSTALL:
                sdkbox.PluginFacebook.logout();
                sdkbox.PluginFacebook.login();
                break;
            case BUILD_WEB_MOBILE:

                break;
            default:
                break;
        }
    },
    LoginWithFacebookData(data) {
        var loginFbMethod = function () {
            var str = {
                loginType: 2,
                fbId: data.id,
                fbToken: data.token,
                apiKey: WarpConst.GameBase.api_key,
                platform: WarpConst.GameBase.platform,
                model: WarpConst.GameBase.model,
                provider_code: WarpConst.GameBase.providerCode,
                apiKey: WarpConst.GameBase.api_key,
                client_version: WarpConst.GameBase.clientVersion
            };
            var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
            m[1] = WarpConst.WarpRequestTypeCode.AUTH;
            WebsocketClient.Socket.send(m);
        }
        this.popUps[2].active = true;
        WebsocketClient.instance.connectToServer(loginFbMethod, true);
        return;
    },

    verify_FAK() {
        var data = WarpConst.GameBase.userInfo.desc;
        if (data.mobile == null || data.mobile.length == 0) {
            cc.log("GGGG Verifying ");

            switch (BUILD_TYPE) {
                case BUILD_FACEBOOK:

                    break;
                case BUILD_APPLE_STORE:
                case BUILD_APPLE_STORE_AND_GAME_FAKE:
                case BUILD_IPA_INSTALL:
                    window.jsb.reflection.callStaticMethod("AppController", "callNativeUIWithTitle:andContent:", "giang", "lai la giang");
                    break;
                case BUILD_GOOGLE_PLAY:
                case BUILD_GOOGLE_PLAY_AND_GAME_FAKE:
                case BUILD_APK_INSTALL:
                    window.jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "AndroidVerifyByPhone", "(Ljava/lang/String;)V", "login as phone");
                    break;
                case BUILD_WEB_MOBILE:
                    var countryCode = "+84";
                    var phoneNumber = "";
                    AccountKit.login(
                        'PHONE', {
                            countryCode: countryCode,
                            phoneNumber: phoneNumber
                        }, // will use default values if not specified
                        this.callback_FAK
                    );
                    break;
                default:
                    break;
            }
        } else {
            cc.log("GGGG Verified ");
        }
    },

    callback_FAK(response) {
        cc.log("GGGG callback_FAK : ", response);
        if (response.status === "PARTIALLY_AUTHENTICATED") {
            cc.log("GGGG PARTIALLY_AUTHENTICATED");
            var code = response.code;
            var csrf = response.state;
            const endpoint = "https://graph.accountkit.com/v1.3/access_token?grant_type=authorization_code&code=" + code + "&access_token=AA|373320146735298|48f59cc6acd8d266fff0b4b448460edb";
            fetch(endpoint).then(res => res.json()).then(response => {
                cc.log('GGGG resp', response)
                var access_token = response.access_token;
                const graph_URL = "https://graph.accountkit.com/v1.3/me/?access_token=" + access_token;
                fetch(graph_URL).then(res => res.json()).then(response => {
                    cc.log("GGGG response : ", response);
                    cc.log('GGGG Phone Number : ', response.phone.number);

                    if (response.phone.number.length > 0) {
                        if (response.phone.number.substring(0, 3) !== "+84") {
                            LobbyCtl.instance.showDialog(" Xác thực thất bại. \n Vui lòng thử lại sau ! ");
                            return;
                        }
                        var raw_phone = response.phone.number.substring(3);
                        LobbyCtl.instance.cache_phonenumber = raw_phone;
                        var str = {
                            mobile: raw_phone,
                            passport: 0
                        };
                        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
                        m[1] = WarpConst.WarpRequestTypeCode.UPDATE_USER_MOBILE;
                        WebsocketClient.Socket.send(m);
                    }
                })
            })

            // Send code to server to exchange for access token
        } else if (response.status === "NOT_AUTHENTICATED") {
            // handle authentication failure
            cc.log("GGGG NOT_AUTHENTICATED");
        } else if (response.status === "BAD_PARAMS") {
            // handle bad parameters
            cc.log("GGGG BAD_PARAMS");
        }
    },

    //#endregion


    // Hot Update

    open_Event_Touch() {
        LobbyCtl.instance.Slot_1_Download_Frame.active = false;
        LobbyCtl.instance.Slot_2_Download_Frame.active = false;
        LobbyCtl.instance.Slot_3_Download_Frame.active = false;
        LobbyCtl.instance.Slot_4_Download_Frame.active = false;
        LobbyCtl.instance.Btn_Slot_1.getComponent(cc.Button).interactable = true;
        LobbyCtl.instance.Btn_Slot_2.getComponent(cc.Button).interactable = true;
        LobbyCtl.instance.Btn_Slot_3.getComponent(cc.Button).interactable = true;
        LobbyCtl.instance.Btn_Slot_4.getComponent(cc.Button).interactable = true;
    },

    versionCompareHandle(versionA, versionB) {
        cc.log("GGGG JS Custom Version Compare: version A is " + versionA + ', version B is ' + versionB);
        var vA = versionA.split('.');
        var vB = versionB.split('.');
        for (var i = 0; i < vA.length; ++i) {
            var a = parseInt(vA[i]);
            var b = parseInt(vB[i] || 0);
            if (a === b) {
                continue;
            } else {
                return a - b;
            }
        }
        if (vB.length > vA.length) {
            return -1;
        } else {
            return 0;
        }
    },

    HotUpdate_checkCb(event) {
        LobbyCtl.instance._updating = false;

        /* se co 2 lan nhay vao day : code 5 roi code 3 khi thanh cong
            5 thi se nhay vao default roi thoat khoi ham HotUpdate_checkCb luon
            3 thi vao case NEW_VERSION_FOUND
          */

        cc.log('GGGG GGGG Code: ' + event.getEventCode());
        switch (event.getEventCode()) {
            case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                cc.log('GGGG checkCb : No local manifest file found, hot update skipped.');
                LobbyCtl.instance.showDialog(" Tải trò chơi thất bại \n Vui lòng thử lại sau ! ");
                LobbyCtl.instance.open_Event_Touch();
                break;
            case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                cc.log('GGGG checkCb : Fail to download manifest file, hot update skipped.');
                LobbyCtl.instance.showDialog(" Tải trò chơi thất bại \n Vui lòng thử lại sau ! ");
                LobbyCtl.instance.open_Event_Touch();
                break;
            case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                cc.log('GGGG checkCb : Already up to date with the latest remote version.');
                var data = LobbyCtl.instance.Game_Enter;
                UIController.instance.show_Splash_Slot(data);
                cc.audioEngine.stopAll();
                clearInterval(LobbyCtl.instance.update_PotAll_Data);
                clearInterval(LobbyCtl.instance.keepAlive);
                LobbyCtl.instance.headLineText.node.stopAllActions();
                setTimeout(function () {
                    cc.director.preloadScene(
                        data,
                        function (completedCount, totalCount, item) {
                            UIController.instance.update_load_Scene(completedCount / totalCount);
                        },
                        function () {
                            cc.director.loadScene(data, function () {
                                UIController.instance.hide_Splash_Slot();
                            });
                        }
                    );
                }, 10);
                break;
            case jsb.EventAssetsManager.NEW_VERSION_FOUND:
                cc.log('GGGG checkCb : New version found, please try to update.');
                LobbyCtl.instance.Btn_Slot_1.getComponent(cc.Button).interactable = false;
                LobbyCtl.instance.Btn_Slot_2.getComponent(cc.Button).interactable = false;
                LobbyCtl.instance.Btn_Slot_3.getComponent(cc.Button).interactable = false;
                LobbyCtl.instance.Btn_Slot_4.getComponent(cc.Button).interactable = false;

                LobbyCtl.instance.Slot_1_Download_Frame.active = false;
                LobbyCtl.instance.Slot_2_Download_Frame.active = false;
                LobbyCtl.instance.Slot_3_Download_Frame.active = false;
                LobbyCtl.instance.Slot_4_Download_Frame.active = false;

                switch (LobbyCtl.instance.Game_Enter) {
                    case "Slot1":
                        LobbyCtl.instance.Slot_1_Download_Frame.active = true;
                        LobbyCtl.instance.Slot_1_Download_Progress.fillRange = 0;
                        LobbyCtl.instance.Slot_1_Download_Text.string = " 0 % ";
                        break;
                    case "Slot2":
                        LobbyCtl.instance.Slot_2_Download_Frame.active = true;
                        LobbyCtl.instance.Slot_2_Download_Progress.fillRange = 0;
                        LobbyCtl.instance.Slot_2_Download_Text.string = " 0 % ";
                        break;
                    case "Slot3":
                        LobbyCtl.instance.Slot_3_Download_Frame.active = true;
                        LobbyCtl.instance.Slot_3_Download_Progress.fillRange = 0;
                        LobbyCtl.instance.Slot_3_Download_Text.string = " 0 % ";
                        break;
                    case "Slot4":
                        LobbyCtl.instance.Slot_4_Download_Frame.active = true;
                        LobbyCtl.instance.Slot_4_Download_Progress.fillRange = 0;
                        LobbyCtl.instance.Slot_4_Download_Text.string = " 0 % ";
                        break;
                    default:
                        break;
                }
                LobbyCtl.instance.HotUpdage_updateGame();
                break;
            default:
                return;
        }
    },

    HotUpdate_updateCb(event) {
        var needRestart = false;
        var failed = false;
        switch (event.getEventCode()) {
            case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                cc.log('GGGG updateCb :No local manifest file found, hot update skipped.');
                failed = true;
                break;
            case jsb.EventAssetsManager.UPDATE_PROGRESSION:
                cc.log('GGGG updateCb : UPDATE_PROGRESSION');
                cc.log("GGGG label_file_progress : " + event.getPercent());

                switch (LobbyCtl.instance.Game_Enter) {
                    case "Slot1":
                        LobbyCtl.instance.Slot_1_Download_Progress.fillRange = event.getPercent();
                        LobbyCtl.instance.Slot_1_Download_Text.string = " " + (100 * event.getPercent()).toFixed(0) + " % ";
                        break;
                    case "Slot2":
                        LobbyCtl.instance.Slot_2_Download_Progress.fillRange = event.getPercent();
                        LobbyCtl.instance.Slot_2_Download_Text.string = " " + (100 * event.getPercent()).toFixed(0) + " % ";
                        break;
                    case "Slot3":
                        LobbyCtl.instance.Slot_3_Download_Progress.fillRange = event.getPercent();
                        LobbyCtl.instance.Slot_3_Download_Text.string = " " + (100 * event.getPercent()).toFixed(0) + " % ";
                        break;
                    case "Slot4":
                        LobbyCtl.instance.Slot_4_Download_Progress.fillRange = event.getPercent();
                        LobbyCtl.instance.Slot_4_Download_Text.string = " " + (100 * event.getPercent()).toFixed(0) + " % ";
                        break;
                    default:
                        break;
                }
                break;
            case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                cc.log("GGGG updateCb Fail to download manifest file, hot update skipped.");
                failed = true;
                break;
            case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                cc.log("GGGG updateCb Already up to date with the latest remote version.");
                failed = true;
                break;
            case jsb.EventAssetsManager.UPDATE_FINISHED:
                cc.log("GGGG updateCb Update finished.");
                needRestart = true;
                break;
            case jsb.EventAssetsManager.UPDATE_FAILED:
                cc.log("GGGG updateCb Update failed. : " + event.getMessage());
                // LobbyCtl.instance.panel.retryBtn.active = true;
                LobbyCtl.instance._updating = false;
                LobbyCtl.instance._canRetry = true;
                break;
            case jsb.EventAssetsManager.ERROR_UPDATING:
                cc.log("GGGG updateCb Asset update error: " + event.getAssetId() + ', ' + event.getMessage());
                break;
            case jsb.EventAssetsManager.ERROR_DECOMPRESS:
                cc.log("GGGG updateCb ERROR_DECOMPRESS : " + event.getMessage());
                break;
            default:
                break;
        }

        if (failed) {
            LobbyCtl.instance.open_Event_Touch();

            LobbyCtl.instance.showDialog(" Tải trò chơi thất bại \n Vui lòng thử lại sau ! ");

            LobbyCtl.instance._am.setEventCallback(null);
            LobbyCtl.instance._updateListener = null;
            LobbyCtl.instance._updating = false;
        }

        if (needRestart) {
            LobbyCtl.instance.open_Event_Touch();

            LobbyCtl.instance._updating = false;
            LobbyCtl.instance._am.setEventCallback(null);
            LobbyCtl.instance._updateListener = null;
            // Prepend the manifest's search path
            var searchPaths = jsb.fileUtils.getSearchPaths();
            var newPaths = LobbyCtl.instance._am.getLocalManifest().getSearchPaths();
            cc.log("GGGG " + JSON.stringify(newPaths));
            Array.prototype.unshift.apply(searchPaths, newPaths);
            // This value will be retrieved and appended to the default search path during game startup,
            // please refer to samples/js-tests/main.js for detailed usage.
            // !!! Re-add the search paths in main.js is very important, otherwise, new scripts won't take effect.
            cc.sys.localStorage.setItem('HotUpdateSearchPaths', JSON.stringify(searchPaths));
            jsb.fileUtils.setSearchPaths(searchPaths);
            cc.log("GGGG searchPaths: ", searchPaths);
            // cc.audioEngine.stopAll();
            // cc.game.restart();
        }
    },

    HotUpdate_usingCustomManifest(gameID) {
        if (!cc.sys.isNative) {
            return;
        }

        LobbyCtl.instance._am = null; // test
        switch (gameID) {
            case 1:
                LobbyCtl.instance.Slot_1_StoragePath = ((jsb.fileUtils ? jsb.fileUtils.getWritablePath() : '/') + 'Slot_1_StoragePath');
                LobbyCtl.instance._am = new jsb.AssetsManager('', LobbyCtl.instance.Slot_1_StoragePath, LobbyCtl.instance.versionCompareHandle);
                if (LobbyCtl.instance._am.getState() === jsb.AssetsManager.State.UNINITED) {
                    var manifest = new jsb.Manifest(Slot_1_Custom_Manifest, LobbyCtl.instance.Slot_1_StoragePath);
                    LobbyCtl.instance._am.loadLocalManifest(manifest, LobbyCtl.instance.Slot_1_StoragePath);
                    cc.log("GGGG Using Slot_1_Custom_Manifest");
                }
                break;
            case 2:
                LobbyCtl.instance.Slot_2_StoragePath = ((jsb.fileUtils ? jsb.fileUtils.getWritablePath() : '/') + 'Slot_2_StoragePath');
                LobbyCtl.instance._am = new jsb.AssetsManager('', LobbyCtl.instance.Slot_2_StoragePath, LobbyCtl.instance.versionCompareHandle);
                if (LobbyCtl.instance._am.getState() === jsb.AssetsManager.State.UNINITED) {
                    var manifest = new jsb.Manifest(Slot_2_Custom_Manifest, LobbyCtl.instance.Slot_2_StoragePath);
                    LobbyCtl.instance._am.loadLocalManifest(manifest, LobbyCtl.instance.Slot_2_StoragePath);
                    cc.log("GGGG Using Slot_2_Custom_Manifest ");
                }
                break;
            case 3:
                LobbyCtl.instance.Slot_3_StoragePath = ((jsb.fileUtils ? jsb.fileUtils.getWritablePath() : '/') + 'Slot_3_StoragePath');
                LobbyCtl.instance._am = new jsb.AssetsManager('', LobbyCtl.instance.Slot_3_StoragePath, LobbyCtl.instance.versionCompareHandle);
                if (LobbyCtl.instance._am.getState() === jsb.AssetsManager.State.UNINITED) {
                    var manifest = new jsb.Manifest(Slot_3_Custom_Manifest, LobbyCtl.instance.Slot_3_StoragePath);
                    LobbyCtl.instance._am.loadLocalManifest(manifest, LobbyCtl.instance.Slot_3_StoragePath);
                    cc.log("GGGG Using Slot_3_Custom_Manifest");
                }
                break;
            case 4:
                LobbyCtl.instance.Slot_4_StoragePath = ((jsb.fileUtils ? jsb.fileUtils.getWritablePath() : '/') + 'Slot_4_StoragePath');
                LobbyCtl.instance._am = new jsb.AssetsManager('', LobbyCtl.instance.Slot_4_StoragePath, LobbyCtl.instance.versionCompareHandle);
                if (LobbyCtl.instance._am.getState() === jsb.AssetsManager.State.UNINITED) {
                    var manifest = new jsb.Manifest(Slot_4_Custom_Manifest, LobbyCtl.instance.Slot_4_StoragePath);
                    LobbyCtl.instance._am.loadLocalManifest(manifest, LobbyCtl.instance.Slot_4_StoragePath);
                    cc.log("GGGG Using Slot_4_Custom_Manifest ");
                }
                break;
            default:
                break;
        }


        LobbyCtl.instance._am.setVerifyCallback(function (path, asset) {
            var compressed = asset.compressed;
            var expectedMD5 = asset.md5;
            var relativePath = asset.path;
            var size = asset.size;
            if (compressed) {
                return true;
            } else {
                return true;
            }
        });

        cc.log("GGGG Hot update is ready, please check or directly update.");

        if (cc.sys.os === cc.sys.OS_ANDROID) {
            LobbyCtl.instance._am.setMaxConcurrentTask(2);
        }

        LobbyCtl.instance.HotUpdate_checkUpdate();

    },

    HotUpdate_checkUpdate() {
        if (LobbyCtl.instance._updating) {
            cc.log("GGGG Game is Checking or updating ...");
            return;
        }
        LobbyCtl.instance._am.setEventCallback(LobbyCtl.instance.HotUpdate_checkCb.bind(LobbyCtl.instance));
        LobbyCtl.instance._am.checkUpdate();
        LobbyCtl.instance._updating = true;
    },

    HotUpdage_updateGame() {
        if (LobbyCtl.instance._am && !LobbyCtl.instance._updating) {
            LobbyCtl.instance._am.setEventCallback(LobbyCtl.instance.HotUpdate_updateCb.bind(LobbyCtl.instance));
            LobbyCtl.instance._failCount = 0;
            LobbyCtl.instance._am.update();
            LobbyCtl.instance._updating = true;
        }
    },

    Retry_Update_Game() {
        if (!LobbyCtl.instance._updating && LobbyCtl.instance._canRetry) {
            // this.panel.retryBtn.active = false;
            LobbyCtl.instance._canRetry = false;
            LobbyCtl.instance.label_update_info.string = 'Retry failed Assets...';
            LobbyCtl.instance._am.downloadFailedAssets();
        }
    },

    onDestroy() {
        if (LobbyCtl.instance._updateListener) {
            LobbyCtl.instance._am.setEventCallback(null);
            LobbyCtl.instance._updateListener = null;
        }
    },

    Stop_Game_Anim() {
        for (let index = 0; index < this.Spine_Anim.length; index++) {
            if (index !== 7) {
                this.Spine_Anim[index].getComponent("sp.Skeleton").clearTrack(0);
            }
        }
    },
});