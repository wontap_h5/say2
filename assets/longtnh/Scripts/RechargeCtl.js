var WebsocketClient = require("WebsocketClient");
var WarpConst = require("WarpConst");
var App = require("App");

var sdkbox_config_iap_name = [
    "gold_pack_20k",
    "gold_pack_50k",
    "gold_pack_100k",
    "gold_pack_500k"
];

var sdkbox_config_iap_id = [];

switch (BUILD_TYPE) {
    case BUILD_FACEBOOK:

        break;
    case BUILD_APPLE_STORE:
    case BUILD_APPLE_STORE_AND_GAME_FAKE:
    case BUILD_IPA_INSTALL:
        sdkbox_config_iap_id = [
            "com.creatorg.golden.20k",
            "com.creatorg.golden.50k",
            "com.creatorg.golden.100k",
            "com.creatorg.golden.500k",
        ];
        break;
    case BUILD_GOOGLE_PLAY:
    case BUILD_GOOGLE_PLAY_AND_GAME_FAKE:
    case BUILD_APK_INSTALL:
        sdkbox_config_iap_id = [
            "com.creatorg.golden.20k",
            "com.creatorg.golden.50k",
            "com.creatorg.golden.100k",
            "com.creatorg.golden.500k",
        ];
        break;
    case BUILD_WEB_MOBILE:
        break;
    default:
        break;
}

var check_IAP_Init = false;

var RechargeCtl = cc.Class({
    extends: cc.Component,

    properties: {
        loadingIcon: cc.Node,
        tabEle: {
            default: [],
            type: [cc.Node],
        },
        tabText: {
            default: [],
            type: [cc.Node],
        },
        tabTextColor: {
            default: [],
            type: [cc.Color],
        },
        //Recharge by card - RBC
        RbcContentPanel: cc.Node,
        RbcCardNetBtn: {
            default: [],
            type: [cc.Node],
        },
        RbcValueChooserPanel: cc.Node,
        RbcValueChosen: cc.Label,
        /**0: seri|1: pin */
        RbcCardNetIpf: {
            default: [],
            type: [cc.EditBox],
        },
        RbcRateEle: cc.Node,
        RbcRateList: [],
        RbcChoosenValueEle: cc.Node,
        //Recharge His List - RHL
        RhlContentPanel: cc.Node,
        RhlEles: {
            default: [],
            type: [cc.Node],
        },
        //Giftcode - Gf
        GfContentPanel: cc.Node,
        GfIpf: cc.EditBox,

        //Private variable
        _rechargeConfig: null,
        _rechargeRate: null,
        _currentSelectedTelco: null,
        _currentChosenValue: 0,
        UI_IAP: {
            default: null,
            type: cc.Node
        },
        IAP_List_Item: {
            default: null,
            type: cc.Node
        },
        Item_IAP_Prefab: {
            default: null,
            type: cc.Prefab
        },
        List_Tab_Btn: {
            default: [],
            type: [cc.Node]
        }
    },
    statics: {
        instance: null,
        showDialog: null,
        show_Popup_Need_Verify: null,
    },


    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        RechargeCtl.instance = this;
        this.RbcRateEle.active = false;
        this.RbcContentPanel.active = false;
        this.RbcChoosenValueEle.active = false;
        this.RbcValueChooserPanel.active = false;
        this.RhlContentPanel.active = false;
        this.GfContentPanel.active = false;
        this.RhlEles[0].active = false;
        this.RhlEles[1].active = false;
    },

    start() {

    },

    onEnable() {
        WebsocketClient.OnSendGiftCodeDone = this.OnSendGiftCodeDone;
        WebsocketClient.OnGetKoinExchangeDone = this.OnGetKoinExchangeDone;

        RechargeCtl.instance._rechargeConfig = WarpConst.GameBase.config_recharge;
        cc.log("__Giang__ _rechargeConfig :  ", RechargeCtl.instance._rechargeConfig);
    },
    onDisable() {
        WebsocketClient.OnSendGiftCodeDone = null;
        WebsocketClient.OnGetKoinExchangeDone = null;
    },
    show(tab_open) {
        RechargeCtl.instance.node.active = true;

        /*
          allowExchange    : Btn Shop
          allowRechargeSc  : Tab Nạp thẻ cào
          allowRechargeIap : Tab IAP
          allowAgency      : Tab Đại lý
        */

       cc.log("RRR RechargeCtl  show : ", JSON.stringify(WarpConst.GameBase.userInfo.desc));
       cc.log("RRR RechargeCtl  List_Tab_Btn : ", RechargeCtl.instance.List_Tab_Btn[0].active);

        RechargeCtl.instance.List_Tab_Btn[0].active = WarpConst.GameBase.userInfo.desc.allowRechargeSc;
        RechargeCtl.instance.List_Tab_Btn[1].active = WarpConst.GameBase.userInfo.desc.allowRechargeSc;
        RechargeCtl.instance.List_Tab_Btn[3].active = WarpConst.GameBase.userInfo.desc.allowRechargeIap;

        if (tab_open === "2") {
            RechargeCtl.instance.onClickTabBtn(null, tab_open);
        } else {
            if (WarpConst.GameBase.userInfo.desc.allowRechargeSc) {
                RechargeCtl.instance.onClickTabBtn(null, "0");
            } else {
                RechargeCtl.instance.onClickTabBtn(null, "2");
            }
        }

    },
    close() {
        this.node.active = false;
    },

    onClickTabBtn(event, index) {
        for (let i = 0; i < this.tabEle.length; i++) {
            this.tabEle[i].active = false;
            this.tabEle[i + 1].active = true;
            this.tabText[i / 2].color = this.tabTextColor[0];
            i++;
        }
        //cc.log(index);
        this.tabEle[parseInt(index * 2)].active = true;
        this.tabText[parseInt(index)].color = this.tabTextColor[1];

        this.RbcContentPanel.active = false;
        this.RhlContentPanel.active = false;
        this.GfContentPanel.active = false;
        this.UI_IAP.active = false;
        switch (index) {
            case "0": //Recharge by card
                this.RbcContentPanel.active = true;
                RechargeCtl.instance.RbcValueChooserPanel.active = false;
                RechargeCtl.instance.RbcCardNetIpf[0].node.active = true;
                RechargeCtl.instance.RbcCardNetIpf[1].node.active = true;

                for (let i = 0; i < this.RbcCardNetIpf.length; i++) {
                    this.RbcCardNetIpf[i].string = "";
                }

                for (var i = this.RbcRateList.length - 1; i > -1; i--) {
                    this.RbcRateList[i].destroy();
                    this.RbcRateList.pop();
                }

                cc.log("___ Nap the ___ : ", this._rechargeConfig);
                if (this._rechargeConfig == null) {
                    for (var j = 0; j < RechargeCtl.instance.RbcCardNetBtn.length; j++) {
                        RechargeCtl.instance.RbcCardNetBtn[j].parent.active = false;
                    }

                    //Load data
                    //{"name":"GAME_SLOTS","value":[{"name":"Viettel","code":"VTT","min_pin":11,"max_pin":15,"min_serial":11,"max_serial":15,"provider":"RAMOPAY"},{"name":"Mobifone","code":"VMS","min_pin":12,"max_pin":16,"min_serial":12,"max_serial":16,"provider":"RAMOPAY"},{"name":"Vinaphone","code":"VNP","min_pin":12,"max_pin":16,"min_serial":12,"max_serial":16,"provider":"RAMOPAY"}]}
                    this.loadingIcon.active = true;
                    var jsonData = {
                        api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1"
                    }

                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', WarpConst.Links.outSideHttpRequest + 'card-type-config', true);
                    xhr.onload = function () {
                        // cc.log(App.dycryptAes256Cbc(this.responseText));
                        RechargeCtl.instance.loadingIcon.active = false;
                        var data = App.dycryptAes256Cbc(this.responseText);
                        //cc.log(data);
                        var json = JSON.parse(data);
                        //var json = JSON.parse('{"value":[{"name":"Mobifone","code":"VMS","min_pin":12,"max_pin":16,"min_serial":12,"max_serial":16,"provider":"RAMOPAY"},{"name":"Vinaphone","code":"VNP","min_pin":12,"max_pin":16,"min_serial":12,"max_serial":16,"provider":"RAMOPAY"}],"name":"GAME_SLOTS"}');
                        RechargeCtl.instance._rechargeConfig = json;

                        for (var i = 0; i < json.value.length; i++) {
                            for (var j = 0; j < RechargeCtl.instance.RbcCardNetBtn.length; j++) {
                                if (RechargeCtl.instance.RbcCardNetBtn[j].parent.name == json.value[i].code) {
                                    RechargeCtl.instance.RbcCardNetBtn[j].parent.active = true;
                                }
                            }
                        }
                        for (var j = 0; j < RechargeCtl.instance.RbcCardNetBtn.length; j++) {
                            //cc.log(RechargeCtl.instance.RbcCardNetBtn[j].parent);
                            if (RechargeCtl.instance.RbcCardNetBtn[j].parent.active) {
                                RechargeCtl.instance.onClickCardNetBtn(null, j.toString());
                                break;
                            }

                        }
                    };
                    xhr.send(JSON.stringify({
                        data: App.encryptAes256Cbc(jsonData)
                    }));
                } else {
                    var json = RechargeCtl.instance._rechargeConfig;

                    for (var i = 0; i < json.value.length; i++) {
                        for (var j = 0; j < RechargeCtl.instance.RbcCardNetBtn.length; j++) {
                            if (RechargeCtl.instance.RbcCardNetBtn[j].parent.name == json.value[i].code) {
                                RechargeCtl.instance.RbcCardNetBtn[j].parent.active = true;
                            }
                        }
                    }

                    for (var j = 0; j < RechargeCtl.instance.RbcCardNetBtn.length; j++) {
                        //cc.log(RechargeCtl.instance.RbcCardNetBtn[j].parent);
                        if (RechargeCtl.instance.RbcCardNetBtn[j].parent.active) {
                            this.onClickCardNetBtn(null, j.toString());
                            break;
                        }
                    }

                    this._currentChosenValue = null;
                    this.RbcValueChosen.string = " Chọn mệnh giá ";
                    this.RbcValueChooserPanel.active = false;
                }


                cc.log("___ Nap the ___ : ", this._rechargeRate);
                if (this._rechargeRate == null) {
                    for (var i = this.RbcRateList.length - 1; i > -1; i--) {
                        this.RbcRateList[i].destroy();
                        this.RbcRateList.pop();
                    }

                    var str = {};
                    var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
                    m[1] = WarpConst.WarpRequestTypeCode.GET_KOIN_EXCHANGE;
                    WebsocketClient.Socket.send(m);
                } else {
                    var json = RechargeCtl.instance._rechargeRate;
                    cc.log(json.value.length);
                    for (var i = 0; i < json.value.length; i++) {
                        if (json.value[i].type == "card") {
                            var obj = cc.instantiate(RechargeCtl.instance.RbcRateEle);
                            obj.parent = RechargeCtl.instance.RbcRateEle.parent;
                            //var textArr = obj.getComponent(cc.Label);
                            obj.getComponent(cc.Label).string = App.formatMoney(json.value[i].money) + " VNĐ = " + App.formatMoney(json.value[i].gold) + " GOLD";
                            obj.active = true;
                            RechargeCtl.instance.RbcRateList.push(obj);

                            var obj1 = cc.instantiate(RechargeCtl.instance.RbcChoosenValueEle);
                            obj1.parent = RechargeCtl.instance.RbcChoosenValueEle.parent;
                            obj1.getComponentInChildren(cc.Label).string = App.formatMoney(json.value[i].money);
                            var button = obj1.getComponent(cc.Button);
                            button.clickEvents[0].customEventData = json.value[i].money;
                            obj1.active = true;
                            RechargeCtl.instance.RbcRateList.push(obj1);
                        }
                    }

                }
                //cc.log(this.RbcRateList.length);
                break;
            case "1": //Recharge his
                this.RhlContentPanel.active = true;

                for (var i = this.RbcRateList.length - 1; i > -1; i--) {
                    this.RbcRateList[i].destroy();
                    this.RbcRateList.pop();
                }

                this.loadRechargeHis();
                break;
            case "2": //Giftcode
                var data = WarpConst.GameBase.userInfo.desc;
                cc.log("VietNam : ", JSON.stringify(data));
                if (data.mobile == null || data.mobile.length == 0) {
                    cc.log("VietNam Not Verify");
                    RechargeCtl.show_Popup_Need_Verify("Recharge");
                    return;
                }
                this.GfIpf.string = "";
                this.GfContentPanel.active = true;
                break;
            case "3":
                if (BUILD_TYPE == BUILD_APPLE_STORE || BUILD_TYPE == BUILD_APPLE_STORE_AND_GAME_FAKE || BUILD_TYPE == BUILD_GOOGLE_PLAY || BUILD_TYPE == BUILD_GOOGLE_PLAY_AND_GAME_FAKE) {
                    this.UI_IAP.active = true;
                    if (!check_IAP_Init) {
                        check_IAP_Init = true;
                        sdkbox.IAP.init();
                        sdkbox.IAP.setListener({
                            onSuccess: function (product) {
                                cc.log("TTT onSuccess 1 : ", product.id);
                                var item_index = -1;
                                switch (product.id) {
                                    case sdkbox_config_iap_id[0]:
                                        item_index = 1;
                                        break;
                                    case sdkbox_config_iap_id[1]:
                                        item_index = 2;
                                        break;
                                    case sdkbox_config_iap_id[2]:
                                        item_index = 3;
                                        break;
                                    case sdkbox_config_iap_id[3]:
                                        item_index = 4;
                                        break;
                                    default:
                                        break;
                                }

                                var type_upload = "";
                                var osType = "";
                                switch (BUILD_TYPE) {
                                    case BUILD_FACEBOOK:
                                        type_upload = "__________";
                                        osType = "__________";
                                        break;
                                    case BUILD_APPLE_STORE:
                                        type_upload = "apple";
                                        osType = "iOs";
                                        break;
                                    case BUILD_APPLE_STORE_AND_GAME_FAKE:
                                        type_upload = "apple";
                                        osType = "iOs";
                                        break;
                                    case BUILD_IPA_INSTALL:
                                        type_upload = "apple";
                                        osType = "iOs";
                                        break;
                                    case BUILD_GOOGLE_PLAY:
                                        type_upload = "google";
                                        osType = "Android";
                                        break;
                                    case BUILD_GOOGLE_PLAY_AND_GAME_FAKE:
                                        type_upload = "google";
                                        osType = "Android";
                                        break;
                                    case BUILD_APK_INSTALL:
                                        type_upload = "google";
                                        osType = "Android";
                                        break;
                                    case BUILD_WEB_MOBILE:
                                        type_upload = "__________";
                                        osType = "__________";
                                        break;
                                    default:
                                        break;
                                }

                                if (item_index > 0 && type_upload !== "" && osType !== "") {
                                    cc.log("Send IAP");
                                    var str = {
                                        item: "com.game.bai.icasino.gold_" + item_index,
                                        transId: WarpConst.GameBase.providerCode + "_" + osType,
                                        transDate: "06/11/2015",
                                        packageName: "gamebai.yoker.online.phom.tienlen.miennam.mienphi",
                                        productId: "gamebai.yoker.online.phom.tienlen.miennam.mienphi.100k",
                                        token: "11111111",
                                        provider: type_upload,
                                        receipt: "yo",
                                    };

                                    var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
                                    m[1] = WarpConst.WarpRequestTypeCode.IAP_TOPUP;
                                    WebsocketClient.Socket.send(m);
                                }

                            },
                            onFailure: function (product, msg) {
                                cc.log("TTT onFailure : ", JSON.stringify(product) + " - " + msg);
                            },
                            onCanceled: function (product) {
                                //Purchase was canceled by user
                                cc.log("TTT onCanceled : ", JSON.stringify(product));
                            },
                            onRestored: function (product) {
                                //Purchase restored
                                cc.log("TTT onRestored : ", JSON.stringify(product));
                            },
                            onRestoreComplete: function (ok, msg) {
                                //Purchase onRestoreComplete
                                cc.log("TTT onRestoreComplete : ", ok + "-" + msg);
                            },
                            onPurchaseHistory: function (data) {
                                cc.log("TTT onPurchaseHistory : ", data);
                            },
                            onProductRequestSuccess: function (products) {
                                //Returns you the data for all the iap products
                                //You can get each item using following method
                                var iap_list_server = WarpConst.GameBase.gameConfig.data.custom_iap.iap_list;
                                for (let index = 0; index < iap_list_server.length; index++) {
                                    if (iap_list_server[index] > 0) {
                                        // init Item
                                        for (let a = 0; a < products.length; a++) {
                                            if (products[a].id === sdkbox_config_iap_id[index]) {
                                                var item = cc.instantiate(RechargeCtl.instance.Item_IAP_Prefab);
                                                item.getComponent('Item_IAP').setup_Item({
                                                    id: sdkbox_config_iap_name[index],
                                                    price: products[a].price,
                                                    value: iap_list_server[index],
                                                });
                                                RechargeCtl.instance.IAP_List_Item.addChild(item);
                                            }
                                        }
                                    }
                                }
                            },
                            onProductRequestFailure: function (msg) {
                                //When product refresh request fails.
                                cc.log("LLL onProductRequestFailure : ", msg);
                            }
                        });
                    }
                } else {
                    RechargeCtl.showDialog("Tính năng IAP hiện đang bảo trì !");
                }
                break;
        };

    },

    //#region Recharge by card
    //Config Data: {"name":"GAME_SLOTS","value":[{"name":"Viettel","code":"VTT","min_pin":11,"max_pin":15,"min_serial":11,"max_serial":15,"provider":"RAMOPAY"},{"name":"Mobifone","code":"VMS","min_pin":12,"max_pin":16,"min_serial":12,"max_serial":16,"provider":"RAMOPAY"},{"name":"Vinaphone","code":"VNP","min_pin":12,"max_pin":16,"min_serial":12,"max_serial":16,"provider":"RAMOPAY"}]}
    //Config Data Encrypted: 8Hkkb7rKuXmqFSKhslvIcAG9jGu5Mt3/N9VPU0nsVj1N8YjZzfczr/biac9NxBlv27LGiqxY39goiQpKuRVJRP2wI9uoAbegdHUuIXfIu2JUdwyonm9Ok1JZIWItxw86YDhB37ylEHRe8Z1xerVavsHYelk2dRf6GJWUfoUVwpH8pHmqp/lMqRu5u48Fnnc+zE5VSRADCu5nddqiTYslTne8YVvTOo2Qr4TQo66SnzNY/t0N+bHnOuzEGeotaoyuAKvQIvr5Mex8dd8hFjs/8qb/aW5pdug9Ga8VNJL3CsYniZxVhOuUqb/zENuj65kLbDIhu9tjgunnlg/FtaUjW9MsQNsJCg4pb7tOvPcQfuYh9RqTNKC4Lk6BftBw66YDhuYVJZZEXLc48cz1exwzdCIsb1KO2RhdqHqqZ6cv53wEop6OnXrriSIBbWeC2l4aFQE2SXbJysdR2FdKwQBcVhz5yJgtW8VgNOwLV+IaMbzLeEYjI77GXADaXLoODq2GHUyPtydQezYngEJNGGMcQlnhkHeKpW0McSvwjbqTKUk8Jur0zSAPkurku2PIINbxq/XGwNPH8l8vsnp/h7P7bQLIlqMGG1Xm+O1DBOBA4IZTrxvC1Ld83jKiiLgHfMLiFS7hI2dqvRedhrNJaDhrgyssEjdoNm+zApWWxAeyu2cDWFz2r4CRlrXAjj8SAxk+NAuOHc0ikc77ebCvRDyW5zJanm8vj0CI7lYtBVRlxOIg2X00ommuxqRBzgv/zr/Uq3xKctEjHxCzxeXzhQlXzjwqdyX6EoSP/hW/n6mV/paJoFmnX7mhvPerNKaaeVVfXsspUbun64lM84TTT+f5YmOeQf8rYOy0hm1oB+wVqEYSb6Zk3fPgj10qeShasROvENhe6eYyLSIAB+ANa+dK2FxlHh0WhqYA4aYv7mr426jkPUQDva/mMU6J5WSivUt5oadDUH4MI8uSVOyT5woeyg==
    onClickCardNetBtn(event, index) {
        for (let i = 0; i < this.RbcCardNetBtn.length; i++) {
            this.RbcCardNetBtn[i].active = false;
        }
        this.RbcCardNetBtn[parseInt(index)].active = true;
        this._currentSelectedTelco = this.RbcCardNetBtn[parseInt(index)].parent.name;
        //cc.log(this._currentSelectedTelco);
    },
    doRechargeByCard() {
        if (this.RbcCardNetIpf[0].string.length == 0 || this.RbcCardNetIpf[1].string.length == 0) {
            RechargeCtl.showDialog("Vui lòng nhập đầy đủ seri, mã thẻ.");
            return;
        }

        if (RechargeCtl.instance._currentChosenValue == null) {
            RechargeCtl.showDialog(" Vui lòng chọn mệnh giá ");
            return;
        }

        if (this._rechargeConfig != null) {
            for (var i = 0; i < this._rechargeConfig.value.length; i++) {
                if (this._currentSelectedTelco == this._rechargeConfig.value[i].code) {
                    //cc.log(this._rechargeConfig.value[i].min_pin + "|" + this._rechargeConfig.value[i].max_pin + "|" + this._rechargeConfig.value[i].min_serial + "|" + this._rechargeConfig.value[i].max_serial);
                    if (this.RbcCardNetIpf[0].string.length > this._rechargeConfig.value[i].max_serial || this.RbcCardNetIpf[0].string.length < this._rechargeConfig.value[i].min_serial) {
                        RechargeCtl.showDialog("Seri sai định dạng.");
                        return;
                    }

                    if (this.RbcCardNetIpf[1].string.length > this._rechargeConfig.value[i].max_pin || this.RbcCardNetIpf[1].string.length < this._rechargeConfig.value[i].min_pin) {
                        RechargeCtl.showDialog("Mã thẻ sai định dạng.");
                        return;
                    }
                    break;
                }
            }
        }

        var jsonData = {
            api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1",
            user_id: WarpConst.GameBase.userInfo.desc.id, //3 - longtnh123
            serial: this.RbcCardNetIpf[0].string,
            pin: this.RbcCardNetIpf[1].string,
            type: this._currentSelectedTelco,
            money: RechargeCtl.instance._currentChosenValue,
            provider_code: WarpConst.GameBase.providerCode,
            platform: WarpConst.GameBase.platform
        }

        cc.log("__ Nap the __ : ", JSON.stringify(jsonData));

        //Recharge
        this.loadingIcon.active = true;

        var xhr = new XMLHttpRequest();
        xhr.open('POST', WarpConst.Links.outSideHttpRequest + 'card-charge', true);
        xhr.onload = function () {
            // cc.log(App.dycryptAes256Cbc(this.responseText));
            RechargeCtl.instance.loadingIcon.active = false;
            var data = App.dycryptAes256Cbc(this.responseText);
            cc.log(data);
            var json = JSON.parse(data);
            //var json = JSON.parse('{"value":[{"name":"Mobifone","code":"VMS","min_pin":12,"max_pin":16,"min_serial":12,"max_serial":16,"provider":"RAMOPAY"},{"name":"Vinaphone","code":"VNP","min_pin":12,"max_pin":16,"min_serial":12,"max_serial":16,"provider":"RAMOPAY"}],"name":"GAME_SLOTS"}');
            try {
                //{"status":1,"message":"N\u1ea1p th\u1ebb th\u00e0nh c\u00f4ng, b\u1ea1n \u0111\u01b0\u1ee3c c\u1ed9ng th\u00eam 20000 v\u00e0o t\u00e0i kho\u1ea3n"}
                RechargeCtl.showDialog(decodeURIComponent(unescape(json.message)));
                RechargeCtl.instance.RbcCardNetIpf[0].string = RechargeCtl.instance.RbcCardNetIpf[1].string = "";
            } catch (error) {
                RechargeCtl.showDialog("Đã xảy ra lỗi. Vui lòng thử lại sau.");
            }
        };
        xhr.send(JSON.stringify({
            data: App.encryptAes256Cbc(jsonData)
        }));

    },
    OnGetKoinExchangeDone(data) {
        //{"value":[{"type":"sms","money":10000.0,"gold":4000},{"type":"sms","money":20000.0,"gold":8000},{"type":"sms","money":50000.0,"gold":20000},{"type":"card","money":10000.0,"gold":10000},{"type":"card","money":20000.0,"gold":20000},{"type":"card","money":50000.0,"gold":50000},{"type":"card","money":100000.0,"gold":100000},{"type":"card","money":200000.0,"gold":200000},{"type":"card","money":300000.0,"gold":300000},{"type":"card","money":500000.0,"gold":500000},{"type":"iap","money":0.99,"gold":200000},{"type":"iap","money":1.99,"gold":500000}]}
        var json = JSON.parse(data);
        cc.log(json.value.length);
        for (var i = 0; i < json.value.length; i++) {
            if (json.value[i].type == "card") {
                var obj = cc.instantiate(RechargeCtl.instance.RbcRateEle);
                obj.parent = RechargeCtl.instance.RbcRateEle.parent;
                //var textArr = obj.getComponent(cc.Label);
                obj.getComponent(cc.Label).string = App.formatMoney(json.value[i].money) + " VNĐ = " + App.formatMoney(json.value[i].gold) + " GOLD";
                obj.active = true;
                RechargeCtl.instance.RbcRateList.push(obj);

                var obj1 = cc.instantiate(RechargeCtl.instance.RbcChoosenValueEle);
                obj1.parent = RechargeCtl.instance.RbcChoosenValueEle.parent;
                obj1.getComponentInChildren(cc.Label).string = App.formatMoney(json.value[i].money);
                var button = obj1.getComponent(cc.Button);
                button.clickEvents[0].customEventData = json.value[i].money;
                obj1.active = true;
                RechargeCtl.instance.RbcRateList.push(obj1);
            }
        }

        RechargeCtl.instance._rechargeRate = json;
    },
    OnClickValueBtn(event, data) {
        if (data == -1) {
            RechargeCtl.instance.RbcValueChooserPanel.active = true;
            RechargeCtl.instance.RbcCardNetIpf[0].node.active = false;
            RechargeCtl.instance.RbcCardNetIpf[1].node.active = false;
            return;
        }
        RechargeCtl.instance._currentChosenValue = parseInt(data);
        RechargeCtl.instance.RbcValueChosen.string = App.formatMoney(RechargeCtl.instance._currentChosenValue);
        RechargeCtl.instance.RbcValueChooserPanel.active = false;
        RechargeCtl.instance.RbcCardNetIpf[0].node.active = true;
        RechargeCtl.instance.RbcCardNetIpf[1].node.active = true;
    },
    //#endregion

    //#region Recharge His
    loadRechargeHis() {
        //{"status":1,"message":"Success","data":[{"datetime":"2019-01-19 22:21:42","card_provider":"VTT","serial":"10001705446666","pin":"119195138376666","money":"0","eq_koin":"0","process_status":"-1","net":"Viettel"}]}
        this.loadingIcon.active = true;
        var jsonData = {
            api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1",
            user_id: WarpConst.GameBase.userInfo.desc.id, //3,
            page: 1,
            limit: 20
        }

        var xhr = new XMLHttpRequest();
        xhr.open('POST', WarpConst.Links.outSideHttpRequest + "/card-history", true);
        xhr.onload = function () {
            //cc.log(App.dycryptAes256Cbc(this.responseText));
            RechargeCtl.instance.loadingIcon.active = false;
            RechargeCtl.instance.setRechargeHisData(App.dycryptAes256Cbc(this.responseText));
        };
        xhr.send(JSON.stringify({
            data: App.encryptAes256Cbc(jsonData)
        }));
    },
    setRechargeHisData(jsonString) {
        var data = JSON.parse(jsonString).data;
        var len = data.length;
        // cc.log(data.message);
        // return;
        for (var i = 0; i < len; i++) {
            var obj = cc.instantiate(this.RhlEles[i % 2]);
            obj.parent = this.RhlEles[0].parent;

            var textArr = obj.getComponentsInChildren(cc.Label);
            var dateTime = data[i].datetime.split(" ");
            textArr[0].string = dateTime[0] + "\n" + dateTime[1];
            //textArr[0].string = data[i].datetime;
            textArr[1].string = data[i].serial;
            textArr[2].string = data[i].pin;
            textArr[3].string = data[i].net;
            var status = "Thẻ lỗi";
            if (data[i].process_status == "1") {
                status = App.formatMoney(data[i].money);
            } else if (data[i].process_status == "0" || data[i].process_status == "3") {
                status = "Đang chờ xử lý";
            }
            textArr[4].string = status;

            obj.active = true;
            this.RbcRateList.push(obj);
        }
        //cc.log(this.RhlEles[0].getComponentsInChildren(cc.Label).length);
    },
    //#endregion

    //#region Recharge by Giftcode
    sendGiftCode() {
        if (this.GfIpf.string.length == 0) {
            return;
        }

        var str = {
            code: this.GfIpf.string
        }
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.GIFT_CODE;
        WebsocketClient.Socket.send(m);
    },
    OnSendGiftCodeDone(dataJson, resultCode) {
        //{"desc":"Xin lỗi, hệ thống không nhận diện được giftcode!"}
        var data = JSON.parse(dataJson);
        var str = "";
        if (resultCode == WarpConst.WarpResponseResultCode.SUCCESS) {
            str = "Nhập gift code thành công. Tài khoản được cộng " + App.formatMoney(data.value) + " " + WarpConst.GameBase.MoneyName;
        } else if (resultCode == WarpConst.WarpResponseResultCode.GIFT_CODE_NOT_EXITS) {
            str = decodeURIComponent(escape(data.desc));
        } else if (resultCode == WarpConst.WarpResponseResultCode.GIFT_CODE_USED) {
            var clock = new Date(data.used_time);
            str = "Giftcode đã được sử dụng bởi " + data.user_name_used + " lúc " + clock.toLocaleString("en-US");
        } else {
            str = "Đã có lỗi xảy ra. Mã lỗi: " + resultCode;
        }

        if (RechargeCtl.showDialog != null) {
            RechargeCtl.showDialog(str);
        }
    },
    //#endregion

    buy_Item_IAP(index) {
        cc.log("buy_Item_IAP : ", index);
        sdkbox.IAP.purchase(index);
    }
});