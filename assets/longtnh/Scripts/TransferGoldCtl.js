/**
 * By: longtnh
 * To do: Control TRANSFER GOLD POPUP
 * Date: 2019.03.21
 */

var WebsocketClient = require("WebsocketClient");
var WarpConst = require("WarpConst");
var App = require("App");

var TransferGoldCtl = cc.Class({
    extends: cc.Component,

    properties: {
        loadingIcon: cc.Node,
        tabEleActive: {
            default: [],
            type: [cc.Node],
        },
        tabText: {
            default: [],
            type: [cc.Node],
        },
        tabTextColor: {
            default: [],
            type: [cc.Color],
        },

        //TAB - Transfer
        TfsContentPanel: cc.Node,
        TfsIdIpf: cc.EditBox,
        TfsValueIpf: cc.EditBox,

        //TAB - Dealer
        DlrContentPanel: cc.Node,
        DlrEle: cc.Node,
        DlrValueToTransferIpf: cc.EditBox,
        //TAB - Rule
        RuleContentPanel: cc.Node,
        RuleContentText: cc.Label,

        List_Tab_Btn: {
            default: [],
            type: [cc.Node]
        },

        //Private variables
        _SpawnedEles: [],
        _dealerConfig: null,
    },
    statics: {
        instance: null,
        showDialog: null,
        setUserGold: null,
        show_Popup_Need_Verify: null,
    },
    onLoad() {
        TransferGoldCtl.instance = this;
        this.DlrEle.active = false;
    },
    onEnable() {
        WebsocketClient.OnGetDealerConfigDone = this.OnGetDealerConfigDone;
        WebsocketClient.OnTransferGoldDone = this.OnTransferGoldDone;
        WebsocketClient.OnGetUserInfoDone = this.OnGetUserInfoDone;
    },
    onDisable() {
        WebsocketClient.OnGetDealerConfigDone = null;
        WebsocketClient.OnTransferGoldDone = null;
        WebsocketClient.OnGetUserInfoDone = null;
        for (var i = this._SpawnedEles.length - 1; i > -1; i--) {
            this._SpawnedEles[i].destroy();
            this._SpawnedEles.pop();
        }
    },
    show(tab_open) {
        //cc.log(RechargeCtl.instance);
        TransferGoldCtl.instance.node.active = true;
        /*
          allowExchange    : Btn Shop
          allowRechargeSc  : Tab Nạp thẻ cào
          allowRechargeIap : Tab IAP
          allowAgency      : Tab Đại lý
        */

        cc.log("RRR TransferGoldCtl  show : ", JSON.stringify(WarpConst.GameBase.userInfo.desc));
        cc.log("RRR TransferGoldCtl  List_Tab_Btn : ", TransferGoldCtl.instance.List_Tab_Btn[0].active);

        TransferGoldCtl.instance.List_Tab_Btn[0].active = WarpConst.GameBase.userInfo.desc.allowAgency;

        if (tab_open === "0") {
            TransferGoldCtl.instance.onClickTabBtn(null, tab_open);
        } else {
            if (this._dealerConfig == null) {
                for (let i = 0; i < this.tabEleActive.length; i++) {
                    this.tabEleActive[i].active = false;
                    this.tabText[i].color = this.tabTextColor[0];
                }
                this.tabEleActive[2].active = true;
                this.tabText[2].color = this.tabTextColor[1];
                this.GetDealerConfig();
            } else {
                TransferGoldCtl.instance.onClickTabBtn(null, "2");
            }
        }


    },
    close() {
        this.node.active = false;
    },
    onClickTabBtn(event, index) {
        for (let i = 0; i < this.tabEleActive.length; i++) {
            this.tabEleActive[i].active = false;
            this.tabText[i].color = this.tabTextColor[0];
        }
        //cc.log(index);
        this.tabEleActive[parseInt(index)].active = true;
        this.tabText[parseInt(index)].color = this.tabTextColor[1];

        this.TfsContentPanel.active = false;
        this.DlrContentPanel.active = false;
        this.RuleContentPanel.active = false;

        switch (index) {
            case "0": //Transfer
                var data = WarpConst.GameBase.userInfo.desc;
                cc.log("VietNam :  ", JSON.stringify(data));
                if (data.mobile == null || data.mobile.length == 0) {
                    cc.log("VietNam Not Verify");
                    TransferGoldCtl.show_Popup_Need_Verify("TransferGold");
                    return;
                }
                TransferGoldCtl.instance.TfsIdIpf.string = TransferGoldCtl.instance.TfsValueIpf.string = "";
                this.TfsContentPanel.active = true;
                break;
            case "1": //Dealer list
                TransferGoldCtl.instance.DlrValueToTransferIpf.string = "";
                this.FillDealerContentPanel();
                this.DlrContentPanel.active = true;
                break;
            case "2": //Rule
                this.FillRuleContentPanel();
                this.RuleContentPanel.active = true;
                break;
        }
    },
    FillDealerContentPanel() {
        for (var i = this._SpawnedEles.length - 1; i > -1; i--) {
            this._SpawnedEles[i].destroy();
            this._SpawnedEles.pop();
        }
        //TransferGoldCtl.instance._dealerConfig = JSON.parse('{"data":{"agency":[{"add":"HÃ  Ná»i","name":"Thu Huyá»n","tel":"0969","fb":"fb.con/xxx","userId":8505,"numb":"Äáº¡i lÃ½ cáº¥p 1"},{"add":"Háº£i DÆ°Æ¡ng","name":"Háº£i Yáº¿n","tel":"0969","fb":"fb.con/xxx","userId":192,"numb":"Äáº¡i lÃ½ cáº¥p 1"},{"add":"ÄÃ  Náºµng","name":"VÄn cÆ°á»ng","tel":"0969","fb":"fb.con/xxx","userId":194,"numb":"Äáº¡i lÃ½ cáº¥p 1"},{"add":"SÃ i GÃ²n","name":"Ngá»c Linh","tel":"0969","fb":"fb.con/xxx","userId":195,"numb":"Äáº¡i lÃ½ cáº¥p 1"},{"add":"TPHCM","name":"Nguyá»n PhÃº","tel":"0969","fb":"fb.con/xxx","userId":196,"numb":"Äáº¡i lÃ½ cáº¥p 1"},{"add":"HÃ  Ná»i","name":"Mai ThÆ°Æ¡ng","tel":"0969","fb":"fb.con/xxx","userId":197,"numb":"Äáº¡i lÃ½ cáº¥p 1"}],"BalanceAferTransfer":100000000,"rules":[{"rule":"- Sá» tiá»n chuyá»n khoáº£n tá»i thiá»u lÃ  100,000,000 Gold."},{"rule":"- Sá» Gold sau khi chuyá»n khoáº£n pháº£i nhiá»u hÆ¡n 100,000,000 Gold."},{"rule":"- Há» thá»ng sáº½ thu pháº¿ 5% má»i láº§n giao dá»ch."}],"MaxPerTurn":100000000,"locked":false,"MinPerTurn":100000000,"TransferRate":0.95}}');
        var metadata = this._dealerConfig.data.agency;
        for (var i = 0; i < metadata.length; i++) {
            var obj = cc.instantiate(this.DlrEle);
            obj.parent = this.DlrEle.parent;
            var textArr = obj.getComponentsInChildren(cc.Label);
            textArr[0].string = App.formatUnicode(metadata[i].name);
            textArr[1].string = App.formatUnicode(metadata[i].add);
            textArr[2].string = App.formatUnicode(metadata[i].numb);

            var btn = obj.getComponentInChildren(cc.Button);
            btn.clickEvents[0].customEventData = {
                name: App.formatUnicode(metadata[i].name),
                id: metadata[i].userId
            };

            this._SpawnedEles.push(obj);
            obj.active = true;
        }
    },
    OnClickDealerEleBtn(event, data) {
        //cc.log(data);
        if (this.DlrValueToTransferIpf.string.length == 0) {
            TransferGoldCtl.showDialog("Vui lòng nhập số tiền cần chuyển.");
            return;
        }

        var valueToTransfer = parseInt(this.DlrValueToTransferIpf.string);
        if (valueToTransfer < this._dealerConfig.data.MinPerTurn) {
            TransferGoldCtl.showDialog("Số tiền chuyển quá nhỏ. Vui lòng xem chi tiết trong phần QUY ĐỊNH.");
            return;
        }
        if (valueToTransfer > this._dealerConfig.data.MaxPerTurn) {
            TransferGoldCtl.showDialog("Số tiền chuyển quá lớn. Vui lòng xem chi tiết trong phần QUY ĐỊNH.");
            return;
        }
        if (valueToTransfer > WarpConst.GameBase.userInfo.desc.gold) {
            TransferGoldCtl.showDialog("Số tiền chuyển phải nhỏ hơn số dư trong tài khoản.");
            return;
        }
        if (WarpConst.GameBase.userInfo.desc.gold - valueToTransfer < this._dealerConfig.data.BalanceAferTransfer) {
            TransferGoldCtl.showDialog("Số dư không đủ để chuyển tiền. Vui lòng xem chi tiết trong phần QUY ĐỊNH.");
            return;
        }

        var str = {
            userId: data.id,
            amount: parseInt(this.DlrValueToTransferIpf.string),
            message: "Chuyển tiền cho đại lý " + data.name + ", id = " + data.id,
            rate: this._dealerConfig.data.TransferRate
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.TRANSFER_GOLD;
        WebsocketClient.Socket.send(m);
    },
    OnClickTransferToUser() {
        if (this.TfsIdIpf.string.length == 0) {
            TransferGoldCtl.showDialog("Vui lòng nhập id cần chuyển.");
            return;
        }

        if (this.TfsValueIpf.string.length == 0) {
            TransferGoldCtl.showDialog("Vui lòng nhập số tiền cần chuyển.");
            return;
        }

        var valueToTransfer = parseInt(this.TfsValueIpf.string);
        if (valueToTransfer < this._dealerConfig.data.MinPerTurn) {
            TransferGoldCtl.showDialog("Số tiền chuyển quá nhỏ. Vui lòng xem chi tiết trong phần QUY ĐỊNH.");
            return;
        }
        if (valueToTransfer > this._dealerConfig.data.MaxPerTurn) {
            TransferGoldCtl.showDialog("Số tiền chuyển quá lớn. Vui lòng xem chi tiết trong phần QUY ĐỊNH.");
            return;
        }
        if (valueToTransfer > WarpConst.GameBase.userInfo.desc.gold) {
            TransferGoldCtl.showDialog("Số tiền chuyển phải nhỏ hơn số dư trong tài khoản.");
            return;
        }
        if (WarpConst.GameBase.userInfo.desc.gold - valueToTransfer < this._dealerConfig.data.BalanceAferTransfer) {
            TransferGoldCtl.showDialog("Số dư không đủ để chuyển tiền. Vui lòng xem chi tiết trong phần QUY ĐỊNH.");
            return;
        }

        var str = {
            userId: parseInt(this.TfsIdIpf.string)
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.GET_USER_INFO;
        WebsocketClient.Socket.send(m);
    },
    FillRuleContentPanel() {
        var str = "";
        for (var i = 0; i < TransferGoldCtl.instance._dealerConfig.data.rules.length; i++) {
            str += App.formatUnicode(TransferGoldCtl.instance._dealerConfig.data.rules[i].rule) + "\n";
        }
        TransferGoldCtl.instance.RuleContentText.string = str;
    },
    //#region REQUEST - RESPONSE WITH SERVER
    GetDealerConfig() {
        var str = {};
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.DEALER_CONFIG;
        WebsocketClient.Socket.send(m);
    },
    OnGetDealerConfigDone(data) {
        //{"data":{"agency":[{"add":"HÃ  Ná»i","name":"Thu Huyá»n","tel":"0969","fb":"fb.con/xxx","userId":8505,"numb":"Äáº¡i lÃ½ cáº¥p 1"},{"add":"Háº£i DÆ°Æ¡ng","name":"Háº£i Yáº¿n","tel":"0969","fb":"fb.con/xxx","userId":192,"numb":"Äáº¡i lÃ½ cáº¥p 1"},{"add":"ÄÃ  Náºµng","name":"VÄn cÆ°á»ng","tel":"0969","fb":"fb.con/xxx","userId":194,"numb":"Äáº¡i lÃ½ cáº¥p 1"},{"add":"SÃ i GÃ²n","name":"Ngá»c Linh","tel":"0969","fb":"fb.con/xxx","userId":195,"numb":"Äáº¡i lÃ½ cáº¥p 1"},{"add":"TPHCM","name":"Nguyá»n PhÃº","tel":"0969","fb":"fb.con/xxx","userId":196,"numb":"Äáº¡i lÃ½ cáº¥p 1"},{"add":"HÃ  Ná»i","name":"Mai ThÆ°Æ¡ng","tel":"0969","fb":"fb.con/xxx","userId":197,"numb":"Äáº¡i lÃ½ cáº¥p 1"}],"BalanceAferTransfer":100000000,"rules":[{"rule":"- Sá» tiá»n chuyá»n khoáº£n tá»i thiá»u lÃ  100,000,000 Gold."},{"rule":"- Sá» Gold sau khi chuyá»n khoáº£n pháº£i nhiá»u hÆ¡n 100,000,000 Gold."},{"rule":"- Há» thá»ng sáº½ thu pháº¿ 5% má»i láº§n giao dá»ch."}],"MaxPerTurn":100000000,"locked":false,"MinPerTurn":100000000,"TransferRate":0.95}}
        TransferGoldCtl.instance._dealerConfig = JSON.parse(data);
        TransferGoldCtl.instance.onClickTabBtn(null, "2");
    },
    OnTransferGoldDone(data) {
        //{"gold":1974490}
        //cc.log("Gold transfed " + data);
        TransferGoldCtl.instance.DlrValueToTransferIpf.string = TransferGoldCtl.instance.TfsIdIpf.string = TransferGoldCtl.instance.TfsValueIpf.string = "";
        var json = JSON.parse(data);
        TransferGoldCtl.showDialog("Đã chuyển khoản thành công!" + "\n" + "Bạn hiện có <size=35><color=#FFC800FF>" + App.formatMoney(json.gold) + "</color> " + WarpConst.GameBase.MoneyName + "</size>");
        if (TransferGoldCtl.setUserGold)
            TransferGoldCtl.setUserGold(json.gold);
    },
    OnGetUserInfoDone(str, resultCode) {
        //{"allTarget":20,"level":0,"mobile":"","faceBookId":0,"target":20,"archs":[],"passport":0,"allExp":0,"isFriend":false,"exp":0,"user":{"id":3,"avatar":"m3","faceBookId":null,"displayName":"longtnh123","koin":100,"gold":157335056,"chipChange":0,"order":0,"exp":0,"level":0,"allLevel":0,"gender":1},"allLevel":0}
        // cc.log(data);
        if (resultCode != WarpConst.WarpResponseResultCode.SUCCESS) {
            TransferGoldCtl.showDialog("Người chơi không tồn tại. Vui lòng kiểm tra lại");
            return;
        }
        var data = JSON.parse(str);
        var str = {
            userId: data.user.id,
            amount: parseInt(TransferGoldCtl.instance.TfsValueIpf.string),
            //message: "Chuyển tiền cho người chơi " + data.user.displayName + ", id = " + data.user.id,
            message: "Id = " + data.user.id,
            rate: TransferGoldCtl.instance._dealerConfig.data.TransferRate
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.TRANSFER_GOLD;
        WebsocketClient.Socket.send(m);
    },
    //#endregion
});