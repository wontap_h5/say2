/**
 * By: longtnh
 * To do: Control Websocket connect with server
 * Date: 2019.03.02
 */
var WarpConst = require("WarpConst");
var App = require("App");
var WebsocketClient = cc.Class({
	extends: cc.Component,

	properties: {
		loadingIcon: cc.Node,
	},
	statics: {
		instance: null,
		Socket: null,
		//#region //CALLBACK LIST
		//USER
		OnLoginDone: null,
		OnGetHeadLineDone: null,
		OnUpdateRefCodeDone: null,
		OnTransferGoldDone: null,
		OnWorldChat: null,
		OnGetUserInfoDone: null,
		OnChangePassDone: null,
		OnGetMessageDone: null,
		OnDelOneMessageDone: null,
		OnSendFeedBack: null,
		OnMoneyChangeDone: null,
		OnLogOutDone: null,
		OnLoginOnOtherDevice: null,
		OnLoginOnOtherDevice2: null,
		OnDisconnect: null,
		OnDisconnect2: null,
		OnUpdateUserInfoDone: null,
		OnUpdateUserPhoneDone: null,
		//TÀI XỈU
		OnTaiXiuSubDone: null,
		OnTaiXiuMiniRoomChange: null,
		OnTaiXiuGetUserHistoryDone: null,
		OnAddBetDone: null,
		OnTaiXiuHistoryDone: null,
		//SLOT
		OnSubscribeGameDone: null,
		OnJoinLobbyDone: null,
		OnSubscribeLobby: null,
		OnGetRoomsDone: null,
		OnJoinRoomsDone: null,
		OnSlotStartDone: null,
		OnSlotGetUserHistoryDone: null,
		OnSlotGetGloryDone: null,
		OnJackpotChange: null,
		OnNotifyAll: null,

		//MINI GOLDRUSH:
		OnMiniGoldRushSubDone: null,
		OnMiniGoldRushHideDone: null,
		OnMiniGoldRushDelOneMatchDone: null,
		OnMiniGoldRushGetUserHistoryDone: null,
		OnMiniGoldRushFindMatchDone: null,
		OnMiniGoldRushGetHideListDone: null,
		OnMiniGoldRushChooseOneDone: null,

		//MINI SLOT
		OnMiniSlotSubDone: null,
		OnMiniSlotStartDone: null,
		OnMiniSlotGetUserHistoryDone: null,
		OnMiniSlotGetGloryDone: null,

		//MINI POKER
		OnMiniPokerSubDone: null,
		OnMiniPokerStartDone: null,
		OnMiniPokerGetUserHistoryDone: null,
		OnMiniPokerGetGloryDone: null,

		//MINI SLOT 2
		OnMiniSlot_2_SubDone: null,
		OnMiniSlot_2_StartDone: null,
		OnMiniSlot_2_GetUserHistoryDone: null,
		OnMiniSlot_2_GetGloryDone: null,

		//RECHARGE
		OnSendGiftCodeDone: null,
		OnGetKoinExchangeDone: null,

		//OTHER
		OnGetGameConfigDone: null,
		OnGetDealerConfigDone: null,
		OnGetAllJackpotAmount: null,
		//#endregion

		sessionid: 0,
	},

	CheckRequestFail: null,
	TimeOutCheckBadRequest: null,

	// use this for initialization
	onLoad() {
		WebsocketClient.instance = this;
		if (WarpConst.GameBase.userInfo != null) {
			return;
		}
		WebsocketClient.instance.getApiToken();
		WebsocketClient.instance.getConfigRecharge();
		WebsocketClient.instance.getConfigExchange();
	},

	start() {},

	getConfigRecharge() {
		var jsonData = {
			api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1"
		}

		var xhr = new XMLHttpRequest();
		xhr.open('POST', WarpConst.Links.outSideHttpRequest + 'card-type-config', true);
		xhr.onload = function () {
			var data = App.dycryptAes256Cbc(this.responseText);
			//cc.log(data);
			var json = JSON.parse(data);
			//var json = JSON.parse('{"value":[{"name":"Mobifone","code":"VMS","min_pin":12,"max_pin":16,"min_serial":12,"max_serial":16,"provider":"RAMOPAY"},{"name":"Vinaphone","code":"VNP","min_pin":12,"max_pin":16,"min_serial":12,"max_serial":16,"provider":"RAMOPAY"}],"name":"GAME_SLOTS"}');
			WarpConst.GameBase.config_recharge = json;
			cc.log("__Giang__ getConfigRecharge :  ", WarpConst.GameBase.config_recharge);
		};
		xhr.send(JSON.stringify({
			data: App.encryptAes256Cbc(jsonData)
		}));
	},

	getConfigExchange() {
		var jsonData = {
			api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1"
		}

		var xhr = new XMLHttpRequest();
		xhr.open('POST', WarpConst.Links.outSideHttpRequest + 'dap-trung/get-config', true);
		xhr.onload = function () {
			var data = App.dycryptAes256Cbc(this.responseText);
			var json = JSON.parse(data);
			WarpConst.GameBase.config_exchange = json;
			cc.log("__Giang__ getConfigExchange :  ", WarpConst.GameBase.config_exchange);
		};
		xhr.send(JSON.stringify({
			data: App.encryptAes256Cbc(jsonData)
		}));
	},

	//#region Https Request
	getApiToken() {
		// WebsocketClient.instance.loadingIcon.active = true;
		var jsonData = {
			api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1"
		}

		//{status: 0, message: "Success", data: "2BA0CB14CD0224CA4F56AFC33747A6AC0DB056FBE4904166C7"}

		var xhr = new XMLHttpRequest();
		xhr.open('POST', WarpConst.Links.outSideHttpRequest + "game/initialize", true);
		xhr.onreadystatechange = function () {
			if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
				cc.log("___ SUCCESS ___ ");
				// WebsocketClient.instance.loadingIcon.active = false;
				var response = xhr.responseText;
				cc.log(response);
				cc.log(App.dycryptAes256Cbc(response));
				WarpConst.GameBase.apiToken = JSON.parse(App.dycryptAes256Cbc(response)).data;
				cc.log(WarpConst.GameBase.apiToken);
				WebsocketClient.instance.GetVersionConfigure();
			} else {
				cc.log("___ FAIL ___");
			}
		};

		xhr.send(JSON.stringify({
			data: App.encryptAes256Cbc(jsonData)
		}));
	},
	GetVersionConfigure() {
		// this.loadingIcon.active = true;


		// var jsonData = {
		// 	api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1",
		// 	os_type: 2, // 1: iOS, 2: Android  3: WindowsPhone 4: PC
		// 	version: "2.0.0",
		// 	provider_code: WarpConst.GameBase.providerCode
		// }

		var jsonData = {};

		switch (BUILD_TYPE) {
			case BUILD_FACEBOOK:
			case BUILD_WEB_MOBILE:
				jsonData = {
					api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1",
					os_type: 4, // 1: iOS, 2: Android  3: WindowsPhone 4: PC
					version: "2.0.0",
					provider_code: WarpConst.GameBase.providerCode
				}
				break;
			case BUILD_APPLE_STORE:
			case BUILD_APPLE_STORE_AND_GAME_FAKE:
			case BUILD_IPA_INSTALL:
				jsonData = {
					api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1",
					os_type: 1, // 1: iOS, 2: Android  3: WindowsPhone 4: PC
					version: "2.0.0",
					provider_code: WarpConst.GameBase.providerCode
				}
				break;
			case BUILD_GOOGLE_PLAY:
			case BUILD_GOOGLE_PLAY_AND_GAME_FAKE:
			case BUILD_APK_INSTALL:
				jsonData = {
					api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1",
					os_type: 2, // 1: iOS, 2: Android  3: WindowsPhone 4: PC
					version: "2.0.0",
					provider_code: WarpConst.GameBase.providerCode
				}
				break;
			default:
				break;
		}

		//{"status":0,"message":"Success","data":{"custom_ads_id":{"interstitial_id":"ca-app-pub-2028962319778010/5230228888","video_count_per_day":10,"interstitial_count_per_day":10,"app_id":"ca-app-pub-2028962319778010~8971707936","video_id":"ca-app-pub-2028962319778010/3962923831"},"custom_active_games":{"slot2":true,"slot1":true,"dao_vang":true,"tai_xiu":true,"mini_slot":true},"under_review_versions":["1.0"],"fanpage":"https://www.facebook.com/H%C5%A9-To-Ch%C6%A1i-Game-N%E1%BB%95-H%C5%A9-%C4%90%E1%BB%89nh-Cao-1905624156416681","custom_event_banner":[{"img_link":"https://gamebai.online/Banner/slot2.png","event_link":"https://taigamebai.net/say2"},{"img_link":"https://gamebai.online/Banner/slot3.png","event_link":"https://www.facebook.com/1905624156416681/photos/a.2108192389493189/2131657853813309/?type=3&theater"},{"img_link":"https://gamebai.online/Banner/slot1.png","event_link":"https://www.facebook.com/1905624156416681/videos/600510537085388"},{"img_link":"https://gamebai.online/Banner/slot4.png","event_link":"https://www.facebook.com/1905624156416681"}],"custom_install_app":{"metadata":[{"img":"https://sv1.uphinhnhanh.com/images/2019/01/18/Logo-game-slot.png","des":"Tặng vốn khởi nghiệp khi đăng ký, giao diện đẹp tỷ lệ chuẩn, Tài Xỉu cầu cực chuẩn","link":"https://www.facebook.com/Hũ-To-Chơi-Game-Nổ-Hũ-Đỉnh-Cao-1905624156416681","title":"SAY2 - Quay hũ đỉnh"}],"enable":true},"link":"LINK NAOT2 http://google.com.vn","custom_need_update_to_play":{"link":"https://play.google.com/store/apps/details?id=com.frogmind.badlandbrawl","message":"Đã có phiên bản mới","apply_to_vers":["0.0"]},"custom_recharge":{"enable":true,"list_temp":["GIFTCODE","SMS","CARD","DEALER","IAP","HIS"],"list_to_show":["CARD","HIS","GIFTCODE"]},"custom_exchange":{"enable":true,"message":"Hien tai dang bao tri"}}}

		var xhr = new XMLHttpRequest();
		xhr.open('POST', WarpConst.Links.outSideHttpRequest + "api/version_cfg", true);
		xhr.onreadystatechange = function () {
			if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
				cc.log("___ SUCCESS ___ ");
				cc.log(App.dycryptAes256Cbc(this.responseText));
				WebsocketClient.instance.CheckRequestFail = false;
				clearTimeout(WebsocketClient.instance.TimeOutCheckBadRequest);
				WarpConst.GameBase.gameConfig = JSON.parse(App.dycryptAes256Cbc(this.responseText));
				if (WebsocketClient.OnGetGameConfigDone) {
					WebsocketClient.OnGetGameConfigDone();
				}
			} else {
				WebsocketClient.instance.CheckRequestFail = true;
				WebsocketClient.instance.TimeOutCheckBadRequest = setTimeout(function () {
					if (WebsocketClient.instance.CheckRequestFail) {
						cc.log("___ FAIL ___");
						var default_config = JSON.stringify({
							"status": 0,
							"message": "Success",
							"data": {
								"custom_ads_id": {
									"interstitial_id": "ca-app-pub-2028962319778010/5230228888",
									"video_count_per_day": 10,
									"interstitial_count_per_day": 10,
									"app_id": "ca-app-pub-2028962319778010~8971707936",
									"video_id": "ca-app-pub-2028962319778010/3962923831"
								},
								"custom_active_games": {
									"slot4": true,
									"slot3": true,
									"slot2": true,
									"slot1": true,
									"dao_vang": true,
									"tai_xiu": true,
									"mini_slot": true,
									"mini_poker": true,
									"mini_slot_2": true
								},
								"under_review_versions": ["1.0"],
								"fanpage": "https://www.facebook.com/H%C5%A9-To-Ch%C6%A1i-Game-N%E1%BB%95-H%C5%A9-%C4%90%E1%BB%89nh-Cao-1905624156416681",
								"custom_event_banner": [{
									"img_link": "http://creatorg.huto.top/banner/slot2.png",
									"event_link": "https://taigamebai.net/say2"
								}, {
									"img_link": "http://creatorg.huto.top/banner/slot3.png",
									"event_link": "https://www.facebook.com/1905624156416681"
								}, {
									"img_link": "http://creatorg.huto.top/banner/slot1.png",
									"event_link": "https://www.facebook.com/1905624156416681"
								}, {
									"img_link": "http://creatorg.huto.top/banner/slot4.png",
									"event_link": "https://www.facebook.com/1905624156416681"
								}],
								"custom_install_app": {
									"metadata": [{
										"img": "http://creatorg.huto.top/banner/ca.png",
										"des": "Bắn cá đổi thưởng, săn quà cực đã. Đăng ký nhận code khởi nghiệp.",
										"link": "http://bit.ly/2Mir7Ie",
										"title": "Cabasa - Trùm bắn cá"
									}, {
										"img": "http://creatorg.huto.top/banner/bai.png",
										"des": "Đầy đủ các thể loại game bài lá dân gian việt, nạp đổi cực nhanh.",
										"link": "http://bit.ly/30Knw8U",
										"title": "Anto - Game bài "
									}],
									"enable": true
								},
								"link": "LINK NAOT2 http://google.com.vn",
								"custom_need_update_to_play": {
									"link": "https://play.google.com/store/apps/details?id=com.frogmind.badlandbrawl",
									"message": "Đã có phiên bản mới",
									"apply_to_vers": ["0.0"]
								},
								"custom_recharge": {
									"enable": false,
									"list_temp": ["GIFTCODE", "SMS", "CARD", "DEALER", "IAP", "HIS"],
									"list_to_show": ["CARD", "HIS", "GIFTCODE"]
								},
								"custom_exchange": {
									"enable": false,
									"message": "Hien tai dang bao tri"
								}
							}
						});
						WarpConst.GameBase.gameConfig = JSON.parse(default_config);
						if (WebsocketClient.OnGetGameConfigDone) {
							WebsocketClient.OnGetGameConfigDone();
						}
					} else {
						cc.log("___ SUCCESS ___ in Fail case");
					}
				}, 6000);
			}
		};
		xhr.send(JSON.stringify({
			data: App.encryptAes256Cbc(jsonData)
		}));
	},
	//#endregion

	//#region SOCKET CLIENT
	connectToServer: function (data, isMethodAsParam) {
		var x = this;
		// WebsocketClient.Socket = new WebSocket("wss://192.168.1.69:8443/websocket");
		// WebsocketClient.Socket = new WebSocket("ws://192.168.1.68:8443/websocket");
		// WebsocketClient.Socket = new WebSocket("wss://192.168.1.243:8443/websocket");
		// WebsocketClient.Socket = new WebSocket("ws://game.huto.top/websocket");

		switch (BUILD_TYPE) {
			case BUILD_FACEBOOK:
			case BUILD_WEB_MOBILE:
				WebsocketClient.Socket = new WebSocket("wss://game.huto.top/websocket");
				break;
			case BUILD_APPLE_STORE:
			case BUILD_IPA_INSTALL:
			case BUILD_GOOGLE_PLAY:
			case BUILD_APK_INSTALL:
				// WebsocketClient.Socket = new WebSocket("ws://say2.huto.top/websocket");
				WebsocketClient.Socket = new WebSocket("ws://game.huto.top/websocket");
				// WebsocketClient.Socket = new WebSocket("ws://192.168.1.68:8443/websocket");
				// WebsocketClient.Socket = new WebSocket("wss://192.168.1.243:8443/websocket");
				break;
			case BUILD_APPLE_STORE_AND_GAME_FAKE:
			case BUILD_GOOGLE_PLAY_AND_GAME_FAKE:
				WebsocketClient.Socket = new WebSocket("ws://game.huto.top/websocket");
				break;
			default:
				break;
		}

		WebsocketClient.Socket.binaryType = 'arraybuffer';

		WebsocketClient.Socket.onopen = function (event) {
			cc.log("socket opend ");
			if (!isMethodAsParam) {
				var dataBytes = x.encodeDemo(data);
				var dataSent = new Uint8Array(dataBytes.length);
				for (var i = 0; i < dataSent.length; i++) {
					dataSent[i] = dataBytes[i];
				}
				cc.log("sent byte array: " + dataSent.toString());

				WebsocketClient.Socket.send(dataSent);
			} else {
				data();
			}

			//Socket.send(new Uint8Array([0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 1, 85, 218, 1, 82, 123, 34, 117, 115, 101, 114, 110, 97, 109, 101, 34, 58, 34, 108, 111, 110, 103, 116, 110, 104, 49, 50, 51, 34, 44, 34, 112, 97, 115, 115, 119, 111, 114, 100, 34, 58, 34, 49, 50, 51, 52, 53, 54, 34, 44, 34, 108, 111, 103, 105, 110, 84, 121, 112, 101, 34, 58, 49, 44, 34, 102, 98, 73, 100, 34, 58, 34, 34, 44, 34, 102, 98, 84, 111, 107, 101, 110, 34, 58, 34, 34, 44, 34, 99, 108, 105, 101, 110, 116, 95, 118, 101, 114, 115, 105, 111, 110, 34, 58, 34, 49, 46, 48, 46, 49, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 111, 115, 95, 118, 101, 114, 115, 105, 111, 110, 34, 58, 34, 85, 110, 107, 111, 119, 110, 34, 44, 34, 109, 111, 100, 101, 108, 34, 58, 34, 66, 56, 53, 77, 45, 68, 51, 86, 45, 65, 32, 40, 71, 105, 103, 97, 98, 121, 116, 101, 32, 84, 101, 99, 104, 110, 111, 108, 111, 103, 121, 32, 67, 111, 46, 44, 32, 76, 116, 100, 46, 41, 34, 44, 34, 100, 101, 118, 105, 99, 101, 95, 117, 117, 105, 100, 34, 58, 34, 97, 99, 100, 102, 98, 98, 51, 97, 100, 101, 54, 100, 102, 54, 49, 49, 51, 57, 102, 48, 99, 53, 57, 48, 54, 101, 50, 99, 51, 56, 102, 97, 50, 50, 98, 97, 48, 99, 56, 99, 34, 44, 34, 112, 114, 111, 118, 105, 100, 101, 114, 95, 99, 111, 100, 101, 34, 58, 34, 105, 65, 72, 121, 97, 34, 44, 34, 114, 101, 102, 99, 111, 100, 101, 34, 58, 34, 34, 44, 34, 97, 112, 105, 75, 101, 121, 34, 58, 34, 101, 100, 101, 57, 102, 103, 100, 52, 54, 57, 101, 52, 50, 102, 98, 54, 102, 48, 54, 52, 50, 51, 50, 100, 102, 103, 100, 102, 103, 34, 125]));
		};

		WebsocketClient.Socket.onmessage = function (event) {
			cc.log("WebSocket message received:");
			x.responseData(event.data);
		};

		WebsocketClient.Socket.onerror = function (evt) {

			cc.log("error " + JSON.stringify(evt));
			if (WebsocketClient.OnDisconnect)
				WebsocketClient.OnDisconnect();
			if (WebsocketClient.OnDisconnect)
				WebsocketClient.OnDisconnect2();
		};
		WebsocketClient.Socket.onclose = function (event) {

			if (!WarpConst.GameBase.disconnectByLogoutBtn) {
				cc.log("Disconnect by without logout btn. ");
				if (WebsocketClient.OnDisconnect)
					WebsocketClient.OnDisconnect();
				if (WebsocketClient.OnDisconnect)
					WebsocketClient.OnDisconnect2();
			} else {
				cc.log("Disconnect by logout btn. ");
				WarpConst.GameBase.disconnectByLogoutBtn = false;
			}
		};
	},
	//#endregion

	//#region ENCODE & DECODE
	encodeDemo: function (data) {
		cc.log(data.userName + "|" + data.pass);
		//var str = "{\"username\":\"" + data.userName + "\",\"password\":\"" + data.pass + "\",\"loginType\":1,\"fbId\":\"\",\"fbToken\":\"\",\"client_version\":\"1.0.1\",\"platform\":\"Android\",\"os_version\":\"Unkown\",\"model\":\"B85M-D3V-A (Gigabyte Technology Co., Ltd.)\",\"device_uuid\":\"acdfbb3ade6df61139f0c5906e2c38fa22ba0c8c\",\"provider_code\":\"iAHya\",\"refcode\":\"\",\"apiKey\":\"ede9fgd469e42fb6f064232dfgdfg\"}";
		// var str = "{\"username\":\"" + data.userName + "\",\"password\":\"" + data.pass + "\",\"loginType\":1,\"fbId\":\"\",\"fbToken\":\"\",\"client_version\":\"1.0.1\",\"platform\":\"Android\",\"os_version\":\"Unkown\",\"model\":\"B85M-D3V-A (Gigabyte Technology Co., Ltd.)\",\"device_uuid\":\"acdfbb3ade6df61139f0c5906e2c38fa22ba0c8c\",\"provider_code\":\"iAHya\",\"refcode\":\"\",\"apiKey\":\"ede9fgd469e42fb6f064232dfgdfg\"}";

		// var str = "{\"username\":\"" + data.userName + "\",\"password\":\"" + data.pass + "\",\"loginType\":1,\"fbId\":\"\",\"fbToken\":\"\",\"client_version\":\"1.0.1\",\"platform\":" + data.platform + ",\"os_version\":\"Unkown\",\"model\":\"B85M-D3V-A (Gigabyte Technology Co., Ltd.)\",\"device_uuid\":\"acdfbb3ade6df61139f0c5906e2c38fa22ba0c8c\",\"provider_code\":\"iAHya\",\"refcode\":\"\",\"apiKey\":\"ede9fgd469e42fb6f064232dfgdfg\"}";
		var str = "{\"username\":\"" + data.userName + "\",\"password\":\"" + data.pass + "\",\"loginType\":1,\"fbId\":\"\",\"fbToken\":\"\",\"client_version\":\"" + WarpConst.GameBase.clientVersion + "\",\"platform\":" + data.platform + ",\"os_version\":\"Unkown\",\"model\":\"B85M-D3V-A (Gigabyte Technology Co., Ltd.)\",\"device_uuid\":\"acdfbb3ade6df61139f0c5906e2c38fa22ba0c8c\",\"provider_code\":\"iAHya\",\"refcode\":\"\",\"apiKey\":\"ede9fgd469e42fb6f064232dfgdfg\"}";
		//0: callTpye|1: requestType|2-5: sessionId|6-9: requestId|10: reserved|11: payLoadType|12-15: payLoadSize
		var result = [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 1];

		var arr = new ArrayBuffer(4);
		var view = new DataView(arr);
		view.setUint32(0, str.length, false);
		var tmp = new Uint8Array(arr);

		for (var i = 0; i < 4; i++) {
			result[12 + i] = tmp[i];
		}

		//payload bytes
		for (var i = 0; i < str.length; i++) {
			result.push(str.charCodeAt(i));
		}
		//cc.log("FAC: " + (result.length - 16));
		//cc.log("Encode: " + result.toString());
		//cc.log("back: " + String.fromCharCode.apply(String, result));
		return result;
	},
	encodeRequest: function (data) {
		var str = data;
		//0: callTpye|1: requestType|2-5: sessionId|6-9: requestId|10: reserved|11: payLoadType|12-15: payLoadSize
		var result = [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 1];
		result[1] = 53;

		//requestId
		var arr = new ArrayBuffer(4);
		var view = new DataView(arr);
		var tmp = new Uint8Array(arr);
		// view.setUint32(0, 53, false);
		// for (var i = 0; i < 4; i++){
		// 	result[6 + i] = tmp[i];
		// }

		//sessionId
		arr = new ArrayBuffer(4);
		view = new DataView(arr);
		view.setUint32(0, WebsocketClient.sessionid, false);
		tmp = new Uint8Array(arr);
		for (var i = 0; i < 4; i++) {
			result[2 + i] = tmp[i];
		}

		//payLoadSize
		arr = new ArrayBuffer(4);
		view = new DataView(arr);
		view.setUint32(0, str.length, false);
		tmp = new Uint8Array(arr);
		for (var i = 0; i < 4; i++) {
			result[12 + i] = tmp[i];
		}

		//payload bytes
		for (var i = 0; i < str.length; i++) {
			result.push(str.charCodeAt(i));
		}

		var dataSent = new Uint8Array(result.length);
		for (var i = 0; i < dataSent.length; i++) {
			dataSent[i] = result[i];
		}
		//cc.log("sent byte array: " + dataSent.toString());
		//WebsocketClient.Socket.send(dataSent);
		return dataSent;
	},
	responseData: function (data) {
		var result = new Uint8Array(data);
		var result2 = [];
		//cc.log("RECV " + result);
		var callType = result[0];

		if (callType == WarpConst.WarpConst.RESPONSE) {
			var requestType = result[1];
			var resultCode = result[2];
			var reserved = result[3];
			var payLoadType = result[4];
			var payLoadSize = new Uint8Array([0, 0, 0, 0]);
			for (var i = 0; i < 4; i++) {
				payLoadSize[i] = result[5 + i];
			}
			var view = new DataView(payLoadSize.buffer)
			//cc.log(payLoadSize.toString() + "|result:" + view.getInt32());
			//cc.log(result.toString());

			for (var i = 9; i < result.length; i++) {
				result2.push(result[i]);
			}
			//cc.log("DELETE " + result2.toString());
			var t = String.fromCharCode.apply(String, new Uint8Array(result2));
			var t_json = JSON.parse(t);
			//cc.log(t);

			//decodeURIComponent(escape(
			//cc.log("WebsocketResponse Recved | " + t);
			switch (requestType) {
				case WarpConst.WarpRequestTypeCode.AUTH: //1
					cc.log("[RESPONSE] OnLoginDone: " + t);
					if (WebsocketClient.OnLoginDone)
						WebsocketClient.OnLoginDone(resultCode, t);
					if (resultCode == WarpConst.WarpResponseResultCode.SUCCESS)
						this.getHeadLine();
					break;
				case WarpConst.WarpRequestTypeCode.SIGNOUT: //4
					cc.log("[RESPONSE] OnLogOutDone: " + t);
					if (WebsocketClient.OnLogOutDone)
						WebsocketClient.OnLogOutDone(resultCode, t);
					break;
				case WarpConst.WarpRequestTypeCode.GET_USER_INFO: //5
					cc.log("[RESPONSE] Get User Info Done: " + t);
					if (WebsocketClient.OnGetUserInfoDone)
						WebsocketClient.OnGetUserInfoDone(t, resultCode);
					break;
				case WarpConst.WarpRequestTypeCode.UPDATE_AVATAR: //6
					cc.log("[RESPONSE] OnChangeAvaDone: " + t);
					if (WebsocketClient.OnChangeAvaDone)
						WebsocketClient.OnChangeAvaDone(resultCode, t);
					break;
				case WarpConst.WarpRequestTypeCode.JOIN_LOBBY: //10
					cc.log("[RESPONSE] Join Lobby Done: " + t);
					if (WebsocketClient.OnJoinLobbyDone)
						WebsocketClient.OnJoinLobbyDone(t);
					break;
				case WarpConst.WarpRequestTypeCode.SUBSCRIBE_LOBBY: //11
					cc.log("[RESPONSE] Subscribe Lobby Done: " + t);
					if (WebsocketClient.OnSubscribeLobby)
						WebsocketClient.OnSubscribeLobby(t);
					break;
				case WarpConst.WarpRequestTypeCode.GET_ROOMS: //27
					cc.log("[RESPONSE] Get Rooms Done: " + t);
					if (WebsocketClient.OnGetRoomsDone)
						WebsocketClient.OnGetRoomsDone(t);
					break;
				case WarpConst.WarpRequestTypeCode.JOIN_ROOM: //22
					cc.log("[RESPONSE] Join Rooms Done: " + t);
					if (WebsocketClient.OnJoinRoomsDone)
						WebsocketClient.OnJoinRoomsDone(t);
					break;
				case WarpConst.WarpRequestTypeCode.UPDATE_PEERS: //28

					if (t_json.type == WarpConst.MiniGame.START_MATCH) {
						cc.log("[RESPONSE] Slot Start Done: " + t);
						if (WebsocketClient.OnSlotStartDone)
							WebsocketClient.OnSlotStartDone(t);
					}
					if (t_json.type == WarpConst.MiniGame.GET_USER_HISTORY) {
						cc.log("[RESPONSE] Slot Get User His Done: " + t);
						if (WebsocketClient.OnSlotGetUserHistoryDone)
							WebsocketClient.OnSlotGetUserHistoryDone(t);
					}
					if (t_json.type == WarpConst.MiniGame.GET_JACKPOT_HISTORY) {
						cc.log("[RESPONSE] Slot Get Glory Done: " + t);
						if (WebsocketClient.OnSlotGetGloryDone)
							WebsocketClient.OnSlotGetGloryDone(t);
					}

					break;
				case WarpConst.WarpRequestTypeCode.SEND_KEEP_ALIVE: //30
					cc.log("[RESPONSE] PING-PONG");
					break;
				case WarpConst.WarpRequestTypeCode.GET_MESSAGES: //44
					cc.log("[RESPONSE] OnGetMessageDone: " + t);
					if (WebsocketClient.OnGetMessageDone)
						WebsocketClient.OnGetMessageDone(resultCode, t);
					break;
				case WarpConst.WarpRequestTypeCode.CHANGE_PASS: //46
					cc.log("[RESPONSE] OnChangePassDone: " + t);
					if (WebsocketClient.OnChangePassDone)
						WebsocketClient.OnChangePassDone(resultCode, t);
					break;
				case WarpConst.WarpRequestTypeCode.DELETE_MESSAGES: //50
					cc.log("[RESPONSE] OnDelOneMessageDone: " + t);
					if (WebsocketClient.OnDelOneMessageDone)
						WebsocketClient.OnDelOneMessageDone(resultCode, t);
					break;
				case WarpConst.WarpRequestTypeCode.GET_SYSTEM_MESSAGE: //53
					if (WebsocketClient.OnGetHeadLineDone)
						WebsocketClient.OnGetHeadLineDone(t);
					break;
				case WarpConst.WarpRequestTypeCode.GET_KOIN_EXCHANGE: //56
					cc.log("[RESPONSE] OnGetKoinExchangeDone: " + t);
					if (WebsocketClient.OnGetKoinExchangeDone)
						WebsocketClient.OnGetKoinExchangeDone(t);
					break;
				case WarpConst.WarpRequestTypeCode.FEEDBACK: //72
					cc.log("[RESPONSE] OnSendFeedBack: " + t);
					if (WebsocketClient.OnSendFeedBack)
						WebsocketClient.OnSendFeedBack(resultCode, t);
					break;
				case WarpConst.WarpRequestTypeCode.UPDATE_USER_MOBILE: //88
					cc.log("[RESPONSE] OnUpdateUserPhoneDone: " + t);
					if (WebsocketClient.OnUpdateUserPhoneDone)
						WebsocketClient.OnUpdateUserPhoneDone(resultCode, t);
					break;
				case WarpConst.WarpRequestTypeCode.SUBSCRIBE_GAME: //94
					cc.log("[RESPONSE] Subscribe Game Done: " + t);
					if (WebsocketClient.OnSubscribeGameDone)
						WebsocketClient.OnSubscribeGameDone(t);
					break;
				case WarpConst.LobbyId.MINI_TAIXIU: //96
					//cc.log("TAI XIU RESPONSE | " + requestType + "|" + t);
					if (t_json.type == WarpConst.MiniGame.SUBSCRIBE_ROOM) {
						cc.log("[RESPONSE] Subscribe TaiXiu Done " + t);
						if (WebsocketClient.OnTaiXiuSubDone)
							WebsocketClient.OnTaiXiuSubDone(t);
					}
					if (t_json.type == WarpConst.MiniGame.GET_USER_HISTORY) {
						cc.log("[RESPONSE] Get User His TaiXiu Done " + t);
						if (WebsocketClient.OnTaiXiuGetUserHistoryDone)
							WebsocketClient.OnTaiXiuGetUserHistoryDone(t);
					}
					if (t_json.type == WarpConst.MiniGame.GET_JACKPOT_HISTORY) {
						cc.log("[RESPONSE] Get His TaiXiu Done " + t);
						if (WebsocketClient.OnTaiXiuHistoryDone)
							WebsocketClient.OnTaiXiuHistoryDone(t);
					}
					if (t_json.type == WarpConst.MiniGame.ADD_BET) {
						cc.log("[RESPONSE] Add Bet Done " + t);
						if (WebsocketClient.OnAddBetDone)
							WebsocketClient.OnAddBetDone(t);
					}

					break;
				case WarpConst.WarpRequestTypeCode.TRANSFER_GOLD: //101
					cc.log("[RESPONSE] Transfer Gold Done: " + t);
					if (WebsocketClient.OnTransferGoldDone)
						WebsocketClient.OnTransferGoldDone(t);
					break;
				case WarpConst.WarpRequestTypeCode.GIFT_CODE: //103
					cc.log("[RESPONSE] Enter Giftcode Done: " + t);
					if (WebsocketClient.OnSendGiftCodeDone)
						WebsocketClient.OnSendGiftCodeDone(t, resultCode);
					break;
				case WarpConst.LobbyId.MINI_GOLDRUSH: //111
					if (t_json.type == WarpConst.MiniGame.SUBSCRIBE_ROOM) {
						cc.log("[RESPONSE] Subscribe GoldRush Done " + t);
						if (WebsocketClient.OnMiniGoldRushSubDone)
							WebsocketClient.OnMiniGoldRushSubDone(t);
					}
					if (t_json.type == WarpConst.MiniGame.GOLDRUSH_HIDE) {
						if (resultCode == 0) {
							cc.log("[RESPONSE] GoldRush Hide Done " + t);
							if (WebsocketClient.OnMiniGoldRushHideDone)
								WebsocketClient.OnMiniGoldRushHideDone(t);
						}
					}
					if (t_json.type == WarpConst.MiniGame.GOLDRUSH_DELETE_MATCH) {
						cc.log("[RESPONSE] GoldRush Del Hide Match Done: " + t);
						if (WebsocketClient.OnMiniGoldRushDelOneMatchDone)
							WebsocketClient.OnMiniGoldRushDelOneMatchDone(t);
					}
					if (t_json.type == WarpConst.MiniGame.GET_USER_HISTORY) {
						cc.log("[RESPONSE] GoldRush Get User His Done: " + t);
						if (WebsocketClient.OnMiniGoldRushGetUserHistoryDone)
							WebsocketClient.OnMiniGoldRushGetUserHistoryDone(t);
					}
					if (t_json.type == WarpConst.MiniGame.GOLDRUSH_FIND_MATCH) {
						if (t_json.hide_list) {
							cc.log("[RESPONSE] Subscribe Mini GoldRush GetHideList Done " + t);
							if (WebsocketClient.OnMiniGoldRushGetHideListDone)
								WebsocketClient.OnMiniGoldRushGetHideListDone(t);
						} else {
							cc.log("[RESPONSE] Subscribe Mini GoldRush FindMatch Done " + t);
							if (WebsocketClient.OnMiniGoldRushFindMatchDone)
								WebsocketClient.OnMiniGoldRushFindMatchDone(t);
						}
					}
					if (t_json.type == WarpConst.MiniGame.GOLDRUSH_RUSH_ONE) {
						cc.log("[RESPONSE] GoldRush Choose One Done: " + t);
						if (WebsocketClient.OnMiniGoldRushChooseOneDone)
							WebsocketClient.OnMiniGoldRushChooseOneDone(t);
					}
					break;
				case WarpConst.LobbyId.MINI_SLOT: //112
					if (t_json.type == WarpConst.MiniGame.SUBSCRIBE_ROOM) {
						cc.log("[RESPONSE] Subscribe Mini Slot Done " + t);
						if (WebsocketClient.OnMiniSlotSubDone)
							WebsocketClient.OnMiniSlotSubDone(resultCode, t);
					}
					if (t_json.type == WarpConst.MiniGame.START_MATCH) {
						cc.log("[RESPONSE] Mini Slot Start Done: " + t);
						if (WebsocketClient.OnMiniSlotStartDone)
							WebsocketClient.OnMiniSlotStartDone(t);
					}
					if (t_json.type == WarpConst.MiniGame.GET_USER_HISTORY) {
						cc.log("[RESPONSE] MiniSlot Get User His Done: " + t);
						if (WebsocketClient.OnMiniSlotGetUserHistoryDone)
							WebsocketClient.OnMiniSlotGetUserHistoryDone(t);
					}
					if (t_json.type == WarpConst.MiniGame.GET_JACKPOT_HISTORY) {
						cc.log("[RESPONSE] MiniSlot Get Glory Done: " + t);
						if (WebsocketClient.OnMiniSlotGetGloryDone)
							WebsocketClient.OnMiniSlotGetGloryDone(t);
					}
					break;
				case WarpConst.LobbyId.MINI_POKER: // 98
					if (t_json.type == WarpConst.MiniGame.SUBSCRIBE_ROOM) {
						cc.log("[RESPONSE] Subscribe Mini Poker Done " + t);
						if (WebsocketClient.OnMiniPokerSubDone)
							WebsocketClient.OnMiniPokerSubDone(resultCode, t);
					}
					if (t_json.type == WarpConst.MiniGame.START_MATCH) {
						cc.log("[RESPONSE] Mini Poker Start Done: " + t);
						if (WebsocketClient.OnMiniPokerStartDone)
							WebsocketClient.OnMiniPokerStartDone(t);
					}
					if (t_json.type == WarpConst.MiniGame.GET_USER_HISTORY) {
						cc.log("[RESPONSE] Mini Poker Get User His Done: " + t);
						if (WebsocketClient.OnMiniPokerGetUserHistoryDone)
							WebsocketClient.OnMiniPokerGetUserHistoryDone(t);
					}
					if (t_json.type == WarpConst.MiniGame.GET_JACKPOT_HISTORY) {
						cc.log("[RESPONSE] Mini Poker Get Glory Done: " + t);
						if (WebsocketClient.OnMiniPokerGetGloryDone)
							WebsocketClient.OnMiniPokerGetGloryDone(t);
					}
					break;
				case WarpConst.LobbyId.MINI_SLOT_2: //116
					if (t_json.type == WarpConst.MiniGame.SUBSCRIBE_ROOM) {
						cc.log("[RESPONSE] Subscribe Mini Slot 2 Done " + t);
						if (WebsocketClient.OnMiniSlot_2_SubDone)
							WebsocketClient.OnMiniSlot_2_SubDone(resultCode, t);
					}
					if (t_json.type == WarpConst.MiniGame.START_MATCH) {
						cc.log("[RESPONSE] Mini Slot 2 Start Done: " + t);
						if (WebsocketClient.OnMiniSlot_2_StartDone)
							WebsocketClient.OnMiniSlot_2_StartDone(t);
					}
					if (t_json.type == WarpConst.MiniGame.GET_USER_HISTORY) {
						cc.log("[RESPONSE] MiniSlot 2 Get User His Done: " + t);
						if (WebsocketClient.OnMiniSlot_2_GetUserHistoryDone)
							WebsocketClient.OnMiniSlot_2_GetUserHistoryDone(t);
					}
					if (t_json.type == WarpConst.MiniGame.GET_JACKPOT_HISTORY) {
						cc.log("[RESPONSE] MiniSlot 2 Get Glory Done: " + t);
						if (WebsocketClient.OnMiniSlot_2_GetGloryDone)
							WebsocketClient.OnMiniSlot_2_GetGloryDone(t);
					}
					break;
				case WarpConst.WarpRequestTypeCode.DEALER_CONFIG: //113
					cc.log("[RESPONSE] Get Dealer Config Done " + t);
					if (WebsocketClient.OnGetDealerConfigDone)
						WebsocketClient.OnGetDealerConfigDone(t);
					break;
				case WarpConst.WarpRequestTypeCode.REF_CODE: //114
					cc.log("[RESPONSE] Update RefCode Done " + t);
					if (WebsocketClient.OnUpdateRefCodeDone)
						WebsocketClient.OnUpdateRefCodeDone(t);
					break;
				case WarpConst.WarpRequestTypeCode.GET_ALL_JACKPOT_AMOUNT: //115
					cc.log("[RESPONSE] OnGetAllJackpotAmount " + t);
					if (WebsocketClient.OnGetAllJackpotAmount)
						WebsocketClient.OnGetAllJackpotAmount(t);
					break;
				default:
					cc.log("UNKNOW RESPONSE | " + requestType + "|" + t);
					break;
			}


		} else if (callType == WarpConst.WarpConst.UPDATE) {
			var notifyType = result[1];
			var reserved = result[2];
			var payLoadType = result[3];

			var payLoadSize = new Uint8Array([0, 0, 0, 0]);
			for (var i = 0; i < 4; i++) {
				payLoadSize[i] = result[4 + i];
			}
			var view = new DataView(payLoadSize.buffer)
			//cc.log(payLoadSize.toString() + "|result:" + view.getInt32());
			//cc.log(result.toString());

			for (var i = 8; i < result.length; i++) {
				result2.push(result[i]);
			}
			//cc.log("DELETE " + result2.toString());
			var t = String.fromCharCode.apply(String, new Uint8Array(result2));

			var json = JSON.parse(t);
			//decodeURIComponent(escape(
			// if (json.message) {
			// 	cc.log(decodeURIComponent(escape(json.message)));
			// }
			switch (notifyType) {
				case WarpConst.WarpNotifyTypeCode.UPDATE_MONEY: //8
					cc.log("[NOTIFY] OnMoneyChangeDone: " + t);
					if (WebsocketClient.OnMoneyChangeDone)
						WebsocketClient.OnMoneyChangeDone(t);
					break;
				case WarpConst.WarpNotifyTypeCode.NOTIFY_SESSION_EXPIRED: //47
					cc.log("[NOTIFY] OnLoginOnOtherDevice: " + t);
					if (WebsocketClient.OnLoginOnOtherDevice)
						WebsocketClient.OnLoginOnOtherDevice(t);
					if (WebsocketClient.OnLoginOnOtherDevice2)
						WebsocketClient.OnLoginOnOtherDevice2(t);
					break;
				case WarpConst.WarpNotifyTypeCode.WORLD_CHAT_NOTIFICATION: //53
					cc.log("[NOTIFY] WorldChat: " + t);
					if (WebsocketClient.OnWorldChat)
						WebsocketClient.OnWorldChat(t);
					break;
				case WarpConst.WarpNotifyTypeCode.TAI_XIU: //59
					cc.log("[NOTIFY] MiniTaiXiu: " + t);
					if (WebsocketClient.OnTaiXiuMiniRoomChange)
						WebsocketClient.OnTaiXiuMiniRoomChange(t);
					break;
				case WarpConst.WarpNotifyTypeCode.UPDATE_JACKPOT:
					cc.log("[NOTIFY] Update Jackpot: " + t);
					if (WebsocketClient.OnJackpotChange)
						WebsocketClient.OnJackpotChange(t);
					break;
				case WarpConst.WarpNotifyTypeCode.NOTIFY_ALL: //73
					cc.log("[NOTIFY] JACKPOT: " + t);
					if (WebsocketClient.OnNotifyAll)
						WebsocketClient.OnNotifyAll(t);
					break;

				default:
					cc.log("UNKNOW NOTIFY | " + notifyType + "|" + t);
					break;
			}

		}

		return;
	},
	//#endregion

	//#region //SOCKET REQUEST
	getHeadLine: function () {
		var str = "{\"type\":3}";
		var m = this.encodeRequest(str);
		m[1] = WarpConst.WarpRequestTypeCode.GET_SYSTEM_MESSAGE;
		WebsocketClient.Socket.send(m);
		return;

		//0: callTpye|1: requestType|2-5: sessionId|6-9: requestId|10: reserved|11: payLoadType|12-15: payLoadSize
		var result = [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 1];
		result[1] = 53;

		//requestId
		var arr = new ArrayBuffer(4);
		var view = new DataView(arr);
		var tmp = new Uint8Array(arr);
		// view.setUint32(0, 53, false);
		// for (var i = 0; i < 4; i++){
		// 	result[6 + i] = tmp[i];
		// }

		//sessionId
		arr = new ArrayBuffer(4);
		view = new DataView(arr);
		view.setUint32(0, WebsocketClient.sessionid, false);
		tmp = new Uint8Array(arr);
		for (var i = 0; i < 4; i++) {
			result[2 + i] = tmp[i];
		}

		//payLoadSize
		arr = new ArrayBuffer(4);
		view = new DataView(arr);
		view.setUint32(0, str.length, false);
		tmp = new Uint8Array(arr);
		for (var i = 0; i < 4; i++) {
			result[12 + i] = tmp[i];
		}

		//payload bytes
		for (var i = 0; i < str.length; i++) {
			result.push(str.charCodeAt(i));
		}

		var dataSent = new Uint8Array(result.length);
		for (var i = 0; i < dataSent.length; i++) {
			dataSent[i] = result[i];
		}
		cc.log("sent byte array: " + dataSent.toString());
		WebsocketClient.Socket.send(dataSent);
	},
	keepAlive() {
		var str = {};
		var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
		m[1] = WarpConst.WarpRequestTypeCode.SEND_KEEP_ALIVE;
		WebsocketClient.Socket.send(m);
	},
	//#endregion

	//#region  TEST
	/**cai nay de test ma thoi */
	testCallBack: function () {
		var m = this.testTrace;
		var n = [];
		n.push(m);
		n.push(m);
		n[0](2);
		n[0](99);
	},
	testTrace: function (data) {
		cc.log(data);
	},
	testHttpRequest: function () {
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function () {
			if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
				var response = xhr.responseText;
				cc.log("http request");
				cc.log(response);
			}
		};
		xhr.open("GET", "https://tolsgnoc.blogspot.com/feeds/posts/default?alt=json-in-script&callback=myFunc", true);
		xhr.send();
	}
	//#endregion
});