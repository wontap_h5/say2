/**
 * By: longtnh
 * To do: Control USER REG
 * Date: 2019.03.24
 */
var App = require("App");
var WarpConst = require("WarpConst");
var RegCtl = cc.Class({
    extends: cc.Component,

    properties: {
        loadingIcon: cc.Node,

        userNameIpf: cc.EditBox,
        passIpf: cc.EditBox,
        rePassIpf: cc.EditBox,
    },

    start() {

    },
    statics: {
        instance: null,
        showDialog: null,
        loginAfterReg: null,
    },
    onLoad() {
        RegCtl.instance = this;
    },
    show() {
        this.userNameIpf.string = this.passIpf.string = this.rePassIpf.string = "";
        this.node.active = true;
    },
    close() {
        this.node.active = false;
    },
    RegNewAccount() {
        if (this.userNameIpf.string.length == 0 || this.passIpf.string.length == 0 || this.rePassIpf.string.length == 0) {
            RegCtl.showDialog("Vui lòng điền đầy đủ thông tin.");
            return;
        }

        if (this.userNameIpf.string.length < 4 || this.userNameIpf.string.length > 25) {
            RegCtl.showDialog("Tên tài khoản phải có độ dài từ 4 - 25 ký tự");
            return;
        }

        if (this.passIpf.string != this.rePassIpf.string) {
            RegCtl.showDialog("Mật khẩu không trùng khớp.");
            return;
        }

        if (this.passIpf.string.length < 4 || this.passIpf.string.length > 12) {
            RegCtl.showDialog("Mật khẩu phải có độ dài từ 4 - 12 ký tự");
            return;
        }

        if (/^[a-zA-Z0-9]*$/.test(this.userNameIpf.string) == false) {
            RegCtl.showDialog("Tài khoản không được chứa ký tự đặc biệt");
            return;
        }

        if (/^[a-zA-Z0-9]*$/.test(this.passIpf.string) == false) {
            RegCtl.showDialog("Mật khẩu không được chứa ký tự đặc biệt");
            return;
        }

        var rand_device_uuid = "";
        for (let i = 0; i < 20; i++) {
            rand_device_uuid += WarpConst.GameBase.alphabet.charAt(Math.floor(Math.random() * WarpConst.GameBase.alphabet.length));
        }
        cc.log(rand_device_uuid);

        cc.log("platform : ", WarpConst.GameBase.platform);
        var jsonData = {
            api_key: "b62b716096c0a337bac5cbf4fb8e881ecf3fb3b1",
            username: this.userNameIpf.string,
            password: this.passIpf.string,
            mobile: "",
            provider_code: WarpConst.GameBase.providerCode,
            client_version: WarpConst.GameBase.clientVersion,
            platform: WarpConst.GameBase.platform,
            model: WarpConst.GameBase.model,
            device_uuid: rand_device_uuid,
            refCode: "",
            token: WarpConst.GameBase.apiToken,
        }
        RegCtl.instance.loadingIcon.active = true;

        var xhr = new XMLHttpRequest();
        xhr.open('POST', WarpConst.Links.outSideHttpRequest + 'game/register', true);
        xhr.onload = function () {
            // cc.log(App.dycryptAes256Cbc(this.responseText));
            RegCtl.instance.loadingIcon.active = false;
            var data = App.dycryptAes256Cbc(this.responseText);
            cc.log(data);
            //Đã tồn tại: {"status":5,"message":"User existed","data":null}
            //Thành công: {"status":0,"message":"Register successfully","data":1}
            var json = JSON.parse(data);
            if (json.status == 5) { //User exits
                RegCtl.showDialog("Tài khoản đã tồn tại.");
            } else if (json.status == 0) { //Reg success
                if (RegCtl.loginAfterReg) {
                    RegCtl.loginAfterReg(RegCtl.instance.userNameIpf.string, RegCtl.instance.passIpf.string);
                    RegCtl.instance.close();
                }
            } else { //Unknow
                RegCtl.showDialog("Đã xảy ra lỗi. Vui lòng thử lại.");
            }

        };
        xhr.send(JSON.stringify({
            data: App.encryptAes256Cbc(jsonData)
        }));
    }
});