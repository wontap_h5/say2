/**
 * By: longtnh
 * To do: Control Pot All Group
 * Date: 2019.03.25
 */
var default_PotAll = [
    5024562,
    17427954,
    63214690,
    3654785,
    24853157,
    58746348,
    6582143,
    13596423,
    40215735,
    5002562,
    24153157,
    40264735,
    1204862,
    19153657,
    50212735,
    1022962,
    26427624,
    38214790,
    1202562,
    32153157,
    57264735
];
var App = require("App");
var PotAllCtl = cc.Class({
    extends: cc.Component,

    properties: {
        tabText: {
            default: [],
            type: [cc.Node],
        },
        tabTextColor: {
            default: [],
            type: [cc.Color],
        },
        tabBtnAct: {
            default: [],
            type: [cc.Node],
        },
        chestValueText: {
            default: [],
            type: [cc.Label]
        },

        //--- private variables ---
        PotAll_Data: null,
        Last_PotAll_Data: null,
        current_tab_ID: null,
    },
    statics: {
        instance: null,
    },
    
    onLoad() {
        PotAllCtl.instance = this;

        this.current_tab_ID = 0;
        this.PotAll_Data = [];
        this.Last_PotAll_Data = [];
        for (let index = 0; index < 21; index++) {
            this.Last_PotAll_Data.push(index);
        }

        var get_Pot_LocalStorage = cc.sys.localStorage.getItem("potall");
        if (get_Pot_LocalStorage) {
            cc.log("LocalStorage PotAll is available");
            if (JSON.parse(get_Pot_LocalStorage).length == 21) {
                cc.log("LocalStorage PotAll is available : Data ok ");
                this.PotAll_Data = JSON.parse(get_Pot_LocalStorage);
            } else {
                cc.log("LocalStorage PotAll is available : Data ver old -> use default ");
                this.PotAll_Data = default_PotAll;
            }

        } else {
            cc.log("LocalStorage PotAll is not available");
            this.PotAll_Data = default_PotAll;
        }

        cc.log("PotAll_Data : ", this.PotAll_Data);
    },

    notifyPotAllDataStateChange(data) {
        cc.log("____ PotAllCtl ___");
        cc.log("notifyPotAllDataStateChange : ", data);

        cc.sys.localStorage.setItem("potall", JSON.stringify(data));

        if (this.node.active) {
            var id = this.current_tab_ID;
            for (let index = 0; index < this.chestValueText.length; index++) {
                this.updateChestValue(this.chestValueText[index], this.Last_PotAll_Data[3 * index + id], data[3 * index + id]);
            }
            this.PotAll_Data = data;
            this.Last_PotAll_Data = data;
        }
    },

    show() {
        this.node.active = true;
        this.OnClickTabBtn(null, 0);
    },
    close() {
        this.node.active = false;
    },
    OnClickTabBtn(event, index) {
        for (let i = 0; i < this.tabBtnAct.length; i++) {
            this.tabBtnAct[i].active = false;
            this.tabText[i].color = this.tabTextColor[0];
        }
        this.current_tab_ID = parseInt(index);
        this.tabBtnAct[parseInt(index)].active = true;
        this.tabText[parseInt(index)].color = this.tabTextColor[1];
        this.SetChestValues(parseInt(index));
    },
    SetChestValues(id) {
        cc.log(this.PotAll_Data);
        for (var i = 0; i < this.chestValueText.length; i++) { // 5
            this.SetChestValue(this.chestValueText[i], this.PotAll_Data[3 * i + id]);
        }
    },
    SetChestValue(label, toNum) {
        var steps = 10;
        var fromNum = 0;
        var addNum = Math.floor(toNum / steps);
        var add = cc.sequence(
            cc.delayTime(0.05),
            cc.callFunc(function () {
                fromNum += addNum;
                label.string = App.formatMoney(fromNum);
            })
        );
        label.node.runAction(
            cc.sequence(
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                add,
                cc.callFunc(function () {
                    label.string = App.formatMoney(toNum);
                })
            )
        );
    },

    updateChestValue(label, fromNum, toNum) {
        var steps = 10;
        if ((toNum - fromNum) > 0) {
            var addNum = Math.floor((toNum - fromNum) / steps);
            var add = cc.sequence(
                cc.delayTime(0.05),
                cc.callFunc(function () {
                    fromNum += addNum;
                    label.string = App.formatMoney(fromNum);
                })
            );
            label.node.runAction(
                cc.sequence(
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    add,
                    cc.callFunc(function () {
                        label.string = App.formatMoney(toNum);
                    })
                )
            );
        }
    }
});