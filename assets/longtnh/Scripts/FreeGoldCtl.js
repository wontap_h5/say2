var WebsocketClient = require("WebsocketClient");
var WarpConst = require("WarpConst");
var App = require("App");

var FreeGoldCtl = cc.Class({
    extends: cc.Component,

    properties: {
        loadingIcon: cc.Node,
        tabEle: {
            default: [],
            type: [cc.Node],
        },
        tabText: {
            default: [],
            type: [cc.Node],
        },
        tabTextColor: {
            default: [],
            type: [cc.Color],
        },

        //TAB - Free Gold
        FreeGoldContentPanel: cc.Node,

        //TAB - Share Refcode
        RefContenPanel: cc.Node,
        RefMyCode: cc.Label,
        RefMyId: cc.Label,
        RefDesText: cc.Label,
        RefIdIpf: cc.EditBox,
        RefCodeIpf: cc.EditBox,
        //TAB - Install App
        InsContentPanel: cc.Node,
        InsEle: cc.Node,
        //private variable
        myRefcodeIsLoaded: false,
        SpawnedEles: [],
        btn_Varify: cc.Node,
        sprite_Dis: cc.SpriteFrame,
        sprite_On: cc.SpriteFrame,
    },

    statics: {
        instance: null,
        showDialog: null,
        show_Popup_Need_Verify : null
    },

    onLoad() {
        FreeGoldCtl.instance = this;

        this.InsEle.active = false;
    },
    onEnable() {
        WebsocketClient.OnUpdateRefCodeDone = this.OnUpdateRefCodeDone;
        //cc.log(WebsocketClient.OnUpdateRefCodeDone == null);
    },
    onDisable() {
        WebsocketClient.OnUpdateRefCodeDone = null;
        for (var i = this.SpawnedEles.length - 1; i > -1; i--) {
            this.SpawnedEles[i].destroy();
            this.SpawnedEles.pop();
        }
    },
    show(tab_open) {
        //cc.log(RechargeCtl.instance);
        FreeGoldCtl.instance.node.active = true;
        if (tab_open === "0") {
            FreeGoldCtl.instance.onClickTabBtn(null, tab_open);
        } else {
            FreeGoldCtl.instance.onClickTabBtn(null, "2");
        }
    },
    close() {
        this.node.active = false;
    },
    onClickTabBtn(event, index) {
        for (let i = 0; i < this.tabEle.length; i++) {
            this.tabEle[i].active = false;
            this.tabText[i].color = this.tabTextColor[0];
        }
        //cc.log(index);
        this.tabEle[parseInt(index)].active = true;
        this.tabText[parseInt(index)].color = this.tabTextColor[1];

        this.RefContenPanel.active = false;
        this.InsContentPanel.active = false;
        this.FreeGoldContentPanel.active = false;

        switch (index) {
            case "0": //Enter Ref
            var data = WarpConst.GameBase.userInfo.desc;
            cc.log("VietNam :  ", JSON.stringify(data));
            if (data.mobile == null || data.mobile.length == 0) {
                cc.log("VietNam Not Verify");
                FreeGoldCtl.show_Popup_Need_Verify("FreeGold");
                return;
            }
                this.RefContenPanel.active = true;
                if (this.myRefcodeIsLoaded) {
                    return;
                }
                this.myRefcodeIsLoaded = true;
                this.updateRefCode(-1, -1, "");
                break;
            case "1": //Install App
                this.InsContentPanel.active = true;
                this.FillInstallAppContent();
                break;
            case "2": //Enter Free Gold
                this.FreeGoldContentPanel.active = true;
                var data = WarpConst.GameBase.userInfo.desc;
                if (data.mobile == null || data.mobile.length == 0) {
                    cc.log("Varifying");
                    this.btn_Varify.getComponent(cc.Sprite).spriteFrame = this.sprite_On;
                } else {
                    cc.log("Varified");
                    this.btn_Varify.getComponent(cc.Sprite).spriteFrame = this.sprite_Dis;
                    this.btn_Varify.getComponent(cc.Button).interactable = false;
                    this.btn_Varify.children[0].getComponent(cc.Label).string = " ĐÃ NHẬN ";
                }
                break;
        }
    },
    EnterRefCode() {
        if (this.RefIdIpf.string.length == 0 || this.RefCodeIpf.string.length == 0) {
            FreeGoldCtl.showDialog(" Vui lòng điền đầy đủ thông tin ");
            return;
        }
        this.updateRefCode(WarpConst.GameBase.userInfo.desc.id, this.RefIdIpf.string, this.RefCodeIpf.string);
    },
    FillInstallAppContent() {
        cc.log(WarpConst.GameBase.gameConfig.data.custom_install_app);
        if (!WarpConst.GameBase.gameConfig.data.custom_install_app.enable)
            return;
        for (var i = this.SpawnedEles.length - 1; i > -1; i--) {
            this.SpawnedEles[i].destroy();
            this.SpawnedEles.pop();
        }
        //WarpConst.GameBase.gameConfig = JSON.parse('{"status":0,"message":"Success","data":{"custom_ads_id":{"interstitial_id":"ca-app-pub-2028962319778010/5230228888","video_count_per_day":10,"interstitial_count_per_day":10,"app_id":"ca-app-pub-2028962319778010~8971707936","video_id":"ca-app-pub-2028962319778010/3962923831"},"custom_active_games":{"slot2":true,"slot1":true,"dao_vang":true,"tai_xiu":true,"mini_slot":true},"under_review_versions":["1.0"],"fanpage":"https://www.facebook.com/H%C5%A9-To-Ch%C6%A1i-Game-N%E1%BB%95-H%C5%A9-%C4%90%E1%BB%89nh-Cao-1905624156416681","custom_event_banner":[{"img_link":"https://gamebai.online/Banner/slot2.png","event_link":"https://taigamebai.net/say2"},{"img_link":"https://gamebai.online/Banner/slot3.png","event_link":"https://www.facebook.com/1905624156416681/photos/a.2108192389493189/2131657853813309/?type=3&theater"},{"img_link":"https://gamebai.online/Banner/slot1.png","event_link":"https://www.facebook.com/1905624156416681/videos/600510537085388"},{"img_link":"https://gamebai.online/Banner/slot4.png","event_link":"https://www.facebook.com/1905624156416681"}],"custom_install_app":{"metadata":[{"img":"https://sv1.uphinhnhanh.com/images/2019/01/18/Logo-game-slot.png","des":"Tặng vốn khởi nghiệp khi đăng ký, giao diện đẹp tỷ lệ chuẩn, Tài Xỉu cầu cực chuẩn","link":"https://www.facebook.com/Hũ-To-Chơi-Game-Nổ-Hũ-Đỉnh-Cao-1905624156416681","title":"SAY2 - Quay hũ đỉnh"},{"img":"https://sv1.uphinhnhanh.com/images/2019/01/18/Logo-game-slot.png","des":"Tặng vốn khởi nghiệp khi đăng ký, giao diện đẹp tỷ lệ chuẩn, Tài Xỉu cầu cực chuẩn","link":"https://www.facebook.com/Hũ-To-Chơi-Game-Nổ-Hũ-Đỉnh-Cao-1905624156416681","title":"SAY2 - Quay hũ đỉnh"}],"enable":true},"link":"LINK NAOT2 http://google.com.vn","custom_need_update_to_play":{"link":"https://play.google.com/store/apps/details?id=com.frogmind.badlandbrawl","message":"Đã có phiên bản mới","apply_to_vers":["0.0"]},"custom_recharge":{"enable":true,"list_temp":["GIFTCODE","SMS","CARD","DEALER","IAP","HIS"],"list_to_show":["CARD","HIS","GIFTCODE"]},"custom_exchange":{"enable":true,"message":"Hien tai dang bao tri"}}}');
        var metadata = WarpConst.GameBase.gameConfig.data.custom_install_app.metadata;

        for (var i = 0; i < metadata.length; i++) {
            this.setup_Game(metadata[i], i);
        }
    },

    setup_Game(data, i) {
        setTimeout(function () {
            var obj = cc.instantiate(FreeGoldCtl.instance.InsEle);
            obj.parent = FreeGoldCtl.instance.InsEle.parent;
            var textArr = obj.getComponentsInChildren(cc.Label);
            textArr[0].string = unescape(data.title);
            textArr[1].string = unescape(data.des);

            var img = obj.getComponentInChildren(cc.Sprite);
            cc.loader.load(data.img, function (err, texture) {
                // Use texture to create sprite frame
                try {
                    img.spriteFrame = new cc.SpriteFrame(texture);
                } catch (error) {
                    cc.log(error);
                }
            });

            var btn = obj.getComponentInChildren(cc.Button);
            btn.clickEvents[0].customEventData = i;
            obj.active = true;
            FreeGoldCtl.instance.SpawnedEles.push(obj);
        }, i * 3000);
    },
    OnClickInstallEleBtn(event, index) {
        cc.sys.openURL(WarpConst.GameBase.gameConfig.data.custom_install_app.metadata[parseInt(index)].link);
    },
    //#region REQUEST - RESPONSE WITH SERVER
    updateRefCode(userId, targetId, code) {
        var str = {
            code: code,
            userId: userId,
            targetId: targetId
        };
        var m = WebsocketClient.instance.encodeRequest(JSON.stringify(str));
        m[1] = WarpConst.WarpRequestTypeCode.REF_CODE;
        WebsocketClient.Socket.send(m);
    },
    OnUpdateRefCodeDone(data) {
        var json = JSON.parse(data);
        if (json.ref_code) { // {"ref_code":"4abcbb","value":500}
            FreeGoldCtl.instance.RefMyCode.string = "CODE: " + json.ref_code.substring(0, 6);
            FreeGoldCtl.instance.RefMyId.string = "ID: " + WarpConst.GameBase.userInfo.desc.id;
            FreeGoldCtl.instance.RefDesText.string = "Vui lòng nhập ID và mã chia sẻ của người giới thiệu để nhận ngay " + App.formatMoney(json.value) + WarpConst.GameBase.MoneyName;
        } else {
            FreeGoldCtl.showDialog(App.formatUnicode(json.desc));
        }
        //FreeGoldCtl.instance.myRefcodeIsLoaded = json;
    }
    //#endregion
});