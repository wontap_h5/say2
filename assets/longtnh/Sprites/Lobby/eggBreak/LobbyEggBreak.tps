<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.12.1</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>LobbyEggBreak.plist</filename>
            </struct>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (1).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (10).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (11).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (12).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (13).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (14).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (15).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (16).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (17).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (18).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (19).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (2).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (20).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (21).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (22).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (23).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (24).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (25).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (26).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (27).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (28).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (29).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (3).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (30).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (4).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (5).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (6).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (7).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (8).png</key>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (9).png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,74,97,147</rect>
                <key>scale9Paddings</key>
                <rect>48,74,97,147</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../../CongGameSlotResources/_Sprites/Lobby/EggBreak_act.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,25,105,49</rect>
                <key>scale9Paddings</key>
                <rect>53,25,105,49</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../../../PROJECTS/LTA/CongGameSlot/Downloaded Files/Toanpt/Say2/2019.01.03 16.14 GIAO DIEN SLOT/10.dap trung - ok/EggBreakReturnCard.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,18,64,35</rect>
                <key>scale9Paddings</key>
                <rect>32,18,64,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../../../PROJECTS/LTA/CongGameSlot/Downloaded Files/Toanpt/Say2/2019.01.03 16.14 GIAO DIEN SLOT/10.dap trung - ok/EggBreak_dis.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>41,13,81,26</rect>
                <key>scale9Paddings</key>
                <rect>41,13,81,26</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (14).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (15).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (16).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (17).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (18).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (19).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (20).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (21).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (22).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (23).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (24).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (25).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (26).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (27).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (28).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (29).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (30).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (1).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (2).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (3).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (4).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (5).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (6).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (7).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (8).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (9).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (10).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (11).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (12).png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/EggBreak/EggBreak (13).png</filename>
            <filename>../../../../../../../PROJECTS/LTA/CongGameSlot/Downloaded Files/Toanpt/Say2/2019.01.03 16.14 GIAO DIEN SLOT/10.dap trung - ok/EggBreakReturnCard.png</filename>
            <filename>../../../../../../../PROJECTS/LTA/CongGameSlot/Downloaded Files/Toanpt/Say2/2019.01.03 16.14 GIAO DIEN SLOT/10.dap trung - ok/EggBreak_dis.png</filename>
            <filename>../../../../../../CongGameSlotResources/_Sprites/Lobby/EggBreak_act.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
