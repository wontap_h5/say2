<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.12.1</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>Lobby.plist</filename>
            </struct>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BgInputField.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>112,21,224,42</rect>
                <key>scale9Paddings</key>
                <rect>112,21,224,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BgLobbyFooter.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>325,21,649,43</rect>
                <key>scale9Paddings</key>
                <rect>325,21,649,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BgLobbyHeader.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>427,32,853,64</rect>
                <key>scale9Paddings</key>
                <rect>427,32,853,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BgPopupContent.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,44,71,87</rect>
                <key>scale9Paddings</key>
                <rect>36,44,71,87</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BgRechargeNetEle.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,37,101,74</rect>
                <key>scale9Paddings</key>
                <rect>50,37,101,74</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/Black.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/Trans5Percent.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,8,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,8,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnBlue.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnBlue_pr.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnYel.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnYel_pr.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,22,128,45</rect>
                <key>scale9Paddings</key>
                <rect>64,22,128,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnChangeAva.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,19,41,37</rect>
                <key>scale9Paddings</key>
                <rect>21,19,41,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnClose.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnClose_pr.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,27,55,55</rect>
                <key>scale9Paddings</key>
                <rect>27,27,55,55</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnExchange.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnExchange_pr.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnFreeXu.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnFreeXu_pr.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnGiftCode.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnGiftCode_pr.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnTransferMoney.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnTransferMoney_pr.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>43,43,85,85</rect>
                <key>scale9Paddings</key>
                <rect>43,43,85,85</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnFacebook.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnFacebook_pr.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>68,22,137,45</rect>
                <key>scale9Paddings</key>
                <rect>68,22,137,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnFacebookConnect.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnFacebookConnect_pr.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/Lobby Setting Reply pr.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/Lobby Setting Reply.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnGrey.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnGrey_pr.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>60,23,121,46</rect>
                <key>scale9Paddings</key>
                <rect>60,23,121,46</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnInbox.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnInbox_pr.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnLockMoney.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnLockMoney_pr.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnSetting.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnSetting_pr.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>40,40,80,80</rect>
                <key>scale9Paddings</key>
                <rect>40,40,80,80</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnInboxEleDel.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,15,25,29</rect>
                <key>scale9Paddings</key>
                <rect>13,15,25,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnInboxNum.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,20,20</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnLobbyBack.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnLobbyBack_pr.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/LobbyRechargeArrow.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/LobbyRechargeArrow_pr.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,20,39,39</rect>
                <key>scale9Paddings</key>
                <rect>20,20,39,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnPot.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnPot_pr.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,63,63</rect>
                <key>scale9Paddings</key>
                <rect>32,32,63,63</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnRecharge.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/BtnRecharge_pr.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,15,31,31</rect>
                <key>scale9Paddings</key>
                <rect>15,15,31,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/Icon Banner.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>87,141,175,281</rect>
                <key>scale9Paddings</key>
                <rect>87,141,175,281</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/Icon Game Slot Developing.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>106,147,211,293</rect>
                <key>scale9Paddings</key>
                <rect>106,147,211,293</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/Line 2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>47,1,93,1</rect>
                <key>scale9Paddings</key>
                <rect>47,1,93,1</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/Line.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>236,5,471,11</rect>
                <key>scale9Paddings</key>
                <rect>236,5,471,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/LoadingIcon.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/loading.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,64,128,128</rect>
                <key>scale9Paddings</key>
                <rect>64,64,128,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/LobbyRechargeByCardRateBg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>84,88,168,175</rect>
                <key>scale9Paddings</key>
                <rect>84,88,168,175</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/LobbyUserMoneyBg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,16,57,33</rect>
                <key>scale9Paddings</key>
                <rect>28,16,57,33</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/LobbyUserMoneyCoin.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,31,31</rect>
                <key>scale9Paddings</key>
                <rect>16,16,31,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/Net_VMS.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/Net_VNP.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/Net_VTT.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>37,24,73,47</rect>
                <key>scale9Paddings</key>
                <rect>37,24,73,47</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/Phone.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,22,39,45</rect>
                <key>scale9Paddings</key>
                <rect>20,22,39,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/PopupHisBotEle.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,20,72,41</rect>
                <key>scale9Paddings</key>
                <rect>36,20,72,41</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/PopupHisBotEle2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,19,72,37</rect>
                <key>scale9Paddings</key>
                <rect>36,19,72,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/PopupHisTopEle.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,17,72,33</rect>
                <key>scale9Paddings</key>
                <rect>36,17,72,33</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/PotAllBg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,144,201,288</rect>
                <key>scale9Paddings</key>
                <rect>100,144,201,288</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/PotAllLine.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>87,24,174,47</rect>
                <key>scale9Paddings</key>
                <rect>87,24,174,47</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/PotAllMiniSlot.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/PotAllSlot1.png</key>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/PotAllSlot2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,25,50,50</rect>
                <key>scale9Paddings</key>
                <rect>25,25,50,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/PotAll_act.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,17,55,34</rect>
                <key>scale9Paddings</key>
                <rect>28,17,55,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/PotAll_nor.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,17,55,33</rect>
                <key>scale9Paddings</key>
                <rect>27,17,55,33</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/Slider_bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,140,9,281</rect>
                <key>scale9Paddings</key>
                <rect>5,140,9,281</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/Slider_fg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,14,17,27</rect>
                <key>scale9Paddings</key>
                <rect>8,14,17,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/TabActive.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>65,35,130,69</rect>
                <key>scale9Paddings</key>
                <rect>65,35,130,69</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../CongGameSlotResources/_Sprites/Lobby/line vertical.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,134,11,267</rect>
                <key>scale9Paddings</key>
                <rect>5,134,11,267</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../../PROJECTS/LTA/CongGameSlot/Clones/Say2UpStore/Assets/_Sprites/Lobby/NotifyAllBg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,74,53,147</rect>
                <key>scale9Paddings</key>
                <rect>27,74,53,147</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BgLobbyHeader.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnFacebook.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnFacebook_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnBlue.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnBlue_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnYel_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnYel.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BgInputField.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnClose.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnClose_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/LobbyUserMoneyBg.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/LobbyUserMoneyCoin.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnRecharge.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnRecharge_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnLockMoney.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnLockMoney_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnInbox.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnInbox_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnSetting.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnSetting_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnInboxNum.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnLobbyBack.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnLobbyBack_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/Icon Game Slot Developing.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnTransferMoney.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnTransferMoney_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnFreeXu.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnFreeXu_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnGiftCode.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnGiftCode_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnExchange.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnExchange_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/TabActive.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/Line 2.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/line vertical.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/Line.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BgPopupContent.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/Trans5Percent.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/LobbyRechargeByCardRateBg.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BgRechargeNetEle.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/Net_VNP.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/Net_VTT.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/Net_VMS.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/PopupHisBotEle.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/PopupHisBotEle2.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/PopupHisTopEle.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/Slider_bg.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/Slider_fg.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/loading.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/Black.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/LoadingIcon.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/Icon Banner.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnChangeAva.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/Phone.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnGrey_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnGrey.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnInboxEleDel.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/PotAllBg.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/PotAll_nor.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/PotAll_act.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/PotAllLine.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/PotAllMiniSlot.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/PotAllSlot1.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/PotAllSlot2.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnPot.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnPot_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnFacebookConnect.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BtnFacebookConnect_pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/Lobby Setting Reply pr.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/Lobby Setting Reply.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/BgLobbyFooter.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/LobbyRechargeArrow.png</filename>
            <filename>../../../../../CongGameSlotResources/_Sprites/Lobby/LobbyRechargeArrow_pr.png</filename>
            <filename>../../../../../../PROJECTS/LTA/CongGameSlot/Clones/Say2UpStore/Assets/_Sprites/Lobby/NotifyAllBg.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
